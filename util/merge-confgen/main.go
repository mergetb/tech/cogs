package main

import (
	"encoding/binary"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"os"
	"strings"

	//"github.com/davecgh/go-spew/spew"

	"github.com/spf13/cobra"
	"gitlab.com/mergetb/tech/cogs/pkg/runtime"
	"gitlab.com/mergetb/tech/cogs/pkg/task"
	"gitlab.com/mergetb/tech/nex/pkg"
	"gitlab.com/mergetb/xir/lang/go/v0.2"
	"gitlab.com/mergetb/xir/lang/go/v0.2/tb"
)

func main() {

	log.SetFlags(0)

	root := &cobra.Command{
		Use:   "merge-confgen",
		Short: "Generate merge configuration files from XIR testbed models",
	}

	ansible := &cobra.Command{
		Use:   "ansible",
		Short: "Generate ansible files",
	}
	root.AddCommand(ansible)

	tbdeploy := &cobra.Command{
		Use:   "tb-deploy <tb-xir.json>",
		Short: "generate a vars file for use with the tb-deploy playbook",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			tbDeploy(args[0])
		},
	}
	ansible.AddCommand(tbdeploy)

	var autocomplete = &cobra.Command{
		Use:   "autocomplete",
		Short: "Generates bash completion scripts",
		Run: func(cmd *cobra.Command, args []string) {
			err := root.GenBashCompletion(os.Stdout)
			if err != nil {
				log.Fatal(err)
			}
		},
	}
	root.AddCommand(autocomplete)

	err := root.Execute()
	if err != nil {
		log.Print(err)
	}

}

type EtcdClusterMember struct {
	Alias    string `json:"alias"`
	Endpoint string `json:"endpoint"`
}

type FabricConfig struct {
	Interfaces         []string
	BGPIP              string
	ASN                int
	PeerGroup          string
	AdvertiseVNI       bool
	NeighborInterfaces []string
	BridgedInterfaces  []string `json:",omitempty"`
}

type LeafConfig struct {
	Downlinks []string
	Uplinks   []string
	SVIaddr   string
}

type FoundryInfo struct {
	Name  string `json:"name"`
	Mac   string `json:"mac"`
	XpIfx string `json:"xpIfx"`
	Addr  string `json:"addr"`
}

type SledInfo struct {
	Mac string `json:"mac"`
}

type TbDeployVars struct {
	SiteFQDN        string
	Facility        string
	CommanderHosts  []string
	RexHosts        []string
	DriverHosts     []string
	ManagerHosts    []string
	EtcdHosts       []string
	SledHosts       []string
	MoaHosts        []string
	EtcdCluster     []EtcdClusterMember
	EtcdEndpoints   []string
	RuntimeConfig   map[string]runtime.Config
	FabricSwitches  []string
	IFabricSwitches []string
	LeafSwitches    []string
	ILeafSwitches   []string
	FabricConfig    map[string]FabricConfig
	LeafConfig      map[string]LeafConfig

	TestbedNodes     []string
	HarborNexEntries []nex.Member
	HarborEdges      []task.EdgeInfo
	HarborFoundry    []FoundryInfo
	HarborSled       map[string]SledInfo
	HarborEndpoints  map[string]map[string][]string
}

func tbDeploy(srcXIR string) {

	topo, err := xir.FromFile(srcXIR)
	if err != nil {
		log.Fatal(err)
	}

	vars := &TbDeployVars{
		Facility:        strings.Split(topo.Id, ".")[0],
		SiteFQDN:        topo.Id,
		RuntimeConfig:   make(map[string]runtime.Config),
		FabricConfig:    make(map[string]FabricConfig),
		LeafConfig:      make(map[string]LeafConfig),
		HarborSled:      make(map[string]SledInfo),
		HarborEndpoints: make(map[string]map[string][]string),
	}

	addr := []byte{10, 0, 0, 1}
	ip := binary.BigEndian.Uint32(addr)

	for _, x := range topo.AllNodes() {

		if tb.HasRole(x, tb.EtcdHost) {

			vars.EtcdCluster = append(vars.EtcdCluster, EtcdClusterMember{
				Alias: x.Label(),
				Endpoint: fmt.Sprintf(
					"%s=https://%s:2380", x.Label(), x.Label()),
			})

			vars.EtcdHosts = append(vars.EtcdHosts, x.Label())

			vars.EtcdEndpoints = append(vars.EtcdEndpoints,
				fmt.Sprintf("https://%s:2379", x.Label()))
		}

		if tb.HasRole(x, tb.CommanderHost) {
			vars.CommanderHosts = append(vars.CommanderHosts, x.Label())
		}

		if tb.HasRole(x, tb.RexHost) {

			vars.RexHosts = append(vars.RexHosts, x.Label())
			runtimeConfig(vars, x, tb.InfraLink, true)

		}

		if tb.HasRole(x, tb.DriverHost) {
			vars.DriverHosts = append(vars.DriverHosts, x.Label())
		}

		if tb.HasRole(x, tb.ManagerHost) {
			vars.ManagerHosts = append(vars.ManagerHosts, x.Label())
		}

		if tb.HasRole(x, tb.Leaf) {

			cfg := LeafConfig{}

			if tb.HasRole(x, tb.InfraSwitch) {

				vars.ILeafSwitches = append(vars.ILeafSwitches, x.Label())

				r := tb.GetResourceSpec(x)
				if r == nil {
					log.Fatalf("infraleaf %s has no resource spec", x.Label())
				}
				hes := r.GetLinksByRole(tb.HarborEndpoint)
				if len(hes) == 0 {
					log.Fatalf("node %s has no harbor endpoints", x.Label())
				}
				if len(hes) == 0 {
					log.Fatalf("infra leaf %s has no harbor endpoint", x.Label())
				}
				if len(hes[0].Addrs) == 0 {
					log.Fatalf("infra leaf %s has no harbor address", x.Label())
				}
				cfg.SVIaddr = hes[0].Addrs[0]

			}

			vars.LeafSwitches = append(vars.LeafSwitches, x.Label())

			for _, e := range x.Endpoints {
				for _, n := range e.Neighbors {
					if tb.HasRole(n.Endpoint.Parent, tb.Fabric) {
						cfg.Uplinks = append(cfg.Uplinks, e.Label())
					} else {
						cfg.Downlinks = append(cfg.Downlinks, e.Label())
					}
				}
			}

			vars.LeafConfig[x.Label()] = cfg

		}

		if tb.HasRole(x, tb.Fabric) {

			r := tb.GetResourceSpec(x)
			if r == nil {
				log.Fatalf("fabric switch %s has no resource spec", x.Label())
			}

			if r.System == nil {
				log.Fatalf("fabric switch %s has no sysconfig", x.Label())
			}

			if r.System.OS == nil {
				log.Fatalf("fabric switch %s has no os", x.Label())
			}

			if r.System.OS.Config == nil {
				log.Fatalf("fabric switch %s has no osconfig", x.Label())
			}

			if r.System.OS.Config.Evpn == nil {
				log.Fatalf("fabric switch %s has evpn osconfig", x.Label())
			}

			vars.FabricSwitches = append(vars.FabricSwitches, x.Label())
			if tb.HasRole(x, tb.InfraSwitch) {
				vars.IFabricSwitches = append(vars.IFabricSwitches, x.Label())
			}

			evpn := r.System.OS.Config.Evpn

			cfg := FabricConfig{
				BGPIP:        evpn.TunnelIP,
				ASN:          evpn.ASN,
				AdvertiseVNI: true,
			}

			for _, e := range x.Endpoints {
				for _, n := range e.Neighbors {

					if tb.HasRole(n.Endpoint.Parent,
						tb.Fabric,
						tb.InfraServer,
						tb.StorageServer,
						tb.NetworkEmulator) {

						cfg.NeighborInterfaces = append(
							cfg.NeighborInterfaces, e.Label(),
						)
					}
					if tb.HasRole(n.Endpoint.Parent, tb.Leaf) {
						cfg.BridgedInterfaces = append(
							cfg.BridgedInterfaces, e.Label(),
						)
					}

					cfg.Interfaces = append(cfg.Interfaces, e.Label())

				}
			}

			if tb.HasRole(x, tb.InfraSwitch) || tb.HasRole(x, tb.Gateway) {
				cfg.PeerGroup = "ifabric"
			} else if tb.HasRole(x, tb.XpSwitch) {
				cfg.PeerGroup = "xfabric"
			} else {
				log.Fatalf(
					"fabric switch %s is neither an experiment switch "+
						"nor an infranet switch", x.Label())
			}

			vars.FabricConfig[x.Label()] = cfg

		}

		if tb.HasRole(x, tb.SledHost) {

			vars.SledHosts = append(vars.SledHosts, x.Label())
			runtimeConfig(vars, x, tb.InfraLink, false)

			r := tb.GetResourceSpec(x)
			if r == nil {
				log.Fatalf("fabric switch %s has no resource spec", x.Label())
			}

			hes := r.GetLinksByRole(tb.HarborEndpoint)
			if len(hes) == 0 {
				log.Fatalf("node %s has no harbor endpoints", x.Label())
			}

			_, ok := vars.HarborEndpoints[x.Label()]
			if !ok {
				vars.HarborEndpoints[x.Label()] = make(map[string][]string)
			}

			for _, he := range hes {
				vars.HarborEndpoints[x.Label()][he.Name] = he.Addrs
			}

		}

		if tb.HasRole(x, tb.NetworkEmulator) {
			vars.MoaHosts = append(vars.MoaHosts, x.Label())
			runtimeConfig(vars, x, tb.EmuLink, false)
		}

		if tb.HasRole(x, tb.Node) {

			r := tb.GetResourceSpec(x)
			if r == nil {
				log.Fatalf("%s has no resource spec", x.Label())
			}

			infralinks := r.GetLinksByRole(tb.InfraLink)
			if len(infralinks) == 0 {
				log.Fatalf("node %s has no infralinks", x.Label())
			}
			if len(infralinks) > 1 {
				log.Printf("node %s has more than 1 infralink", x.Label())
			}

			e := x.GetEndpoint(infralinks[0].Name)
			if e == nil {
				log.Fatalf("node %s has no endpoint %s", x.Label(), infralinks[0].Name)
			}

			mac, ok := e.Props["mac"]
			if !ok {
				log.Fatalf("%s.%s has no mac", x.Label(), infralinks[0].Name)
			}

			vars.HarborNexEntries = append(vars.HarborNexEntries, nex.Member{
				Name: x.Label(),
				Mac:  mac.(string),
			})

			vars.HarborEdges = append(vars.HarborEdges, task.EdgeInfo{
				NodeName:      x.Label(),
				NodeInterface: e.Label(),
			})

			vars.TestbedNodes = append(vars.TestbedNodes, x.Label())

			xplinks := r.GetLinksByRole(tb.XpLink)
			if len(xplinks) == 0 {
				log.Fatalf("node %s has no xplinks", x.Label())
			}

			ex := x.GetEndpoint(xplinks[0].Name)
			if ex == nil {
				log.Fatalf("node %s has no endpoint %s", x.Label(), xplinks[0].Name)
			}

			vars.HarborFoundry = append(vars.HarborFoundry, FoundryInfo{
				Name:  x.Label(),
				Mac:   mac.(string),
				XpIfx: ex.Label(),
				Addr:  net.IP(addr).To4().String(),
			})

			ip++
			binary.BigEndian.PutUint32(addr, ip)

			vars.HarborSled[x.Label()] = SledInfo{Mac: mac.(string)}
		}

	}

	out, err := json.MarshalIndent(vars, "", "  ")
	if err != nil {
		log.Fatal(err)
	}

	err = ioutil.WriteFile("tb-deploy-vars.json", out, 0644)
	if err != nil {
		log.Println(err)
	}

}

func runtimeConfig(
	vars *TbDeployVars, x *xir.Node, linkRole tb.Role, externalConfig bool) {

	r := tb.GetResourceSpec(x)
	if r == nil {
		log.Fatalf("%s has no resource spec", x.Label())
	}

	infraLinks := r.GetLinksByRole(linkRole)
	if len(infraLinks) == 0 {
		log.Fatalf("%s has no %s", x.Label(), linkRole)
	}
	if len(infraLinks) > 1 {
		log.Printf("%s has more than 1 %s", x.Label(), linkRole)
		log.Printf("%+v", infraLinks)
	}
	infra := infraLinks[0]

	gw := ""
	if externalConfig {
		gatewayLinks := r.GetLinksByRole(tb.GatewayLink)
		if len(gatewayLinks) == 0 {
			log.Fatalf("%s has no gateway link", x.Label())
		}
		if len(gatewayLinks) > 1 {
			log.Printf("%s has more than one gateway links", x.Label())
		}
		gw = gatewayLinks[0].Name
	}

	if r.System == nil {
		log.Fatalf("%s has no sysconfig", x.Label())
	}

	if r.System.OS == nil {
		log.Fatalf("%s has no os", x.Label())
	}

	if r.System.OS.Config == nil {
		log.Fatalf("%s has no osconfig", x.Label())
	}

	if r.System.OS.Config.Evpn == nil {
		log.Fatalf("%s has evpn osconfig", x.Label())
	}

	if r.System.OS.Config.Evpn.TunnelIP == "" {
		log.Fatalf("%s has evpn tunnelip", x.Label())
	}

	cfg := runtime.Config{
		Net: &runtime.NetConfig{
			VtepIfx:         infra.Name,
			Mtu:             9216,
			VtepMtu:         9166,
			ServiceTunnelIP: r.System.OS.Config.Evpn.TunnelIP,
			BgpAS:           r.System.OS.Config.Evpn.ASN,
			ExternalIfx:     gw,
		},
	}
	vars.RuntimeConfig[x.Label()] = cfg

}
