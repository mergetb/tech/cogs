package main

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/mcc/pkg"
	"gitlab.com/mergetb/xir/lang/go/v0.2"
	"gitlab.com/mergetb/xir/lang/go/v0.2/sys"
)

func chart(a, b string, topo *xir.Net) {

	flat := xir.NewNet(topo.Id)
	mcc.Flatten(topo, flat)

	_, _, na := flat.GetNode(a)
	if na == nil {
		log.Fatalf("%s not a thing", a)
	}

	_, _, nb := flat.GetNode(b)
	if nb == nil {
		log.Fatalf("%s not a thing", b)
	}

	ds := mcc.DijkstraTree(na, flat, mcc.AllSiblings)
	if ds == nil {
		log.Fatalf("failed to build DijkstraTree")
	}

	_, sp := mcc.ShortestPathN2N(na, nb, ds)
	if sp == nil {
		log.Fatalf("no path from %s to %s", na.Label(), nb.Label())
	}

	for _, link := range sp {
		log.Printf("%v", sys.LinkLabel(link))
	}
}
