package main

import (
	"fmt"
	"sort"
	"strings"

	"github.com/golang/protobuf/ptypes"
	"github.com/osrg/gobgp/api"
	log "github.com/sirupsen/logrus"

	"gitlab.com/mergetb/tech/cogs/pkg/task"
	"gitlab.com/mergetb/tech/gobble/pkg"
	"gitlab.com/mergetb/xir/lang/go/v0.2"
	"gitlab.com/mergetb/xir/lang/go/v0.2/build"
	"gitlab.com/mergetb/xir/lang/go/v0.2/tb"
)

// NodeEvpnInfo associates EVPN advertisements with node information including
// the status of the node in a materialization. Advertisements are considered
// zombies when they can not be associated with a node in an active
// materialization.
type NodeEvpnInfo struct {
	Node     *xir.Node
	MzInfo   *task.MzInfo
	State    task.NodeState
	Zombie   bool
	Type2    gobble.Type2EvpnRoute
	Type3    *gobble.Type3EvpnRoute
	NextHops []*tb.Resource
}

// The EvpnTable wraps a collection of NodeEvpnInfor objects for the purpose of
// presentation through a customized String method.
type EvpnTable []*NodeEvpnInfo

func buildMacIndex(topo *xir.Net) map[string]*xir.Node {

	index := make(map[string]*xir.Node)

	for _, node := range topo.AllNodes() {
		for _, endpoint := range node.Endpoints {
			mac, ok := endpoint.Props["mac"].(string)
			if ok {
				index[mac] = node
			}
		}
	}

	return index

}

func buildNodeIndex() map[string]*task.NodeState {

	nodes, err := task.ListNodes()
	if err != nil {
		log.Fatal(err)
	}

	index := make(map[string]*task.NodeState)
	for _, node := range nodes {
		index[node.Name] = node
	}

	return index

}

func buildFabricIndex(topo *xir.Net) map[string]*tb.Resource {

	result := make(map[string]*tb.Resource)

	filter := func(x *xir.Node) bool {
		return tb.HasRole(x, tb.Fabric, tb.Spine, tb.InfrapodServer, tb.NetworkEmulator)
	}

	for _, x := range topo.Select(filter) {

		r := tb.GetResourceSpec(x)
		if r == nil {
			continue
		}

		if r.System == nil {
			continue
		}
		if r.System.OS == nil {
			continue
		}
		if r.System.OS.Config == nil {
			continue
		}
		if r.System.OS.Config.Evpn == nil {
			continue
		}

		result[r.System.OS.Config.Evpn.TunnelIP] = r

	}

	return result

}

func buildType3Index(bgp *gobble.BgpState) map[string]*gobble.Type3EvpnRoute {

	index := make(map[string]*gobble.Type3EvpnRoute)

	for _, r := range bgp.Type3Routes {

		var rd gobgpapi.RouteDistinguisherIPAddress
		err := ptypes.UnmarshalAny(r.Route.Rd, &rd)
		if err != nil {
			log.Warn(err)
		}
		rp := &gobble.Type3EvpnRoute{}
		*rp = r
		index[rdString(rd)] = rp

	}

	return index

}

func buildMzIndex() map[string]*task.MzInfo {

	mzs, err := task.ListMaterializations("")
	if err != nil {
		log.Fatal(err)
	}

	index := make(map[string]*task.MzInfo)

	for _, mz := range mzs {
		index[mz.Mzid] = mz
	}

	return index

}

func rdString(rd gobgpapi.RouteDistinguisherIPAddress) string {
	return fmt.Sprintf("%s:%d", rd.Admin, rd.Assigned)
}

func evpnChart(topo *xir.Net, router string) {

	err := gobble.ApplyDefaultConfig()
	if err != nil {
		log.Warn(err)
	}
	gobble.Cfg.Router = router

	bgp, err := gobble.ReadBgp()
	if err != nil {
		log.Fatal(err)
	}

	fabricIndex := buildFabricIndex(topo)
	macIndex := buildMacIndex(topo)
	nodeIndex := buildNodeIndex()
	type3Index := buildType3Index(bgp)
	mzIndex := buildMzIndex()

	var table EvpnTable
	for _, r := range bgp.Type2Routes {

		if len(r.Route.Labels) == 1 && r.Route.Labels[0] == 2 {
			continue
		}

		if r.Route.MacAddress == "" {
			continue
		}

		node, ok := macIndex[r.Route.MacAddress]
		if !ok {
			log.Warnf("%s is not associated to a node", r.Route.MacAddress)
			continue
		}

		info := &NodeEvpnInfo{
			Node:  node,
			Type2: r,
		}
		state, ok := nodeIndex[node.Label()]
		if ok {
			info.State = *state
			mzi, ok := mzIndex[state.Mzid]
			info.MzInfo = mzi
			info.Zombie = !ok
		}

		var rd gobgpapi.RouteDistinguisherIPAddress
		err := ptypes.UnmarshalAny(r.Route.Rd, &rd)
		if err != nil {
			log.Warn(err)
		}
		type3, ok := type3Index[rdString(rd)]
		if ok {
			info.Type3 = type3
		}

		for _, x := range info.Type2.NextHops {
			r, ok := fabricIndex[x]
			if ok {
				info.NextHops = append(info.NextHops, r)
			}
		}

		table = append(table, info)

	}

	table.Show()

}

// Show prints a tab dilimited table representing the EvpnTable object.
func (t EvpnTable) Show() {

	fmt.Fprintf(tw, "%s\t%s\t%s\t%s\t%s\t%s\t%s\n",
		black("NODE"),
		black("MZID"),
		black("MAC"),
		black("RD"),
		black("VNI"),
		black("UNDERLAY ROUTE"),
		black("NEXT HOP"),
	)

	sort.Slice(t, func(i, j int) bool {

		if t[i].State.Mzid == t[j].State.Mzid {
			return build.InvSort(t[i].State.Name, t[j].State.Name)
		}

		return t[i].State.Mzid < t[j].State.Mzid
	})

	for _, x := range t {

		var rd gobgpapi.RouteDistinguisherIPAddress
		err := ptypes.UnmarshalAny(x.Type2.Route.Rd, &rd)
		if err != nil {
			log.Warn(err)
		}

		var t3 string
		if x.Type3 != nil {
			t3 = green("ok")
		} else {
			t3 = red("missing")
		}

		name := blue(x.Node.Label())
		mzid := green(x.State.Mzid)
		if x.Zombie {
			mzid = red(x.State.Mzid)
		}

		labels := fmt.Sprintf("%v", x.Type2.Route.Labels)
		if len(x.Type2.Route.Labels) > 0 && x.MzInfo != nil &&
			int(x.Type2.Route.Labels[0]) != x.MzInfo.Vni {
			labels = red(labels)
		} else {
			labels = green(labels)
		}

		fmt.Fprintf(tw, "%s\t%s\t%s\t%s\t%v\t%v\t%s\n",
			name,
			mzid,
			white(x.Type2.Route.MacAddress),
			white(fmt.Sprintf("%s:%d", rd.Admin, rd.Assigned)),
			labels,
			//white(fmt.Sprintf("%v", x.Type2.NextHops)),
			t3,
			//white(fmt.Sprintf("%v", x.NextHops)),
			nextHops(x.NextHops),
		)

	}

	tw.Flush()

}

func nextHops(rs []*tb.Resource) string {

	var hs []string
	for _, x := range rs {

		if x.HasRole(tb.Spine) {
			hs = append(hs, blue(x.System.Name))
		}
		if x.HasRole(tb.Fabric) {
			hs = append(hs, cyan(x.System.Name))
		}
		if x.HasRole(tb.InfrapodServer) {
			hs = append(hs, purple(x.System.Name))
		}
		if x.HasRole(tb.NetworkEmulator) {
			hs = append(hs, yellow(x.System.Name))
		}

	}

	return strings.Join(hs, ",")
}
