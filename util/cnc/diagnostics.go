package main

import (
	"fmt"

	log "github.com/sirupsen/logrus"

	"gitlab.com/mergetb/tech/canopy/lib"
	"gitlab.com/mergetb/tech/cogs/pkg/fabric"
	"gitlab.com/mergetb/tech/cogs/pkg/task"
	"gitlab.com/mergetb/xir/lang/go/v0.2"
	"gitlab.com/mergetb/xir/lang/go/v0.2/tb"
)

// The Diagnostic interface defines a diagnostic as any type that can present
// itself through a Message and solve itself through a fix. When the cnc
// inspection code finds problems in networks, it creates diagnostics that can
// be presented to system administirators and offer avenues for self correction.
// This design allows us to capture the logic for issue presentation and
// solution in one place.
type Diagnostic interface {
	Message() string
	Fix() error
}

// VlanTagToNowhere captures information about a VLAN tag on a switch port that
// has no corresponding neighbor or access to downstream devices. For example,
// as spine switch may have a VLAN 47 tag on a trunk to a leaf switch, but that
// leaf switch does not have VLAN 47 on any of it's bridges or ports, so tag 47
// on the spine interface of the trunk is a tag to nowhere.
type VlanTagToNowhere struct {
	Host string
	Port string
	Vid  int
}

// Message returns a descriptive string associated with this diagnostic
func (x VlanTagToNowhere) Message() string {
	return fmt.Sprintf("vid %d on %s:%s is a path to nowhere",
		x.Vid,
		x.Host,
		x.Port,
	)
}

// Fix attempts to resolve the issue identified by this diagnostic.
func (x VlanTagToNowhere) Fix() error {

	cc := canopy.NewClient()

	cc.SetTaggedPort(
		&canopy.Tagged{
			Presence: canopy.Presence_Absent,
			Vids:     []int32{int32(x.Vid)},
		},
		x.Port,
		x.Host,
	)

	return cc.Commit()
}

// PortNotFoundInModel occurs when a port is read on the live system but not
// found in the corresponding XIR network topology model.
type PortNotFoundInModel struct {
	Host string
	Port string
}

// Message returns a descriptive string associated with this diagnostic
func (x PortNotFoundInModel) Message() string {
	return fmt.Sprintf("port %s:%s not found in model", x.Host, x.Port)
}

// Fix attempts to resolve the issue identified by this diagnostic.
func (x PortNotFoundInModel) Fix() error {

	return nil

}

// PortNotFoundOnDevice occurs when a link is discoverd between two devices, but
// when following the link in the model, on of the speficied ports on the link
// can not be found on the connected devices.
type PortNotFoundOnDevice struct {
	Host     string
	Port     string
	PeerHost string
	PeerPort string
}

// Message returns a descriptive string associated with this diagnostic
func (x PortNotFoundOnDevice) Message() string {
	return fmt.Sprintf(
		"port %s:%s not found on device, linked from %s:%s",
		x.Host,
		x.Port,
		x.PeerHost,
		x.PeerPort,
	)
}

// Fix attempts to resolve the issue identified by this diagnostic.
func (x PortNotFoundOnDevice) Fix() error {

	return nil

}

// TrunkToNowhere is a more specific version of VlanTagToNowhere, this comes up
// when the VLAN tag is specifically a trunk, whereas VlanTagToNowhere could
// also arise on untagged, PVID or access ports.
type TrunkToNowhere struct {
	Host string
	Port string
	Peer string
	Vid  int
}

// Message returns a descriptive string associated with this diagnostic
func (x TrunkToNowhere) Message() string {
	return fmt.Sprintf("host %s is trunked from %s:%s on vid %d but has no ports",
		x.Peer,
		x.Host,
		x.Port,
		x.Vid,
	)
}

// Fix attempts to resolve the issue identified by this diagnostic.
func (x TrunkToNowhere) Fix() error {

	cc := canopy.NewClient()

	cc.SetTaggedPort(
		&canopy.Tagged{
			Presence: canopy.Presence_Absent,
			Vids:     []int32{int32(x.Vid)},
		},
		x.Port,
		x.Host,
	)

	return cc.Commit()

}

// TrunkToBlackhole occurs when a VLAN trunk is found on a port that is not
// connected to anything.
type TrunkToBlackhole struct {
	Host string
	Port string
	Peer string
	Vid  int
}

// Message returns a descriptive string associated with this diagnostic
func (x TrunkToBlackhole) Message() string {
	return fmt.Sprintf(
		"host %s is trunked from %s:%s on vid %d but has no matching ports",
		x.Peer,
		x.Host,
		x.Port,
		x.Vid,
	)
}

// Fix attempts to resolve the issue identified by this diagnostic.
func (x TrunkToBlackhole) Fix() error {

	cc := canopy.NewClient()

	cc.SetTaggedPort(
		&canopy.Tagged{
			Presence: canopy.Presence_Absent,
			Vids:     []int32{int32(x.Vid)},
		},
		x.Port,
		x.Host,
	)

	return cc.Commit()

}

// DanglingTrunk occurs when a trunk into a fabric switch is found, but there is
// no corresponding VTEP to carry that trunk anywhere else in the network.
type DanglingTrunk struct {
	Host string
	Port string
	Vid  int
}

// Message returns a descriptive string associated with this diagnostic
func (x DanglingTrunk) Message() string {
	return fmt.Sprintf(
		"trunk %d on %s:%s is dangling",
		x.Vid,
		x.Host,
		x.Port,
	)
}

// Fix attempts to resolve the issue identified by this diagnostic.
func (x DanglingTrunk) Fix() error {

	cc := canopy.NewClient()

	cc.SetTaggedPort(
		&canopy.Tagged{
			Presence: canopy.Presence_Absent,
			Vids:     []int32{int32(x.Vid)},
		},
		x.Port,
		x.Host,
	)

	return cc.Commit()

}

// DanglingVtep occurs when a VTEP is found on a fabric switch that is not
// connected to a materialization network.
type DanglingVtep struct {
	Host string
	Port string
	Vni  int
}

// Message returns a descriptive string associated with this diagnostic
func (x DanglingVtep) Message() string {
	return fmt.Sprintf(
		"vtep with vni %d on %s:%s is dangling",
		x.Vni,
		x.Host,
		x.Port,
	)
}

// Fix attempts to resolve the issue identified by this diagnostic.
func (x DanglingVtep) Fix() error {

	cc := canopy.NewClient()

	cc.SetVtep(
		&canopy.VtepPresence{
			Presence: canopy.Presence_Absent,
			Vni:      int32(x.Vni),
		},
		x.Port,
		x.Host,
	)

	return cc.Commit()

}

// run =========================================================================

func showDiags(ds []Diagnostic) {

	for _, d := range ds {
		log.Warn(d.Message())
	}

}

func fixDiags(ds []Diagnostic) {

	for _, d := range ds {
		log.Infof("fixing: %s", d.Message())
		err := d.Fix()
		if err != nil {
			log.Error(err)
		}
	}

}

func runDiagnostics(topo *xir.Net) {

	ivg, xvg := fetchNets(topo)

	showDiags(ivg.Diagnostics)
	showDiags(xvg.Diagnostics)
	showDiags(checkForFabricTrunkLeaks(topo, ivg))
	showDiags(checkForLeafTrunkLeaks(topo, ivg, tb.InfraSwitch))
	showDiags(checkForLeafTrunkLeaks(topo, xvg, tb.XpSwitch))
	showDiags(checkForVtepLeaks(topo, ivg, tb.InfraSwitch))

}

func checkForFabricTrunkLeaks(topo *xir.Net, ivg *Vgraph) []Diagnostic {

	var diags []Diagnostic

	for _, x := range topo.AllNodes() {

		if tb.HasRole(x, tb.Fabric) && tb.HasRole(x, tb.InfraSwitch) {

			ports := ivg.Ports[x.Label()]
			vmap := ivg.Vteps[x.Label()]

			for vid, portmap := range ports {
				if vid <= 1 {
					continue
				}
				for name := range portmap {

					vteps := vmap[vid]
					if len(vteps) == 0 {

						if vid <= 2 {
							continue
						}
						diags = append(diags, &DanglingTrunk{
							Host: x.Label(),
							Port: name,
							Vid:  vid,
						})

					}

				}
			}

		}

	}

	return diags

}

func checkForLeafTrunkLeaks(topo *xir.Net, vg *Vgraph, role tb.Role) []Diagnostic {

	var diags []Diagnostic

	filter := func(x *xir.Node) bool {
		return tb.HasRole(x, tb.Leaf) && tb.HasRole(x, role)
	}

	for _, x := range topo.Select(filter) {

		uplinkTags := make(map[int32]string)
		downlinkTags := make(map[int32][]string)

		ports := vg.ports[x.Label()]

		_, uplink, _, err := fabric.GetFabric(x)
		if err != nil {
			log.Fatal(err)
		}

		for name, port := range ports {

			if name == fabric.LinkName(uplink) {
				for _, t := range port.Tagged {
					uplinkTags[t] = name
				}
				continue
			}

			for _, t := range append(port.Tagged, port.Untagged...) {
				downlinkTags[t] = append(downlinkTags[t], name)
			}

		}

		for vid, uplink := range uplinkTags {

			_, ok := downlinkTags[vid]
			if !ok {
				if vid <= 2 {
					continue
				}
				diags = append(diags, &DanglingTrunk{
					Host: x.Label(),
					Port: uplink,
					Vid:  int(vid),
				})
			}

		}

	}

	return diags

}

func fixDiagnostics(topo *xir.Net) {

	ivg, xvg := fetchNets(topo)

	fixDiags(ivg.Diagnostics)
	fixDiags(xvg.Diagnostics)
	fixDiags(checkForFabricTrunkLeaks(topo, ivg))
	fixDiags(checkForLeafTrunkLeaks(topo, ivg, tb.InfraSwitch))
	fixDiags(checkForLeafTrunkLeaks(topo, xvg, tb.XpSwitch))
	fixDiags(checkForVtepLeaks(topo, ivg, tb.InfraSwitch))

}

func mzByVniIndex() map[int]*task.MzInfo {

	mzs, err := task.ListMaterializations("")
	if err != nil {
		log.Fatal(err)
	}

	index := make(map[int]*task.MzInfo)

	for _, mz := range mzs {
		index[mz.Vni] = mz
	}

	return index

}

func checkForVtepLeaks(topo *xir.Net, vg *Vgraph, role tb.Role) []Diagnostic {

	var diags []Diagnostic

	mzi := mzByVniIndex()

	filter := func(x *xir.Node) bool {
		return tb.HasRole(x, tb.Fabric) && tb.HasRole(x, role)
	}

	for _, x := range topo.Select(filter) {
		for name, vtep := range vg.vteps[x.Label()] {

			if vtep.Vni <= task.MaxSystemVindex {
				continue
			}

			_, ok := mzi[int(vtep.Vni)]
			if !ok {
				diags = append(diags, DanglingVtep{
					Host: x.Label(),
					Port: name,
					Vni:  int(vtep.Vni),
				})
			}

		}
	}

	return diags

}
