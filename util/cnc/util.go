package main

import (
	"strings"
)

func tr(s ...string) string {
	return strings.Join(s, "\t") + "\n"
}

func th(s ...string) string {
	var h []string
	for _, x := range s {
		h = append(h, black(x))
	}
	return strings.Join(h, "\t") + "\n"
}
