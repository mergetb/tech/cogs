package main

import (
	"fmt"
	log "github.com/sirupsen/logrus"

	"gitlab.com/mergetb/tech/canopy/lib"
	"gitlab.com/mergetb/tech/cogs/pkg/fabric"
	"gitlab.com/mergetb/tech/cogs/pkg/task"
	"gitlab.com/mergetb/xir/lang/go/v0.2"
	"gitlab.com/mergetb/xir/lang/go/v0.2/tb"
)

// A Vgraph is a virtual network graph that is the combination of all the VLAN
// ports, VXLAN VTEPs that make up a set of testbed virtual networks. The
// networks are held in Vnet objects indexed by VXLAN VNI. The Vgraph object
// also has a Diagnostics member for keeping track of issues discovered when
// constructing and traversing the graph.
type Vgraph struct {

	// device -> name -> port
	ports map[string]map[string]*canopy.Port
	// device -> name -> vtep
	vteps map[string]map[string]*canopy.Vtep

	// device -> VID -> name -> port, a lookup table for ports first by device then by
	// VID
	Ports map[string]map[int]map[string]*canopy.Port

	// VNI -> port -> visited, tracks if port has already been considered for a
	// particular VNI.
	visited map[int]map[*canopy.Port]bool

	// device -> VID -> name -> vteps
	Vteps map[string]map[int]map[string]*canopy.Vtep

	Vnets map[int]*Vnetwk

	// device -> name -> bridge
	Bridges map[string]map[string]*canopy.Bridge

	Diagnostics []Diagnostic
}

// Diagnostic adds the specified diagnostic to the Vgraph
func (vg *Vgraph) Diagnostic(d Diagnostic) {
	vg.Diagnostics = append(vg.Diagnostics, d)
}

// Vtep associates a port name to a Canopy Vtep object
type Vtep struct {
	Name    string
	Element *canopy.Vtep
}

// Vlan associates a VLAN ID to a Canpy Port object
type Vlan struct {
	Vid     int
	Element *canopy.Port
}

// A Vnetwk is a virtual network that associates a VXLAN virtual network
// identifier to a set of VXLAN Vteps and VLAN Ports that collectively comprise
// the virtual network.
type Vnetwk struct {
	Vni int

	// device -> vtep
	Vteps map[string]*Vtep

	// device -> name -> []port
	Ports map[string]map[string]*Vlan
}

// NewVgraph creates a new empty Vgraph
func NewVgraph() *Vgraph {

	return &Vgraph{
		ports:   make(map[string]map[string]*canopy.Port),
		vteps:   make(map[string]map[string]*canopy.Vtep),
		Ports:   make(map[string]map[int]map[string]*canopy.Port),
		visited: make(map[int]map[*canopy.Port]bool),
		Vnets:   make(map[int]*Vnetwk),
		Vteps:   make(map[string]map[int]map[string]*canopy.Vtep),
		Bridges: make(map[string]map[string]*canopy.Bridge),
	}

}

// NewVnetwk creates a new empty Vnetwk
func NewVnetwk() *Vnetwk {

	return &Vnetwk{
		Vteps: make(map[string]*Vtep),
		Ports: make(map[string]map[string]*Vlan),
	}

}

// AddPort adds a canopy port to the specified device and port name
func (vg *Vgraph) AddPort(device, name string, port *canopy.Port) {

	_, ok := vg.ports[device]
	if !ok {
		vg.ports[device] = make(map[string]*canopy.Port)
	}

	vg.ports[device][name] = port

	vids := append(port.Tagged, port.Untagged...)
	if len(vids) == 0 {
		vids = append(vids, 0)
	}

	for _, vid := range vids {
		vg.AddPortByVID(device, name, port, int(vid))
	}

}

// AddPortByVID adds a canopy port to the Vgraph with the specified VLAN vid,
// device and port name
func (vg *Vgraph) AddPortByVID(device, name string, port *canopy.Port, vid int) {

	_, ok := vg.Ports[device]
	if !ok {
		vg.Ports[device] = make(map[int]map[string]*canopy.Port)
	}

	_, ok = vg.Ports[device][vid]
	if !ok {
		vg.Ports[device][vid] = make(map[string]*canopy.Port)
	}

	vg.Ports[device][vid][name] = port

}

// AddVtep adds a canopy Vtep to the Vgraph with the specified device and port
// name
func (vg *Vgraph) AddVtep(device, name string, vtep *canopy.Vtep) {

	_, ok := vg.vteps[device]
	if !ok {
		vg.vteps[device] = make(map[string]*canopy.Vtep)
	}
	vg.vteps[device][name] = vtep

	vn, ok := vg.Vnets[int(vtep.Vni)]
	if !ok {
		vn = NewVnetwk()
		vg.Vnets[int(vtep.Vni)] = vn
	}

	vn.Vteps[device] = &Vtep{
		Name:    name,
		Element: vtep,
	}

	_, ok = vg.Vteps[device]
	if !ok {
		vg.Vteps[device] = make(map[int]map[string]*canopy.Vtep)
	}

	_, ok = vg.Vteps[device][int(vtep.BridgeAccess)]
	if !ok {
		vg.Vteps[device][int(vtep.BridgeAccess)] = make(map[string]*canopy.Vtep)
	}

	vg.Vteps[device][int(vtep.BridgeAccess)][name] = vtep

}

// AddBridge adds a bridge to the Vgraph with the specified device and bridge
// name.
func (vg *Vgraph) AddBridge(device, name string, bridge *canopy.Bridge) {

	_, ok := vg.Bridges[device]
	if !ok {
		vg.Bridges[device] = make(map[string]*canopy.Bridge)
	}

	vg.Bridges[device][name] = bridge

}

// AddPortToVnet adds a Canopy port to the virtual network identified by
// the vni.
func (vg *Vgraph) AddPortToVnet(device, name string, vni, vid int, port *canopy.Port) {

	vn, ok := vg.Vnets[vni]
	if !ok {
		vn = new(Vnetwk)
		vg.Vnets[vni] = vn
	}

	_, ok = vn.Ports[device]
	if !ok {
		vn.Ports[device] = make(map[string]*Vlan)
	}

	vn.Ports[device][name] = &Vlan{
		Vid:     vid,
		Element: port,
	}

}

// GetPorts returns the ports of the specivied device on the specivied VLAN vid.
func (vg *Vgraph) GetPorts(device string, vid int) map[string]*canopy.Port {

	_, ok := vg.Ports[device]
	if !ok {
		return nil
	}

	return vg.Ports[device][vid]

}

// Visit marks the port on the network identified by the VNI as visited
func (vg *Vgraph) Visit(vni int, port *canopy.Port) {

	_, ok := vg.visited[vni]
	if !ok {
		vg.visited[vni] = make(map[*canopy.Port]bool)
	}
	vg.visited[vni][port] = true

}

// Visited checks if the port on the virtual network identified by the vni has
// been visited
func (vg *Vgraph) Visited(vni int, port *canopy.Port) bool {

	_, ok := vg.visited[vni]
	if !ok {
		return false
	}
	_, ok = vg.visited[vni][port]
	return ok

}

// GetVPeer gets the connected peer of node on the specified port on the
// specified VLAN id
func (vg *Vgraph) GetVPeer(
	node *xir.Node,
	portName string,
	vid int,
) (*xir.Node, *canopy.Port, string, bool) {

	// get the xir endpoint that matches the port
	var e *xir.Endpoint
	for _, x := range node.Endpoints {

		if fabric.LinkName(x) == portName {
			e = x
			break
		}

		// check for breakout
		if e == nil {
			for _, neighbor := range x.Neighbors {
				// the broken out port is specified on the smaller transciever
				// side e.g.
				//            ........
				//            | swp4 |
				//            /`|``|`\
				//           /  |  |  \
				//      swp4s0  1  2  3
				//
				breakout, ok := neighbor.Endpoint.Props["breakout"].(string)
				if ok && breakout == portName {
					e = x
					break
				}
			}
		}
		if e != nil {
			break
		}
	}

	// could not find an endpoint matching the port
	if e == nil {
		vg.Diagnostic(&PortNotFoundInModel{
			Host: node.Label(),
			Port: portName,
		})
		return nil, nil, "", false
	}

	// go to the peer port connected to the one we're looking at
	for _, neighbor := range e.Neighbors {

		if neighbor.Endpoint.Parent == node {
			continue
		}

		peerEndpoint := neighbor.Endpoint
		peerNode := peerEndpoint.Parent

		if !tb.HasRole(peerNode, tb.InfraSwitch, tb.XpSwitch) {
			return nil, nil, "", false
		}

		pvi, ok := vg.Ports[peerNode.Label()]
		if !ok {
			vg.Diagnostic(&TrunkToNowhere{
				Host: node.Label(),
				Port: portName,
				Peer: peerNode.Label(),
				Vid:  vid,
			})
			return nil, nil, "", false
		}

		ports, ok := pvi[vid]
		if !ok {
			vg.Diagnostic(&TrunkToBlackhole{
				Host: node.Label(),
				Port: portName,
				Peer: peerNode.Label(),
				Vid:  vid,
			})
			return nil, nil, "", false
		}

		peerPortName := fabric.LinkName(peerEndpoint)
		peerPort, ok := ports[peerPortName]
		if !ok {
			vg.Diagnostic(&PortNotFoundOnDevice{
				Host:     peerNode.Label(),
				Port:     peerPortName,
				PeerHost: node.Label(),
				PeerPort: portName,
			})
			return nil, nil, "", false
		}

		// TODO not handling the case of multiple peers through breakout cables yet
		return peerNode, peerPort, peerPortName, true

	}

	log.Fatalf(
		"BUG - %s:%s: reached impossible code path for port",
		node.Label(),
		portName,
	)
	return nil, nil, "", false

}

// ToVnets presents a Vgraph as a collection of Vnet objects
func (vg *Vgraph) ToVnets() []Vnet {

	var vnets []Vnet

	for vni, vnet := range vg.Vnets {

		x := Vnet{
			VNI: vni,
		}

		for device, vtep := range vnet.Vteps {

			x.VTEPs = append(x.VTEPs, VTEP{
				VNI:       vni,
				TunnelIP:  vtep.Element.LocalIP,
				Host:      device,
				Interface: vtep.Name,
			})

		}

		for device, ports := range vnet.Ports {
			for name, port := range ports {

				x.VLANs = append(x.VLANs, VLAN{
					Host:      device,
					Interface: name,
					VID:       port.Vid,
				})
			}
		}

		vnets = append(vnets, x)

	}

	return vnets

}

func processCanopyElements(topo *xir.Net, es map[string][]*canopy.Element) *Vgraph {

	data := NewVgraph()

	// populate our working data set
	for sw, elements := range es {
		for _, e := range elements {

			switch x := e.Value.(type) {

			case *canopy.Element_Port:

				data.AddPort(sw, e.Name, x.Port)

			case *canopy.Element_Vtep:

				data.AddVtep(sw, e.Name, x.Vtep)

			case *canopy.Element_Bridge:

				data.AddBridge(sw, e.Name, x.Bridge)

			}

		}
	}

	// for each vtep, find the connected vlan ports and follow them down through
	// trunks and access ports
	for vni, vnet := range data.Vnets {
		for device, vtep := range vnet.Vteps {

			connectedPorts := data.GetPorts(device, int(vtep.Element.BridgeAccess))
			_, _, node := topo.GetNode(device)

			for name, p := range connectedPorts {
				follow(node, name, p, data, vni, int(vtep.Element.BridgeAccess))
			}

		}

	}

	return data

}

func follow(node *xir.Node, portName string, port *canopy.Port, data *Vgraph, vni, vid int) {

	data.AddPortToVnet(node.Label(), portName, vni, vid, port)
	data.Visit(vni, port)

	peerDevice, _, peerName, ok := data.GetVPeer(node, portName, vid)
	if !ok {
		return
	}

	connectedPorts := data.GetPorts(peerDevice.Label(), vid)
	if len(connectedPorts) == 0 {
		data.Diagnostic(&VlanTagToNowhere{
			Vid:  vid,
			Host: peerDevice.Label(),
			Port: peerName,
		})
	}

	for name, p := range connectedPorts {

		if data.Visited(vni, p) {
			continue
		}

		follow(peerDevice, name, p, data, vni, vid)

	}

}

func fetchNets(topo *xir.Net) (*Vgraph, *Vgraph) {

	var isw, xsw []string
	for _, x := range topo.AllNodes() {

		if tb.HasRole(x, tb.InfraSwitch) {
			isw = append(isw, x.Label())
		}

		if tb.HasRole(x, tb.InfrapodServer) {
			isw = append(isw, x.Label())
		}

		if tb.HasRole(x, tb.XpSwitch) {
			xsw = append(xsw, x.Label())
		}

	}

	ielem, err := canopy.ListPorts(isw)
	if err != nil {
		log.Fatalf("listing infranet ports failed: %v", err)
	}

	xelem, err := canopy.ListPorts(xsw)
	if err != nil {
		log.Fatalf("listing experiment ports failed: %v", err)
	}

	return processCanopyElements(topo, ielem),
		processCanopyElements(topo, xelem)

}

func ls(topo *xir.Net) {

	ivg, xvg := fetchNets(topo)

	tbNets := TBnets{
		Infranets: ivg.ToVnets(),
		XPnets:    xvg.ToVnets(),
	}

	// mzid -> []vni
	mxv := make(map[string][]int)

	// vni -> mzid
	vxm := make(map[int]string)
	vxm[2] = "harbor"

	mzs, err := task.ListMaterializations("")
	if err != nil {
		log.Fatal(err)
	}

	for _, x := range mzs {
		vxm[x.Vni] = x.Mzid
	}

	for _, x := range tbNets.Infranets {

		mzid, ok := vxm[x.VNI]
		if !ok {
			mzid = "?"
		}

		mxv[mzid] = append(mxv[mzid], x.VNI)

	}

	fmt.Fprint(tw, tr("MZID", "VNIS"))

	for mzid, vids := range mxv {
		fmt.Fprint(tw, tr(
			mzid,
			fmt.Sprintf("%v", vids),
		))
	}

	tw.Flush()

}

func hostColor(topo *xir.Net, host string) string {

	_, _, node := topo.GetNode(host)
	if tb.HasRole(node, tb.Fabric, tb.Spine) {
		return blue(host)
	}
	if tb.HasRole(node, tb.Leaf) {
		return cyan(host)
	}
	if tb.HasRole(node, tb.InfrapodServer) {
		return purple(host)
	}

	return white(host)

}

func portColor(x canopy.UpDown, name string) string {

	if x == canopy.UpDown_Up {
		return green(name)
	}
	return red(name)

}

func findPortColor(vg *Vgraph, host, name string) string {

	vx, ok := vg.Ports[host]
	if !ok {
		return white(name)
	}

	for _, vxm := range vx {
		for portName, p := range vxm {
			if portName == name {
				return portColor(p.State, name)
			}
		}
	}

	return white(name)

}

func findBridgeColor(vg *Vgraph, host, name string) string {

	bm, ok := vg.Bridges[host]
	if !ok {
		return white(name)
	}

	b, ok := bm[name]
	if !ok {
		return white(name)
	}

	return portColor(b.State, name)

}

func show(topo *xir.Net, vni int) {

	ivg, _ := fetchNets(topo)

	vn, ok := ivg.Vnets[vni]
	if !ok {
		return
	}

	fmt.Fprint(tw, th("HOST", "DEV", "PARENT", "TUNNELIP", "BRIDGE", "BRIDGE ACCESS"))

	for host, x := range vn.Vteps {

		fmt.Fprint(tw, tr(
			hostColor(topo, host),
			portColor(x.Element.State, x.Name),
			findPortColor(ivg, host, x.Element.Device),
			white(x.Element.LocalIP),
			findBridgeColor(ivg, host, x.Element.Bridge),
			white(fmt.Sprintf("%d", x.Element.BridgeAccess)),
		))
	}
	tw.Flush()
	fmt.Println("")

	fmt.Fprint(tw, th("HOST", "PORT", "BRIDGE", "UNTAGGED", "TAGGED", "PVID"))
	for host, ports := range vn.Ports {
		for name, vlan := range ports {

			var untagged, tagged []int

			for _, x := range vlan.Element.Untagged {
				if int(x) == vlan.Vid {
					untagged = append(untagged, int(x))
				}
			}
			for _, x := range vlan.Element.Tagged {
				if int(x) == vlan.Vid {
					tagged = append(tagged, int(x))
				}
			}

			fmt.Fprint(tw, tr(
				hostColor(topo, host),
				portColor(vlan.Element.State, name),
				findBridgeColor(ivg, host, vlan.Element.Bridge),
				white(fmt.Sprintf("%v", untagged)),
				white(fmt.Sprintf("%v", tagged)),
				white(fmt.Sprintf("%d", vlan.Element.Access)),
			))

		}
	}
	tw.Flush()

}

func showMzid(topo *xir.Net, mzid string) {

	mzs, err := task.ListMaterializations(mzid)
	if err != nil {
		log.Fatal(err)
	}

	if len(mzs) == 0 {
		return
	}

	show(topo, mzs[0].Vni)
}
