package main

// VTEP associates a VXLAN virtual network identifier to the tunnel IP
// address it can be accessed through and the resident interface on the host
// device.
type VTEP struct {
	VNI       int
	TunnelIP  string
	Host      string
	Interface string
}

// VLAN associates a VLAN ID to the interface and host device it is resident on.
type VLAN struct {
	VID       int
	Host      string
	Interface string
}

// Vnet associates a VXLAN virtual network identifier to a set of VTEPs and
// VLANs on a testbed network
type Vnet struct {
	VNI   int
	VTEPs []VTEP
	VLANs []VLAN
}

// TBnets is an aggregate data structure for all the infrastructure and
// experiment networks on a testbed.
type TBnets struct {
	Infranets []Vnet
	XPnets    []Vnet
}
