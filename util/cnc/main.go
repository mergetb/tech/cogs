package main

import (
	"os"
	"strconv"
	"text/tabwriter"

	"github.com/fatih/color"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"

	"gitlab.com/mergetb/tech/cogs/pkg/common"
	"gitlab.com/mergetb/tech/cogs/pkg/runtime"
	"gitlab.com/mergetb/xir/lang/go/v0.2"
)

var tw = tabwriter.NewWriter(os.Stdout, 0, 0, 4, ' ', 0)

var (
	blue   = color.New(color.FgBlue).SprintFunc()
	black  = color.New(color.FgHiBlack).SprintFunc()
	white  = color.New(color.FgWhite).SprintFunc()
	red    = color.New(color.FgRed).SprintFunc()
	green  = color.New(color.FgGreen).SprintFunc()
	yellow = color.New(color.FgYellow).SprintFunc()
	purple = color.New(color.FgMagenta).SprintFunc()
	cyan   = color.New(color.FgCyan).SprintFunc()
)

func main() {

	cobra.EnablePrefixMatching = true

	var tbxirFile string
	var tbxir *xir.Net
	var debug bool
	root := &cobra.Command{
		Use:   "cnc",
		Short: "Cog Network Control",
		PersistentPreRun: func(*cobra.Command, []string) {

			var err error
			tbxir, err = xir.FromFile(tbxirFile)
			if err != nil {
				log.Fatalf("failed to read xir: %v", err)
			}

			if debug {
				log.SetLevel(log.TraceLevel)
			}

		},
	}
	root.PersistentFlags().StringVarP(
		&tbxirFile, "xir", "x", "/etc/cogs/tb-xir.json", "testbed xir model")
	root.PersistentFlags().BoolVarP(
		&debug, "debug", "d", false, "debug mode")

	ls := &cobra.Command{
		Use:   "ls",
		Short: "list networks",
		Args:  cobra.NoArgs,
		Run: func(*cobra.Command, []string) {
			ls(tbxir)
		},
	}
	root.AddCommand(ls)

	show := &cobra.Command{
		Use:   "show <vni | mzid>",
		Short: "Show a virtual network",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {

			cfg := runtime.GetConfig(false)
			common.SetEtcdConfig(cfg.Etcd)

			vni, err := strconv.Atoi(args[0])
			if err != nil {

				showMzid(tbxir, args[0])

			} else {

				show(tbxir, vni)
			}
		},
	}
	root.AddCommand(show)

	diagnostics := &cobra.Command{
		Use:   "diagnostics",
		Short: "Run diagnostics",
		Args:  cobra.NoArgs,
		Run: func(*cobra.Command, []string) {

			cfg := runtime.GetConfig(false)
			common.SetEtcdConfig(cfg.Etcd)

			runDiagnostics(tbxir)

		},
	}
	root.AddCommand(diagnostics)

	fix := &cobra.Command{
		Use:   "fix",
		Short: "Fix diagnostics",
		Args:  cobra.NoArgs,
		Run: func(*cobra.Command, []string) {

			cfg := runtime.GetConfig(false)
			common.SetEtcdConfig(cfg.Etcd)

			fixDiagnostics(tbxir)

		},
	}
	root.AddCommand(fix)

	evpn := &cobra.Command{
		Use:   "evpn [router]",
		Short: "Display EVPN chart",
		Args:  cobra.MaximumNArgs(1),
		Run: func(cmd *cobra.Command, args []string) {

			cfg := runtime.GetConfig(false)
			common.SetEtcdConfig(cfg.Etcd)

			router := "localhost"
			if len(args) == 1 {
				router = args[0]
			}
			evpnChart(tbxir, router)

		},
	}
	root.AddCommand(evpn)

	chart := &cobra.Command{
		Use:   "chart <a> <b>",
		Short: "Chart the path between two nodes",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			chart(args[0], args[1], tbxir)
		},
	}
	root.AddCommand(chart)

	err := root.Execute()
	if err != nil {
		log.Print(err)
	}

}
