package main

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * rscheck: data recovery tool for the portal
 *
 * The purpose of rscheck is to collect information held by the cogs about the
 * materializations and the nodes, links and interfaces they are composed of,
 * and package that information up in a json blob to be consumed by a
 * counterpart rscheck tool on a portal that is used to rebuild realization and
 * materialization metadata.
 *
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"

	"gitlab.com/mergetb/tech/cogs/pkg/common"
	"gitlab.com/mergetb/tech/cogs/pkg/runtime"
	"gitlab.com/mergetb/tech/cogs/pkg/task"
)

type MzData struct {
	Mzid       string
	Instance   string
	Nodes      map[string]string
	Links      map[string][]string
	Interfaces map[string]string
}

func main() {

	log.SetFlags(0)

	cfg := runtime.GetConfig(false)
	common.SetEtcdConfig(cfg.Etcd)

	mzs, err := task.ListMaterializations("")
	if err != nil {
		log.Fatal(err)
	}

	var mzdata []MzData

	ifxc := make(map[string]int)

	for _, x := range mzs {

		data := MzData{
			Mzid:       x.Mzid,
			Instance:   x.Instance,
			Nodes:      make(map[string]string),
			Links:      make(map[string][]string),
			Interfaces: make(map[string]string),
		}

		ni, err := task.ListNodeInfo(x.Mzid)
		if err != nil {
			log.Fatal(err)
		}

		for _, n := range ni {
			data.Nodes[n.Machine] = n.XpName
		}

		li, err := task.ListLinkInfos(x.Mzid)
		if err != nil {
			log.Fatal(err)
		}

		for _, l := range li {
			for _, x := range l.Core {
				data.Links[x] = append(data.Links[x], l.XpID)
			}
			for _, v := range l.TBVLinks {
				for _, e := range v.Edges {

					data.Links[e.Resource] = append(
						data.Links[e.Resource],
						l.XpID,
					)

					ifx := fmt.Sprintf("%s.%s",
						e.NodeName,
						e.NodeInterface[len(e.NodeInterface)-1:],
					)

					xnode := data.Nodes[e.NodeName]
					xifx := fmt.Sprintf("%s.%d", xnode, ifxc[xnode])

					data.Interfaces[xifx] = ifx
					ifxc[xnode]++

				}
			}
		}

		mzdata = append(mzdata, data)
	}

	out, err := json.MarshalIndent(mzdata, "", "  ")
	if err != nil {
		log.Fatal(err)
	}

	err = ioutil.WriteFile("mzdata.json", out, 0644)
	if err != nil {
		log.Fatal(err)
	}

}
