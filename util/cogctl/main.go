package main

import (
	"log"
	"os"
	"text/tabwriter"

	"github.com/fatih/color"
	"github.com/spf13/cobra"
	"gitlab.com/mergetb/tech/cogs/pkg/common"
	"gitlab.com/mergetb/tech/cogs/pkg/runtime"
	"gitlab.com/mergetb/xir/lang/go/v0.2"
)

const (
	idAlphabet = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_@"
)

var (
	blue   = color.New(color.FgBlue).SprintFunc()
	black  = color.New(color.FgHiBlack).SprintFunc()
	white  = color.New(color.FgWhite).SprintFunc()
	red    = color.New(color.FgRed).SprintFunc()
	green  = color.New(color.FgGreen).SprintFunc()
	cyan   = color.New(color.FgCyan).SprintFunc()
	yellow = color.New(color.FgYellow).SprintFunc()
)

var tw = tabwriter.NewWriter(os.Stdout, 0, 0, 4, ' ', 0)

var (
	modelSource string
	debugMode   bool
	configPath  string
)

var __topo *xir.Net = nil

func tbModel() *xir.Net {

	if __topo != nil {
		return __topo
	}

	var err error
	__topo, err = xir.FromFile(modelSource)
	if err != nil {
		log.Fatalf("failed to load testbed model: %v", err)
	}

	return __topo

}

func main() {

	log.SetFlags(0)

	cobra.EnablePrefixMatching = true

	root := &cobra.Command{
		Use:     "cog",
		Short:   "Interact with cogs",
		Version: common.Version,
		PersistentPreRun: func(*cobra.Command, []string) {
			runtime.ConfigPath = configPath
			cfg := runtime.GetConfig(false)
			common.SetEtcdConfig(cfg.Etcd)
		},
	}
	root.PersistentFlags().StringVarP(
		&modelSource, "model", "m", "/etc/cogs/tb-xir.json", "model to use")
	root.PersistentFlags().BoolVarP(
		&debugMode, "debug", "D", false, "turn on debugging")
	root.PersistentFlags().StringVarP(
		&configPath, "config", "c", "/etc/cogs/runtime.yml", "runtime config to use")

	var autocomplete = &cobra.Command{
		Use:   "autocomplete",
		Short: "Generates bash completion scripts",
		Run: func(cmd *cobra.Command, args []string) {
			err := root.GenBashCompletion(os.Stdout)
			if err != nil {
				log.Fatal(err)
			}
		},
	}
	root.AddCommand(autocomplete)

	list := &cobra.Command{
		Use:   "list",
		Short: "List things",
	}
	root.AddCommand(list)

	show := &cobra.Command{
		Use:   "show",
		Short: "show things",
	}
	root.AddCommand(show)

	delete := &cobra.Command{
		Use:   "delete",
		Short: "delete things",
	}
	root.AddCommand(delete)

	create := &cobra.Command{
		Use:   "create",
		Short: "create things",
	}
	root.AddCommand(create)

	check := &cobra.Command{
		Use:   "check",
		Short: "check things",
	}
	root.AddCommand(check)

	read := &cobra.Command{
		Use:   "read",
		Short: "read things",
	}
	root.AddCommand(read)

	set := &cobra.Command{
		Use:   "set",
		Short: "set things",
	}
	root.AddCommand(set)

	cogCmds(list)
	taskCmds(root, create, delete, list, show)
	nodeCmds(set, list)
	containerCmds(delete, list)
	mzCmds(root, show, delete, list)
	harborCmds(show, root)
	pruneCmds(root)
	netCmds(check, read)

	err := root.Execute()
	if err != nil {
		log.Print(err)
	}

}
