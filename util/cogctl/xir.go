package main

import (
	"log"

	"gitlab.com/mergetb/xir/lang/go/v0.2"
)

func readXir() *xir.Net {

	topo, err := xir.FromFile(modelSource)
	if err != nil {
		log.Fatalf("failed to load testbed model: %v", err)
	}
	return topo

}
