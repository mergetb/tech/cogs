package main

import (
	"fmt"
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/mergetb/tech/cogs/pkg/runtime"
	"gitlab.com/mergetb/tech/cogs/pkg/task"
)

func containerCmds(delete, list *cobra.Command) {

	listNamespaces := &cobra.Command{
		Use:   "namespaces",
		Short: "list namespaces",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			listNamespaces()
		},
	}
	list.AddCommand(listNamespaces)

	deleteContainer := &cobra.Command{
		Use:   "container <mzid> <name>",
		Short: "delete a container",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			deleteContainer(args[0], args[1])
		},
	}
	delete.AddCommand(deleteContainer)

}

func listNamespaces() {

	list, err := runtime.ListNamespaces()
	if err != nil {
		log.Fatal(err)
	}

	for _, ns := range list {

		log.Print(ns)

	}

}

func deleteContainer(mzid, name string) {

	id, err := task.GenerateID()
	if err != nil {
		log.Fatal(err)
	}
	t, objs, err := task.NewTask(
		mzid,
		fmt.Sprintf("cogutl-%s", id),
		task.NewStage(
			&task.Action{
				Kind:   "container",
				Action: task.DeleteCtrTask,
				Mzid:   mzid,
				Data: task.DeleteContainerData{
					Name:      name,
					Namespace: mzid,
				},
			},
			&task.Action{
				Kind:   "container",
				Action: task.DeleteCtr,
				Mzid:   mzid,
				Data: task.DeleteContainerData{
					Name:      name,
					Namespace: mzid,
				},
			},
			&task.Action{
				Kind:   "container",
				Action: task.DeleteCtrRecord,
				Mzid:   mzid,
				Data: task.DeleteContainerData{
					Name:      name,
					Namespace: mzid,
				},
			},
		),
	)
	if err != nil {
		log.Fatal(err)
	}

	err = task.LaunchTask(t, objs)
	if err != nil {
		log.Printf("launch task failure")
		log.Fatal(err)
	}

}
