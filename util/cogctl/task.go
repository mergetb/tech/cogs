package main

import (
	"fmt"
	"log"
	"sort"
	"text/tabwriter"
	"time"

	"github.com/mergetb/yaml/v3"
	"github.com/spf13/cobra"

	"gitlab.com/mergetb/tech/cogs/pkg/common"
	"gitlab.com/mergetb/tech/cogs/pkg/task"
)

func taskCmds(root, create, delete, list, show *cobra.Command) {

	var all bool
	listTasks := &cobra.Command{
		Use:   "tasks",
		Short: "list tasks",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			listTasks(all)
		},
	}
	listTasks.Flags().BoolVarP(&all, "all", "a", false, "list all tasks")
	list.AddCommand(listTasks)

	launchTasks := &cobra.Command{
		Use:   "task [task.yaml]",
		Short: "create  tasks from a YAML spec",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			launchTasks(args[0])
		},
	}
	create.AddCommand(launchTasks)

	deleteTask := &cobra.Command{
		Use:   "tasks <mzid> <taskid>",
		Short: "delete a task based on uuid prefix",
		Args:  cobra.MinimumNArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			cancelTask(args[0], args[1])
		},
	}
	delete.AddCommand(deleteTask)

	deleteErrors := &cobra.Command{
		Use:   "task-errors <mzid> <taskid...>",
		Short: "clear errors on a task",
		Args:  cobra.MinimumNArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			clearTaskErrors(args[0], args[1:])
		},
	}
	delete.AddCommand(deleteErrors)

	complete := &cobra.Command{
		Use:   "complete <mzid> <taskid...>",
		Short: "mark a task as complete",
		Args:  cobra.MinimumNArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			complete(args[0], args[1:])
		},
	}
	root.AddCommand(complete)

	showTask := &cobra.Command{
		Use:   "task <mzid> <taskid>",
		Short: "show a task",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			showTask(args[0], args[1])
		},
	}
	show.AddCommand(showTask)

	reset := &cobra.Command{
		Use:   "reset <mzid> <taskid...>",
		Short: "reset a task",
		Args:  cobra.MinimumNArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			resetTask(args[0], args[1:])
		},
	}
	root.AddCommand(reset)

	var unmask bool
	mask := &cobra.Command{
		Use:   "mask <mzid> <taskid> <actionid>",
		Short: "mask an action so it's ignored by rex",
		Args:  cobra.ExactArgs(3),
		Run: func(cmd *cobra.Command, args []string) {
			maskAction(args[0], args[1], args[2], unmask)
		},
	}
	mask.Flags().BoolVarP(&unmask, "unmask", "u", false, "unmask a task")
	root.AddCommand(mask)

	changeInstance := &cobra.Command{
		Use:   "change-instance <mzid> <taskid> <old> <new>",
		Short: "change the instance id of a task",
		Args:  cobra.ExactArgs(4),
		Run: func(cmd *cobra.Command, args []string) {

			changeInstance(args[0], args[1], args[2], args[3])
		},
	}
	root.AddCommand(changeInstance)

}

func maskAction(mzid, taskid, actionid string, unmask bool) {
	err := task.MaskAction(mzid, taskid, actionid, unmask)
	if err != nil {
		log.Fatal(err)
	}
}

func changeInstance(mzid, tid, old, new string) {

	t := &task.Task{Mzid: mzid, ID: tid}
	err := common.Read(t)
	if err != nil {
		log.Fatal(err)
	}
	t.Instance = new

	actions, err := task.GetMzidActions(t.Mzid, t.ID)
	if err != nil {
		log.Fatal(err)
	}

	changeList := make([]common.Object, 0)
	for _, a := range actions {
		if a.MzInstance == old {
			a.MzInstance = new
			changeList = append(changeList, a)
		}
	}

	// update the task's instance as well
	changeList = append(changeList, t)

	err = common.WriteObjects(changeList, false)
	if err != nil {
		log.Fatal(err)
	}

}

// Output is displayed to user
type Output struct {
	Timestamp    string
	Mzid         string
	Taskid       string
	Stageid      string
	StageOrder   int
	Actionid     string
	ActionKind   string
	Instance     string
	ActionName   string
	Dependencies []string
	Completed    bool
	Error        string
	Masked       bool
}

// Format formats output for fprintf
func (o *Output) Format(tw *tabwriter.Writer) {
	fmt.Fprintf(tw, "%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%v\t%v\t%s\t%v\n",
		o.Timestamp, o.Mzid, o.Taskid, o.Stageid,
		o.Actionid, o.ActionKind, o.Instance, o.ActionName,
		o.Dependencies, o.Completed, o.Error, o.Masked)
}

// Sorted sorts the outputs in meaningful way
// timestamp > stage order
func Sorted(output []*Output) []*Output {
	sort.Slice(output, func(i, j int) bool {
		t0, _ := time.Parse(task.TimeFormat, output[i].Timestamp)
		t1, _ := time.Parse(task.TimeFormat, output[j].Timestamp)

		// sort by timestamp first
		if t0.Before(t1) && !t0.Equal(t1) {
			return true
		}
		if t0.After(t1) && !t0.Equal(t1) {
			return false
		}

		// then sort by stage order
		if output[i].StageOrder < output[j].StageOrder {
			return true
		}
		if output[i].StageOrder > output[j].StageOrder {
			return false
		}

		// finally sort by action id
		return output[i].Actionid > output[j].Actionid
	})
	return output
}

func listTasks(all bool) {

	tasks := make(map[string]*task.Task)
	if !all {
		allas, err := task.GetAllReadyAS()
		if err != nil {
			log.Fatal(err)
		}
		for _, as := range allas {
			t, err := task.ParseAction(as)
			if err != nil {
				log.Fatal(err)
			}
			_, ok := tasks[t.ID]
			if !ok {
				tasks[t.ID] = t
			}
		}

	} else {
		tmptasks, err := task.ListTasks("")
		if err != nil {
			log.Fatal(err)
		}

		for _, x := range tmptasks {
			tasks[x.ID] = x
		}

	}

	fmt.Fprintf(tw, "time\tmzid\ttaskid\tstageid\tactionid\tkind\tinstance\taction\tdeps\tcomplete\terror\tmasked\n")

	ordered := make([]*Output, 0)

	for _, t := range tasks {

		actions, err := task.GetMzidActions(t.Mzid, t.ID)
		if err != nil {
			log.Fatal(err)
		}

		stats, err := task.GetMzidAS(t.Mzid, t.ID)
		if err != nil {
			log.Fatal(err)
		}

		stages, err := task.GetTaskStagesMap(t.Mzid, t.ID)
		if err != nil {
			log.Fatal(err)
		}

		for _, a := range actions {
			if a == nil || a.ID == "" {
				continue
			}

			as, ok := stats[a.ID]
			if !ok {
				log.Printf("cant fetch status for: %s", a.ID)
			}

			errmsg := ""
			if as.Error != nil {
				errmsg = *as.Error
			}

			if as.Complete && !all {
				continue
			}

			_, ok = stages[a.Stageid]
			if !ok {
				log.Printf("Error: action missing stage: %v %v\n", a, stages)
				continue
			}

			ordered = append(ordered, &Output{
				Timestamp:    t.Timestamp,
				Mzid:         a.Mzid,
				Taskid:       t.ID,
				Stageid:      a.Stageid,
				StageOrder:   stages[a.Stageid].Order,
				Actionid:     a.ID,
				ActionKind:   a.Kind,
				Instance:     a.MzInstance,
				ActionName:   a.Action,
				Dependencies: t.Deps,
				Completed:    as.Complete,
				Error:        errmsg,
				Masked:       as.Masked,
			})
		}
	}

	order := Sorted(ordered)

	for _, thing := range order {
		thing.Format(tw)
	}
	tw.Flush()
}

func showTask(mzid, tid string) {
	t := &task.Task{Mzid: mzid, ID: tid}
	err := common.Read(t)
	if err != nil {
		log.Fatal(err)
	}

	out, err := yaml.Marshal(t)
	if err != nil {
		log.Fatal(err)
	}

	log.Print(string(out))
}

func launchTasks(file string) {

	t, objs, err := task.ReadTask(file)
	if err != nil {
		log.Printf("read task failure")
		log.Fatal(err)
	}

	err = task.LaunchTask(t, objs)
	if err != nil {
		log.Printf("launch task failure")
		log.Fatal(err)
	}

}

func cancelTask(mzid, t string) {
	err := task.CancelTask(mzid, t)
	if err != nil {
		log.Printf("cancel task failure")
		log.Fatal(err)
	}
}

func clearTaskErrors(mzid string, tasks []string) {
	for _, t := range tasks {
		t := &task.Task{Mzid: mzid, ID: t}
		err := common.Read(t)
		if err != nil {
			log.Fatal(err)
		}

		actions, err := task.GetMzidAS(mzid, t.ID)
		if err != nil {
			log.Fatal(err)
		}

		err = t.ClearErrors(actions)
		if err != nil {
			log.Fatal(err)
		}
	}
}

func complete(mzid string, tasks []string) {
	for _, t := range tasks {
		tk := &task.Task{Mzid: mzid, ID: t}
		err := common.Read(tk)
		if err != nil {
			log.Fatal(err)
		}

		err = tk.SetComplete()
		if err != nil {
			log.Fatal(err)
		}
	}
}

func resetTask(mzid string, tasks []string) {
	for _, t := range tasks {
		tk := &task.Task{Mzid: mzid, ID: t}
		err := common.Read(tk)
		if err != nil {
			log.Fatal(err)
		}

		actions, err := task.GetMzidAS(mzid, t)
		if err != nil {
			log.Fatal(err)
		}

		err = tk.Reset(actions)
		if err != nil {
			log.Fatal(err)
		}
	}
}
