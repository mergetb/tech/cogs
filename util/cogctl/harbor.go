package main

import (
	"context"
	"fmt"
	"os"
	"text/tabwriter"
	"time"

	"github.com/spf13/cobra"
	"gitlab.com/mergetb/site/api"
	"gitlab.com/mergetb/tech/cogs/pkg/task"
	"gitlab.com/mergetb/tech/nex/pkg"
)

const (
	nexPort = 6000
)

func harborCmds(show, root *cobra.Command) {

	var (
		reboot bool
	)
	var add = &cobra.Command{
		Use:   "recycle [node...]",
		Short: "recycle nodes",
		Args:  cobra.MinimumNArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			addToHarbor(args, reboot)
		},
	}
	add.Flags().BoolVarP(&reboot, "reboot", "r", false, "reboot the nodes")
	root.AddCommand(add)

	var showHarbor = &cobra.Command{
		Use:   "harbor",
		Short: "show harbor info",
		Args:  cobra.NoArgs,
		Run: func(*cobra.Command, []string) {
			showHarbor()
		},
	}
	show.AddCommand(showHarbor)

}

func addToHarbor(nodes []string, reboot bool) {

	var fragments []*site.MzFragment
	for _, node := range nodes {

		fragments = append(fragments, &site.MzFragment{
			Resources: []string{node},
		})

	}

	err := task.NodeRecycle(tbModel(), tempMzid("admin"), tempMzInstance(), fragments)
	if err != nil {
		fmt.Print(err)
	}

}

func showHarbor() {

	net := task.HarborMzid
	var tw = tabwriter.NewWriter(os.Stdout, 0, 0, 4, ' ', 0)
	endpoint := fmt.Sprintf("%s:%d", task.HarborControlAddr, nexPort)

	fmt.Println(blue("leases"))
	err := withNexClient(endpoint, func(cli nex.NexClient) error {

		resp, err := cli.GetMembers(context.TODO(), &nex.GetMembersRequest{Network: net})
		if err != nil {
			grpcFatal(err)
		}

		fmt.Fprint(tw, "mac\tname\tip4\n")
		for _, m := range resp.Members {
			fmt.Fprintf(tw, "%s\t%s\t%s\n", m.Mac, m.Name, showLease(m.Ip4))
		}
		tw.Flush()
		return nil

	})

	if err != nil {
		fmt.Print(err)
	}

}

// XXX burgled from nex - expose as library function from nex
func showLease(lease *nex.Lease) string {

	if lease == nil {
		return ""
	}

	expires := ""
	if lease.Expires != nil {
		t := time.Until(time.Unix(lease.Expires.Seconds, int64(lease.Expires.Nanos)))
		expires = fmt.Sprintf(" (%d:%d:%d)",
			int(t.Hours()), int(t.Minutes())%60, int(t.Seconds())%60)
	}

	return fmt.Sprintf("%s%s", lease.Address, expires)

}
