package main

import (
	"fmt"
	"log"
	"sort"

	"github.com/spf13/cobra"
	"gitlab.com/mergetb/tech/cogs/pkg/common"
	"gitlab.com/mergetb/tech/cogs/pkg/task"
)

func nodeCmds(set, list *cobra.Command) {

	listNodes := &cobra.Command{
		Use:   "nodes",
		Short: "list nodes",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			listNodes()
		},
	}
	list.AddCommand(listNodes)

	setNode := &cobra.Command{
		Use:   "nodes",
		Short: "set nodes",
	}
	set.AddCommand(setNode)

	setNodeStatus := &cobra.Command{
		Use:   "status <current | target | all> <setup | recycling | ready | clean | none> <nodeA nodeB ...>",
		Short: "status <state-name> <state-value> <node>",
		Args:  cobra.MinimumNArgs(3),
		Run: func(cmd *cobra.Command, args []string) {
			setNodeStatus(args[0], args[1], args[2:])
		},
	}
	setNode.AddCommand(setNodeStatus)

	setNodeMzid := &cobra.Command{
		Use:   "mzid <mzid> <nodeA nodeB ...>",
		Short: "mzid <mzid> <node>",
		Args:  cobra.MinimumNArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			setNodeMzid(args[0], args[1:])
		},
	}
	setNode.AddCommand(setNodeMzid)
}

func setNodeMzid(mzid string, nodes []string) {

	for _, node := range nodes {

		nodestate, err := task.GetNodeStatus(node)
		if err != nil {
			log.Fatal(err)
		}

		nodestate.Mzid = mzid

		err = common.Write(nodestate)
		if err != nil {
			fmt.Printf("Error: Unable to update state for: %s: %v", node, err)
			return
		}
	}
}

func setNodeStatus(target, status string, nodes []string) {

	targets := make(map[string]bool)
	switch target {
	case "current":
		targets[target] = true
	case "target":
		targets[target] = true
	case "all":
		targets["current"] = true
		targets["target"] = true
	default:
		log.Fatalf("Error: Unknown %s, value must be: current, target, all", target)
	}

	for _, node := range nodes {

		nodestate, err := task.GetNodeStatus(node)
		if err != nil {
			log.Fatal(err)
		}

		_, ok := targets["current"]
		if ok {
			switch status {
			case "setup":
				nodestate.Status.Current = task.Setup
			case "recycling":
				nodestate.Status.Current = task.Recycling
			case "ready":
				nodestate.Status.Current = task.Ready
			case "clean":
				nodestate.Status.Current = task.Clean
			case "none":
				nodestate.Status.Current = task.NodeNone
			default:
				log.Fatalf("Error: Unknown status: %s, value must be: none,setup,ready,recycling,clean", status)
			}
		}
		_, ok = targets["target"]
		if ok {
			switch status {
			case "setup":
				nodestate.Status.Target = task.Setup
			case "recycling":
				nodestate.Status.Target = task.Recycling
			case "ready":
				nodestate.Status.Target = task.Ready
			case "clean":
				nodestate.Status.Target = task.Clean
			case "none":
				nodestate.Status.Target = task.NodeNone
			default:
				log.Fatalf("Error: Unknown status: %s, value must be: none,setup,ready,recycling,clean", status)
			}
		}

		// now update the object back to datastore, this is writing one object back at a time
		// future use may want to append objects and do a single write objects
		err = common.Write(nodestate)
		if err != nil {
			fmt.Printf("Error: Unable to update state for: %s: %v", node, err)
			return
		}
	}
}

func listNodes() {

	nodes, err := task.ListNodes()
	if err != nil {
		log.Fatal(err)
	}

	sort.Slice(nodes, func(i, j int) bool {
		return nodes[i].Name < nodes[j].Name
	})

	fmt.Fprintf(tw, "%s\t%s\t%s\t%s\n", "node", "mzid", "current state", "target state")
	for _, x := range nodes {

		fmt.Fprintf(tw, "%s\t%s\t%s\t%s\n", x.Name, x.Mzid, x.Status.Current.String(),
			x.Status.Target.String())

	}

	tw.Flush()

}
