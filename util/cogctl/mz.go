package main

import (
	"context"
	"fmt"
	"log"
	"os/exec"
	"sort"
	"strings"
	"time"

	"github.com/containerd/containerd"
	"github.com/containerd/containerd/namespaces"
	"github.com/spf13/cobra"
	"github.com/teris-io/shortid"

	"gitlab.com/mergetb/site/api"
	"gitlab.com/mergetb/tech/canopy/lib"
	"gitlab.com/mergetb/tech/cogs/pkg/common"
	"gitlab.com/mergetb/tech/cogs/pkg/fabric"
	"gitlab.com/mergetb/tech/cogs/pkg/runtime"
	"gitlab.com/mergetb/tech/cogs/pkg/task"
	"gitlab.com/mergetb/tech/nex/pkg"
	"gitlab.com/mergetb/xir/lang/go/v0.2"
	"gitlab.com/mergetb/xir/lang/go/v0.2/tb"
)

func mzCmds(root, show, delete, list *cobra.Command) {

	showMz := &cobra.Command{
		Use:   "mz <mzid>",
		Short: "show materialization info",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			mzInfo(args[0])
		},
	}
	show.AddCommand(showMz)

	listMzs := &cobra.Command{
		Use:   "mz [prefix]",
		Short: "list materializations",
		Run: func(cmd *cobra.Command, args []string) {
			pfx := ""
			if len(args) > 0 {
				pfx = args[0]
			}
			listMzs(pfx)
		},
	}
	list.AddCommand(listMzs)

	var all bool
	listMzTasks := &cobra.Command{
		Use:   "mztasks <mzid>",
		Short: "list materialization tasks",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			listMzTasks(args[0], all)
		},
	}
	listMzTasks.Flags().BoolVarP(&all, "all", "a", false, "list all tasks")
	list.AddCommand(listMzTasks)

	rematerialize := &cobra.Command{
		Use:   "remat <mzid>",
		Short: "rematerialize and experiment",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			rematerialize(args[0])
		},
	}
	root.AddCommand(rematerialize)

	tmux := &cobra.Command{
		Use:   "tmux <mzid>",
		Short: "tmux an experiment",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			tmux(args[0])
		},
	}
	root.AddCommand(tmux)

	var recordOnly bool
	del := &cobra.Command{
		Use:   "mz <mzid>",
		Short: "delete a materialization",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			delMz(args[0], recordOnly)
		},
	}
	del.Flags().BoolVarP(
		&recordOnly, "record-only", "r", false,
		"delete record only (do not dematerialize)",
	)
	delete.AddCommand(del)

	mz := &cobra.Command{
		Use:   "mz",
		Short: "advanced materialization control",
	}
	root.AddCommand(mz)

	relink := &cobra.Command{
		Use:   "relink <taskid...>",
		Short: "relink a materialization",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			relink(args[0])
		},
	}
	mz.AddCommand(relink)

	readv := &cobra.Command{
		Use:   "readv <taskid...>",
		Short: "readvertise materialization experiment links",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			readv(args[0])
		},
	}
	mz.AddCommand(readv)

	set := &cobra.Command{
		Use:   "set",
		Short: "set materialization properties",
	}
	mz.AddCommand(set)

	setStatus := &cobra.Command{
		Use:   "status <mzid> <none|materializing|active|dematerializing>",
		Short: "set the materialization status",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			setStatus(args[0], args[1])
		},
	}
	set.AddCommand(setStatus)

	ctr := &cobra.Command{
		Use:   "container",
		Short: "container control",
	}
	mz.AddCommand(ctr)

	ctrUp := &cobra.Command{
		Use:   "up <mzid> [container ...]",
		Short: "bring up containers",
		Long:  "Bring up containers. No arguments implies all containers",
		Args:  cobra.MinimumNArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			ctrUp(args[0], args[1:])
		},
	}
	ctr.AddCommand(ctrUp)

	ctrDown := &cobra.Command{
		Use:   "down <mzid> [container ...]",
		Short: "bring down containers",
		Long:  "Bring down containers. No arguments implies all containers",
		Args:  cobra.MinimumNArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			ctrDown(args[0], args[1:])
		},
	}
	ctr.AddCommand(ctrDown)

}

func delMz(mzid string, recordOnly bool) {

	if recordOnly {
		mzi := &task.MzInfo{Mzid: mzid}
		err := common.Delete(mzi)
		if err != nil {
			log.Fatal(err)
		}
	} else {
		demat(mzid)
	}

}

func demat(mzid string) {

	mzi := &task.MzInfo{Mzid: mzid}
	err := common.Read(mzi)
	if err != nil {
		log.Fatalf("could not read mzinfo %v", err)
	}
	// collect fragments ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	// nodes
	var nodeFragments []*site.MzFragment
	nodeInfos, err := task.ListNodeInfo(mzid)
	if err != nil {
		log.Fatal(err)
	}
	for _, ni := range nodeInfos {
		nodeFragments = append(nodeFragments, &site.MzFragment{
			Elements:  []string{ni.XpName},
			Resources: []string{ni.Machine},
			Model: &site.MzFragment_NodeModel{
				NodeModel: &site.NodeModel{
					Model: "{}",
				},
			},
		})
	}
	// links
	var linkFragments []*site.MzFragment
	linkinfos, err := task.ListLinkInfos(mzid)
	if err != nil {
		log.Fatal(err)
	}
	for _, li := range linkinfos {
		resources := li.Core
		for _, l := range li.TBVLinks {
			for _, e := range l.Edges {
				resources = append(resources, e.Resource)
			}
		}
		linkFragments = append(linkFragments, &site.MzFragment{
			Resources: resources,
			Model: &site.MzFragment_LinkModel{
				LinkModel: &site.LinkModel{
					Model: "{}",
				},
			},
		})
	}
	err = task.DestroyLinks(tbModel(), mzi, linkFragments)
	if err != nil {
		log.Printf("link destroy failed")
	}
	err = task.NodeRecycle(tbModel(), mzid, mzi.Instance, nodeFragments)
	if err != nil {
		log.Printf("node recycle failed")
	}
	// teardown ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	err = task.TeardownInfranet(mzi, nil)
	if err != nil {
		log.Printf("infranet teardown failed")
	}

}

func listMzTasks(prefix string, all bool) {

	tasks, err := task.ListTasks(prefix)
	if err != nil {
		log.Fatal(err)
	}

	sort.Slice(tasks, func(i, j int) bool {

		t0, _ := time.Parse(task.TimeFormat, tasks[i].Timestamp)
		t1, _ := time.Parse(task.TimeFormat, tasks[j].Timestamp)

		return t0.Before(t1)

	})

	fmt.Fprintf(tw,
		"time\tmzid\ttaskid\tstageid\tactionid\tkind\tinstance\taction\tdeps\tcomplete\terror\tmasked\n")

	ordered := make([]*Output, 0)
	stageMap := make(map[string]*task.Stage)
	for _, t := range tasks {
		stages, err := task.GetTaskStages(t.Mzid, t.ID)
		if err != nil {
			log.Fatal(err)
		}
		for _, s := range stages {
			stageMap[s.ID] = s
		}
		actions, err := task.GetMzidActions(t.Mzid, t.ID)
		if err != nil {
			log.Fatal(err)
		}
		for _, a := range actions {

			as, err := task.GetActionStatus(a.Mzid, t.ID, a.ID)
			if err != nil {
				log.Println("Error:", err)
				continue
			}

			if !strings.HasPrefix(a.Mzid, prefix) {
				continue
			}

			if a == nil || a.ID == "" {
				continue
			}

			if as.Complete && !all {
				continue
			}

			errmsg := ""
			if as.Error != nil {
				errmsg = *as.Error
			}

			_, ok := stageMap[a.Stageid]
			if !ok {
				log.Printf("Error: action missing stage: %v %v\n", a, stageMap)
				continue
			}

			ordered = append(ordered, &Output{
				Timestamp:    t.Timestamp,
				Mzid:         a.Mzid,
				Taskid:       t.ID,
				Stageid:      a.Stageid,
				StageOrder:   stageMap[a.Stageid].Order,
				Actionid:     a.ID,
				ActionKind:   a.Kind,
				Instance:     a.MzInstance,
				ActionName:   a.Action,
				Dependencies: t.Deps,
				Completed:    as.Complete,
				Error:        errmsg,
				Masked:       as.Masked,
			})
		}
	}

	order := Sorted(ordered)

	for _, thing := range order {
		thing.Format(tw)
	}

	tw.Flush()

}

func listMzs(prefix string) {

	mzs, err := task.ListMaterializations(prefix)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Fprintf(tw, "MZID\tVINDEX\tVNI\tVID\n")
	for _, mz := range mzs {
		fmt.Fprintf(tw, "%s\t%d\t%d\t%d\n", mz.Mzid, mz.Vindex, mz.Vni, mz.Vid)
	}
	tw.Flush()

}

func mzInfo(mzid string) {

	topo := readXir()

	mzi := &task.MzInfo{Mzid: mzid}
	err := common.Read(mzi)
	if err != nil {
		log.Fatalf("could not read mzinfo %v", err)
	}

	var status string
	switch mzi.Status {
	case task.MzNone:
		status = red("none")
	case task.Materializing:
		status = yellow("materializing")
	case task.Active:
		status = green("active")
	case task.Dematerializing:
		status = yellow("dematerializing")
	default:
		status = red("none")
	}
	fmt.Printf("%s: %s\n", black("status"), status)

	// basics
	fmt.Println(blue("infranet"))
	fmt.Fprintf(tw, "%s\t%s\t%s\t%s\n",
		black("vindex"),
		black("vni"),
		black("vid"),
		black("svcaddr"),
	)
	fmt.Fprintf(tw, "%s\t%s\t%s\t%s\n",
		white(fmt.Sprintf("%d", mzi.Vindex)),
		white(fmt.Sprintf("%d", mzi.Vni)),
		white(fmt.Sprintf("%d", mzi.Vid)),
		white(task.SvcAddress(mzi.Vindex)),
	)
	tw.Flush()

	// containers
	client, err := runtime.ContainerdClient()
	if err != nil {
		log.Fatalf("failed to connect to containerd: %v", err)
	}
	defer client.Close()

	ctx := namespaces.WithNamespace(context.TODO(), mzid)
	containers, err := client.Containers(ctx)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(blue("containers"))
	fmt.Fprintf(tw, "%s\t%s\n",
		black("container"),
		black("info"),
	)
	for _, c := range containers {

		t, err := c.Task(ctx, nil)
		if err != nil {
			fmt.Fprintf(tw, "%s\t%v\n", red(c.ID()), err)
			continue
		}

		s, err := t.Status(ctx)
		if err != nil {
			fmt.Fprintf(tw, "%s\t%v\n", red(c.ID()), err)
			continue
		}

		if s.Status != containerd.Running {
			fmt.Fprintf(tw, "%s\t%s\n", yellow(c.ID()), s.Status)
			continue
		}

		fmt.Fprintf(tw, "%s\t%s\n", green(c.ID()), "")

	}
	tw.Flush()

	// node info

	fmt.Println(blue("nodes"))
	fmt.Fprintf(tw, "%s\t%s\t%s\t%s\t%s\t%s\t%s\n",
		black("node"),
		black("xpname"),
		black("image"),
		black("imac"),
		black("address"),
		black("leaf"),
		black("vid"),
	)

	leafinfo := make(map[string]*LeafInfo)
	cc := canopy.NewClient()

	nodeInfos, err := task.ListNodeInfo(mzid)
	if err != nil {
		log.Fatal(err)
	}
	for _, ni := range nodeInfos {

		leaf, port, local, err := fabric.GetInfraSwitch(topo, ni.Machine)
		if err != nil {
			log.Fatalf("failed to get infra leaf")
		}
		lnkname := fabric.LinkName(port)
		breakout, ok := fabric.BreakoutName(local)
		if ok {
			lnkname = breakout
		}
		leafinfo[ni.Machine] = &LeafInfo{
			Node: ni.Machine,
			Leaf: leaf.Label(),
			Port: lnkname,
		}
		cc.GetPort(lnkname, leaf.Label())

	}
	err = cc.Query()
	if err != nil {
		log.Fatalf("canopy error: %v", err)
	}

	conn, nxc := dialNex(fmt.Sprintf("%s:6000", task.SvcAddress(mzi.Vindex)))
	defer conn.Close()

	nexMembers, err := nxc.GetMembers(ctx, &nex.GetMembersRequest{Network: mzid})
	if err != nil {
		log.Fatalf("error contacting nex: %v", err)
	}
	memberLookup := make(map[string]*nex.Member)

	for _, m := range nexMembers.Members {
		memberLookup[m.Mac] = m
	}

	nodeLookup := make(map[string]string)

	for _, ni := range nodeInfos {
		nodeLookup[ni.Machine] = ni.XpName
		node := ni.Machine

		l := leafinfo[node]
		p := cc.Elements[l.Leaf][l.Port].GetPort()

		if p == nil {
			log.Fatalf("%s.%s - not a port in canopy", l.Leaf, l.Port)
		}

		portstring := fmt.Sprintf("%s.%s", l.Leaf, l.Port)
		if p.State == canopy.UpDown_Up {
			portstring = green(portstring)
		} else {
			portstring = red(portstring)
		}

		addr := ""
		m, ok := memberLookup[ni.Nex.Mac]
		if ok {
			addr = showLease(m.Ip4)
		}

		vid := red(fmt.Sprintf("%d", mzi.Vid))
		for _, t := range p.Untagged {
			if int(t) == mzi.Vid && int(p.Access) == mzi.Vid {
				vid = green(fmt.Sprintf("%d", mzi.Vid))
			}
		}

		fmt.Fprintf(tw, "%s\t%s\t%s\t%s\t%s\t%s\t%s\n",
			white(node),
			white(ni.XpName),
			white(ni.Image.Image.Name),
			white(ni.Nex.Mac),
			white(addr),
			portstring,
			vid,
			//white(fmt.Sprintf("%d", p.Access)),
			//white(fmt.Sprintf("%v", p.Tagged)),
		)

	}
	tw.Flush()

	// node config
	fmt.Println(blue("interfaces"))
	fmt.Fprintf(tw, "%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n",
		black("node"),
		black("xpname"),
		black("interface"),
		black("addrs"),
		black("mac"),
		black("mtu"),
		black("endpoint"),
		black("vid"),
	)

	cc = canopy.NewClient()

	type Ifx struct {
		Node       string
		XpNode     string
		IfxName    string
		Addrs      string
		Mac        string
		Mtu        int
		SwitchPort string
		Switch     string
		Vid        int
	}

	var ifxs []Ifx

	for _, ni := range nodeInfos {

		node := ni.Machine

		if ni.Config == nil || ni.Config.Config == nil {
			fmt.Fprintf(tw, "%s\t%s\n",
				white(node),
				yellow("no config"),
			)
			continue
		}

		for _, ifx := range ni.Config.Config.Interfaces {

			leaf, local, leafifx := leafport(topo, node, ifx.Name)
			lnkname := leafifx.Label()
			breakout, ok := fabric.BreakoutName(local)
			if ok {
				lnkname = breakout
			}
			cc.GetPort(lnkname, leaf)

			// holy diver, this is a bit gross
			_, _, xn := topo.GetNode(node)
			if xn != nil {
				ep := xn.GetEndpoint(ifx.Name)
				if ep != nil {
					xmac, ok := ep.Props["mac"].(string)
					if ok {
						ifx.Mac = xmac
					}
				}
			}

			var vid int
			if ifx.Vlan != nil {
				vid = int(ifx.Vlan.Vid)
			}

			ifxs = append(ifxs, Ifx{
				Node:       node,
				XpNode:     ni.XpName,
				IfxName:    ifx.Name,
				Addrs:      fmt.Sprintf("%+v", ifx.Addrs),
				Mac:        ifx.Mac,
				Mtu:        int(ifx.Mtu),
				SwitchPort: lnkname,
				Switch:     leaf,
				Vid:        vid,
			})

		}
	}

	err = cc.Query()
	if err != nil {
		log.Fatalf("canopy error: %v", err)
	}

	for _, x := range ifxs {

		p := cc.Elements[x.Switch][x.SwitchPort].GetPort()

		portstring := fmt.Sprintf("%s.%s", x.Switch, x.SwitchPort)
		if p.State == canopy.UpDown_Up {
			portstring = green(portstring)
		} else {
			portstring = red(portstring)
		}

		mtu := green(fmt.Sprintf("%d", x.Mtu))
		if int(p.Mtu) != x.Mtu {
			mtu = red(fmt.Sprintf("%d != %d", p.Mtu, x.Mtu))
		}

		vid := ""
		for _, t := range p.Tagged {
			if int(t) == x.Vid {
				vid = green(fmt.Sprintf("[%d]", x.Vid))
			}
		}
		if vid == "" {
			vid = green(fmt.Sprintf("%d", p.Access))
		}

		fmt.Fprintf(tw, "%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n",
			white(x.Node),
			white(x.XpNode),
			white(x.IfxName),
			white(x.Addrs),
			white(x.Mac),
			mtu,
			portstring,
			vid,
		)
	}
	tw.Flush()

	// links
	fmt.Println(blue("links"))
	printMzLinks(mzi, "", nodeLookup)
	tw.Flush()

}

func printMzLinks(
	mzi *task.MzInfo, kind task.LinkKind, nodeLookup map[string]string) {

	links, err := task.ListLinkInfos(mzi.Mzid)
	if err != nil {
		log.Fatal(err)
	}

	linkplan := &fabric.LinkPlan{Mzid: mzi.Mzid, Role: fabric.ExperimentLP}
	err = common.Read(linkplan)
	if err != nil {
		log.Fatal(err)
	}
	lpl := linkplan.LookupTable()

	for _, l := range links {

		var s, xs []string
		for _, x := range l.Edges() {
			s = append(s, x.NodeName)
			xs = append(xs, nodeLookup[x.NodeName])
		}

		if len(s) > 3 {
			s = []string{
				s[0],
				"...",
				s[len(s)-1],
			}
			xs = []string{
				xs[0],
				"...",
				xs[len(s)-1],
			}
		}

		fmt.Printf("%s\n",
			cyan(strings.Join(s, "~")+" "+strings.Join(xs, "~")),
		)

		fmt.Fprintf(tw, "%s\t%s\t%s\n",
			black("vni"),
			black("vid"),
			black("edges"),
		)

		edgeVX := make(map[int]int)

		for _, v := range l.TBVLinks {

			edgeVX[v.Vid] = v.Vni

			var xpedge []string
			for _, e := range v.Edges {
				xpedge = append(xpedge, nodeLookup[e.NodeName])
			}

			fmt.Fprintf(tw, "%s\t%s\t%s\n",
				white(fmt.Sprintf("%d", v.Vni)),
				white(fmt.Sprintf("%d", v.Vid)),
				white(fmt.Sprintf("%v %v", v.Edges, xpedge)),
			)

		}

		tw.Flush()

		fmt.Fprintf(tw, "%s\t%s\t%s\n",
			black("host"),
			black("port"),
			black("vid/vni"),
		)

		cc := canopy.NewClient()

		for _, t := range lpl.Trunk[l.XpID] {
			cc.GetPort(t.Name, t.Host)
		}

		for _, v := range lpl.Vtep[l.XpID] {
			cc.GetPort(v.Name, v.Host)
		}

		for _, a := range lpl.Access[l.XpID] {
			cc.GetPort(a.Name, a.Host)
		}

		err := cc.Query()
		if err != nil {
			log.Fatalf("canopy error: %v", err)
		}

		for _, a := range lpl.Access[l.XpID] {

			p := cc.Elements[a.Host][a.Name].GetPort()
			portstring := a.Name
			if p.State == canopy.UpDown_Up {
				portstring = green(portstring)
			} else {
				portstring = red(portstring)
			}

			vid := red(fmt.Sprintf("%d", a.Vid))
			if a.Vid == int(p.Access) {
				vid = green(fmt.Sprintf("%d", a.Vid))
			}

			fmt.Fprintf(tw, "%s\t%s\t%s\n",
				white(a.Host),
				portstring,
				vid,
			)
		}

		for _, t := range lpl.Trunk[l.XpID] {

			var trunkVIDs []int
			for _, vid := range t.Vids {

				_, ok := edgeVX[vid]
				if ok {
					trunkVIDs = append(trunkVIDs, vid)
				}
			}

			p := cc.Elements[t.Host][t.Name].GetPort()
			portstring := t.Name
			if p.State == canopy.UpDown_Up {
				portstring = green(portstring)
			} else {
				portstring = red(portstring)
			}

			var ts []string
			for _, x := range trunkVIDs {
				s := red(fmt.Sprintf("%d", x))
				for _, y := range p.Tagged {
					if x == int(y) {
						s = green(fmt.Sprintf("%d", x))
					}

				}
				ts = append(ts, s)
			}

			if len(ts) < 10 {
				fmt.Fprintf(tw, "%s\t%s\t%s\n",
					white(t.Host),
					portstring,
					fmt.Sprintf("%v", ts),
				)
			} else {
				sort.Ints(t.Vids)
				fmt.Fprintf(tw, "%s\t%s\t%s\n",
					white(t.Host),
					portstring,
					fmt.Sprintf("%v", ts[:10]),
				)
				for i := 10; i < len(ts); i += 10 {
					begin := i
					end := i + 10
					if end >= len(ts) {
						end = len(ts)
					}
					fmt.Fprintf(tw, "%s\t%s\t%s\n",
						white(""),
						white(""),
						fmt.Sprintf("%v", ts[begin:end]),
					)
				}
			}
		}

		for _, v := range lpl.Vtep[l.XpID] {

			p := cc.Elements[v.Host][v.Name].GetVtep()
			portstring := v.Name
			if p == nil || p.State == canopy.UpDown_Down {
				portstring = red(portstring)
			} else {
				portstring = green(portstring)
			}

			vni := red(fmt.Sprintf("%d", v.Vni))
			if v.Vni == int(p.Vni) {
				vni = green(fmt.Sprintf("%d", v.Vni))
			}

			vid := red(fmt.Sprintf("%d", v.Vid))
			if v.Vid == int(p.BridgeAccess) {
				vid = green(fmt.Sprintf("%d", v.Vid))
			}

			fmt.Fprintf(tw, "%s\t%s\t%s\n",
				white(v.Host),
				portstring,
				fmt.Sprintf("%s/%s", vid, vni),
			)
		}

		advs := lpl.Advertisements[l.XpID]
		if len(advs) > 0 {

			fmt.Fprintf(tw, "%s\t%s\t%s\t%s\t%s\n",
				black("router api"),
				black("router host"),
				black("addr"),
				black("asn"),
				black("vni"),
			)

			for _, adv := range advs {

				fmt.Fprintf(tw, "%s\t%s\t%s\t%s\t%s\n",
					white(adv.RouterAPI),
					white(adv.RouterHost),
					white(adv.Addr),
					white(fmt.Sprintf("%d", adv.ASN)),
					white(fmt.Sprintf("%d", adv.VNI)),
				)
			}

		}

		tw.Flush()

	}

}

func rematerialize(mzid string) {

	mzi := &task.MzInfo{Mzid: mzid}
	err := common.Read(mzi)
	if err != nil {
		log.Fatalf("could not read mzinfo %v", err)
	}

	// collect fragments ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	// nodes
	var nodeFragments []*site.MzFragment
	nodeInfos, err := task.ListNodeInfo(mzid)
	if err != nil {
		log.Fatal(err)
	}
	for _, ni := range nodeInfos {
		nodeFragments = append(nodeFragments, &site.MzFragment{
			Resources: []string{ni.Machine},
		})
	}

	// links
	var linkFragments []*site.MzFragment
	/* TODO
	linkspecs, err := cogs.ListLinkSpec(mzid)
	if err != nil {
		log.Fatal(err)
	}
	for _, spec := range linkspecs {
		linkFragments = append(linkFragments, &site.MzFragment{
			Resources: spec.Links,
		})
	}
	*/

	// teardown ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	err = task.TeardownInfranet(mzi, nil)
	if err != nil {
		log.Printf("infranet teardown failed")
	}

	err = task.NodeRecycle(tbModel(), mzid, mzi.Instance, nodeFragments)
	if err != nil {
		log.Printf("node recycle failed")
	}

	err = task.DestroyLinks(tbModel(), mzi, linkFragments)
	if err != nil {
		log.Printf("link destroy failed")
	}

	// stand up ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	err = task.InitInfranet(mzi, nil)
	if err != nil {
		log.Printf("infranet init failed")
	}

	// nodes
	err = task.NodeSetup(tbModel(), mzid, mzi.Instance, nodeFragments)
	if err != nil {
		log.Printf("node setup failed")
	}

	// links
	err = task.CreateLinks(tbModel(), mzi, linkFragments)
	if err != nil {
		log.Printf("link create failed")
	}

}

func tmux(mzid string) {

	mzi := &task.MzInfo{Mzid: mzid}
	err := common.Read(mzi)
	if err != nil {
		log.Fatalf("could not read mzinfo %v", err)
	}

	out, err := exec.Command("tmux", "start-server").CombinedOutput()
	if err != nil {
		log.Fatal(string(out))
	}

	session := strings.Replace(mzid, ".", "-", -1)
	out, err = exec.Command(
		"tmux", "new-session",
		"-x", "2000",
		"-y", "2000",
		"-d", "-s", session).CombinedOutput()
	if err != nil {
		log.Fatal(string(out))
	}

	nodeInfos, err := task.ListNodeInfo(mzid)
	if err != nil {
		log.Fatal(err)
	}
	for _, ni := range nodeInfos {

		out, err = exec.Command("tmux", "splitw").CombinedOutput()
		if err != nil {
			log.Fatal(string(out))
		}

		cmd := fmt.Sprintf("ssh %s", ni.Machine)
		out, err = exec.Command("tmux", "send-keys", cmd, "C-m").CombinedOutput()
		if err != nil {
			log.Fatal(string(out))
		}

		out, err = exec.Command("tmux", "select-layout", "tiled").CombinedOutput()
		if err != nil {
			log.Fatal(string(out))
		}

	}

}

func tempMzid(prefix string) string {

	sid, err := shortid.New(1, idAlphabet, 4747)
	if err != nil {
		log.Fatal(err)
	}
	return fmt.Sprintf("%s-%s", prefix, sid.MustGenerate())

}

func tempMzInstance() string {

	sid, err := shortid.New(1, idAlphabet, 4747)
	if err != nil {
		log.Fatal(err)
	}
	return sid.MustGenerate()

}

// LeafInfo encapsulates info about leaf switch ports
type LeafInfo struct {
	Node string
	Leaf string
	Port string
}

func leafport(topo *xir.Net, node, ifx string) (string, *xir.Endpoint, *xir.Endpoint) {

	_, _, xnode := topo.GetNodeByName(node)
	if xnode == nil {
		_, _, xnode = topo.GetNode(node)
		if xnode == nil {
			log.Fatalf("node %s not found in model", node)
		}
	}

	var ep *xir.Endpoint
	for _, e := range xnode.Endpoints {
		if e.Label() == ifx {
			ep = e
			break
		}
	}
	if ep == nil {
		log.Fatalf("interface %s.%s not found in model", node, ifx)
	}
	var nbr, local *xir.Endpoint
	for _, x := range ep.Neighbors {
		if tb.HasRole(x.Endpoint.Parent, tb.Node) {
			continue
		}
		nbr = x.Endpoint
		local = ep
	}
	if nbr == nil {
		log.Fatalf("interface %s.eth0 not connected in model", node)
	}

	return nbr.Parent.Label(), local, nbr

}

func ctrUp(mzid string, containers []string) {

	ctrMod(mzid, containers, func(ctx context.Context, c containerd.Container) {

		t, err := c.Task(ctx, nil)
		if err != nil {

			err := runtime.CreateTask(ctx, c, mzid, c.ID())
			if err != nil {
				fmt.Printf("%s: %v\n", red(c.ID()), err)
			}

			return
		}

		s, err := t.Status(ctx)
		if err != nil {
			fmt.Printf("%s: %v\n", red(c.ID()), err)
			return
		}

		if s.Status == containerd.Stopped {
			err = t.Resume(ctx)
			if err != nil {
				fmt.Printf("%s: failed to start task: %v\n", red(c.ID()), err)
			}
		}

	})

}

func ctrDown(mzid string, containers []string) {

	ctrMod(mzid, containers, func(ctx context.Context, c containerd.Container) {

		err := runtime.DeleteTask(ctx, c)
		if err != nil {
			fmt.Printf("%s: %v\n", yellow(c.ID()), err)
			return
		}

	})

}

func ctrMod(mzid string, containers []string,
	f func(context.Context, containerd.Container)) {

	client, err := runtime.ContainerdClient()
	if err != nil {
		log.Fatalf("failed to connect to containerd: %v", err)
	}
	defer client.Close()

	ctx := namespaces.WithNamespace(context.TODO(), mzid)
	cs, err := client.Containers(ctx)
	if err != nil {
		log.Fatal(err)
	}

	m := make(map[string]containerd.Container)

	if len(containers) == 0 {

		for _, x := range cs {
			m[x.ID()] = x
		}

	} else {

		for _, c := range containers {
			m[c] = nil
			for _, x := range cs {
				if x.ID() == c {
					m[c] = x
				}
			}
		}

		for c, x := range m {
			if x == nil {
				log.Printf("%s: not found", c)
			}
		}

	}

	for _, x := range m {

		f(ctx, x)

	}

}

func setStatus(mzid, status string) {

	var s task.MzStatus
	switch status {

	case "none":
		s = task.MzNone
	case "materializing":
		s = task.Materializing
	case "active":
		s = task.Active
	case "dematerializing":
		s = task.Dematerializing
	default:
		log.Fatal(
			"status must be one of: none, materializing, active, dematerializing")

	}

	err := common.RUC(&task.MzInfo{Mzid: mzid}, func(o common.Object) {
		o.(*task.MzInfo).Status = s
	})
	if err != nil {
		log.Print(err)
	}

}

func relink(mzid string) {

	mzi := &task.MzInfo{Mzid: mzid}
	err := common.Read(mzi)
	if err != nil {
		log.Fatalf("could not read mzinfo %v", err)
	}

	tasks, err := task.ListTasks(mzid)
	if err != nil {
		log.Fatal(err)
	}

	stageMap := make(map[string]*task.Stage)
	for _, t := range tasks {
		stages, err := task.GetTaskStages(t.Mzid, t.ID)
		if err != nil {
			log.Fatal(err)
		}
		for _, s := range stages {
			stageMap[s.ID] = s
		}
		actions, err := task.GetMzidActions(t.Mzid, t.ID)
		if err != nil {
			log.Fatal(err)
		}
		for _, a := range actions {

			if !strings.HasPrefix(a.Mzid, mzid) {
				continue
			}

			if a.MzInstance != mzi.Instance {
				continue
			}

			if a.Kind != task.CanopyKind {
				continue
			}

			as, err := task.GetActionStatus(a.Mzid, t.ID, a.ID)
			if err != nil {
				log.Println("Error:", err)
				continue
			}

			errmsg := ""
			if as.Error != nil {
				errmsg = *as.Error
			}

			_, ok := stageMap[a.Stageid]
			if !ok {
				log.Printf("Error: action missing stage: %v %v\n", a, stageMap)
				continue
			}

			fmt.Fprintf(tw, "%s\t%s\t%s\t%d\t%s\t%s\t%s\t%s\t%v\t%v\t%s\n",
				t.Timestamp, t.ID, a.Stageid, stageMap[a.Stageid].Order, a.Kind,
				a.Mzid, a.ID, a.Action, t.Deps, as.Complete, errmsg)

			err = common.RUC(as, func(o common.Object) {
				o.(*task.ActionStatus).Error = nil
				o.(*task.ActionStatus).Complete = false
			})
			if err != nil {
				log.Print(err)
			}

		}
	}

	tw.Flush()
}

func readv(mzid string) {

	mzi := &task.MzInfo{Mzid: mzid}
	err := common.Read(mzi)
	if err != nil {
		log.Fatalf("could not read mzinfo %v", err)
	}

	tasks, err := task.ListTasks(mzid)
	if err != nil {
		log.Fatal(err)
	}

	stageMap := make(map[string]*task.Stage)
	for _, t := range tasks {
		stages, err := task.GetTaskStages(t.Mzid, t.ID)
		if err != nil {
			log.Fatal(err)
		}
		for _, s := range stages {
			stageMap[s.ID] = s
		}
		actions, err := task.GetMzidActions(t.Mzid, t.ID)
		if err != nil {
			log.Fatal(err)
		}
		for _, a := range actions {

			if !strings.HasPrefix(a.Mzid, mzid) {
				continue
			}

			if a.MzInstance != mzi.Instance {
				continue
			}

			if a.Kind != task.PlumbingKind {
				continue
			}

			if a.Action != task.AdvertiseMac {
				continue
			}

			as, err := task.GetActionStatus(a.Mzid, t.ID, a.ID)
			if err != nil {
				log.Println("Error:", err)
				continue
			}

			errmsg := ""
			if as.Error != nil {
				errmsg = *as.Error
			}

			_, ok := stageMap[a.Stageid]
			if !ok {
				log.Printf("Error: action missing stage: %v %v\n", a, stageMap)
				continue
			}

			fmt.Fprintf(tw, "%s\t%s\t%s\t%d\t%s\t%s\t%s\t%s\t%v\t%v\t%s\n",
				t.Timestamp, t.ID, a.Stageid, stageMap[a.Stageid].Order, a.Kind,
				a.Mzid, a.ID, a.Action, t.Deps, as.Complete, errmsg)

			// TODO: make transaction if perf is bad
			err = common.RUC(as, func(o common.Object) {
				o.(*task.ActionStatus).Error = nil
				o.(*task.ActionStatus).Complete = false
			})
			if err != nil {
				fmt.Print(err)
			}
		}
	}

	tw.Flush()
}
