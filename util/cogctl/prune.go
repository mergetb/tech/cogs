package main

import (
	"fmt"
	"log"
	"sort"
	"strconv"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/mergetb/tech/canopy/lib"
	"gitlab.com/mergetb/tech/cogs/pkg/common"
	"gitlab.com/mergetb/tech/cogs/pkg/runtime"
	"gitlab.com/mergetb/tech/cogs/pkg/task"
	"gitlab.com/mergetb/tech/gobble/pkg"
	"gitlab.com/mergetb/tech/rtnl"
	"gitlab.com/mergetb/xir/lang/go/v0.2/tb"
)

var dryRun bool

func pruneCmds(root *cobra.Command) {

	prune := &cobra.Command{
		Use:   "prune",
		Short: "prune unused resources",
	}
	prune.PersistentFlags().BoolVarP(
		&dryRun, "dry-run", "d", false, "show actions but do not execute")
	root.AddCommand(prune)

	net := &cobra.Command{
		Use:   "ifx",
		Short: "prune network interfaces",
		Args:  cobra.NoArgs,
		Run:   func(cmd *cobra.Command, args []string) { pruneIfx() },
	}
	prune.AddCommand(net)

	counters := &cobra.Command{
		Use:   "counters",
		Short: "prune network counters",
		Args:  cobra.NoArgs,
		Run:   func(cmd *cobra.Command, args []string) { pruneCounters() },
	}
	prune.AddCommand(counters)

	tasks := &cobra.Command{
		Use:   "tasks",
		Short: "prune tasks",
		Args:  cobra.NoArgs,
		Run:   func(cmd *cobra.Command, args []string) { pruneTasks() },
	}
	prune.AddCommand(tasks)

	vteps := &cobra.Command{
		Use:   "vteps",
		Short: "prune vteps",
		Args:  cobra.NoArgs,
		Run:   func(cmd *cobra.Command, args []string) { pruneVteps() },
	}
	prune.AddCommand(vteps)

	adverts := &cobra.Command{
		Use:   "adv",
		Short: "prune evpn advertisements",
		Args:  cobra.NoArgs,
		Run:   func(cmd *cobra.Command, args []string) { pruneAdverts() },
	}
	prune.AddCommand(adverts)

}

func pruneIfx() {

	// materializations
	mzs, err := task.ListMaterializations("")
	if err != nil {
		log.Fatal(err)
	}

	mx := make(map[int]bool)
	for _, m := range mzs {
		mx[m.Vindex] = true
	}

	// indicies
	vindex := &common.CountSet{Name: task.VindexKey}
	vni := &common.CountSet{Name: task.InfraVniKey}
	vid := &common.CountSet{Name: task.InfraVidKey}
	objs := []common.Object{vindex, vni, vid}

	_, err = common.ReadObjects(objs)
	if err != nil {
		log.Fatal(err)
	}

	cx := make(map[int]bool)
	for _, x := range vindex.Values {
		cx[x] = true
	}

	// interfaces

	ctx, err := rtnl.OpenDefaultContext()
	if err != nil {
		log.Fatal(err)
	}

	links, err := rtnl.ReadLinks(ctx, nil)
	if err != nil {
		log.Fatal(err)
	}

	var remove []string
	for _, l := range links {

		i, err := extractIndex(l.Info.Name)

		// ignore system level
		if i < task.MaxSystemVindex {
			continue
		}

		if err == nil {

			// mzinfo objects
			_, ok := mx[i]
			if !ok {
				remove = append(remove, l.Info.Name)
				log.Printf("%s (mzinfo)", l.Info.Name)
				if !dryRun {
					err = l.Absent(ctx)
					if err != nil {
						log.Printf("error deleting link: %v", err)
					}
				}
			}

			// counter objects
			_, ok = cx[i]
			if !ok {
				remove = append(remove, l.Info.Name)
				log.Printf("%s (vindex)", l.Info.Name)
				if !dryRun {
					err = l.Absent(ctx)
					if err != nil {
						log.Printf("error deleting link: %v", err)
					}
				}
			}
		}

	}

}

// ActiveIndices encapsulates an active index multimap
type ActiveIndices struct {
	Vindex, Vni, Vid, VniX, VidX map[int]bool
}

func getActiveIndices() ActiveIndices {

	mzs, err := task.ListMaterializations("")
	if err != nil {
		log.Fatal(err)
	}

	ais := ActiveIndices{
		Vindex: make(map[int]bool),
		Vni:    make(map[int]bool),
		Vid:    make(map[int]bool),
		VniX:   make(map[int]bool),
		VidX:   make(map[int]bool),
	}

	for _, m := range mzs {

		ais.Vindex[m.Vindex] = true
		ais.Vni[m.Vni] = true
		ais.Vid[m.Vid] = true

		linkinfos, err := task.ListLinkInfos(m.Mzid)
		if err != nil {
			log.Fatal(err)
		}

		for _, l := range linkinfos {
			for _, v := range l.TBVLinks {
				ais.VniX[v.Vni] = true
				ais.VidX[v.Vid] = true
			}
		}

	}

	return ais

}

func pruneCounters() {

	ais := getActiveIndices()

	// indicies
	vindex := common.CountSet{Name: task.VindexKey}
	vni := common.CountSet{Name: task.InfraVniKey}
	xvni := common.CountSet{Name: task.XpVniKey}
	vid := common.CountSet{Name: task.InfraVidKey}
	xvid := common.CountSet{Name: task.XpVidKey}
	objs := []common.Object{&vindex, &vni, &vid, &xvni, &xvid}

	_, err := common.ReadObjects(objs)
	if err != nil {
		log.Fatal(err)
	}

	var rVindex, rVni, rVid, rVniX, rVidX []int

	// vindex
	for _, x := range vindex.Values {
		ok := ais.Vindex[x]
		if !ok {
			log.Printf("%d (vindex)", x)
			if !dryRun {
				rVindex = append(rVindex, x)
			}
		}
	}
	for _, x := range rVindex {
		vindex = vindex.Remove(x)
	}

	// vni
	for _, x := range vni.Values {
		ok := ais.Vni[x]
		if !ok {
			log.Printf("%d (vni)", x)
			if !dryRun {
				rVni = append(rVni, x)
			}
		}
	}
	for _, x := range rVni {
		vni = vni.Remove(x)
	}

	// xvni
	for _, x := range xvni.Values {
		ok := ais.VniX[x]
		if !ok {
			log.Printf("%d (xvni)", x)
			if !dryRun {
				rVniX = append(rVniX, x)
			}
		}
	}
	for _, x := range rVniX {
		xvni = xvni.Remove(x)
	}

	// vid
	for _, x := range vid.Values {
		ok := ais.Vid[x]
		if !ok {
			log.Printf("%d (vid)", x)
			if !dryRun {
				rVid = append(rVid, x)
			}
		}
	}
	for _, x := range rVid {
		vid = vid.Remove(x)
	}

	// xvid
	for _, x := range xvid.Values {
		ok := ais.VidX[x]
		if !ok {
			log.Printf("%d (xvid)", x)
			if !dryRun {
				rVidX = append(rVidX, x)
			}
		}
	}
	for _, x := range rVidX {
		xvid = xvid.Remove(x)
	}

	if !dryRun {
		err := common.WriteObjects([]common.Object{
			&vindex, &vni, &vid, &xvid, &xvni,
		}, true)
		if err != nil {
			log.Fatal(err)
		}
	}

}

func pruneTasks() {

	// materializations
	mzs, err := task.ListMaterializations("")
	if err != nil {
		log.Fatal(err)
	}

	mx := make(map[string]*task.MzInfo)
	for _, m := range mzs {
		mx[m.Mzid] = m
	}

	// tasks
	tasks, err := task.ListTasks("")
	if err != nil {
		log.Fatal(err)
	}

	actions, err := task.GetAllAS()
	if err != nil {
		log.Fatal(err)
	}

	sort.Slice(tasks, func(i, j int) bool {
		return tasks[i].Mzid < tasks[j].Mzid
	})

	fmt.Fprintf(tw, "MZID\tTASK\n")
	for _, t := range tasks {

		complete, err := t.Complete(actions)
		if err != nil {
			log.Fatal(err)
		}

		mz, ok := mx[t.Mzid]
		if ok {
			ok = t.Instance == mz.Instance
		}
		if !ok && complete {
			fmt.Fprintf(tw, "%s\t%s\n", t.Mzid, t.ID)
			if !dryRun {
				err := t.Cancel()
				if err != nil {
					log.Printf("%s %s: %v", t.Mzid, t.ID, err)
				}
			}
		}

	}

	tw.Flush()

}

func getFabrics() ([]string, []string) {

	topo := readXir()

	// infra and experiment fabrics
	var ifs, xfs []string

	for _, x := range topo.AllNodes() {

		if tb.HasRole(x, tb.Fabric) {
			if tb.HasRole(x, tb.InfraSwitch) {
				ifs = append(ifs, x.Label())
			}
			if tb.HasRole(x, tb.XpSwitch) {
				xfs = append(xfs, x.Label())
			}
		}

	}

	return ifs, xfs
}

func pruneVteps() {

	ais := getActiveIndices()
	ifs, xfs := getFabrics()

	ielements, err := canopy.ListPorts(ifs)
	if err != nil {
		log.Fatal(err)
	}

	xelements, err := canopy.ListPorts(xfs)
	if err != nil {
		log.Fatal(err)
	}

	// host, port list
	toRemove := make(map[string][]string)

	toPrune(ielements, toRemove, ais.Vni)
	toPrune(xelements, toRemove, ais.VniX)

	fmt.Fprintf(tw, "HOST\tVTEP\n")
	cc := canopy.NewClient()
	for host, vteps := range toRemove {
		for _, vtep := range vteps {
			presence := &canopy.VtepPresence{Presence: canopy.Presence_Absent}
			cc.SetVtep(presence, vtep, host)
			fmt.Fprintf(tw, "%s\t%s\n", host, vtep)
		}
	}
	tw.Flush()

	if !dryRun {
		err := cc.Commit()
		if err != nil {
			log.Fatal(err)
		}
	}

}

func toPrune(
	emap map[string][]*canopy.Element,
	remove map[string][]string,
	active map[int]bool,
) {

	for host, elements := range emap {
		for _, element := range elements {

			vtep := element.GetVtep()
			if vtep != nil {

				// ignore system vteps
				if vtep.Vni < task.MaxSystemVindex {
					continue
				}
				_, ok := active[int(vtep.Vni)]
				if !ok {
					remove[host] = append(remove[host], element.Name)
				}
			}

		}
	}

}

// EthAdv encapsulates an EVPN MAC advertisement
type EthAdv struct {
	Mac string
	Vni int
}

func pruneAdverts() {

	cfg := runtime.GetConfig(false)
	bgpAddr := cfg.Net.ServiceTunnelIP + "/32"
	bgpAS := cfg.Net.BgpAS

	ais := getActiveIndices()

	bgpstate, err := gobble.ReadBgp()
	if err != nil {
		log.Fatal(err)
	}

	t2, t3 := pruneAdvertByVni(ais, bgpstate)

	log.Printf("Type 2 Advertisements")
	fmt.Fprintf(tw, "MAC\tVNI\n")
	for _, x := range t2 {
		fmt.Fprintf(tw, "%s\t%d\n", x.Mac, x.Vni)

		//XXX broken cannot assume local AS BGPAddr ...
		if !dryRun {
			err := runtime.EvpnWithdrawMac(
				bgpAddr, "localhost", x.Mac, bgpAS, x.Vni, x.Vni)
			if err != nil {
				log.Printf("%v", err)
			}
		}

	}
	tw.Flush()
	fmt.Printf("")

	log.Printf("Type 3 Advertisements")
	for _, x := range t3 {
		log.Printf("%d", x)

		//XXX broken cannot assume local AS BGPAddr ...
		if !dryRun {
			err := runtime.EvpnWithdrawMulticast(
				bgpAddr, "localhost", bgpAS, x, x)
			if err != nil {
				log.Printf("%v", err)
			}
		}
	}

}

func pruneAdvertByVni(
	ais ActiveIndices, bgpstate *gobble.BgpState) ([]EthAdv, []int) {

	var toRemoveT2 []EthAdv

	for _, x := range bgpstate.Type2Routes {

		if len(x.Route.Labels) == 0 {
			continue
		}

		vni := int(x.Route.Labels[0])

		_, ok := ais.Vni[vni]
		if !ok && vni > task.MaxSystemVindex {
			toRemoveT2 = append(toRemoveT2, EthAdv{x.Route.MacAddress, vni})
		}

	}

	var toRemoveT3 []int

	for _, x := range bgpstate.Type3Routes {

		vni := int(x.CommunityLocalAdmin)

		_, ok := ais.Vni[vni]
		if !ok && vni > task.MaxSystemVindex {
			toRemoveT3 = append(toRemoveT3, vni)
		}
	}

	return toRemoveT2, toRemoveT3

}

func extractIndex(ifx string) (int, error) {

	if strings.HasPrefix(ifx, "mzbr") && len(ifx) > 4 {
		return strconv.Atoi(ifx[4:])
	}

	if strings.HasPrefix(ifx, "ifr") && len(ifx) > 3 {
		return strconv.Atoi(ifx[3:])
	}

	if strings.HasPrefix(ifx, "svc") && len(ifx) > 3 {
		return strconv.Atoi(ifx[3:])
	}

	if strings.HasPrefix(ifx, "vtep") && len(ifx) > 4 {
		return strconv.Atoi(ifx[4:])
	}

	return -1, fmt.Errorf("not an mz interface")

}
