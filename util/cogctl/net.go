package main

import (
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/mergetb/tech/canopy/lib"
	"gitlab.com/mergetb/tech/cogs/pkg/common"
	"gitlab.com/mergetb/tech/cogs/pkg/task"
	"gitlab.com/mergetb/xir/lang/go/v0.2"
	"gitlab.com/mergetb/xir/lang/go/v0.2/tb"
)

func netCmds(check, read *cobra.Command) {

	checkSwitch := &cobra.Command{
		Use:   "switch <switch>",
		Short: "check a switch configuration",
		Args:  cobra.ExactArgs(1),
		Run:   func(cmd *cobra.Command, args []string) { checkSwitch(args[0]) },
	}
	check.AddCommand(checkSwitch)

	checkMz := &cobra.Command{
		Use:   "mz <mzid>",
		Short: "check materialization networks",
		Args:  cobra.ExactArgs(1),
		Run:   func(cmd *cobra.Command, args []string) { checkMz(args[0]) },
	}
	check.AddCommand(checkMz)

	readPorts := &cobra.Command{
		Use:   "ports",
		Short: "read switch ports",
		Args:  cobra.NoArgs,
		Run:   func(*cobra.Command, []string) { readPorts(dryRun) },
	}
	readPorts.Flags().BoolVarP(
		&dryRun, "dry-run", "y", false, "do not update ports in db")
	read.AddCommand(readPorts)

}

func checkSwitch(host string) {

	//TODO

}

func checkMz(mzid string) {

	//TODO

}

func readPorts(dryRun bool) {

	topo := readXir()

	switches := topo.Select(func(x *xir.Node) bool {
		return tb.HasRole(x, tb.XpSwitch, tb.InfraSwitch)
	})

	hosts := make([]string, 0, len(switches))
	for _, s := range switches {
		hosts = append(hosts, s.Label())
	}

	lists, err := canopy.ListPorts(hosts)
	if err != nil {
		log.Fatal(err)
	}

	var objs []common.Object

	for host, elements := range lists {

		for _, e := range elements {
			switch e.Value.(type) {

			case *canopy.Element_Port:
				p := e.GetPort()
				o := &task.PortObj{
					Host: host,
					Port: e.Name,
				}
				if p.Access != 0 {
					o.Mode = task.Access
					o.Vids = []int{int(p.Access)}
				} else {
					o.Mode = task.Trunk
					for _, t := range p.Tagged {
						o.Vids = append(o.Vids, int(t))
					}
				}
				if p.State == canopy.UpDown_Up {
					o.Up = true
				} else {
					o.Up = false
				}
				objs = append(objs, o)

			case *canopy.Element_Vtep:
				v := e.GetVtep()
				objs = append(objs, &task.VtepObj{
					Host: host,
					Name: e.Name,
					Vni:  int(v.Vni),
					Vid:  int(v.BridgeAccess),
				})

			}
		}

	}

	if !dryRun {
		err = common.WriteObjects(objs, false)
		if err != nil {
			log.Fatal(err)
		}
	}

}
