package main

import (
	"log"

	"google.golang.org/grpc"
	"google.golang.org/grpc/status"

	"gitlab.com/mergetb/tech/nex/pkg"
)

func dialNex(endpoint string) (*grpc.ClientConn, nex.NexClient) {

	conn, err := grpc.Dial(endpoint, grpc.WithInsecure())
	if err != nil {
		log.Fatal(err)
	}

	return conn, nex.NewNexClient(conn)
}

func withNexClient(endpoint string, f func(nex.NexClient) error) error {

	conn, cli := dialNex(endpoint)
	defer conn.Close()

	return f(cli)

}

func grpcFatal(err error) {

	s, ok := status.FromError(err)
	if !ok {
		log.Fatal(err)
	}
	log.Fatal(s.Message())

}
