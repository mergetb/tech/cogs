package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"

	"gitlab.com/mergetb/tech/cogs/pkg/fabric"
	"gitlab.com/mergetb/tech/cogs/pkg/runtime"
	"gitlab.com/mergetb/tech/cogs/pkg/task"
	"gitlab.com/mergetb/tech/gobble/pkg"
	"gitlab.com/mergetb/xir/lang/go/v0.2/tb"
)

func checkInfraPipes(repair bool) {

	ctx := NewContext(
		tb.Fabric,
		tb.Spine,
		tb.InfrapodServer,
		tb.NetworkEmulator,
		tb.StorageServer,
	)
	ctx.Repair = repair

	var diags Diagnostics

	current, err := readPipes(tb.InfraSwitch)
	if err != nil {
		log.Fatal(err)
	}

	planned, err := readPlans()
	if err != nil {
		log.Fatal(err)
	}

	for _, x := range current.Type3Adv {

		diag, ok := InfraCheckType3(x, planned, ctx)
		if !ok {
			diags.Type3 = append(diags.Type3, diag)
		}

	}

	for _, x := range current.Type2Adv {

		diag, ok := InfraCheckType2(x, planned, ctx)
		if !ok {
			diags.Type2 = append(diags.Type2, diag)
		}

	}

	diags.Access = append(
		diags.Access, checkAccess(current.AccessPorts, planned, ctx, Infranet)...,
	)

	diags.Trunks = append(
		diags.Trunks, checkTrunks(current.TrunkPorts, planned, ctx, Infranet)...,
	)

	diags.Show(ctx)

}

func fixInfranetLinkplans() {

	mzs, err := task.ListMaterializations("")
	if err != nil {
		log.Fatal(err)
	}

	for _, mz := range mzs {

		var nodes []string

		ns, err := task.ListNodeInfo(mz.Mzid)
		if err != nil {
			log.Fatal(err)
		}
		for _, x := range ns {
			nodes = append(nodes, x.Machine)
		}

		linkinfos, err := task.InfranetLinkInfos(
			nodes, mz.Vni, mz.Vid, mz.Mzid, tbmodel)

		if err != nil {
			log.Fatal(err)
		}

		mtu := runtime.GetConfig(false).Net.VtepMtu
		lp, err := fabric.NewLinkPlan(
			mz.Mzid, tbmodel, fabric.InfraLP, mtu, linkinfos...)
		if err != nil {
			log.Fatal(err)
		}

		if dryRun {

			out, err := json.MarshalIndent(lp, "", "  ")
			if err != nil {
				log.Fatalf("serialization failed: %v", err)
			}

			fn := fmt.Sprintf("%s-linkplan.json", mz.Mzid)
			err = ioutil.WriteFile(fn, out, 0644)
			if err != nil {
				log.Fatalf("linkplan save failed: %v", err)
			}

		} else {

			err := lp.Save()
			if err != nil {
				log.Fatalf("linkplan save failed: %v", err)
			}

		}

	}

}

func InfraCheckType2(
	adv gobble.Type2EvpnRoute, pp *Pipeplans, ctx *Context) (Diagnostic, bool) {

	if len(adv.Route.Labels) == 0 {
		return NewType2Leak(adv, NoLabels, ctx), false
	}
	if len(adv.Route.Labels) > 1 {
		return NewType2Leak(adv, TooManyLabels, ctx), false
	}
	vni := int(adv.Route.Labels[0])

	if vni == 2 {
		return nil, true
	}

	// ensure VNI is a thing
	mzp, ok := pp.ByVNI[int(vni)]
	if !ok {
		leak := NewType2Leak(adv, VNINotFound, ctx)
		if ctx.Repair {
			err := leak.Repair(ctx)
			if err != nil {
				log.Print(err)
			}
		}
		return leak, false
	}

	info := ExtractType2Info(adv, ctx)

	router, ok := ctx.RouterIpIndex[info.Destination]
	if !ok {
		return NewType2Leak(adv, UnknownRouter, ctx), false
	}

	// TODO Infrapod enclave MACs expected on infranet networks, but are not
	// currently tracked by LinkPlans. Infrapod hosts should be included in
	// LinkPlans, remove this logic once they are.
	if router.HasRole(tb.InfrapodServer) {
		return nil, true
	}

	// ensure MAC is a thing
	node, ok := ctx.NodeMACIndex[adv.Route.MacAddress]
	if !ok {
		return NewType2Leak(adv, NodeNotFound, ctx), false
	}

	// ensure node status is correct
	status, ok := ctx.NodeStatusIndex[node.System.Name]
	if !ok {
		return NewType2Leak(adv, NoNodeStatus, ctx), false
	}

	// ensure the node status mzid belongs to the Mzid we found by VNI
	if status.Mzid != mzp.MzInfo.Mzid {
		return NewType2Leak(adv, UnexpectedMzid, ctx), false
	}

	return nil, true

}

func InfraCheckType3(
	adv gobble.Type3EvpnRoute, pp *Pipeplans, ctx *Context) (Diagnostic, bool) {

	vni := adv.CommunityLocalAdmin
	if vni == 2 {
		return nil, true
	}

	// ensure VNI is a thing
	mzp, ok := pp.ByVNI[int(vni)]
	if !ok {
		leak := NewType3Leak(adv, VNINotFound, ctx)
		if ctx.Repair {
			err := leak.Repair(ctx)
			if err != nil {
				log.Print(err)
			}
		}
		return leak, false
	}

	diag, ok := CheckType3Destination(adv, mzp.Infranet, ctx)
	if !ok {
		return diag, false
	}

	return nil, true

}
