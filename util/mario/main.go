package main

import (
	"log"
	"os"
	"text/tabwriter"

	"github.com/fatih/color"
	"github.com/spf13/cobra"
	"gitlab.com/mergetb/tech/cogs/pkg/common"
	"gitlab.com/mergetb/tech/cogs/pkg/fabric"
	"gitlab.com/mergetb/tech/cogs/pkg/runtime"
	"gitlab.com/mergetb/tech/gobble/pkg"
	"gitlab.com/mergetb/xir/lang/go/v0.2"
)

var (
	modelSrc = "/etc/cogs/tb-xir.json"
	tbmodel  *xir.Net
	tw       = tabwriter.NewWriter(os.Stdout, 0, 0, 4, ' ', 0)
	dryRun   = false
	router   = "localhost"
)

var (
	blue  = color.New(color.FgBlue).SprintFunc()
	black = color.New(color.FgHiBlack).SprintFunc()
	white = color.New(color.FgWhite).SprintFunc()
	red   = color.New(color.FgRed).SprintFunc()
)

func main() {

	log.SetFlags(0)

	root := &cobra.Command{
		Use:     "mario",
		Short:   "the plumber",
		Version: common.Version,
		PersistentPreRun: func(*cobra.Command, []string) {

			err := gobble.ApplyDefaultConfig()
			if err != nil {
				log.Fatal(err)
			}
			gobble.Cfg.Router = router

			mdl, err := xir.FromFile(modelSrc)
			if err != nil {
				log.Fatal(err)
			}

			tbmodel = mdl
			cfg := runtime.GetConfig(false)
			common.SetEtcdConfig(cfg.Etcd)

		},
	}
	root.PersistentFlags().StringVarP(
		&modelSrc, "model", "m", "/etc/cogs/tb-xir.json", "testbed model to use",
	)
	root.PersistentFlags().BoolVarP(
		&dryRun, "dry-run", "d", false, "don't actually do anything",
	)
	root.PersistentFlags().StringVarP(
		&router, "router", "r", "localhost", "GoBGP router to use",
	)

	// infranet ---------------------------------------------------------------

	infranet := &cobra.Command{
		Use:   "infranet",
		Short: "attend to infranet plumbing",
	}
	root.AddCommand(infranet)

	var repair bool
	icheck := &cobra.Command{
		Use:   "check",
		Short: "check infranet pipes",
		Run: func(*cobra.Command, []string) {
			checkInfraPipes(repair)
		},
	}
	icheck.Flags().BoolVarP(
		&repair, "repair", "R", false, "repair issues identified")
	infranet.AddCommand(icheck)

	iLinkplan := &cobra.Command{
		Use:   "linkplan",
		Short: "linkplan management",
	}
	infranet.AddCommand(iLinkplan)

	fixInfranetLinkplans := &cobra.Command{
		Use:   "fix",
		Short: "fix infranet linkplans caused by cogs bug",
		Run: func(*cobra.Command, []string) {
			fixInfranetLinkplans()
		},
	}
	iLinkplan.AddCommand(fixInfranetLinkplans)

	var all bool
	var target string
	execInfranetLinkplan := &cobra.Command{
		Use:   "exec",
		Short: "execute a link plan for an mzid",
		Run: func(cmd *cobra.Command, args []string) {

			if len(args) == 0 && !all {
				log.Fatal("must provide mzid or --all flag")
			}
			if all {
				rebuildLinkPlans("", fabric.InfraLP, target)
			} else {
				rebuildLinkPlans(args[0], fabric.InfraLP, target)
			}

		},
	}
	execInfranetLinkplan.Flags().BoolVarP(
		&all, "all", "a", false, "all active mzids",
	)
	execInfranetLinkplan.Flags().StringVarP(
		&target, "target", "t", "", "target a specific switch",
	)
	iLinkplan.AddCommand(execInfranetLinkplan)

	// xpnet ------------------------------------------------------------------

	xpnet := &cobra.Command{
		Use:   "xpnet",
		Short: "attend to xpnet plumbing",
	}
	root.AddCommand(xpnet)

	xcheck := &cobra.Command{
		Use:   "check",
		Short: "check experiment pipes",
		Run: func(*cobra.Command, []string) {
			checkXpPipes(repair)
		},
	}
	xcheck.Flags().BoolVarP(
		&repair, "repair", "R", false, "repair issues identified")
	xpnet.AddCommand(xcheck)

	xLinkplan := &cobra.Command{
		Use:   "linkplan",
		Short: "linkplan management",
	}
	xpnet.AddCommand(xLinkplan)

	fixXpLinkplans := &cobra.Command{
		Use:   "fix",
		Short: "fix linkplans caused by cogs bug",
		Run: func(*cobra.Command, []string) {
			fixXpLinkplans()
		},
	}
	xLinkplan.AddCommand(fixXpLinkplans)

	execXpLinkplan := &cobra.Command{
		Use:   "exec [mzid]",
		Short: "execute a link plan for an mzid",
		Run: func(cmd *cobra.Command, args []string) {

			if len(args) == 0 && !all {
				log.Fatal("must provide mzid or --all flag")
			}
			if all {
				rebuildLinkPlans("", fabric.ExperimentLP, target)
			} else {
				rebuildLinkPlans(args[0], fabric.ExperimentLP, target)
			}

		},
	}
	execXpLinkplan.Flags().BoolVarP(
		&all, "all", "a", false, "all active mzids",
	)
	execXpLinkplan.Flags().StringVarP(
		&target, "target", "t", "", "target a specific switch",
	)
	xLinkplan.AddCommand(execXpLinkplan)

	readv := &cobra.Command{
		Use:   "readv [mzid]",
		Short: "readvertise EVPN routes",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {

			xreadv(args[0])

		},
	}
	xpnet.AddCommand(readv)

	// exec -------------------------------------------------------------------

	err := root.Execute()
	if err != nil {
		log.Print(err)
	}

}
