package main

import (
	"fmt"
	"log"
	"strconv"
	"strings"
	"time"

	"github.com/golang/protobuf/ptypes"
	"github.com/osrg/gobgp/api"
	"github.com/osrg/gobgp/pkg/packet/bgp"
	"gitlab.com/mergetb/tech/canopy/lib"
	"gitlab.com/mergetb/tech/cogs/pkg/fabric"
	"gitlab.com/mergetb/tech/cogs/pkg/runtime"
	"gitlab.com/mergetb/tech/cogs/pkg/task"
	"gitlab.com/mergetb/tech/gobble/pkg"
	"gitlab.com/mergetb/xir/lang/go/v0.2"
	"gitlab.com/mergetb/xir/lang/go/v0.2/tb"
)

type Diagnostic interface {
	Show(c *Context)
}

type Diagnostics struct {
	Type3  []Diagnostic
	Type2  []Diagnostic
	Access []Diagnostic
	Trunks []Diagnostic
	Vteps  []Diagnostic
}

type Pipeworks struct {
	Type3Adv    []gobble.Type3EvpnRoute
	Type2Adv    []gobble.Type2EvpnRoute
	AccessPorts map[string]map[string]int
	TrunkPorts  map[string]map[string][]int
	Vteps       map[string]map[string]int
}

type Pipeplans struct {
	ByMzid map[string]*MzPlans
	ByVNI  map[int]*MzPlans
	ByXVNI map[int]*MzPlans
	ByVID  map[int]*MzPlans
}

type MzPlans struct {
	MzInfo   *task.MzInfo
	Infranet *fabric.LinkPlan
	Xpnet    *fabric.LinkPlan
}

type Type3Info struct {
	DestinationHost string
	Source          string
	Destination     string
	Age             string
	VNI             int
	Path            []string
	SourceASN       int
}

type Type2Info struct {
	Mac         string
	Source      string
	VNI         int
	Destination string
	Age         string
	Node        string
	Path        []string
	Labels      []int
	SourceASN   int
}

type Leak uint16

const (
	Unknown Leak = iota
	VNINotFound
	UnknownRouter
	UnexpectedDestination
	NoLabels
	TooManyLabels
	NodeNotFound
	NoNodeStatus
	UnexpectedMzid
	VLANNotFound
)

func (l Leak) String() string {
	switch l {
	case Unknown:
		return "?"
	case VNINotFound:
		return "VNI not found"
	case UnknownRouter:
		return "Unknown router"
	case UnexpectedDestination:
		return "Unexpected destination"
	case NoLabels:
		return "No VNI Labels"
	case TooManyLabels:
		return "Too Many Labels"
	case NodeNotFound:
		return "Node not found"
	case NoNodeStatus:
		return "Node status not found"
	case UnexpectedMzid:
		return "Unexpected mzid"
	case VLANNotFound:
		return "VLAN not found"

	}

	return "?"
}

type Type3Leak struct {
	Info Type3Info
	Leak Leak
}

type Type2Leak struct {
	Info Type2Info
	Leak Leak
}

type Context struct {
	RouterIpIndex   map[string]*tb.Resource
	RouterASNIndex  map[int]*tb.Resource
	NodeMACIndex    map[string]*tb.Resource
	NodeStatusIndex map[string]*task.NodeState
	Repair          bool
}

func NewContext(routerRoles ...tb.Role) *Context {

	c := new(Context)
	c.buildNodeIndex()
	c.buildRouterIndex(routerRoles...)

	return c

}

func checkAccess(
	access map[string]map[string]int,
	pp *Pipeplans,
	ctx *Context,
	ntwk NetSelect,

) []Diagnostic {

	var ds []Diagnostic

	for host, ifxs := range access {

		for ifx, tag := range ifxs {

			ok := findAccess(host, ifx, tag, pp, ntwk)
			if !ok {
				leak := NewAccessLeak(host, ifx, tag, VLANNotFound)
				if ctx.Repair {
					err := leak.Repair()
					if err != nil {
						log.Print(err)
					}
				}
				ds = append(ds, leak)
			}

		}
	}

	return ds
}

func checkTrunks(
	trunk map[string]map[string][]int,
	pp *Pipeplans,
	ctx *Context,
	ntwk NetSelect,
) []Diagnostic {

	var ds []Diagnostic

	for host, ifxs := range trunk {

		for ifx, tags := range ifxs {
			for _, tag := range tags {

				ok := findTrunk(host, ifx, tag, pp, ntwk)
				if !ok {
					leak := NewTrunkLeak(host, ifx, []int{tag}, VLANNotFound)
					if ctx.Repair {
						err := leak.Repair()
						if err != nil {
							log.Print(err)
						}
					}
					ds = append(ds, leak)
				}

			}
		}
	}

	return ds
}

func Xpnet(mzp *MzPlans) *fabric.LinkPlan {
	return mzp.Xpnet
}

func Infranet(mzp *MzPlans) *fabric.LinkPlan {
	return mzp.Infranet
}

type NetSelect func(*MzPlans) *fabric.LinkPlan

func findAccess(
	host, ifx string, tag int, pp *Pipeplans, ntwk NetSelect) bool {

	if tag <= 2 {
		return true
	}

	for _, mzp := range pp.ByMzid {
		ports, ok := ntwk(mzp).AccessMap[host]
		if !ok {
			continue
		}

		port, ok := ports[ifx]
		if !ok {
			continue
		}

		if port.Vid == tag {
			return true
		}
	}

	return false

}

func findTrunk(host, ifx string, tag int, pp *Pipeplans, ntwk NetSelect) bool {

	if tag <= 2 {
		return true
	}

	for _, mzp := range pp.ByMzid {
		ports, ok := ntwk(mzp).TrunkMap[host]
		if !ok {
			continue
		}

		for pifx, port := range ports {

			if pifx != ifx && port.Name != ifx {
				continue
			}

			for _, y := range port.Vids {
				if tag == y {
					return true
				}
			}

		}
	}

	return false

}

func readPipes(role ...tb.Role) (*Pipeworks, error) {

	bgp, err := gobble.ReadBgp()
	if err != nil {
		return nil, err
	}

	access, trunk, vxlan, err := readCanopy(role...)
	if err != nil {
		return nil, err
	}

	return &Pipeworks{
		Type3Adv:    bgp.Type3Routes,
		Type2Adv:    bgp.Type2Routes,
		AccessPorts: access,
		TrunkPorts:  trunk,
		Vteps:       vxlan,
	}, nil

}

func readPlans() (*Pipeplans, error) {

	mzs, err := task.ListMaterializations("")
	if err != nil {
		log.Fatal(err)
	}

	pp := NewPipeplans()

	for _, mzi := range mzs {

		xlp, err := fabric.FetchLinkPlan(mzi.Mzid, fabric.ExperimentLP)
		if err != nil {
			log.Printf("cannt read xp link plan for %s, skipping", mzi.Mzid)
			continue
		}

		ilp, err := fabric.FetchLinkPlan(mzi.Mzid, fabric.InfraLP)
		if err != nil {
			log.Printf("cannt read infra link plan for %s, skipping", mzi.Mzid)
			continue
		}

		mzp := &MzPlans{
			MzInfo:   mzi,
			Xpnet:    xlp,
			Infranet: ilp,
		}

		pp.ByMzid[mzi.Mzid] = mzp
		pp.ByVNI[mzi.Vni] = mzp

		for _, vteps := range xlp.VtepMap {
			for _, vtep := range vteps {
				pp.ByXVNI[int(vtep.Vni)] = mzp
			}
		}

	}

	return pp, nil

}

func NewPipeplans() *Pipeplans {

	return &Pipeplans{
		ByMzid: make(map[string]*MzPlans),
		ByVNI:  make(map[int]*MzPlans),
		ByXVNI: make(map[int]*MzPlans),
		ByVID:  make(map[int]*MzPlans),
	}

}

func CheckType3Destination(
	adv gobble.Type3EvpnRoute, lp *fabric.LinkPlan, ctx *Context,
) (Diagnostic, bool) {

	//TODO this is done in many places, should just do up front on all routes
	info := ExtractType3Info(adv, ctx)

	router, ok := ctx.RouterIpIndex[info.Destination]
	if !ok {
		leak := NewType3Leak(adv, UnknownRouter, ctx)
		if ctx.Repair {
			err := leak.Repair(ctx)
			if err != nil {
				log.Print(err)
			}
		}
		return leak, false
	}

	// TODO Infrapod hosts expected on infranet networks, but are not currently
	// tracked by LinkPlans. Infrapod hosts should be included in LinkPlans,
	// remove this logic once they are.
	if router.HasRole(tb.InfrapodServer) {
		return nil, true
	}

	// get the list of planned VTEPs on the destination router
	vteps, ok := lp.VtepMap[router.System.Name]
	if !ok {
		leak := NewType3Leak(adv, UnexpectedDestination, ctx)
		if ctx.Repair {
			err := leak.Repair(ctx)
			if err != nil {
				log.Print(err)
			}
		}
		return leak, false
	}

	// check that the advertisement VNI is resident on one of the planned VTEPs
	for _, vtep := range vteps {
		if vtep.Vni == int(adv.CommunityLocalAdmin) {
			return nil, true
		}
	}

	return NewType3Leak(adv, UnexpectedDestination, ctx), false

}

func NewType3Leak(
	adv gobble.Type3EvpnRoute, leak Leak, ctx *Context) *Type3Leak {

	return &Type3Leak{
		Info: ExtractType3Info(adv, ctx),
		Leak: leak,
	}

}

func NewType2Leak(
	adv gobble.Type2EvpnRoute,
	leak Leak,
	ctx *Context,
) *Type2Leak {

	return &Type2Leak{
		Info: ExtractType2Info(adv, ctx),
		Leak: leak,
	}

}

type AccessLeak struct {
	Host string
	Ifx  string
	Tag  int
	Leak Leak
}

type TrunkLeak struct {
	Host string
	Ifx  string
	Tags []int
	Leak Leak
}

type VtepLeak struct {
	Host string
	Vtep string
	Leak Leak
}

func NewAccessLeak(host, ifx string, tag int, leak Leak) *AccessLeak {

	return &AccessLeak{
		Host: host,
		Ifx:  ifx,
		Tag:  tag,
		Leak: leak,
	}

}

func NewTrunkLeak(host, ifx string, tags []int, leak Leak) *TrunkLeak {

	return &TrunkLeak{
		Host: host,
		Ifx:  ifx,
		Tags: tags,
		Leak: leak,
	}

}

func NewVtepLeak(host string, vni int, leak Leak, ctx *Context) *VtepLeak {

	return &VtepLeak{
		Host: host,
		Vtep: fmt.Sprintf("vtep%d", vni),
		Leak: leak,
	}

}

func (d Diagnostics) Show(c *Context) {

	if len(d.Type3) > 0 {

		fmt.Fprintf(tw,
			"%s\t%s\t%s\t%s\t%s\t%s\n",
			black("TYPE-3 LEAK"),
			black("SOURCE"),
			black("DESTINATION"),
			black("AGE"),
			black("VNI"),
			black("PATH"),
		)

		for _, x := range d.Type3 {
			x.Show(c)
		}
		tw.Flush()

	}

	if len(d.Type2) > 0 {

		fmt.Fprintf(tw,
			"%s\t%s\t%s\t%s\t%s\t%s\t%s\n",
			black("TYPE-2 LEAK"),
			black("NODE"),
			black("SOURCE"),
			black("DESTINATION"),
			black("AGE"),
			black("VNI"),
			black("PATH"),
		)

		for _, x := range d.Type2 {
			x.Show(c)
		}
		tw.Flush()

	}

	if len(d.Access) > 0 {

		fmt.Fprintf(tw,
			"%s\t%s\t%s\t%s\n",
			black("ACCESS LEAK"),
			black("HOST"),
			black("PORT"),
			black("TAG"),
		)

		for _, x := range d.Access {
			x.Show(c)
		}
		tw.Flush()

	}

	if len(d.Trunks) > 0 {

		fmt.Fprintf(tw,
			"%s\t%s\t%s\t%s\n",
			black("TRUNK LEAK"),
			black("HOST"),
			black("PORT"),
			black("TAGS"),
		)

		for _, x := range d.Trunks {
			x.Show(c)
		}
		tw.Flush()

	}

	if len(d.Vteps) > 0 {

		fmt.Fprintf(tw,
			"%s\t%s\t%s\n",
			black("VTEP LEAK"),
			black("HOST"),
			black("PORT"),
		)

		for _, x := range d.Vteps {
			x.Show(c)
		}
		tw.Flush()

	}

}

func (l *Type3Leak) Show(c *Context) {

	fmt.Fprintf(tw,
		"%s\t%s\t%s\t%s\t%s\t%s\n",
		red(l.Leak),
		blue(c.RouterNameByIp(l.Info.Source)),
		blue(c.RouterNameByIp(l.Info.Destination)),
		white(l.Info.Age),
		white(l.Info.VNI),
		blue(strings.Join(l.Info.Path, " ")),
	)

}

func (l *Type3Leak) Repair(c *Context) error {

	_, _, n := tbmodel.GetNode(c.RouterNameByIp(l.Info.Source))
	if n == nil {
		return fmt.Errorf("cannot fix leak for %s: not found", l.Info.Source)
	}

	rtr := tb.GetResourceSpec(n)
	if rtr == nil {
		return fmt.Errorf("resource not found for %s", l.Info.Source)
	}

	log.Printf("removing %v %v %v %v %v",
		l.Info.Destination+"/32",
		router,
		l.Info.SourceASN,
		l.Info.VNI,
		l.Info.VNI,
	)

	return runtime.EvpnWithdrawMulticast(
		l.Info.Destination+"/32",
		router,
		l.Info.SourceASN,
		l.Info.VNI,
		l.Info.VNI,
	)

}

func (l *Type2Leak) Show(c *Context) {

	fmt.Fprintf(tw,
		"%s\t%s\t%s\t%s\t%s\t%s\t%s\n",
		red(l.Leak),
		blue(l.Info.Node),
		blue(c.RouterNameByIp(l.Info.Source)),
		blue(c.RouterNameByIp(l.Info.Destination)),
		white(l.Info.Age),
		white(l.Info.Labels),
		blue(strings.Join(l.Info.Path, " ")),
	)

}

func (l *Type2Leak) Repair(c *Context) error {

	_, _, n := tbmodel.GetNode(c.RouterNameByIp(l.Info.Source))
	if n == nil {
		return fmt.Errorf("cannot fix leak for %s: not found", l.Info.Source)
	}

	rtr := tb.GetResourceSpec(n)
	if rtr == nil {
		return fmt.Errorf("resource not found for %s", l.Info.Source)
	}

	return runtime.EvpnWithdrawMac(
		l.Info.Destination+"/32",
		router,
		l.Info.Mac,
		l.Info.SourceASN,
		l.Info.VNI,
		l.Info.VNI,
	)

}

func (l *VtepLeak) Show(c *Context) {

	fmt.Fprintf(tw,
		"%s\t%s\t%s\n",
		red(l.Leak),
		blue(l.Host),
		white(l.Vtep),
	)

}

func (l *VtepLeak) Repair(c *Context) error {

	cc := canopy.NewClient()

	cc.SetVtep(
		&canopy.VtepPresence{
			Presence: canopy.Presence_Absent,
		},
		l.Vtep,
		l.Host,
	)

	return cc.Commit()
}

func (l *AccessLeak) Show(c *Context) {

	fmt.Fprintf(tw,
		"%s\t%s\t%s\t%s\n",
		red(l.Leak),
		blue(l.Host),
		white(l.Ifx),
		white(l.Tag),
	)

}

func (l *AccessLeak) Repair() error {

	cc := canopy.NewClient()

	cc.SetAccessPort(
		&canopy.Access{
			Presence: canopy.Presence_Absent,
			Vid:      int32(l.Tag),
		},
		l.Ifx,
		l.Host,
	)

	return cc.Commit()

}

func (l *TrunkLeak) Show(c *Context) {

	fmt.Fprintf(tw,
		"%s\t%s\t%s\t%s\n",
		red(l.Leak),
		blue(l.Host),
		white(l.Ifx),
		white(l.Tags),
	)

}

func (l *TrunkLeak) Repair() error {

	cc := canopy.NewClient()

	var vids []int32
	for _, x := range l.Tags {
		vids = append(vids, int32(x))
	}

	cc.SetTaggedPort(
		&canopy.Tagged{
			Presence: canopy.Presence_Absent,
			Vids:     vids,
		},
		l.Ifx,
		l.Host,
	)

	return cc.Commit()

}

func ExtractType2Info(adv gobble.Type2EvpnRoute, ctx *Context) Type2Info {

	src := adv.Path.SourceId
	if src == "<nil>" {
		src = router
	}

	info := Type2Info{
		VNI:       int(adv.Route.Labels[0]),
		Mac:       adv.Route.MacAddress,
		Node:      ctx.NodeNameByMac(adv.Route.MacAddress),
		Source:    src,
		Age:       PathAge(adv.Path).String(),
		Path:      PathInfo(adv.Path, ctx),
		SourceASN: int(adv.Path.SourceAsn),
	}

	if len(adv.NextHops) != 0 {
		info.Destination = adv.NextHops[0]
	}

	for _, x := range adv.Route.Labels {
		info.Labels = append(info.Labels, int(x))
	}

	return info

}

func ExtractType3Info(adv gobble.Type3EvpnRoute, ctx *Context) Type3Info {

	src := adv.Path.SourceId
	if src == "<nil>" {
		src = router
	}

	return Type3Info{
		VNI:         int(adv.CommunityLocalAdmin),
		Source:      src,
		Destination: adv.Route.IpAddress,
		Age:         PathAge(adv.Path).String(),
		Path:        PathInfo(adv.Path, ctx),
		SourceASN:   RTAS(adv.Path),
	}

}

func PathAge(gpath *gobgpapi.Path) time.Duration {

	t, _ := ptypes.Timestamp(gpath.Age)
	return time.Duration(time.Now().Unix()-t.Unix()) * time.Second

}

func PathInfo(gpath *gobgpapi.Path, ctx *Context) []string {

	attrs, _ := runtime.GetNativePathAttributes(gpath)

	var path []string
	for _, attr := range attrs {
		switch a := attr.(type) {
		case *bgp.PathAttributeAsPath:
			for _, x := range a.Value {
				for _, y := range x.GetAS() {
					name := ctx.RouterNameByASN(int(y))
					path = append(path, name)
				}
			}
		}
	}
	return path

}

func RTAS(gpath *gobgpapi.Path) int {

	attrs, _ := runtime.GetNativePathAttributes(gpath)

	var rtas int
	for _, attr := range attrs {
		switch a := attr.(type) {
		case *bgp.PathAttributeExtendedCommunities:
			for _, x := range a.Value {
				_, st := x.GetTypes()
				if st == bgp.EC_SUBTYPE_ROUTE_TARGET {
					rt := x.String()
					parts := strings.Split(rt, ":")
					if len(parts) == 2 {
						rtas, _ = strconv.Atoi(parts[0])
					}
				}
			}
		}
	}

	return rtas

}

func (c *Context) NodeNameByMac(mac string) string {

	r, ok := c.NodeMACIndex[mac]
	if !ok {
		return mac
	}

	return r.System.Name

}

func (c *Context) RouterNameByIp(ip string) string {

	r, ok := c.RouterIpIndex[ip]
	if !ok {
		return ip
	}

	return r.System.Name

}

func (c *Context) RouterNameByASN(asn int) string {

	r, ok := c.RouterASNIndex[asn]
	if !ok {
		return fmt.Sprintf("%d", asn)
	}

	return r.System.Name

}

func (c *Context) buildRouterIndex(roles ...tb.Role) {

	c.RouterIpIndex = make(map[string]*tb.Resource)
	c.RouterASNIndex = make(map[int]*tb.Resource)

	filter := func(x *xir.Node) bool {
		return tb.HasRole(x, roles...)
	}

	for _, x := range tbmodel.Select(filter) {

		r := tb.GetResourceSpec(x)
		if r == nil {
			continue
		}

		if r.System == nil {
			continue
		}
		if r.System.OS == nil {
			continue
		}
		if r.System.OS.Config == nil {
			continue
		}
		if r.System.OS.Config.Evpn == nil {
			continue
		}

		c.RouterIpIndex[r.System.OS.Config.Evpn.TunnelIP] = r
		c.RouterASNIndex[r.System.OS.Config.Evpn.ASN] = r

	}

}

func (c *Context) buildNodeIndex() {

	c.buildNodeMACIndex()
	c.buildNodeMzidIndex()

}

func (c *Context) buildNodeMzidIndex() {

	c.NodeStatusIndex = make(map[string]*task.NodeState)

	nodes, err := task.ListNodes()
	if err != nil {
		log.Fatal(err)
	}

	for _, x := range nodes {
		c.NodeStatusIndex[x.Name] = x
	}

}

func (c *Context) buildNodeMACIndex() {

	c.NodeMACIndex = make(map[string]*tb.Resource)

	filter := func(x *xir.Node) bool {
		return tb.HasRole(x, tb.Node)
	}

	for _, x := range tbmodel.Select(filter) {

		r := tb.GetResourceSpec(x)
		if r == nil {
			continue
		}

		for _, x := range x.Endpoints {
			mac, ok := x.Props["mac"].(string)
			if !ok {
				continue
			}
			c.NodeMACIndex[strings.ToLower(mac)] = r
		}

	}

}

func readCanopy(role ...tb.Role) (
	map[string]map[string]int,
	map[string]map[string][]int,
	map[string]map[string]int,
	error,
) {

	filter := func(x *xir.Node) bool {
		return tb.HasRole(x, role...)
	}

	var hosts []string
	for _, x := range tbmodel.Select(filter) {

		hosts = append(hosts, x.Label())

	}

	elementMap, err := canopy.ListPorts(hosts)
	if err != nil {
		return nil, nil, nil, err
	}

	access := make(map[string]map[string]int)
	trunk := make(map[string]map[string][]int)
	vxlan := make(map[string]map[string]int)

	for host, elements := range elementMap {

		for _, e := range elements {

			switch p := e.Value.(type) {

			case *canopy.Element_Port:

				if p.Port.Access > 1 {

					_, ok := access[host]
					if !ok {
						access[host] = make(map[string]int)
					}
					access[host][e.Name] = int(p.Port.Access)

				} else if len(p.Port.Tagged) > 0 {

					_, ok := trunk[host]
					if !ok {
						trunk[host] = make(map[string][]int)
					}
					var tagged []int
					for _, t := range p.Port.Tagged {
						tagged = append(tagged, int(t))
					}
					trunk[host][e.Name] = tagged

				}

			case *canopy.Element_Vtep:

				if p.Vtep.Vni > 2 {

					_, ok := vxlan[host]
					if !ok {
						vxlan[host] = make(map[string]int)
					}
					vxlan[host][e.Name] = int(p.Vtep.Vni)

				}

			}
		}
	}

	return access, trunk, vxlan, nil

}

func rebuildLinkPlans(mzid string, role fabric.LinkPlanRole, target string) {

	if mzid == "" {

		mzs, err := task.ListMaterializations("")
		if err != nil {
			log.Fatal(err)
		}

		for _, mz := range mzs {
			rebuildLinkplan(mz.Mzid, role, target)
		}

	} else {

		rebuildLinkplan(mzid, role, target)

	}

}

func rebuildLinkplan(mzid string, role fabric.LinkPlanRole, target string) {

	lp, err := fabric.FetchLinkPlan(mzid, role)
	if err != nil {
		log.Fatal(err)
	}

	if target == "" {
		err = lp.Create().Exec()
	} else {
		err = lp.Create().ExecFor(target)
	}

	if err != nil {
		log.Fatal(err)
	}

}
