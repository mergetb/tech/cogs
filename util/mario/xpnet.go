package main

import (
	"fmt"
	"log"

	"gitlab.com/mergetb/tech/cogs/pkg/common"
	"gitlab.com/mergetb/tech/cogs/pkg/fabric"
	"gitlab.com/mergetb/tech/cogs/pkg/runtime"
	"gitlab.com/mergetb/tech/cogs/pkg/task"
	"gitlab.com/mergetb/tech/gobble/pkg"
	"gitlab.com/mergetb/xir/lang/go/v0.2/tb"
)

func checkXpPipes(repair bool) {

	ctx := NewContext(
		tb.Fabric,
		tb.Spine,
		tb.NetworkEmulator,
	)
	ctx.Repair = repair

	var diags Diagnostics

	current, err := readPipes(tb.XpSwitch, tb.NetworkEmulator)
	if err != nil {
		log.Fatal(err)
	}

	planned, err := readPlans()
	if err != nil {
		log.Fatal(err)
	}

	for _, x := range current.Type3Adv {

		diag, ok := XpCheckType3(x, planned, ctx)
		if !ok {
			diags.Type3 = append(diags.Type3, diag)
		}

	}

	for _, x := range current.Type2Adv {

		diag, ok := XpCheckType2(x, planned, ctx)
		if !ok {
			diags.Type2 = append(diags.Type2, diag)
		}

	}

	for host, vteps := range current.Vteps {
		for _, vni := range vteps {

			diag, ok := XpCheckVtep(vni, host, planned, ctx)
			if !ok {
				diags.Vteps = append(diags.Vteps, diag)
			}

		}
	}

	diags.Access = append(
		diags.Access, checkAccess(current.AccessPorts, planned, ctx, Xpnet)...,
	)

	diags.Trunks = append(
		diags.Trunks, checkTrunks(current.TrunkPorts, planned, ctx, Xpnet)...,
	)

	diags.Show(ctx)

}

func XpCheckVtep(
	vni int, host string, pp *Pipeplans, ctx *Context) (Diagnostic, bool) {

	_, ok := pp.ByXVNI[vni]
	if !ok {
		leak := NewVtepLeak(host, vni, VNINotFound, ctx)
		if ctx.Repair {
			err := leak.Repair(ctx)
			if err != nil {
				log.Print(err)
			}
		}
		return leak, false
	}

	return nil, true

}

func XpCheckType3(
	adv gobble.Type3EvpnRoute, pp *Pipeplans, ctx *Context) (Diagnostic, bool) {

	vni := adv.CommunityLocalAdmin
	if vni == 2 {
		return nil, true
	}

	// ensure VNI is a thing
	_, ok := pp.ByXVNI[int(vni)]
	if !ok {
		leak := NewType3Leak(adv, VNINotFound, ctx)
		if ctx.Repair {
			err := leak.Repair(ctx)
			if err != nil {
				log.Print(err)
			}
		}
		return leak, false
	}

	// TODO may cause damage
	/*
		diag, ok := CheckType3Destination(adv, mzp.Xpnet, ctx)
		if !ok {
			return diag, false
		}
	*/

	return nil, true

}

func XpCheckType2(
	adv gobble.Type2EvpnRoute, pp *Pipeplans, ctx *Context) (Diagnostic, bool) {

	if len(adv.Route.Labels) == 0 {
		return NewType2Leak(adv, NoLabels, ctx), false
	}
	if len(adv.Route.Labels) > 1 {
		return NewType2Leak(adv, TooManyLabels, ctx), false
	}
	vni := int(adv.Route.Labels[0])

	if vni == 2 {
		return nil, true
	}

	// ensure VNI is a thing
	_, ok := pp.ByXVNI[int(vni)]
	if !ok {
		leak := NewType2Leak(adv, VNINotFound, ctx)
		if ctx.Repair {
			err := leak.Repair(ctx)
			if err != nil {
				log.Print(err)
			}
		}
		return leak, false
	}

	info := ExtractType2Info(adv, ctx)

	_, ok = ctx.RouterIpIndex[info.Destination]
	if !ok {
		return NewType2Leak(adv, UnknownRouter, ctx), false
	}

	return nil, true
}

func fixXpLinkplans() {

	mzs, err := task.ListMaterializations("")
	if err != nil {
		log.Fatal(err)
	}

	for _, mz := range mzs {

		fixXpLinkPlan(mz.Mzid)

	}
}

func fixXpLinkPlan(mzid string) {

	// fetch the list of linkplans for this mzid

	links, err := task.ListLinkInfos(mzid)
	if err != nil {
		log.Fatal(err)
	}

	// get the emulator info

	emu, evpnConf, parent, err := runtime.EmuInfo()
	if err != nil {
		log.Fatal(err)
	}

	// getch the linkplan to fix

	lp, err := fabric.FetchLinkPlan(mzid, fabric.ExperimentLP)
	if err != nil {
		log.Fatal(err)
	}

	for _, link := range links {

		// only fixing moa links

		if link.Kind != task.MoaLink {
			continue
		}

		for _, vlink := range link.TBVLinks {

			ifx := fmt.Sprintf("vtep%d", vlink.Vni)

			if lp.VtepMap == nil {
				lp.VtepMap = make(fabric.VtepMap)
			}

			lp.VtepMap.Add(emu.System.Name, ifx, &fabric.Vtep{
				Name:     ifx,
				Vni:      vlink.Vni,
				TunnelIP: evpnConf.TunnelIP,
				XpID:     link.XpID,
				Mtu:      runtime.GetConfig(false).Net.VtepMtu,
				Device:   parent,
			})

		}

	}

	err = lp.Save()
	if err != nil {
		log.Fatal(err)
	}
}

func xreadv(mzid string) {

	links, err := task.ListLinkInfos(mzid)
	if err != nil {
		log.Fatal(err)
	}

	topo := common.TbModel()

	_, _, emu := topo.GetNode(runtime.DefaultEmulator)
	if emu == nil {
		log.Fatalf("%s - not a thing", runtime.DefaultEmulator)
	}
	r := tb.GetResourceSpec(emu)
	if r == nil {
		log.Fatalf("%s - not a resource", runtime.DefaultEmulator)
	}

	for _, linkInfo := range links {

		if linkInfo.Kind == task.MoaLink {

			err := runtime.AdvertiseMoaLink(
				linkInfo,
				r.System.OS.Config.Evpn.TunnelIP+"/32",
				r.System.OS.Config.Evpn.ASN,
			)
			if err != nil {
				log.Printf("Failed to advertise moa link: %v", err)
			}

		}

	}

}
