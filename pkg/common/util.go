package common

import (
	"fmt"
	"gitlab.com/mergetb/xir/lang/go/v0.2"
)

const (
	xirpath = "/etc/cogs/tb-xir.json"
)

var (
	__tbxir *xir.Net
)

// GetIntLike is needed for data that gets cross-serialized and the number
// format is uncertain. This function works for both float64 and int underlying
// data types.
func GetIntLike(x interface{}) (int, error) {

	i, ok := x.(int)
	if !ok {
		_i, ok := x.(float64)
		if !ok {
			return -1, fmt.Errorf("not int like'%#v %T'", x, x)
		}
		i = int(_i)
	}
	return i, nil

}

//TODO get from MinIO, and watch for changes
func TbModel() *xir.Net {

	if __tbxir == nil {
		tb, err := xir.FromFile(xirpath)
		if err != nil {
			panic(err)
		}
		__tbxir = tb
	}

	return __tbxir

}
