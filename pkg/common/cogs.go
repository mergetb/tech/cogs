package common

// Version contains the cogs version string
var Version = "undefined"

// MaxMessageSize for both grpc and etcd communications for
// driver and rex communications
var MaxMessageSize = 512 * 1024 * 1024
