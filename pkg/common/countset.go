package common

import "fmt"

// A CountSet is a dense counter. When the counter is incremented the lowest
// unused index is returned. When an index is deleted, it's available for use
// again.
type CountSet struct {
	Name   string `json:"name"`
	Size   int    `json:"size,omitempty"`
	Values []int  `json:"values"`
	Offset int    `json:"offset"`

	ver int64
}

// Next returns the nex available index.
func (cs CountSet) Next() (int, int) {

	i := cs.Offset
	for j, x := range cs.Values {
		if x != i {
			return i, j
		}
		i++
	}
	return i, len(cs.Values)

}

// Add increments the counter and returns the created index value.
func (cs CountSet) Add() (int, CountSet, error) {

	if len(cs.Values) == cs.Size {
		return -1, cs, fmt.Errorf("overflow")
	}
	i, j := cs.Next()
	cs.Values = append(cs.Values[:j], append([]int{i}, cs.Values[j:]...)...)
	return i, cs, nil

}

// Remove removes an index from the counter, freeing it for future use.
func (cs CountSet) Remove(i int) CountSet {
	for j, x := range cs.Values {
		if i == x {
			var tail []int
			if j < len(cs.Values)-1 {
				tail = cs.Values[j+1:]
			}
			cs.Values = append(cs.Values[:j], tail...)
			return cs
		}
	}
	return cs
}

// Key returns the datastore key for this object.
func (c *CountSet) Key() string { return "/counters/" + c.Name }

// GetVersion returns the current datastore version of the object
func (c *CountSet) GetVersion() int64 { return c.ver }

// SetVersion sets the current datastore version of the object
func (c *CountSet) SetVersion(v int64) { c.ver = v }

// Value returns this object as an interface{}
func (c *CountSet) Value() interface{} { return c }
