package common

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"strings"
	"time"

	etcd "github.com/coreos/etcd/clientv3"
	"github.com/golang/protobuf/jsonpb"
	"github.com/golang/protobuf/proto"
	log "github.com/sirupsen/logrus"
)

// Object defines an interface for cogs datastore objects.
type Object interface {
	Key() string
	GetVersion() int64
	SetVersion(int64)
	Value() interface{}
}

// ErrNotFound means an object was not found.
var ErrNotFound = fmt.Errorf("not found")

// Read reads an object from the datastore
func Read(obj Object) error {
	n, err := ReadObjects([]Object{obj})
	if err != nil {
		return err
	}
	if n == 0 {
		return ErrNotFound
	}
	return nil
}

// ReadNew reads an object form the datastore, and does not throw an error if
// the object is not found.
func ReadNew(obj Object) error {

	err := Read(obj)
	if err != nil && err != ErrNotFound {
		return err
	}

	return nil

}

// ReadTimer tracks timing information about a read.
type ReadTimer struct {
	Period  time.Duration
	Timeout time.Duration
}

//ReadWait attempts to read an object repeatedly until a timeout threshold is
//reached defined by timer. If timer is nil the defaults of 30 seconds with a
//retry period of 250 milliseconds is applied
func ReadWait(obj Object, timer *ReadTimer) error {

	if timer == nil {
		timer = &ReadTimer{
			Period:  250 * time.Millisecond,
			Timeout: 30 * time.Second,
		}
	}

	count := 0
	var err error
	for err = Read(obj); err == ErrNotFound; err = Read(obj) {

		time.Sleep(timer.Period)
		count++
		if time.Duration(count)*timer.Period > timer.Timeout {
			break
		}

	}

	return err

}

// ReadWaitObjects is readwait for many objects
// we check that the number of reads is equal to the number
// of objects (which is what ErrNotFound was doing anyway
func ReadWaitObjects(objs []Object, timer *ReadTimer) error {
	if timer == nil {
		timer = &ReadTimer{
			Period:  250 * time.Millisecond,
			Timeout: 30 * time.Second,
		}
	}

	count := 0
	n, err := ReadObjects(objs)

	for n != len(objs) {
		time.Sleep(timer.Period)
		count++
		if time.Duration(count)*timer.Period > timer.Timeout {
			break
		}
		n, err = ReadObjects(objs)
	}

	return err
}

// FromJSON reads reads on object from byte array encoded json. If the object is
// a protobuf, then protobuf is used instead.
func FromJSON(o Object, b []byte) {

	// if this is a protobuf, unmarshal as such
	msg, ok := o.Value().(proto.Message)
	if ok {
		log.Trace("from: as protobuf")
		err := jsonpb.Unmarshal(bytes.NewReader(b), msg)
		if err == nil {
			return
		}
		WarnEF("fail to unmarshal once", err, log.Fields{"object": b})
		//fallthrough to json
	}

	err := json.Unmarshal(b, o.Value())
	if err != nil {
		panic(err) //all merge objects must be json serializable, the end
	}

}

// ReadObjects reads a set of objects from the datastore in a one-shot
// transaction.
func ReadObjects(objs []Object) (int, error) {

	var ops []etcd.Op
	omap := make(map[string]Object)

	names := make([]string, 0)
	for _, o := range objs {
		names = append(names, o.Key())
		omap[o.Key()] = o
		ops = append(ops, etcd.OpGet(o.Key()))
	}

	n := 0
	objsize := 0
	err := WithEtcd(func(c *etcd.Client) error {

		kvc := etcd.NewKV(c)
		ctx, cancel := context.WithTimeout(context.TODO(), 1*time.Minute)
		resp, err := kvc.Txn(ctx).Then(ops...).Commit()
		cancel()
		if err != nil {
			return err
		}
		if !resp.Succeeded {
			return TxnFailed("")
		}

		for _, r := range resp.Responses {
			rr := r.GetResponseRange()
			if rr == nil {
				continue
			}

			for _, kv := range rr.Kvs {
				n++
				o := omap[string(kv.Key)]

				switch t := o.Value().(type) {
				case *string:
					*t = string(kv.Value)
				default:
					FromJSON(o, kv.Value)
					o.SetVersion(kv.Version)
				}
				objsize += len([]byte(kv.Value))
			}
		}

		return nil

	})
	if err != nil {
		return 0, ErrorE("failed to read", err)
	}
	log.Tracef("Read (%v) Size: %d", names, objsize)

	return n, err

}

// ToJSON marshals an object to JSON form. If the object is a protobuf, protobuf
// is used instead.
func ToJSON(o Object) string {

	// if this is a protobuf, marshal as such
	msg, ok := o.Value().(proto.Message)
	if ok {
		log.Trace("from: as protobuf")
		var buf bytes.Buffer
		m := jsonpb.Marshaler{}
		err := m.Marshal(&buf, msg)
		if err == nil {
			return buf.String()
		}
		WarnEF("failed to pbmarshal", err, log.Fields{"object": o})
		//fallthrough to json
	}

	buf, err := json.MarshalIndent(o.Value(), "", "  ")
	if err != nil {
		panic(err) //all merge objects must be json serializable, the end
	}
	return string(buf)
}

// Write persists an object to the datastore.
func Write(obj Object, opts ...etcd.OpOption) error {
	return WriteObjects([]Object{obj}, false, opts...)
}

// WriteObjects writes objects to the datastore in a single shot transaction. If
// fresh is true, then all objects must be the most recent version, or the write
// will fail.
func WriteObjects(objs []Object, fresh bool, opts ...etcd.OpOption) error {

	var ops []etcd.Op
	var ifs []etcd.Cmp

	names := make([]string, 0)
	objsize := 0
	for _, obj := range objs {
		names = append(names, obj.Key())

		var value string
		switch t := obj.Value().(type) {
		case *string:
			value = *t
		default:
			value = ToJSON(obj)
		}
		objsize += len([]byte(value))

		ops = append(ops, etcd.OpPut(obj.Key(), value, opts...))
		if fresh {
			ifs = append(ifs,
				etcd.Compare(etcd.Version(obj.Key()), "=", obj.GetVersion()))
		}

	}

	log.Tracef("Write (%v) Size: %d", names, objsize)

	return WithEtcd(func(c *etcd.Client) error {
		kvc := etcd.NewKV(c)
		if kvc == nil {
			log.Error("failed to create etcd client")
			return fmt.Errorf("failed to create etcd client")
		}

		ctx, cancel := context.WithTimeout(context.TODO(), 1*time.Minute)
		resp, err := kvc.Txn(ctx).If(ifs...).Then(ops...).Commit()
		cancel()
		if err != nil {
			return err
		}
		if !resp.Succeeded {
			return TxnFailed("state has changed since read")
		}

		for _, o := range objs {
			o.SetVersion(o.GetVersion() + 1)
		}
		return nil
	})
}

// Touch update the key, but not value in data store
func Touch(obj Object) error {
	return TouchObjects([]Object{obj})
}

// TouchObjects updates multiple keys
func TouchObjects(objs []Object) error {

	var ops []etcd.Op
	var ifs []etcd.Cmp

	names := make([]string, 0)
	objsize := 0
	for _, obj := range objs {
		names = append(names, obj.Key())

		ops = append(ops, etcd.OpPut(obj.Key(), "", etcd.WithIgnoreValue()))
	}

	log.Tracef("Write (%v) Size: %d", names, objsize)

	return WithEtcd(func(c *etcd.Client) error {

		kvc := etcd.NewKV(c)
		if kvc == nil {
			log.Error("failed to create etcd client")
			return fmt.Errorf("failed to create etcd client")
		}

		ctx, cancel := context.WithTimeout(context.TODO(), 1*time.Minute)
		resp, err := kvc.Txn(ctx).If(ifs...).Then(ops...).Commit()
		cancel()
		if err != nil {
			return err
		}
		if !resp.Succeeded {
			return TxnFailed("state has changed since read")
		}
		return nil
	})
}

// DeleteObjects deletes a set of objects from the datastore.
func DeleteObjects(objs []Object) error {

	var ops []etcd.Op

	for _, obj := range objs {
		ops = append(ops, etcd.OpDelete(obj.Key()))
	}

	return WithEtcd(func(c *etcd.Client) error {
		kvc := etcd.NewKV(c)
		ctx, cancel := context.WithTimeout(context.TODO(), 1*time.Minute)
		resp, err := kvc.Txn(ctx).Then(ops...).Commit()
		cancel()
		if err != nil {
			return err
		}
		if !resp.Succeeded {
			return TxnFailed("delete objects failed")
		}
		return nil
	})
}

// Delete deletes an object from the datastore.
func Delete(obj Object) error {

	return DeleteObjects([]Object{obj})

}

// ObjectTx encapsulates a set of put and delete operations into a single
// transaction.
type ObjectTx struct {
	Put    []Object
	Delete []Object
}

// RunObjectTx runs an object transaction.
func RunObjectTx(otx ObjectTx) error {

	var ops []etcd.Op

	names := make([]string, 0)
	objsize := 0
	for _, x := range otx.Put {
		names = append(names, x.Key())

		var value string
		switch t := x.Value().(type) {
		case *string:
			value = *t
		default:
			buf, err := json.MarshalIndent(x.Value(), "", "  ")
			if err != nil {
				return err
			}
			value = string(buf)
		}
		objsize += len([]byte(value))

		ops = append(ops, etcd.OpPut(x.Key(), string(value)))
	}

	log.Tracef("WriteTx (%v) Size: %d", names, objsize)
	names = make([]string, 0)
	for _, x := range otx.Delete {
		names = append(names, x.Key())
		ops = append(ops, etcd.OpDelete(x.Key()))
	}
	log.Tracef("DeleteTx: (%v)", names)

	return WithEtcd(func(c *etcd.Client) error {
		kvc := etcd.NewKV(c)
		ctx, cancel := context.WithTimeout(context.TODO(), 1*time.Minute)
		resp, err := kvc.Txn(ctx).Then(ops...).Commit()
		cancel()
		if err != nil {
			return err
		}
		if !resp.Succeeded {
			return TxnFailed("run object txn failed")
		}
		return nil
	})
}

const (
	// TxnFailedPrefix is the prefix message for all transaction failure errors.
	TxnFailedPrefix = "txn failed"
)

// TxnFailed an error message for transaction failures with the proper
// formatting.
func TxnFailed(message string) error {
	err := fmt.Errorf("%s: %s", TxnFailedPrefix, message)
	log.Trace(err)
	return err
}

// IsTxnFailed detects if an error is a transaction failure error.
func IsTxnFailed(err error) bool {
	return strings.HasPrefix(err.Error(), TxnFailedPrefix)
}

// RunObjectTxPrefix a bastaradization of RunObjectTx, making it so
// put is still object array, but delete is a prefix for the txn
func RunObjectTxPrefix(puts []Object, deletePrefix string) error {

	if deletePrefix == "" {
		return fmt.Errorf("attempted to txn delete db")
	}

	var ops []etcd.Op
	names := make([]string, 0)
	objsize := 0
	// put as in RunObjectTx
	for _, x := range puts {
		names = append(names, x.Key())
		var value string
		switch t := x.Value().(type) {
		case *string:
			value = *t
		default:
			buf, err := json.MarshalIndent(x.Value(), "", "  ")
			if err != nil {
				return err
			}
			value = string(buf)
		}
		objsize += len([]byte(value))
		ops = append(ops, etcd.OpPut(x.Key(), string(value)))
	}

	// delete on prefix
	ops = append(ops, etcd.OpDelete(deletePrefix, etcd.WithPrefix()))
	log.Tracef("PutTx: (%v)", names)
	log.Tracef("DeletePrefixTx: (%v)", deletePrefix)

	return WithEtcd(func(c *etcd.Client) error {
		kvc := etcd.NewKV(c)
		ctx, cancel := context.WithTimeout(context.TODO(), 1*time.Minute)
		resp, err := kvc.Txn(ctx).Then(ops...).Commit()
		cancel()
		if err != nil {
			return err
		}
		if !resp.Succeeded {
			return TxnFailed("run object txn failed")
		}
		return nil
	})
}

// ReadRevision reads an object, and returns the etcd key revision for that object
func ReadRevision(obj Object) (revision int64, err error) {
	objsize := 0
	revision = 0
	err = WithEtcd(func(c *etcd.Client) error {
		kvc := etcd.NewKV(c)
		ctx, cancel := context.WithTimeout(context.TODO(), 1*time.Minute)
		resp, err := kvc.Get(ctx, obj.Key())
		cancel()
		if err != nil {
			return err
		}
		if len(resp.Kvs) > 0 {
			kv := resp.Kvs[0]

			FromJSON(obj, kv.Value)
			obj.SetVersion(kv.Version)
			objsize += len([]byte(kv.Value))
			revision = resp.Header.Revision
		} else {
			return ErrNotFound
		}
		return nil
	})
	if err != nil {
		return 0, ErrorE("failed to read", err)
	}
	log.Tracef("ReadRev (%v) Size: %d", obj.Key(), objsize)

	return revision, err
}
