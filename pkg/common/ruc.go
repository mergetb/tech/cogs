package common

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * RUC: Read Update Commit
 * =======================
 *
 *   This file contains code for safely updating complex etcd stor objects
 * concurrently. The basic pattern is that each function takes
 *
 *    - an object to be updates
 *    - a function that updates the object
 *
 * the RUC routine then does the following
 *
 *    - try to perform a versioned update on the object
 *    - if the update fails, try up to 10 times
 *    - returns the result of the transactional update
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

const (
	rucRetry = 10
)

// RUC performs a read-update-commit on the provided object using the specified
// update function.
func RUC(o Object, update func(o Object)) error {

	for i := 0; i < rucRetry; i++ {

		update(o)

		err := WriteObjects([]Object{o}, true)
		if err == nil {
			return nil
		}
		if IsTxnFailed(err) {
			err = Read(o)
			if err != nil {
				return err
			}
			continue
		}
		return err

	}

	return TxnFailed("unable to complete transaction")

}
