package common

import (
	"fmt"
)

// TLSConfig defines a TLS configuration for communicating with a service.
type TLSConfig struct {
	Cacert string
	Cert   string
	Key    string
}

// ServiceConfig encapsulates information for communicating with services.
type ServiceConfig struct {
	Address string
	Port    int
	TLS     *TLSConfig
}

type AlertConfig struct {
	Address string
	Port int
	Username string
	Password string
	Type string
}

// Endpoint returns the endpoint string of a service config.
func (s *ServiceConfig) Endpoint() string {
	return fmt.Sprintf("%s:%d", s.Address, s.Port)
}
