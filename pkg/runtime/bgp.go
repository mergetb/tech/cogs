package runtime

import (
	"context"
	"flag"
	"fmt"
	"net"
	"time"

	api "github.com/osrg/gobgp/api"
	"github.com/osrg/gobgp/pkg/packet/bgp"
	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/tech/cogs/pkg/common"
	"google.golang.org/grpc"
)

const (
	BgpSubnetMask = "/32"
)

var (
	//TODO just read from gobble/gobgp config?
	GobgpdPort = flag.Int("gobgpd-port", 50051, "gobgpd listening port")
)

// EvpnWithdrawData encapsulates information necessary to send an EVPN withdraw
// request
type EvpnWithdrawData struct {
	Asn    int    `yaml:",omitempty"`
	Addr   string `yaml:",omitempty"`
	Vindex int    `yaml:",omitempty"`
	Vni    int    `yaml:",omitempty"`
}

func initBgp() {

	cfg := GetConfig(false)

	bgpAddr := cfg.Net.ServiceTunnelIP + "/32"

	// get the address being advertised
	ip, mask, err := net.ParseCIDR(bgpAddr)
	if ip == nil || err != nil {
		log.
			WithFields(log.Fields{"address": bgpAddr}).
			Fatal("bgp address is invalid, expecting cidr e.g. 1.2.3.4/24")
	}

	// get the address subnet
	ones, _ := mask.Mask.Size()
	nlri := bgp.NewIPAddrPrefix(uint8(ones), ip.String())

	// apply origin and nexthop attributes
	attrs := []bgp.PathAttributeInterface{
		bgp.NewPathAttributeOrigin(bgp.BGP_ORIGIN_ATTR_TYPE_IGP),
		bgp.NewPathAttributeNextHop("0.0.0.0"),
	}

	// send msg to gobgp
	err = withGoBgp("localhost", func(client api.GobgpApiClient) error {

		_, err := client.AddPath(context.TODO(), &api.AddPathRequest{
			TableType: api.TableType_GLOBAL,
			Path:      gobgpNewPath(nlri, false, attrs, time.Now()),
		})

		return err

	})
	if err != nil {
		log.WithError(err).Fatal("failed to init bgp")
	}

}

// EvpnAdvertiseMac advertises the specified MAC on the specified VNI and
// updates associted vindex counters.
func EvpnAdvertiseMac(addr, router, mac string, asn, vni, vindex int) error {
	return evpnUpdateMac(addr, router, mac, asn, vni, vindex, false)
}

// EvpnWithdrawMac withdraws the specified MAC on the specified VNI and updates
// associated vindex counters.
func EvpnWithdrawMac(addr, router, mac string, asn, vni, vindex int) error {
	return evpnUpdateMac(addr, router, mac, asn, vni, vindex, true)
}

func evpnUpdateMac(addr, router, mac string, asn, vni, vindex int, withdraw bool) error {

	info, err := evpnAdvertiseInfo(addr, asn, vni, vindex)
	if err != nil {
		return err
	}

	macaddr, err := net.ParseMAC(mac)
	if err != nil && mac != "" {
		return err
	}

	r := &bgp.EVPNMacIPAdvertisementRoute{
		RD:               info.Rd,
		MacAddressLength: 48,
		MacAddress:       macaddr,
		IPAddressLength:  uint8(net.IPv4len * 8),
		IPAddress:        nil,
		Labels:           []uint32{uint32(vni)},
		ETag:             uint32(0),
	}

	nlri := bgp.NewEVPNNLRI(bgp.EVPN_ROUTE_TYPE_MAC_IP_ADVERTISEMENT, r)

	extcomms := []bgp.ExtendedCommunityInterface{
		bgp.NewEncapExtended(bgp.TUNNEL_TYPE_VXLAN),
		info.Rt,
	}

	// apply origin and nexthop attributes
	attrs := []bgp.PathAttributeInterface{
		bgp.NewPathAttributeOrigin(bgp.BGP_ORIGIN_ATTR_TYPE_IGP),
		bgp.NewPathAttributeNextHop(info.BgpIP.String()),
		bgp.NewPathAttributeExtendedCommunities(extcomms),
	}

	if withdraw {
		return evpnDoWithdraw(router, nlri, attrs)
	}
	return evpnDoAdvertise(router, nlri, attrs)

}

// EvpnInfo encapsulates commonly used EVPN advertisement info.
type EvpnInfo struct {
	BgpIP net.IP
	Rd    bgp.RouteDistinguisherInterface
	Rt    bgp.ExtendedCommunityInterface
}

func evpnAdvertiseInfo(addr string, asn, vni, vindex int) (*EvpnInfo, error) {

	//bip, _, err := net.ParseCIDR(*BgpAddr)
	bip, _, err := net.ParseCIDR(addr)
	if err != nil {
		return nil, common.ErrorEF(
			"failed to parse bgp-ip", err, log.Fields{"ip": addr})
	}

	rd, err := bgp.ParseRouteDistinguisher(
		fmt.Sprintf("%s:%d", bip.String(), vindex),
	)
	if err != nil {
		return nil, common.ErrorE("failed to parse route distinguisher", err)
	}

	rt, err := bgp.ParseRouteTarget(fmt.Sprintf("%d:%d", asn, vni))
	if err != nil {
		return nil, common.ErrorE("failed to parse route target", err)
	}

	return &EvpnInfo{
		BgpIP: bip,
		Rd:    rd,
		Rt:    rt,
	}, nil

}

func evpnDoAdvertise(
	gobgpAddr string,
	nlri bgp.AddrPrefixInterface,
	attrs []bgp.PathAttributeInterface) error {

	return withGoBgp(gobgpAddr, func(client api.GobgpApiClient) error {

		_, err := client.AddPath(context.TODO(), &api.AddPathRequest{
			TableType: api.TableType_GLOBAL,
			VrfId:     "",
			Path:      gobgpNewPath(nlri, false, attrs, time.Now()),
		})

		if err != nil {
			return common.ErrorE("failed to add evpn multicast path", err)
		}
		return nil

	})

}

func evpnDoWithdraw(
	gobgpAddr string,
	nlri bgp.AddrPrefixInterface,
	attrs []bgp.PathAttributeInterface) error {

	return withGoBgp(gobgpAddr, func(client api.GobgpApiClient) error {

		_, err := client.DeletePath(context.TODO(), &api.DeletePathRequest{
			TableType: api.TableType_GLOBAL,
			Family: &api.Family{
				Afi:  api.Family_AFI_L2VPN,
				Safi: api.Family_SAFI_EVPN,
			},
			Path: gobgpNewPath(nlri, false, attrs, time.Now()),
		})

		if err != nil {
			return common.ErrorE("failed to add evpn multicast path", err)
		}
		return nil

	})

}

// EvpnAdvertiseMulticast sends out a type-3 multicast EVPN advertisement. The
// advertisement is sent to a GoBGP instance at the hostname specified by the
// router parameter. The addr parameter specifies the IP address to advertise,
// asn is the autonomous system number, vni is the VXLAN virtual network
// identifier and vindex in the Cogs virtual network index id.
func EvpnAdvertiseMulticast(addr, router string, asn, vni, vindex int) error {

	return evpnUpdateMulticast(addr, router, asn, vni, vindex, false)

}

// EvpnWithdrawMulticast sends out a type-3 multicast EVPN withdraw
func EvpnWithdrawMulticast(addr, router string, asn, vni, vindex int) error {

	return evpnUpdateMulticast(addr, router, asn, vni, vindex, true)

}

func evpnUpdateMulticast(
	addr, router string, asn, vni, vindex int, withdraw bool) error {

	info, err := evpnAdvertiseInfo(addr, asn, vni, vindex)
	if err != nil {
		return err
	}

	r := &bgp.EVPNMulticastEthernetTagRoute{
		RD:              info.Rd,
		IPAddressLength: uint8(net.IPv4len * 8),
		IPAddress:       info.BgpIP,
		ETag:            uint32(0),
	}

	nlri := bgp.NewEVPNNLRI(bgp.EVPN_INCLUSIVE_MULTICAST_ETHERNET_TAG, r)

	extcomms := []bgp.ExtendedCommunityInterface{
		bgp.NewEncapExtended(bgp.TUNNEL_TYPE_VXLAN),
		info.Rt,
	}

	// apply origin and nexthop attributes
	attrs := []bgp.PathAttributeInterface{
		bgp.NewPathAttributeOrigin(bgp.BGP_ORIGIN_ATTR_TYPE_IGP),
		bgp.NewPathAttributeNextHop(info.BgpIP.String()),
		bgp.NewPathAttributeExtendedCommunities(extcomms),
		bgp.NewPathAttributePmsiTunnel(
			bgp.PMSI_TUNNEL_TYPE_INGRESS_REPL,
			false,
			uint32(vni),
			&bgp.IngressReplTunnelID{Value: info.BgpIP},
		),
	}

	if withdraw {
		return evpnDoWithdraw(router, nlri, attrs)
	}
	return evpnDoAdvertise(router, nlri, attrs)

}

func withGoBgp(address string, f func(api.GobgpApiClient) error) error {

	endpoint := fmt.Sprintf("%s:%d", address, *GobgpdPort)
	conn, err := grpc.Dial(endpoint, grpc.WithInsecure())
	if err != nil {
		log.WithError(err).Error("failed to connect to GoBGP")
		return err
	}
	defer conn.Close()

	client := api.NewGobgpApiClient(conn)
	return f(client)
}
