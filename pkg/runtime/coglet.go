package runtime

import (
	"context"
	"encoding/json"
	"time"

	"github.com/coreos/etcd/clientv3"
	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/tech/cogs/pkg/common"
)

// CogletInfo contains bookkeeping information about a coglet replica.
type CogletInfo struct {
	ID   string
	Cog  string
	Host string
	Pid  int

	version int64
}

// storage object implementation

// Key returns the datastore key for this coglet's info
func (c *CogletInfo) Key() string { return "/coglet/" + c.ID }

// GetVersion gets the datastore replica version information
func (c *CogletInfo) GetVersion() int64 { return c.version }

// SetVersion sets the datastore replica version information
func (c *CogletInfo) SetVersion(v int64) { c.version = v }

// Value returns this coglet as an interface object
func (c *CogletInfo) Value() interface{} { return c }

// ListCoglets lists the coglets currently registered with the cogs datastore
func ListCoglets() ([]*CogletInfo, error) {

	var infos []*CogletInfo
	objsize := 0
	err := common.WithEtcd(func(c *clientv3.Client) error {

		ctx, cancel := context.WithTimeout(context.TODO(), 3*time.Second)
		resp, err := c.Get(ctx, "/coglet", clientv3.WithPrefix())
		cancel()
		if err != nil {
			return err
		}

		for _, kv := range resp.Kvs {
			objsize += len([]byte(kv.Value))
			ci, err := extractCoglet(string(kv.Value))
			if err != nil {
				log.WithError(err).WithFields(log.Fields{
					"id": string(kv.Key),
				}).Warn("failed to read coglet")
				continue
			}
			infos = append(infos, ci)
		}
		return nil
	})

	if err != nil {
		return nil, nil
	}

	log.Debugf("Coglet prefix Size: %d", objsize)
	return infos, nil
}

func extractCoglet(cogJSON string) (*CogletInfo, error) {
	ci := &CogletInfo{}
	err := json.Unmarshal([]byte(cogJSON), ci)
	if err != nil {
		return nil, err
	}
	return ci, nil
}
