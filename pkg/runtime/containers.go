package runtime

import (
	"context"
	"flag"
	"fmt"
	"net"
	"os"
	"os/exec"
	"strings"
	"time"

	"gitlab.com/mergetb/tech/cogs/pkg/common"
	"gitlab.com/mergetb/tech/cogs/pkg/task"

	"github.com/containerd/containerd"
	"github.com/containerd/containerd/cio"
	"github.com/containerd/containerd/containers"
	"github.com/containerd/containerd/images"
	"github.com/containerd/containerd/namespaces"
	"github.com/containerd/containerd/oci"
	"github.com/opencontainers/runtime-spec/specs-go"
	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/tech/rtnl"
	"golang.org/x/sys/unix"
	"syscall"
)

var (
	//TODO put in config instead
	svcbrAddr = flag.String(
		"svcip", "172.31.0.1", "control IP for service bridge, assumed /16")
)

// Container Query ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// ListContainers lists the containers in the specified context.
func ListContainers(ctx context.Context) ([]containerd.Container, error) {

	client, err := ContainerdClient()
	if err != nil {
		return nil, err
	}
	defer client.Close()

	//ctx := namespaces.WithNamespace(context.TODO(), namespace)
	return client.Containers(ctx)

}

// ListNamespaces lists current namespace names.
func ListNamespaces() ([]string, error) {

	client, err := ContainerdClient()
	if err != nil {
		return nil, err
	}
	defer client.Close()

	return client.NamespaceService().List(context.TODO())

}

// Container Creation ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// SetupContainerLo sets up a loopback network interface in the specified
// network namespace.
func SetupContainerLo(namespace string) error {

	ctx, err := rtnl.OpenContext(namespace)
	if err != nil {
		return common.ErrorE("open context", err)
	}
	defer ctx.Close()

	return setupLo(ctx)

}

// PullContainerImage pulls the specified container image into the specified
// namespace.
func PullContainerImage(namespace, image string) error {

	client, err := ContainerdClient()
	if err != nil {
		return common.ErrorE("containerd client", err)
	}

	ctx := namespaces.WithNamespace(context.TODO(), namespace)
	_, err = client.Pull(ctx, image, containerd.WithPullUnpack)
	if err != nil {
		return common.ErrorE("pull image", err)
	}

	return nil

}

// CreateContainer creates a container based on the provided specification.
func CreateContainer(ctr *task.ContainerSpec) error {

	client, err := ContainerdClient()
	if err != nil {
		return common.ErrorE("containerd client", err)
	}

	fields := log.Fields{
		"ctrNs": ctr.Namespace,
		"image": ctr.Image,
	}

	ctx := namespaces.WithNamespace(context.TODO(), ctr.Namespace)

	img, err := client.GetImage(ctx, ctr.Image)
	if err != nil {
		return common.ErrorEF("get image", err, fields)
	}

	err = waitForNetns(ctr.Namespace)
	if err != nil {
		return common.ErrorEF("failed to wait for netns", err, fields)
	}

	// kill off any dangling containers
	err = DeleteContainerTask(ctr.Name, ctr.Namespace)
	if err != nil {
		//best effort
		log.WithFields(fields).Warn("failed to delete lingering contianer task")
	}

	err = DeleteContainer(ctr.Name, ctr.Namespace)
	if err != nil {
		log.WithFields(fields).Warn("failed to delete lingering container")
	}

	err = DeleteContainerImg(ctr.Name, ctr.Namespace)
	if err != nil {
		log.WithFields(fields).Warn("failed to delete lingering container image")
	}

	sopts := []oci.SpecOpts{
		oci.WithImageConfig(img),
		oci.WithEnv(ctr.Env()),
		WithCogCNI(ctr.Namespace),
		WithCtrMounts(ctr.Mounts),
	}

	// create container
	_, err = client.NewContainer(
		ctx,
		ctr.Name,
		containerd.WithImage(img),
		containerd.WithNewSnapshot(fmt.Sprintf("%s-snapshot", ctr.Name), img),
		containerd.WithNewSpec(sopts...),
	)
	if err != nil {
		if strings.Contains(err.Error(), "already exists") {
			return nil
		}
		return common.ErrorE("new container", err)
	}

	return nil

}

// CreateContainerTask loads the container from the provided specification and
// creates a task for it.
func CreateContainerTask(ctr *task.ContainerSpec) error {

	client, err := ContainerdClient()
	if err != nil {
		return common.ErrorE("containerd client", err)
	}

	ctx := namespaces.WithNamespace(context.TODO(), ctr.Namespace)

	container, err := client.LoadContainer(ctx, ctr.Name)
	if err != nil {
		return common.ErrorE("load container", err)
	}

	task, err := container.Task(ctx, nil)
	if err != nil || task == nil {

		return CreateTask(ctx, container, ctr.Namespace, ctr.Name)

	}

	return nil

}

// CreateTask starts a task for the specified container, e.g. it runs the
// container.
func CreateTask(
	ctx context.Context, ctr containerd.Container, ns, name string) error {

	// ensure container logfile directory exists
	cnsDir := fmt.Sprintf("/var/log/containers/%s", ns)
	err := os.MkdirAll(cnsDir, 0755)
	if err != nil {
		return common.ErrorE("create container log dir", err)
	}

	// create a task object for the container
	logfile := fmt.Sprintf("%s/%s", cnsDir, name)
	task, err := ctr.NewTask(ctx, cio.LogFile(logfile))
	if err != nil {
		return common.ErrorE("new ctr task", err)
	}

	// start the container task
	err = task.Start(ctx)
	if err != nil {
		return common.ErrorE("failed to start ctr task", err)
	}

	// wait for task to actually start
	err = waitForProcessStart(ctx, task)
	if err != nil {
		return common.ErrorE("ctr task did not start", err)
	}

	return nil

}

func waitForProcessStart(ctx context.Context, task containerd.Task) error {

	for i := 0; i < 10; i++ {

		s, err := task.Status(ctx)
		if err != nil {
			return common.ErrorE("failed to get ctr task status", err)
		}

		if s.Status == containerd.Running {
			return nil
		}

		time.Sleep(250 * time.Millisecond)

	}

	return common.Error("ctr task failed to start")

}

// NetNS is a network namespace record that tracks containers in the netns.
type NetNS struct {
	Name       string
	Containers []string

	version int64
}

// Key returns the datastore key for this object.
func (x *NetNS) Key() string { return fmt.Sprintf("/netns/%s", x.Name) }

// GetVersion returns the current datastore version of the object
func (x *NetNS) GetVersion() int64 { return x.version }

// SetVersion sets the current datastore version of the object
func (x *NetNS) SetVersion(v int64) { x.version = v }

// Value returns this object as an interface{}
func (x *NetNS) Value() interface{} { return x }

// RemoveContainer removes a container from a netns object
func (x *NetNS) RemoveContainer(ctr string) {

	var ctrs []string

	for _, c := range x.Containers {
		if c != ctr {
			ctrs = append(ctrs, c)
		}
	}

	x.Containers = ctrs

}

// WithCtrMounts is an OCI specifier that can be used on container creation to
// plumb in the set of provided container mount points.
func WithCtrMounts(mounts []*task.ContainerMount) oci.SpecOpts {

	return func(
		ctx context.Context,
		client oci.Client,
		c *containers.Container,
		spec *oci.Spec,
	) error {

		for _, mount := range mounts {
			spec.Mounts = append(spec.Mounts, mount.Mount)
		}
		return nil

	}

}

// CreateContainerRecord stores a record of the provided container spec in the
// cogs datastore.
func CreateContainerRecord(spec *task.ContainerSpec) error {

	ctr := &spec.Container

	//TODO XXX dont save/read containers, just do what thet task says ....
	// read existing state if exists
	err := common.Read(ctr)
	if err != nil && err != common.ErrNotFound {
		return common.ErrorE("failed to read container entry", err)
	}

	// create network namespace entry
	netns := &NetNS{
		Name: ctr.Namespace,
	}
	// read existing state if exists
	err = common.Read(netns)
	if err != nil && err != common.ErrNotFound {
		return common.ErrorE("failed to read namespace entry", err)
	}

	// add container to the namespace
	netns.Containers = append(netns.Containers, ctr.Key())

	objs := []common.Object{ctr, netns}

	// save the container and namespace objects to db
	err = common.WriteObjects(objs, false)
	if err != nil {
		return common.ErrorE("failed to save container state", err)
	}

	return nil

}

// WithCogCNI is an OCI specification operator that binds a container to a
// network namespace.
func WithCogCNI(netns string) oci.SpecOpts {

	return func(
		ctx context.Context, client oci.Client, ctr *containers.Container,
		spec *oci.Spec,
	) error {

		// give container access to the network namespace for this materialization

		for i, x := range spec.Linux.Namespaces {
			if x.Type == "network" {
				spec.Linux.Namespaces[i] = specs.LinuxNamespace{
					Type: "network",
					Path: fmt.Sprintf("/var/run/netns/%s", netns),
				}
			}

		}

		return nil

	}

}

func waitForNetns(name string) error {

	var err error
	for i := 0; i < 10; i++ {

		_, err = os.Stat(fmt.Sprintf("/var/run/netns/%s", name))
		// the namespace exists, we're done here
		if err == nil {
			return nil
		}
		// namespace does not exists, wait and continue
		if os.IsNotExist(err) {
			time.Sleep(1 * time.Second)
			continue
		}
		// some other error that we can't handle here, kick it up the stack
		return err

	}

	return common.ErrorE("wait for netns failed", err)

}

// Container Destruction ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// DeleteContainer deletes a container.
func DeleteContainer(name, namespace string) error {

	ctx := namespaces.WithNamespace(context.TODO(), namespace)
	container, err := getContainer(namespace, name)
	if err != nil {
		if err.Error() == "not found" {
			return nil
		}
		return common.ErrorE("failed to get container", err)
	}

	err = container.Delete(ctx, containerd.WithSnapshotCleanup)
	if err != nil {
		return common.ErrorE("failed to delete container", err)
	}

	return nil

}

// DeleteContainerTask loads the container identified by the provided name /
// namespace combo and deletes its associated task.
func DeleteContainerTask(name, namespace string) error {

	fields := log.Fields{
		"namespace": namespace,
		"name":      name,
	}

	ctx := namespaces.WithNamespace(context.TODO(), namespace)
	container, err := getContainer(namespace, name)
	if err != nil {
		if err.Error() == "not found" {
			return nil
		}
		return common.ErrorEF("failed to get container", err, fields)
	}

	return DeleteTask(ctx, container)

}

// DeleteTask deletes a container's task.
func DeleteTask(ctx context.Context, container containerd.Container) error {

	task, err := container.Task(ctx, cio.Load)
	if err != nil {
		if strings.HasPrefix(err.Error(), "no running task found") {
			return nil
		}
		return fmt.Errorf("could not get task: %v", err)
	}

	exit, err := task.Wait(ctx)
	if err != nil {
		return fmt.Errorf("failed to wait on task: %v", err)
	}

	err = task.Kill(ctx, syscall.SIGKILL)
	if err != nil {
		log.Warnf("failed to kill task: %v", err)
	}

	status := <-exit
	code, _, err := status.Result()
	if err != nil {
		return fmt.Errorf("task exit failed: (%d) %v", code, err)
	}

	_, err = task.Delete(ctx)
	if err != nil {
		return fmt.Errorf("failed to delete task: %v", err)
	}

	return nil

}

// DeleteContainerIfx loads the virtual ethernet interface identified by name
// within the provided namespace and deletes it.
func DeleteContainerIfx(namespace, name string) error {

	fields := log.Fields{
		"ifx":       name,
		"namespace": namespace,
	}

	ctx, err := rtnl.OpenContext(namespace)
	if err != nil {
		return common.ErrorE("failed to open namepsace context", err)
	}
	defer ctx.Close()

	veth, err := rtnl.GetLink(ctx, name)
	if err != nil {
		if rtnl.IsNotFound(err) {
			return nil
		}
		return common.ErrorEF("failed to get container interface", err, fields)
	}

	err = veth.Del(ctx)
	if err != nil {
		return common.ErrorEF("failed to delete container interface", err, fields)
	}

	return nil

}

// DeleteContainerRecord locates a record for for the container identified by
// name in the provided namespace and removes it from the cogs data store.
func DeleteContainerRecord(name, namespace string) error {

	ctr := &task.Container{
		Namespace: namespace,
		Name:      name,
	}
	err := common.Read(ctr)
	if err != nil && err != common.ErrNotFound {
		return common.ErrorEF("failed to fetch container record", err, log.Fields{
			"service": namespace,
			"name":    name,
		})
	}

	// remove ctr from namespace
	netns := &NetNS{Name: namespace}
	err = common.Read(netns)
	if err != nil && err != common.ErrNotFound {
		return common.ErrorE("failed to fetch namespace record", err)
	}

	// delete the database entry and remove the container from the namespace
	netns.RemoveContainer(ctr.Key())

	tx := common.ObjectTx{
		Put:    []common.Object{netns},
		Delete: []common.Object{ctr},
	}

	err = common.RunObjectTx(tx)
	if err != nil {
		return common.ErrorE("failed to delete container from db", err)
	}

	return nil

}

// DeleteContainerNs deletes a container namespace.
func DeleteContainerNs(namespace string) error {

	c, err := ContainerdClient()
	if err != nil {
		return err
	}

	ctx := namespaces.WithNamespace(context.TODO(), namespace)

	containers, err := c.Containers(ctx)
	if err != nil {
		return common.ErrorE("failed to get namespace containers", err)
	}

	for _, ctr := range containers {

		img, err := ctr.Image(ctx)
		if err != nil {
			return common.ErrorE("failed to get container image", err)
		}

		err = DeleteTask(ctx, ctr)
		if err != nil {
			return err
		}

		err = ctr.Delete(ctx, containerd.WithSnapshotCleanup)
		if err != nil {
			return err
		}

		err = DeleteContainerImg(img.Name(), namespace)
		if err != nil {
			return err
		}

	}

	err = c.NamespaceService().Delete(context.TODO(), namespace)
	if err != nil {
		return common.ErrorE("failed to delete namespace", err)
	}

	return nil

}

// DeleteContainerImg deletes an image form a container namespace.
func DeleteContainerImg(name, namespace string) error {

	c, err := ContainerdClient()
	if err != nil {
		return err
	}

	ctx := namespaces.WithNamespace(context.TODO(), namespace)

	err = c.ImageService().Delete(ctx, name, images.SynchronousDelete())
	if err != nil {
		if strings.Contains(err.Error(), "not found") {
			return nil
		}
		return common.ErrorE("failed to delete image", err)
	}

	return nil

}

// DestroyNetNS deletes a network namespace.
func DestroyNetNS(netns *NetNS) error {

	netnsPath := fmt.Sprintf("/var/run/netns/%s", netns.Name)
	err := unix.Unmount(netnsPath, unix.MNT_DETACH)
	if err != nil {
		if err.Error() == "no such file or directory" {
			return nil
		}
		return common.ErrorE("failed to unmount netns", err)
	}
	err = unix.Unlink(netnsPath)
	if err != nil {
		return common.ErrorE("failed to unlink netns", err)
	}

	err = common.Delete(netns)
	if err != nil {
		return common.ErrorE("failed to delete netns db object", err)
	}

	return nil

}

// Helpers
// ============================================================================

var _client *containerd.Client

var (
	svcPrefix = net.IPv4Mask(255, 255, 0, 0)
	svcIP     net.IP
)

func initContainers() {

	svcIP = net.ParseIP(*svcbrAddr)
	if svcIP == nil {
		log.WithFields(log.Fields{"ip": *svcbrAddr}).Fatal("invalid service bridge IP")
	}

	initSvcbr()

}

func initSvcbr() {

	ctx, err := rtnl.OpenDefaultContext()
	if err != nil {
		log.WithError(err).Fatal("failed to open default context")
	}

	// ensure the svcbr exists
	svcbr := &rtnl.Link{
		Info: &rtnl.LinkInfo{
			Name:   "svcbr",
			Bridge: &rtnl.Bridge{},
		},
	}
	err = svcbr.Present(ctx)
	if err != nil {
		log.WithError(err).Fatal("failed to ensure svcbr is present")
	}

	addr := rtnl.NewAddress()
	addr.Info.Address = &net.IPNet{
		IP:   svcIP,
		Mask: svcPrefix,
	}
	err = svcbr.AddAddr(ctx, addr)
	if err != nil && !strings.Contains(err.Error(), "file exists") {
		log.WithError(err).Fatal("faled to add ip to svcbr")
	}

	if !svcbr.Info.Promisc {

		err = svcbr.Down(ctx)
		if err != nil {
			log.WithError(err).Fatal("failed to bring interface down to put in to promisc")
		}

		err = svcbr.Promisc(ctx, true)
		if err != nil {
			log.WithError(err).Fatal("failed to bring svcbr into promisc mode")
		}

	}

	err = svcbr.Up(ctx)
	if err != nil {
		log.WithError(err).Fatal("failed to bring svcbr up")
	}

}

// ContainerdClient connects to the local containerd socket interface, returning
// a client.
func ContainerdClient() (*containerd.Client, error) {

	if _client == nil {

		var err error
		_client, err = containerd.New("/run/containerd/containerd.sock")
		if err != nil {
			log.
				WithError(err).
				Error("failed to create containerd client")
			return nil, err
		}

	}

	return _client, nil

}

func getContainer(service, name string) (containerd.Container, error) {

	fields := log.Fields{
		"service": service,
		"op":      "get-container",
		"name":    name,
	}

	client, err := ContainerdClient()
	if err != nil {
		return nil, err
	}

	ctx := namespaces.WithNamespace(context.TODO(), service)
	containers, err := client.Containers(ctx)
	if err != nil {
		log.WithError(err).WithFields(fields).Error("failed to list containers")
		return nil, err
	}

	for _, c := range containers {
		if name == c.ID() {
			return c, nil
		}
	}

	return nil, common.ErrNotFound
}

func ensureMznNetworkNamespace(nsname string) (*NetNS, error) {
	fields := log.Fields{"namespace": nsname}
	netns := &NetNS{
		Name: nsname,
	}

	// make sure it doesnt exist before me add.
	del := exec.Command("ip", "netns", "delete", nsname)
	_ = del.Run()

	add := exec.Command("ip", "netns", "add", nsname)
	err := add.Run()
	if err != nil {
		return nil, common.ErrorEF("failed to create namespace ", err, fields)
	}
	return netns, nil
}

func destroyMznNetworkNamespace(nsname string) error {

	nspath := fmt.Sprintf("/var/run/netns/%s", nsname)
	err := unix.Unmount(nspath, unix.MNT_DETACH)
	if err != nil {
		log.Warn(err)
	}
	return unix.Unlink(nspath)

}

func setupLo(ctx *rtnl.Context) error {

	lo := &rtnl.Link{
		Info: &rtnl.LinkInfo{
			Name: "lo",
			Ns:   uint32(ctx.Fd()),
		},
	}
	err := lo.Read(ctx)
	if err != nil {
		log.WithError(err).Error("failed to get loopback device")
		return common.ErrorE("read lo", err)
	}

	addr, err := rtnl.ParseAddr("127.0.0.1/8")
	if err != nil {
		return common.ErrorE("parse localhost", err)
	}

	err = lo.AddAddr(ctx, addr)
	if err != nil && !strings.Contains(err.Error(), "file exists") {
		return common.ErrorE("add lo", err)
	}

	err = lo.Up(ctx)
	if err != nil {
		return common.ErrorE("lo up", err)
	}

	return nil

}
