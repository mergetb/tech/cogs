package runtime

import (
	"fmt"
	"io/ioutil"

	"github.com/mergetb/yaml/v3"
	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/tech/cogs/pkg/common"
	"gitlab.com/mergetb/tech/rtnl"
)

// SledConfig contains configurable sled items
type SledConfig struct {
	MatTimeout   int `yaml:"mattimeout"`
	DematTimeout int `yaml:"demattimeout"`
	ConnTimeout  int `yaml:"conntimeout"`
}

// Tuning contains tasks that can be tuned by a cogs administrator to tweak
// performance.
type Tuning struct {
	TaskLease int `yaml:"taskLease"`
}

// NetConfig contains the cogs networking configuration.
type NetConfig struct {
	VtepIfx         string `yaml:"vtepIfx,omitempty" json:"vtepIfx,omitempty"`
	Mtu             int    `yaml:"mtu,omitempty" json:"mtu,omitempty"`
	VtepMtu         int    `yaml:"vtepMtu,omitempty" json:"vtepMtu,omitempty"`
	ServiceTunnelIP string `yaml:"serviceTunnelIP,omitempty" json:"serviceTunnelIP,omitempty"`
	BgpAS           int    `yaml:"bgpAS,omitempty" json:"bgpAS,omitempty"`
	ExternalIfx     string `yaml:"externalIfx,omitempty" json:"externalIfx,omitempty"`
	ExternalIP      string `yaml:"externalIP,omitempty" json:"externalIP,omitempty"`
	ExternalSubnet  string `yaml:"externalSubnet,omitempty" json:"externalSubnet,omitempty"`
	ExternalGateway string `yaml:"externalGateway,omitempty" json:"externalGateway,omitempty"`
}

// Config is the top level configuration object for the cogs system.
type Config struct {
	Etcd   *common.ServiceConfig `yaml:",omitempty" json:",omitempty"`
	Beluga *common.ServiceConfig `yaml:",omitempty" json:",omitempty"`
	Tuning *Tuning               `yaml:",omitempty" json:",omitempty"`
	Net    *NetConfig            `yaml:",omitempty" json:",omitempty"`
	Sled   *SledConfig           `yaml:",omitempty" json:",omitempty"`
	Alerts []*common.AlertConfig `yaml:",omitempty" json:",omitempty"`
	Site string `yaml:",omitempty" json:",omitempty"`
}

// ConfigPath is the location of the runtime configuration file
var ConfigPath string

// GetConfig loads the cogs config from a well known file location.
func GetConfig(fullRuntime bool) *Config {

	if ConfigPath == "" {
		ConfigPath = "/etc/cogs/runtime.yml"
	}
	log.Debugf("Runtime configuration file: %s", ConfigPath)

	data, err := ioutil.ReadFile(ConfigPath)
	if err != nil {
		log.Fatalf(
			"could not read configuration file %s %v", ConfigPath, err)
	}

	cfg := &Config{}
	err = yaml.Unmarshal(data, cfg)
	if err != nil {
		log.Fatalf("could not parse configuration file %v", err)
	}

	log.WithFields(log.Fields{
		"config": fmt.Sprintf("%+v", *cfg),
	}).Debug("config")

	if fullRuntime {
		checkConfig(cfg)
	}

	return cfg

}

func checkConfig(cfg *Config) {

	determineExternalNetwork := false

	if cfg.Net == nil {
		log.Fatal("config: Net is not specefied - giving up")
	}

	if cfg.Net.ExternalIP == "" {
		log.Warn("config: ExternalIP not specified, determining dynamically")
		determineExternalNetwork = true
	}
	if cfg.Net.ExternalSubnet == "" {
		log.Warn(
			"config: ExternalSubnet not specified, determining dynamically")
		determineExternalNetwork = true
	}
	if cfg.Net.ExternalGateway == "" {
		log.Warn(
			"config: ExternalSubnet not specified, determining dynamically")
		determineExternalNetwork = true
	}

	if determineExternalNetwork {
		determineExternalNetworkConfig(cfg)
	}

}

func determineExternalNetworkConfig(cfg *Config) {

	if cfg.Net.ExternalIfx == "" {
		log.Fatal("config: ExternalInterface not specified, " +
			"cannot determine network parameters")
	}

	fields := log.Fields{
		"link": cfg.Net.ExternalIfx,
	}

	ctx, err := rtnl.OpenDefaultContext()
	if err != nil {
		log.Fatal(err)
	}

	lnk, err := rtnl.GetLink(ctx, cfg.Net.ExternalIfx)
	if err != nil {
		log.WithError(err).WithFields(fields).Fatal(
			"could not get specified interface")
	}

	addrs, err := lnk.Addrs(ctx)
	if err != nil {
		log.WithError(err).WithFields(fields).Fatal(
			"failed to read gateway addresses")
	}

	if len(addrs) == 0 {
		log.WithFields(fields).Fatal("specified interface has no address")
	}

	// Here we assume that the intended address is the first IPv4 addresss we
	// find
	addr := addrs[0].Info.Address
	cfg.Net.ExternalIP = addr.IP.String()
	cfg.Net.ExternalSubnet = addr.String()

	routes, err := rtnl.ReadRoutes(ctx, nil)
	if err != nil {
		log.WithError(err).Fatal("failed to read routes")
	}

	for _, route := range routes {
		if route.Gateway == nil {
			continue
		}

		if addr.Contains(route.Gateway) {
			cfg.Net.ExternalGateway = route.Gateway.String()
			break
		}
	}

	if cfg.Net.ExternalGateway == "" {
		log.WithFields(fields).Fatal("could not determine external gateway")
	}

	fields["net-config"] = *cfg.Net

	log.WithFields(log.Fields{
		"dev":     cfg.Net.ExternalIfx,
		"addr":    addr.String(),
		"gateway": cfg.Net.ExternalGateway,
	}).Info("successfully determined external network config")

}
