package runtime

import (
	"fmt"
	"net"
	"os/exec"

	log "github.com/sirupsen/logrus"
	"golang.org/x/sys/unix"

	"gitlab.com/mergetb/tech/cogs/pkg/common"
	"gitlab.com/mergetb/tech/cogs/pkg/task"
	"gitlab.com/mergetb/tech/rtnl"
)

// CreateIfx creates the specified virtual ethernet device in the provided
// netns.
func CreateIfx(netns string, ifx *task.EnclaveVeth, save bool) error {

	cfg := GetConfig(false)
	bgpAddr := cfg.Net.ServiceTunnelIP + "/32"
	bgpAS := cfg.Net.BgpAS

	defCtx, err := rtnl.OpenDefaultContext()
	if err != nil {
		return common.ErrorE("rtnl default context", err)
	}
	defer defCtx.Close()

	tgtCtx, err := rtnl.OpenContext(netns)
	if err != nil {
		return common.ErrorE("rtnl context", err)
	}
	defer tgtCtx.Close()

	ip, nw, err := net.ParseCIDR(ifx.Address)
	if err != nil {
		return common.ErrorE("parse cidr", err)
	}

	veth, _, err := ensureVethPair(netns, ifx.Inner, ifx.Outer, defCtx, tgtCtx, ip,
		nw.Mask)
	if err != nil {
		return common.ErrorE("ensure veth pair", err)
	}

	err = enslaveVeth(ifx.Outer, ifx.Bridge, ifx.Vid)
	if err != nil {
		return common.ErrorE("enslave veth", err)
	}

	if ifx.EvpnAdvertise {

		err = EvpnAdvertiseMulticast(bgpAddr, "localhost", bgpAS, ifx.Vni,
			ifx.Vindex)
		if err != nil {
			return common.ErrorE("advertise mcast", err)
		}

		err = EvpnAdvertiseMac(bgpAddr, "localhost", veth.Info.Address.String(),
			bgpAS, ifx.Vni, ifx.Vindex)
		if err != nil {
			return common.ErrorE("advertise mac", err)
		}

	}

	return nil

}

// DeleteIfx removes a virtual ethernet device from a network namespace.
func DeleteIfx(netns string, ifx *task.EnclaveVeth) error {

	cfg := GetConfig(false)
	bgpAddr := cfg.Net.ServiceTunnelIP + "/32"
	bgpAS := cfg.Net.BgpAS

	ctx, err := rtnl.OpenContext(netns)
	if err != nil {
		return common.ErrorE("rtnl default context", err)
	}
	defer ctx.Close()

	veth, err := rtnl.GetLink(ctx, ifx.Inner)
	if err == nil {

		if ifx.EvpnAdvertise {

			err = EvpnWithdrawMulticast(
				bgpAddr, "localhost", bgpAS, ifx.Vni, ifx.Vindex)
			if err != nil {
				return common.ErrorE("withdraw mcast", err)
			}

			err = EvpnWithdrawMac(
				bgpAddr,
				"localhost",
				veth.Info.Address.String(),
				bgpAS,
				ifx.Vni,
				ifx.Vindex,
			)
			if err != nil {
				return common.ErrorE("withdraw mac", err)
			}

		}

		err := veth.Absent(ctx)
		if err != nil {
			log.WithError(err).WithFields(log.Fields{
				"name": ifx.Outer,
			}).Warn("failed to delete veth")
		}

	}

	return nil

}

func ensureVethPair(
	mzid, ctrIfname, hostIfname string,
	defaultCtx, targetCtx *rtnl.Context,
	ip net.IP, prefix net.IPMask,
) (*rtnl.Link, *rtnl.Link, error) {

	log.WithFields(log.Fields{
		"ctr":  ctrIfname,
		"host": hostIfname,
	}).Printf("ensure veth pair")

	// clean out any existing host side veth pair
	dangling, err := rtnl.GetLink(defaultCtx, hostIfname)
	if err == nil {
		err = dangling.Absent(defaultCtx)
		if err != nil {
			log.Warn(err)
		}
	}

	// create eth<X> for the container

	defaultCtx.Target = targetCtx

	ctrIf := &rtnl.Link{
		Info: &rtnl.LinkInfo{
			Name: ctrIfname,
			Ns:   uint32(targetCtx.Fd()),
			Veth: &rtnl.Veth{
				Peer: hostIfname,
			},
		},
	}
	err = ctrIf.Present(defaultCtx)
	if err != nil {
		return nil, nil, common.ErrorE("failed to create container ifx", err)
	}

	ctrIf, err = rtnl.GetLink(targetCtx, ctrIfname)
	if err != nil {
		return nil, nil, common.ErrorE("failed read back ctrif", err)
	}

	err = ctrIf.SetMtu(targetCtx, GetConfig(false).Net.Mtu)
	if err != nil {
		return nil, nil, common.ErrorE("failed to set ctr-veth mtu", err)
	}

	err = ctrIf.Up(targetCtx)
	if err != nil {
		return nil, nil, common.ErrorE("failed to bring up container eth0", err)
	}

	// set container interface's ip address
	addr := &rtnl.Address{
		Info: &rtnl.AddrInfo{
			Address: &net.IPNet{
				IP:   ip,
				Mask: prefix,
			},
		},
	}

	err = ctrIf.AddAddr(targetCtx, addr)
	if err != nil {
		return nil, nil, common.ErrorE("failed to assign eth0 address", err)
	}

	hostIf, err := rtnl.GetLink(defaultCtx, hostIfname)
	if err != nil {
		return nil, nil, common.ErrorE("failed to get host interface", err)
	}

	err = hostIf.SetMtu(defaultCtx, GetConfig(false).Net.Mtu)
	if err != nil {
		return nil, nil, common.ErrorE("failed to set host-veth mtu", err)
	}

	return ctrIf, hostIf, nil

}

func enslaveVeth(vethName, bridgeName string, vid int) error {

	fields := log.Fields{
		"veth":   vethName,
		"bridge": bridgeName,
	}

	ctx, err := rtnl.OpenDefaultContext()
	if err != nil {
		return common.ErrorEF("rtnl context open failed", err, fields)
	}

	// get a handle to the veth device
	veth, err := rtnl.GetLink(ctx, vethName)
	if err != nil {
		return common.ErrorEF("failed to get veth device", err, fields)
	}

	// get a handle to the bridge
	bridge, err := rtnl.GetLink(ctx, bridgeName)
	if err != nil {
		return common.ErrorEF("failed to get bridge device", err, fields)
	}

	// if the veth device is not already on the bridge, enslave it
	if veth.Info.Master != uint32(bridge.Msg.Index) {

		veth.Info.Master = uint32(bridge.Msg.Index)

		err = veth.Set(ctx)
		if err != nil {
			return common.ErrorEF("failed to add veth to bridge", err, fields)
		}

	}

	// ensure the veth device is up
	err = veth.Up(ctx)
	if err != nil {
		return common.ErrorEF("failed to bring up veth", err, fields)
	}

	return nil

}

// CreateInfrapodNS creates a network namespace for an infrapod.
func CreateInfrapodNS(name string) error {

	_, err := ensureMznNetworkNamespace(name)
	if err != nil {
		return common.ErrorE("ensure ns", err)
	}
	return nil

}

// DeleteInfrapodNS deletes the network namespace associated with the infrapod
// identified by name.
func DeleteInfrapodNS(name string) error {

	return destroyMznNetworkNamespace(name)

}

// EnclaveBridge creates the materialization enclave bridge. This bridge
// connects the enclave to a materializtions infrastructure network.
func EnclaveBridge(spec *task.EnclaveSpec, ctx *rtnl.Context) error {

	br := rtnl.NewLink()
	br.Info.Name = fmt.Sprintf("mzbr%d", spec.Vindex)
	br.Info.Bridge = &rtnl.Bridge{VlanAware: false}

	err := br.Present(ctx)
	if err != nil {
		return common.ErrorEF("failed to ensure enclave mzbr", err, log.Fields{
			"name": br.Info.Name,
		})
	}

	err = br.Up(ctx)
	if err != nil {
		return common.ErrorEF("failed to bring upenclave mzbr", err, log.Fields{
			"name": br.Info.Name,
		})
	}

	return nil

}

// DelEnclaveBridge deletes the materialization bridge from an enclave.
func DelEnclaveBridge(spec *task.EnclaveSpec, ctx *rtnl.Context) error {

	br := rtnl.NewLink()
	br.Info.Name = fmt.Sprintf("mzbr%d", spec.Vindex)
	br.Info.Bridge = &rtnl.Bridge{VlanAware: false}

	err := br.Absent(ctx)
	if err != nil {
		return common.ErrorEF("failed to remove enclave mzbr", err, log.Fields{
			"name": br.Info.Name,
		})
	}

	return nil

}

// EnclaveIptables implements the double nat rules for an enclave so testbed
// nodes can access the internet through the infrastructure network.
func EnclaveIptables(mzid string, spec *task.EnclaveSpec) error {

	// XXX src := SvcAddress(spec.Vindex)
	src := spec.SvcAddress

	cmd := exec.Command(
		"ip", "netns", "exec", mzid,
		"iptables", "-t", "nat", "-A",
		"POSTROUTING",
		"-d", "255.255.255.255/32",
		"-j", "RETURN",
	)
	out, err := cmd.CombinedOutput()
	if err != nil {
		return common.ErrorEF("enclave iptables failed", err, log.Fields{
			"args":   cmd.Args,
			"output": string(out),
		})
	}

	cmd = exec.Command(
		"ip", "netns", "exec", mzid,
		"iptables", "-t", "nat", "-A",
		"POSTROUTING",
		"-s", "172.30.0.0/16",
		"!", "-d", "172.30.0.0/16",
		"-j", "SNAT", "--to-source", src,
	)

	out, err = cmd.CombinedOutput()
	if err != nil {
		return common.ErrorEF("enclave iptables failed", err, log.Fields{
			"args":   cmd.Args,
			"output": string(out),
		})
	}

	return nil

}

// DelEnclaveIptables deletes double NAT rules from an enclave
func DelEnclaveIptables(mzid string, spec *task.EnclaveSpec) error {

	// XXX src := SvcAddress(spec.Vindex)
	src := spec.SvcAddress

	cmd := exec.Command(
		"ip", "netns", "exec", mzid,
		"iptables", "-t", "nat", "-D",
		"POSTROUTING",
		"-s", "172.30.0.0/16",
		"!", "-d", "172.30.0.0/16",
		"-j", "SNAT", "--to-source", src,
	)

	out, err := cmd.CombinedOutput()
	if err != nil {
		return common.ErrorEF("enclave iptables failed", err, log.Fields{
			"args":   cmd.Args,
			"output": string(out),
		})
	}

	return nil

}

// NsDefaultRoute sets up a default route in an infrapod namespace
func NsDefaultRoute(mzid string, spec *task.EnclaveSpec) error {

	fields := log.Fields{
		"mzid": mzid,
	}

	ctx, err := rtnl.OpenContext(mzid)
	if err != nil {
		return common.ErrorEF("error opening mzn namespace", err, fields)
	}

	// default

	gwaddr := net.ParseIP(task.DefaultServiceGateway)
	if gwaddr == nil {
		fields["gateway"] = task.DefaultServiceGateway
		return common.ErrorEF("invalid external gateway", err, fields)
	}
	defroute := &rtnl.Route{
		Hdr:     unix.RtMsg{Dst_len: 0},
		Dest:    net.ParseIP("0.0.0.0"),
		Gateway: gwaddr,
	}

	err = defroute.Present(ctx)
	if err != nil {
		return common.ErrorEF("error creating infrapod default route", err, fields)
	}

	return nil

}
