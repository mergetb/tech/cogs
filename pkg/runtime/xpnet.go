package runtime

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/tech/cogs/pkg/common"
	"gitlab.com/mergetb/tech/cogs/pkg/task"
	"gitlab.com/mergetb/xir/lang/go/v0.2/sys"
	"gitlab.com/mergetb/xir/lang/go/v0.2/tb"
)

const (
	// XXX hardcode emu1
	DefaultEmulator = "emu1"
)

func AdvertiseMoaLink(
	linkInfo *task.LinkInfo,
	tunnelip string,
	asn int,
) error {

	for _, tbv := range linkInfo.TBVLinks {
		vni := tbv.Vni
		for _, mac := range tbv.Advertisements {

			err := EvpnAdvertiseMac(tunnelip, DefaultEmulator, mac, asn, vni, vni)
			if err != nil {
				return common.ErrorE("failed to advertise moa mac", err)
			}

			err = EvpnAdvertiseMulticast(tunnelip, DefaultEmulator, asn, vni, vni)
			if err != nil {
				return common.ErrorE("failed to advertise multicast", err)
			}

		}
	}

	return nil

}

func WithdrawMoaLink(
	linkInfo *task.LinkInfo,
	tunnelip string,
	asn int,
) error {

	for _, tbv := range linkInfo.TBVLinks {
		vni := tbv.Vni
		for _, mac := range tbv.Advertisements {

			err := EvpnWithdrawMac(tunnelip, DefaultEmulator, mac, asn, vni, vni)
			if err != nil {
				return common.ErrorE("failed to withdraw moa mac", err)
			}

			err = EvpnWithdrawMulticast(tunnelip, DefaultEmulator, asn, vni, vni)
			if err != nil {
				return common.ErrorE("failed to withdraw multicast", err)
			}

		}
	}

	return nil

}

func EmuInfo() (*tb.Resource, *sys.EvpnConfig, string, error) {

	fields := log.Fields{"emu": DefaultEmulator}

	_, _, emu := common.TbModel().GetNode(DefaultEmulator)
	if emu == nil {
		return nil, nil, "", common.ErrorF("emu not found in model", fields)
	}

	r := tb.GetResourceSpec(emu)
	if r == nil {
		return nil, nil, "", common.ErrorF("emu has no resource spec", fields)
	}

	evpnConfig := GetEvpnConfig(r)
	if evpnConfig == nil {
		return nil, nil, "", common.ErrorF("emu has no evpn config", fields)
	}

	emuLinks := r.GetLinksByRole(tb.EmuLink)
	if len(emuLinks) == 0 {
		return nil, nil, "", common.ErrorF("emu has no emulink", fields)
	}
	parent := emuLinks[0].Name

	return r, evpnConfig, parent, nil

}

func GetEvpnConfig(r *tb.Resource) *sys.EvpnConfig {

	if r.System == nil {
		return nil
	}
	if r.System.OS == nil {
		return nil
	}
	if r.System.OS.Config == nil {
		return nil
	}

	return r.System.OS.Config.Evpn

}
