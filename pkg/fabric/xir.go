package fabric

import (
	"fmt"

	log "github.com/sirupsen/logrus"

	"gitlab.com/mergetb/tech/cogs/pkg/common"
	"gitlab.com/mergetb/tech/cogs/pkg/task"
	"gitlab.com/mergetb/xir/lang/go/v0.2"
	"gitlab.com/mergetb/xir/lang/go/v0.2/sys"
	"gitlab.com/mergetb/xir/lang/go/v0.2/tb"
)

// GetInfraLeaf finds the infrastructure leaf switch attached to the specified
// node. If none exists nil is returned.
func GetInfraLeaf(topo *xir.Net, node string) (*xir.Node, *xir.Endpoint, error) {

	_, _, n := topo.GetNode(node)
	if n == nil {
		return nil, nil, fmt.Errorf("node '%s' not found", node)
	}

	for _, ep := range n.Endpoints {

		leafport := task.GetNodeSwitch(ep)
		if leafport == nil {
			continue
		}
		leaf := leafport.Parent

		if tb.HasRole(leaf, tb.Leaf) && tb.HasRole(leaf, tb.InfraSwitch) {
			return leaf, leafport, nil
		}

	}

	return nil, nil, fmt.Errorf("not found")

}

func GetInfraSwitch(
	topo *xir.Net, node string) (*xir.Node, *xir.Endpoint, *xir.Endpoint, error) {

	_, _, n := topo.GetNode(node)
	if n == nil {
		return nil, nil, nil, fmt.Errorf("node '%s' not found", node)
	}

	for _, ep := range n.Endpoints {

		leafport := task.GetNodeSwitch(ep)
		if leafport == nil {
			continue
		}
		leaf := leafport.Parent

		if tb.HasRole(leaf, tb.InfraSwitch) {
			return leaf, leafport, ep, nil
		}

	}

	return nil, nil, nil, fmt.Errorf("not found")

}

// GetNodeSwitch finds the switch attached to the specified node. If none exists
// nil is returned.
func GetNodeSwitch(
	tb *xir.Net, node, ifx string) (*xir.Node, *xir.Endpoint, *xir.Endpoint, error) {

	_, _, n := tb.GetNode(node)
	if n == nil {
		return nil, nil, nil, fmt.Errorf("node '%s' not found", node)
	}

	ep := n.GetEndpoint(ifx)
	if ep == nil {
		return nil, nil, nil, fmt.Errorf("node interface '%s' not found", ifx)
	}

	swport := task.GetNodeSwitch(ep)
	if swport == nil {
		return nil, nil, nil, fmt.Errorf("node '%s' has no switch for ifx %s", node, ifx)
	}
	sw := swport.Parent

	return sw, ep, swport, nil

}

func GetFabric(leaf *xir.Node) (*xir.Node, *xir.Endpoint, *xir.Endpoint, error) {

	for _, e := range leaf.Endpoints {
		for _, n := range e.Neighbors {
			x := n.Endpoint.Parent
			if tb.HasRole(x, tb.Fabric) || tb.HasRole(x, tb.Spine) {
				return n.Endpoint.Parent, e, n.Endpoint, nil
			}
		}
	}

	return nil, nil, nil, fmt.Errorf("fabric not found")

}

// VxlanConfig encapsulates information about a VXLAN configuration
type VxlanConfig struct {
	TunnelIP string
	Bridge   string
	ASN      int
}

func getVxlanConfig(n *xir.Node) *VxlanConfig {

	fields := log.Fields{"switch": n.Label()}

	sysinfo := sys.Sys(n)
	if sysinfo == nil {
		common.WarnF("system info not found", fields)
		return nil
	}

	if sysinfo.OS == nil {
		common.WarnF("os info not found", fields)
		return nil
	}

	if sysinfo.OS.Config == nil {
		common.WarnF("os config not found", fields)
		return nil
	}

	if sysinfo.OS.Config.Evpn == nil {
		common.WarnF("evpn config not found", fields)
		return nil
	}

	if sysinfo.OS.Config.PrimaryBridge == nil {
		common.WarnF("primary bridge config not found", fields)
		return nil
	}

	return &VxlanConfig{
		TunnelIP: sysinfo.OS.Config.Evpn.TunnelIP,
		Bridge:   sysinfo.OS.Config.PrimaryBridge.Name,
		ASN:      sysinfo.OS.Config.Evpn.ASN,
	}

}
