package fabric

import (
	"gitlab.com/mergetb/tech/cogs/pkg/runtime"
)

func (lp LinkPlan) Advertise() error {

	for _, x := range lp.EvpnAdvertisements {

		err := runtime.EvpnAdvertiseMulticast(x.Addr, x.RouterAPI, x.ASN, x.VNI, x.VNI)
		if err != nil {
			return err
		}
	}

	return nil

}

func (lp LinkPlan) Withdraw() error {

	for _, x := range lp.EvpnAdvertisements {

		err := runtime.EvpnWithdrawMulticast(x.Addr, x.RouterAPI, x.ASN, x.VNI, x.VNI)
		if err != nil {
			return err
		}
	}

	return nil

}
