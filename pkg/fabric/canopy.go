package fabric

import (
	"gitlab.com/mergetb/tech/canopy/lib"
)

// access port ----------------------------------------------------------------

// Create returns a canopy proerty that creates an access port
func (ap AccessPort) Create() *canopy.Property {
	return ap.modify(canopy.Presence_Present)
}

// Destroy returns a canopy proerty taht destroys an access port
func (ap AccessPort) Destroy() *canopy.Property {
	return ap.modify(canopy.Presence_Absent)
}

func (ap AccessPort) modify(presence canopy.Presence) *canopy.Property {

	return &canopy.Property{
		Element: ap.Name,
		Value: &canopy.Property_Access{Access: &canopy.Access{
			Presence: presence,
			Vid:      int32(ap.Vid),
		}},
	}

}

// trunk port -----------------------------------------------------------------

// Create returns a canopy property that creates a trunk port
func (tp TrunkPort) Create() *canopy.Property {
	return tp.modify(canopy.Presence_Present)
}

// Destroy returns a canopy property that destroys a trunk port
func (tp TrunkPort) Destroy() *canopy.Property {
	return tp.modify(canopy.Presence_Absent)
}

func (tp TrunkPort) modify(presence canopy.Presence) *canopy.Property {

	var vids []int32
	for _, x := range tp.Vids {
		persistent := false
		for _, p := range PersistentTrunks {
			if x == p {
				persistent = true
			}
		}
		if persistent && presence == canopy.Presence_Absent {
			continue
		}
		vids = append(vids, int32(x))
	}
	return &canopy.Property{
		Element: tp.Name,
		Value: &canopy.Property_Tagged{Tagged: &canopy.Tagged{
			Presence: presence,
			Vids:     vids,
		}},
	}

}

// vtep -----------------------------------------------------------------------

// Create returns a canopy property that creates a VTEP
func (v Vtep) Create() []*canopy.Property {
	return v.modify(canopy.Presence_Present)
}

// Destroy returns a canopy port that destroys a VTEP
func (v Vtep) Destroy() []*canopy.Property {
	return v.modify(canopy.Presence_Absent)
}

func (v Vtep) modify(presence canopy.Presence) []*canopy.Property {

	switch presence {

	// several things to do to bring up a vtep
	case canopy.Presence_Present:
		if v.Bridge != "" {
			return []*canopy.Property{
				// establish vtep
				{
					Element: v.Name,
					Value: &canopy.Property_Vtep{Vtep: &canopy.VtepPresence{
						Presence: presence,
						Vni:      int32(v.Vni),
						TunnelIp: v.TunnelIP,
					}},
				},
				// add vtep to bridge
				{
					Element: v.Name,
					Value: &canopy.Property_Master{Master: &canopy.Master{
						Value:    v.Bridge,
						Presence: presence,
					}},
				},

				// set vtep access vid
				{
					Element: v.Name,
					Value: &canopy.Property_Access{Access: &canopy.Access{
						Vid:      int32(v.Vid),
						Presence: presence,
					}},
				},
				// set vtep mtu
				{
					Element: v.Name,
					Value:   &canopy.Property_Mtu{Mtu: int32(v.Mtu)},
				},
				// set the link state to up
				{
					Element: v.Name,
					Value:   &canopy.Property_Link{Link: canopy.UpDown_Up},
				},
			}
		}

		if v.Device != "" {
			return []*canopy.Property{
				// establish vtep
				{
					Element: v.Name,
					Value: &canopy.Property_Vtep{Vtep: &canopy.VtepPresence{
						Presence: presence,
						Vni:      int32(v.Vni),
						Parent:   v.Device,
						TunnelIp: v.TunnelIP,
					}},
				},

				// set vtep access vid
				{
					Element: v.Name,
					Value: &canopy.Property_Access{Access: &canopy.Access{
						Vid:      int32(v.Vid),
						Presence: presence,
					}},
				},
				// set vtep mtu
				{
					Element: v.Name,
					Value:   &canopy.Property_Mtu{Mtu: int32(v.Mtu)},
				},
				// set the link state to up
				{
					Element: v.Name,
					Value:   &canopy.Property_Link{Link: canopy.UpDown_Up},
				},
			}
		}

	// only one thing to do to bring down a vtep
	case canopy.Presence_Absent:
		return []*canopy.Property{
			{
				Element: v.Name,
				Value: &canopy.Property_Vtep{Vtep: &canopy.VtepPresence{
					Presence: presence,
					Vni:      int32(v.Vni),
					TunnelIp: v.TunnelIP,
				}},
			},
		}

	}

	return nil

}

// link plan ------------------------------------------------------------------

// Create returns a canopy operation that creates a link plan
func (lp LinkPlan) Create() CanopyOps {

	return lp.modify(canopy.Presence_Present)

}

// Destroy returns a canopy operation that destroys a link plan
func (lp LinkPlan) Destroy() CanopyOps {

	return lp.modify(canopy.Presence_Absent)

}

func (lp LinkPlan) modify(presence canopy.Presence) CanopyOps {

	result := make(CanopyOps)

	for host, ports := range lp.AccessMap {
		for _, port := range ports {
			x := port.modify(presence)
			if x == nil {
				continue
			}
			result[host] = append(result[host], x)
		}
	}

	for host, ports := range lp.TrunkMap {
		for _, port := range ports {
			x := port.modify(presence)
			if x == nil {
				continue
			}
			result[host] = append(result[host], x)
		}
	}

	for host, ports := range lp.VtepMap {
		for _, port := range ports {
			result[host] = append(result[host], port.modify(presence)...)
		}
	}

	return result

}

// canopy ops -----------------------------------------------------------------

// CanopyOps is a map from a target hosts to a list of canopy properties. This
// is used for executing many canopy operations over many hosts.
type CanopyOps map[string][]*canopy.Property

// Exec carries out a set of canopy operations over a set of hosts as defined in
// the underlying operations map.
func (ops CanopyOps) Exec() error {

	cc := canopy.NewClient()

	for host, props := range ops {
		for _, prop := range props {
			cc.Add(prop, host)
		}
	}

	return cc.Commit()

}

func (ops CanopyOps) ExecFor(target string) error {
	cc := canopy.NewClient()

	for host, props := range ops {
		if host != target {
			continue
		}
		for _, prop := range props {
			cc.Add(prop, host)
		}
	}

	return cc.Commit()
}
