package fabric

import (
	"fmt"
	"testing"

	"github.com/davecgh/go-spew/spew"
	"gitlab.com/mergetb/facilities/dcomp/model"
	"gitlab.com/mergetb/site/api"
	"gitlab.com/mergetb/tech/cogs/pkg/task"
	"gitlab.com/mergetb/xir/lang/go/v0.2"
)

func getNodeIfx(node, ifx string, topo *xir.Net, t *testing.T,
) (*xir.Node, *xir.Endpoint) {

	_, _, n := topo.GetNode(node)
	if n == nil {
		t.Fatalf("%s: not found", node)
	}

	i := n.GetEndpoint(ifx)
	if i == nil {
		t.Fatalf("%s.%s: not found", node, ifx)
	}

	return n, i

}

func buildLinkinfo(
	id int, nodes []string, lanewidth []int, vni, vid int, topo *xir.Net, t *testing.T,
) *task.LinkInfo {

	var ifxs []*xir.Endpoint
	for _, node := range nodes {
		_, ifx := getNodeIfx(node, "eth1", topo, t)
		ifxs = append(ifxs, ifx)
	}

	var resources []string
	lanes := make(map[string]uint32)
	for i, ifx := range ifxs {
		for _, nbr := range ifx.Neighbors {
			resources = append(resources, nbr.Link.Id)
			lanes[nbr.Link.Id] = uint32(lanewidth[i])
		}
	}

	var endpointIds []string
	for _, ifx := range ifxs {
		endpointIds = append(endpointIds, ifx.Id())
	}

	mzf := &site.MzFragment{
		Id:        fmt.Sprintf("fragment%d", id),
		Elements:  []string{fmt.Sprintf("element%d", id)},
		Resources: resources,
		Model: &site.MzFragment_LinkModel{LinkModel: &site.LinkModel{
			Vlid:      fmt.Sprintf("link%d", id),
			Endpoints: endpointIds,
			Lanes:     lanes,
		}},
	}

	info, err := task.GenLinkInfo(
		&task.MzInfo{Mzid: "test.io"}, mzf, mzf.GetLinkModel(), topo, false,
	)
	if err != nil {
		t.Fatalf("fagment to LinkInfo failed: %v", err)
	}
	info.TBVLinks[0].Vni = vni
	info.TBVLinks[0].Vid = vid
	t.Logf("%#v", *info.TBVLinks[0])

	return info

}

func testPlan0(t *testing.T, plan *LinkPlan) {

	// xl0

	if plan.TrunkMap["xl0"]["swp49"].Vids[0] != 47 {
		t.Fatalf("trunkmap[xl0][uplink] is not 47")
	}

	if plan.TrunkMap["xl0"]["swp49"].Name != "uplink" {
		t.Fatalf("trunkmap[xl0][uplink] name is not uplink")
	}

	// xl1

	if plan.TrunkMap["xl1"]["swp49"].Vids[0] != 47 {
		t.Fatalf("trunkmap[xl1][uplink] is not 47")
	}

	if plan.TrunkMap["xl1"]["swp49"].Name != "uplink" {
		t.Fatalf("trunkmap[xl1][uplink] name is not uplink")
	}

	// xl2

	if plan.TrunkMap["xl2"]["swp49"].Vids[0] != 47 {
		t.Fatalf("trunkmap[xl2][uplink] is not 47")
	}

	if plan.TrunkMap["xl2"]["swp49"].Name != "uplink" {
		t.Fatalf("trunkmap[xl2][uplink] name is not uplink")
	}

}

func dcompTopo(t *testing.T) *xir.Net {

	topo := dcomp.DCompTB().Net()

	// yes this is gross, but it's because XIR is broke in this way and we need
	// to workaround
	str, err := topo.ToString()
	if err != nil {
		t.Fatal(err)
	}
	topo, err = xir.FromString(str)
	if err != nil {
		t.Fatal(err)
	}

	return topo

}

func TestLinkplan_MultiTrunk(t *testing.T) {

	topo := dcompTopo(t)

	info := buildLinkinfo(
		47,
		[]string{"m10", "m60", "m110"},
		[]int{2, 1, 2},
		470,
		47,
		topo,
		t,
	)

	plan, err := NewLinkPlan("testplan", topo, ExperimentLP, 9216, info)
	if err != nil {
		t.Fatalf("new linkplan failed: %v", err)
	}

	t.Logf("%#v", plan)
	for node, endpoints := range plan.TrunkMap {
		for endpoint, trunk := range endpoints {
			t.Logf("%s.%s -> %+v", node, endpoint, trunk)
		}
	}

	testPlan0(t, plan)

	t.Logf("op list")
	ops := plan.Create()
	for host, oplist := range ops {
		for _, op := range oplist {
			t.Logf("%s %+v", host, op)
		}
	}

}

func testPlan1(t *testing.T, plan *LinkPlan) {

	// xl1

	if plan.AccessMap["xl1"]["swp13"].Vid != 47 {
		t.Fatalf("accessmap[xl1][swp13] is not 99")
	}

	if plan.TrunkMap["xl2"]["swp49"].Vids[1] != 99 {
		t.Fatalf("trunkmap[xl2][swp49] is not 99")
	}

	// xl16

	if plan.TrunkMap["xl16"]["swp49"].Vids[0] != 99 {
		t.Fatalf("trunkmap[xl16][swp49] is not 99")
	}

	// xl16

	if plan.TrunkMap["xl30"]["swp49"].Vids[0] != 99 {
		t.Fatalf("trunkmap[xl30][swp49] is not 99")
	}

	// xl16

	if plan.AccessMap["xl16"]["swp25"].Vid != 99 {
		t.Fatalf("accessmap[xl16][swp25] is not 99")
	}

}

func TestLinkplan_MultiLink(t *testing.T) {

	topo := dcompTopo(t)

	link0 := buildLinkinfo(
		47,
		[]string{"m10", "m60", "m110"},
		[]int{2, 1, 2},
		470,
		47,
		topo,
		t,
	)

	link1 := buildLinkinfo(
		99,
		[]string{"m100", "m600", "m1100"},
		[]int{2, 1, 2},
		990,
		99,
		topo,
		t,
	)

	plan, err := NewLinkPlan("testplan", topo, ExperimentLP, 9216, link0, link1)
	if err != nil {
		t.Fatalf("new linkplan failed: %v", err)
	}

	t.Logf(spew.Sdump(plan))

	testPlan0(t, plan)
	testPlan1(t, plan)

}
