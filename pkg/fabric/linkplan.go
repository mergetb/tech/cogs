package fabric

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	etcd "github.com/coreos/etcd/clientv3"
	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/tech/cogs/pkg/common"
	"gitlab.com/mergetb/tech/cogs/pkg/runtime"
	"gitlab.com/mergetb/tech/cogs/pkg/task"
	"gitlab.com/mergetb/xir/lang/go/v0.2"
	"gitlab.com/mergetb/xir/lang/go/v0.2/sys"
	"gitlab.com/mergetb/xir/lang/go/v0.2/tb"
)

// LinkPlan defines a deployment plan for a link in terms of the ports and
// virtual interfaces that must be created to materialize it.
type LinkPlan struct {

	// materialization id
	Mzid string

	Role LinkPlanRole

	LinkKind task.LinkKind

	MTU int

	AccessMap AccessMap
	TrunkMap  TrunkMap
	VtepMap   VtepMap

	VVMap VlanVxlanMap

	EvpnAdvertisements []EvpnAdvertisement

	Topo *xir.Net `json:"-"`

	ver int64
}

type LinkPlanRole int

// AccessMap is a switch -> port -> access-port mapping
type AccessMap map[string]map[string]*AccessPort

// TrunkMap is a switch -> port -> trunk-port mapping
type TrunkMap map[string]map[string]*TrunkPort

// VtepMap is a fabirc -> interface -> vtep map
type VtepMap map[string]map[string]*Vtep

// VlanVxlanMap maps VLAN Vids onto VXLAN Vnis
type VlanVxlanMap map[int]int

type EvpnAdvertisement struct {
	RouterAPI  string
	RouterHost string
	Addr       string
	ASN        int
	VNI        int
	XpID       string
}

type AccessPort struct {
	Name string
	Vid  int
	XpID string
}

// TrunkPort encapsulates access port info
type TrunkPort struct {
	Name  string
	Vids  []int
	XpIDs []string
}

// Vtep encapsulates vtep info
type Vtep struct {
	Name          string
	Vid           int
	Vni           int
	Bridge        string
	TunnelIP      string
	DirectConnect bool
	XpID          string
	Mtu           int
	Device        string
}

// PersistentTrunks are trunks that never go away
var PersistentTrunks = []int{
	2,
}

// AccessMap ==================================================================

// Add an access port to a switch on the specified endpoint.
func (m AccessMap) Add(leaf *xir.Node, ep *xir.Endpoint, value *AccessPort) {
	m.AddEntry(leaf.Label(), ep.Label(), value)
}

func (m AccessMap) AddEntry(node, name string, value *AccessPort) {

	_, ok := m[node]
	if !ok {
		m[node] = make(map[string]*AccessPort)
	}
	m[node][name] = value

}

// TrunkMap ===================================================================

// Add a trunk port to a switch on the specified endpoint
func (m TrunkMap) Add(node *xir.Node, ep *xir.Endpoint, value *TrunkPort) {
	m.AddEntry(node.Label(), ep.Label(), value)
}

// AddEntry adds a trunk point to a switch at the specified endpoint
func (m TrunkMap) AddEntry(node, ep string, value *TrunkPort) {

	_, ok := m[node]
	if !ok {
		m[node] = make(map[string]*TrunkPort)
	}
	_, ok = m[node][ep]
	if !ok {
		m[node][ep] = value
	} else {
		//XXX n^2
		for _, x := range value.Vids {
			add := true
			for _, v := range m[node][ep].Vids {
				if x == v {
					add = false
					break
				}
			}
			if add {
				m[node][ep].Vids = append(m[node][ep].Vids, x)
			}
		}
		m[node][ep].XpIDs = append(m[node][ep].XpIDs, value.XpIDs...)
	}

}

// VtepMap ====================================================================

// Add a vtep to a switch with the specified name
func (m VtepMap) Add(node, name string, value *Vtep) {

	_, ok := m[node]
	if !ok {
		m[node] = make(map[string]*Vtep)
	}
	m[node][name] = value

}

// LinkPlan ===================================================================

// NewLinkPlan creates a new link plan
func NewLinkPlan(mzid string, topo *xir.Net, role LinkPlanRole, mtu int,
	links ...*task.LinkInfo) (
	*LinkPlan, error) {

	result := &LinkPlan{
		Mzid:      mzid,
		Topo:      topo,
		Role:      role,
		MTU:       mtu,
		AccessMap: make(AccessMap),
		TrunkMap:  make(TrunkMap),
		VtepMap:   make(VtepMap),
		VVMap:     make(VlanVxlanMap),
	}
	for _, link := range links {
		err := result.AddLink(link)
		if err != nil {
			return nil, err
		}
	}

	return result, nil
}

const (
	InfraLP LinkPlanRole = iota
	ExperimentLP
)

func (r LinkPlanRole) String() string {
	switch r {
	case InfraLP:
		return "infra"
	case ExperimentLP:
		return "xp"
	}
	return "?"
}

// Key returns the datastore key for this object.
func (l *LinkPlan) Key() string {
	return fmt.Sprintf("/linkplan/%s/%s", l.Mzid, l.Role)
}

// GetVersion returns the current datastore version of the object
func (l *LinkPlan) GetVersion() int64 { return l.ver }

// SetVersion sets the current datastore version of the object
func (l *LinkPlan) SetVersion(v int64) { l.ver = v }

// Value returns this object as an interface{}
func (l *LinkPlan) Value() interface{} { return l }

// Save a linkplan to the datastore
func (l *LinkPlan) Save() error {

	return common.Write(l)

}

func FetchLinkPlan(mzid string, role LinkPlanRole) (*LinkPlan, error) {

	lp := &LinkPlan{Mzid: mzid, Role: role}

	err := common.Read(lp)
	if err != nil {
		return nil, common.ErrorE("failed to read linkplan", err)
	}

	return lp, nil

}

// Delete a link plan from the datastore
func (l *LinkPlan) Delete() error {

	var objs []common.Object
	objs = append(objs, l)

	for host, portmap := range l.AccessMap {
		for port, ac := range portmap {

			objs = append(objs, &task.PortObj{
				Mzid: l.Mzid,
				Host: host,
				Port: port,
				Mode: task.Access,
				Vids: []int{ac.Vid},
			})

		}
	}

	for host, portmap := range l.TrunkMap {
		for port, pt := range portmap {

			objs = append(objs, &task.PortObj{
				Mzid: l.Mzid,
				Host: host,
				Port: port,
				Mode: task.Trunk,
				Vids: pt.Vids,
			})

		}
	}

	for host, vtepmap := range l.VtepMap {
		for name, info := range vtepmap {

			objs = append(objs, &task.VtepObj{
				Mzid: l.Mzid,
				Host: host,
				Name: name,
				Vni:  info.Vni,
				Vid:  info.Vid,
			})

		}
	}

	return common.DeleteObjects(objs)

}

func (lp *LinkPlan) AddLink(link *task.LinkInfo) error {

	accessMap, leafTrunkMap, vvmap, err := MapNodes(link, lp.Topo)
	if err != nil {
		return err
	}

	trunkMap, vtepMap, advertisements, err := MapFabrics(
		link, accessMap, leafTrunkMap, vvmap, lp.Topo, lp.MTU,
	)
	if err != nil {
		return err
	}

	for zwitch, portmap := range leafTrunkMap {
		for port, trunk := range portmap {
			trunkMap.AddEntry(zwitch, port, trunk)
		}
	}

	// ----

	for s, pm := range accessMap {
		for p, a := range pm {
			a.XpID = link.XpID
			lp.AccessMap.AddEntry(s, p, a)
		}
	}

	for s, pm := range trunkMap {
		for p, t := range pm {
			t.XpIDs = append(t.XpIDs, link.XpID)
			lp.TrunkMap.AddEntry(s, p, t)
		}
	}

	if link.Kind == task.MoaLink {

		_, evpnConfig, parent, err := runtime.EmuInfo()
		if err != nil {
			return err
		}

		for _, tbvl := range link.TBVLinks {

			ifx := fmt.Sprintf("vtep%d", tbvl.Vni)

			lp.VtepMap.Add(
				runtime.DefaultEmulator,
				fmt.Sprintf("vtep%d", tbvl.Vni),
				&Vtep{
					Name:     ifx,
					Vni:      tbvl.Vni,
					TunnelIP: evpnConfig.TunnelIP,
					XpID:     link.XpID,
					Mtu:      lp.MTU,
					Device:   parent,
				},
			)

		}

	}

	// determine whether or not to add vteps to the plan. in the case
	// ofinfrastructure, we always add vteps as there are services
	// elsewhere in the testbed such as nex and rally that always
	// require a routed link across the testbed switching mesh. in
	// the case of an experiment link, we only create a vtep if the link
	// crosses more than one fabric, otherwise simple vlans at the leaf
	// layer are sufficient to plumb the connectivity.
	needsEVPN := link.Kind == task.InfraLink ||
		link.Kind == task.MoaLink ||
		len(vtepMap) > 1

	for fabric, vteps := range vtepMap {
		for ifx, vtep := range vteps {

			if needsEVPN {
				vtep.XpID = link.XpID
				lp.VtepMap.Add(fabric, ifx, vtep)
			}

		}
	}

	if needsEVPN {
		for i := range advertisements {
			advertisements[i].XpID = link.XpID
		}

		lp.EvpnAdvertisements = append(lp.EvpnAdvertisements, advertisements...)
	}

	return nil

}

// MapNodes creates access and turnk maps on switches the specified nodes are
// directly connected to.
func MapNodes(link *task.LinkInfo, topo *xir.Net,
) (AccessMap, TrunkMap, VlanVxlanMap, error) {

	amap := make(AccessMap)
	tmap := make(TrunkMap)
	vvmap := make(VlanVxlanMap)

	for _, vlink := range link.TBVLinks {

		vvmap[vlink.Vid] = vlink.Vni

		for _, e := range vlink.Edges {

			sw, ep, swp, err := GetNodeSwitch(topo, e.NodeName, e.NodeInterface)
			if err != nil {
				return nil, nil, nil, err
			}

			swpName := swp.Label()
			breakout, ok := ep.Props["breakout"].(string)
			if ok {
				swpName = breakout
			}

			if e.Multidegree {

				tmap.Add(sw, swp, &TrunkPort{
					Name: swpName,
					Vids: []int{vlink.Vid},
				})

			} else {

				amap.Add(sw, swp, &AccessPort{
					Name: swpName,
					Vid:  vlink.Vid,
				})

			}

		}

	}

	return amap, tmap, vvmap, nil

}

// MapFabrics creates trunk and vtep maps from the set of testbed fabric
// switches.
func MapFabrics(link *task.LinkInfo, amap AccessMap, ltmap TrunkMap,
	vvmap VlanVxlanMap, topo *xir.Net, mtu int) (
	TrunkMap, VtepMap, []EvpnAdvertisement, error) {

	tmap := make(TrunkMap)
	vmap := make(VtepMap)

	var advertisements []EvpnAdvertisement

	doMap := func(leaf string, vids []int) error {

		directConnect := false

		_, _, ln := topo.GetNode(leaf)
		if ln == nil {
			return common.ErrorF("leaf not found", log.Fields{"leaf": leaf})
		}

		vtepTarget := ln

		if tb.HasRole(ln, tb.Leaf) {

			fabric, leafUplink, fabricDownlink, err := GetFabric(ln)
			if err != nil {
				return err
			}

			vtepTarget = fabric

			// uplink
			tmap.Add(ln, leafUplink, &TrunkPort{
				Name: LinkName(leafUplink),
				Vids: vids,
			})

			// downlink
			tmap.Add(fabric, fabricDownlink, &TrunkPort{
				Name: LinkName(fabricDownlink),
				Vids: vids,
			})

		} else {
			directConnect = true
		}

		for _, vid := range vids {

			vni, ok := vvmap[vid]
			if !ok {
				return common.ErrorF("no vni", log.Fields{"vid": vid})
			}

			// vtep
			if vni == task.HarborInfraVNI {
				return nil // harbor vteps are static
			}
			cfg := getVxlanConfig(vtepTarget)
			if cfg == nil {
				return common.Error("no sysconfig found")
			}

			vname := fmt.Sprintf("vtep%d", vni)
			vmap.Add(vtepTarget.Label(), vname, &Vtep{
				Name:          vname,
				Vni:           vni,
				Vid:           vid,
				TunnelIP:      cfg.TunnelIP,
				Bridge:        cfg.Bridge,
				DirectConnect: directConnect,
				Mtu:           mtu,
			})

		}

		return nil

	}

	for leaf, ports := range amap {
		for _, access := range ports {
			err := doMap(leaf, []int{access.Vid})
			if err != nil {
				return nil, nil, nil, err
			}
		}
	}

	for leaf, ports := range ltmap {
		for _, trunk := range ports {
			err := doMap(leaf, trunk.Vids)
			if err != nil {
				return nil, nil, nil, err
			}
		}
	}

	return tmap, vmap, advertisements, nil

}

// returns the name of the link, if the link is a part of a bond, returns the
// name of the bond
func LinkName(x *xir.Endpoint) string {

	name := x.Label()
	bond := sys.GetBond(x)
	if bond != nil {
		name = bond.Name
	}

	breakout, ok := x.Props["breakout"].(string)
	if ok {
		return breakout
	}

	return name

}

func BreakoutName(x *xir.Endpoint) (string, bool) {

	breakout, ok := x.Props["breakout"].(string)
	if ok {
		return breakout, true
	}

	return "", false

}

// ListLinkPlans lists all link plans associated with the given materialization
// ID.
func ListLinkPlans(mzid string) ([]*LinkPlan, error) {

	var result []*LinkPlan
	err := common.WithEtcd(func(c *etcd.Client) error {

		ctx, cancel := context.WithTimeout(context.TODO(), 3*time.Second)
		resp, err := c.Get(ctx, "/linkplan/"+mzid, etcd.WithPrefix())
		cancel()
		if err != nil {
			return err
		}

		for _, kv := range resp.Kvs {

			mzi := &LinkPlan{}
			err := json.Unmarshal(kv.Value, mzi)
			if err != nil {
				return err
			}
			result = append(result, mzi)

		}

		return nil

	})

	if err != nil {
		return nil, err
	}

	return result, nil

}

type HostAccessPort struct {
	Host string
	AccessPort
}

type HostTrunkPort struct {
	Host string
	TrunkPort
}

type HostVtep struct {
	Host string
	Vtep
}

type LinkPlanLookup struct {
	Access         map[string][]HostAccessPort
	Trunk          map[string][]HostTrunkPort
	Vtep           map[string][]HostVtep
	Advertisements map[string][]EvpnAdvertisement
}

func NewLinkPlanLookup() LinkPlanLookup {

	return LinkPlanLookup{
		Access:         make(map[string][]HostAccessPort),
		Trunk:          make(map[string][]HostTrunkPort),
		Vtep:           make(map[string][]HostVtep),
		Advertisements: make(map[string][]EvpnAdvertisement),
	}

}

func (l LinkPlan) LookupTable() LinkPlanLookup {

	table := NewLinkPlanLookup()

	for sw, ports := range l.AccessMap {
		for _, access := range ports {
			table.Access[access.XpID] = append(table.Access[access.XpID],
				HostAccessPort{
					Host:       sw,
					AccessPort: *access,
				},
			)
		}
	}

	for sw, ports := range l.TrunkMap {
		for _, trunk := range ports {
			for _, xpid := range trunk.XpIDs {
				table.Trunk[xpid] = append(table.Trunk[xpid],
					HostTrunkPort{
						Host:      sw,
						TrunkPort: *trunk,
					},
				)
			}
		}
	}

	for sw, ports := range l.VtepMap {
		for _, vtep := range ports {
			table.Vtep[vtep.XpID] = append(table.Vtep[vtep.XpID],
				HostVtep{
					Host: sw,
					Vtep: *vtep,
				},
			)
		}
	}

	for _, adv := range l.EvpnAdvertisements {
		table.Advertisements[adv.XpID] =
			append(table.Advertisements[adv.XpID], adv)
	}

	return table

}
