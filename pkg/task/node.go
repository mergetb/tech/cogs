package task

import (
	"context"
	"encoding/json"
	"fmt"
	"net"
	"strings"
	"time"

	etcd "github.com/coreos/etcd/clientv3"

	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/site/api"
	"gitlab.com/mergetb/tech/cogs/pkg/common"
	"gitlab.com/mergetb/tech/foundry/api"
	"gitlab.com/mergetb/tech/nex/pkg"
	"gitlab.com/mergetb/xir/lang/go/v0.2"
	"gitlab.com/mergetb/xir/lang/go/v0.2/sys"
	"gitlab.com/mergetb/xir/lang/go/v0.2/tb"
)

// Resource is an amalgamation of model and element
// to track, in order to minimize bulk reads/writes
type Resource struct {
	Model   *site.NodeModel
	Element string
}

// NodeSetup launches a set of tasks that collectively setup a node.
func NodeSetup(
	topo *xir.Net, mzid, instance string, fragments []*site.MzFragment) error {
	return doNodeConfigure(topo, mzid, instance, fragments, true)
}

// NodeRecycle launches a set of tasks that collectively recycle a node.
func NodeRecycle(
	topo *xir.Net, mzid, instance string, fragments []*site.MzFragment) error {
	return doNodeConfigure(topo, mzid, instance, fragments, false)
}

func doNodeConfigure(
	topo *xir.Net, mzid, instance string, fragments []*site.MzFragment, setup bool,
) error {

	// if we are setting up
	statusString := Ready
	function := "NodeSetup"
	withFoundry := true
	nodeMzid := mzid
	kind := NodeSetupKind

	// if we are recycling
	if !setup {
		statusString = Clean
		function = "NodeRecycle"
		withFoundry = false
		nodeMzid = "harbor.dcomptb.io"
		kind = NodeRecycleKind
	}

	fields := log.Fields{
		"function": function,
		"mzid":     mzid,
		"instance": instance,
		"foundry":  withFoundry,
		"nodeop":   statusString,
	}

	mzinfo, err := GetMzInfo(mzid, instance)
	if err != nil {
		return common.ErrorEF("failed to fetch mzinfo", err, fields)
	}

	svcaddr := SvcCidr(mzinfo.Vindex)
	addr, _, err := net.ParseCIDR(svcaddr)
	if err != nil {
		return common.ErrorEF("failed to parse svcaddr", err, fields)
	}

	var actions []*Action

	var nodes []string

	objTracker := make(map[string]*Resource)
	nodeObjs := make([]common.Object, 0)

	for _, fragment := range fragments {
		model := fragment.GetNodeModel()
		if model == nil {
			return common.ErrorF("model not found", fields)
		}

		for _, resource := range fragment.Resources {
			currentNS := &NodeState{
				Name: resource,
				Mzid: nodeMzid,
			}
			nodeObjs = append(nodeObjs, currentNS)

			rmap := &Resource{
				Model:   model,
				Element: fragment.Elements[0],
			}
			objTracker[resource] = rmap
		}
	}

	// read all the objects at once
	_, err = common.ReadObjects(nodeObjs)
	if err != nil {
		return common.ErrorEF("failed to read node status", err, fields)
	}

	// node state change list takes all our node updates and throws
	// them into a transaction, we dont need the transaction property
	// we just want to batch all the writes into a single request
	nodeStateChangeList := make([]common.Object, 0)
	failList := make([]common.Object, 0)

	for _, obj := range nodeObjs {
		// redefine all the initial things we had but we had
		// to wait until after we read all the objects
		nodeName := obj.(*NodeState).Name

		moEle := objTracker[nodeName]
		model := moEle.Model
		element := moEle.Element

		modelData := props(model.Model)
		if modelData == nil {
			log.WithFields(fields).Warn("bad or missing node data")
		}

		nodes = append(nodes, nodeName)

		// if we fail later, keep a list of the node state prior
		currentNS := obj.(*NodeState)
		failList = append(failList, currentNS)

		// update the node with desired future state of the node
		futureNS := currentNS
		futureNS.Mzid = mzid
		futureNS.Status.Target = statusString
		nodeStateChangeList = append(nodeStateChangeList, futureNS)

		var err error
		var spec *NodeSpec
		var info *NodeInfo
		if setup {
			spec, info, err = nodeSpecSetup(
				topo, nodeName, addr, element,
				mzinfo, model, modelData, withFoundry, setup,
			)
			nodeStateChangeList = append(nodeStateChangeList, info)
		} else {
			spec, err = nodeSpecDestroy(
				topo, nodeName, addr, element,
				mzinfo, model, modelData, withFoundry, setup,
			)
		}

		if err != nil {
			return err
		}

		action := &Action{
			Kind:       kind,
			Mzid:       mzid,
			MzInstance: instance,
			Action:     fmt.Sprintf("%s (%s)", spec.Resource, spec.Xname),
			Data:       spec,
		}
		actions = append(actions, action)
	}

	// if we are setting up we need to also create an infra link plan
	// that contains all our nodes
	if setup {
		lpAction, err := CreateInfranetLinkplan(nodes, mzinfo)
		if err != nil {
			return common.ErrorEF("failed to create infranet linkplan", err, fields)
		}
		actions = append(actions, lpAction)
	}

	// update all the nodes to what their future desired state should be:
	// setup: Ready
	// recycle: Clean
	// rex will push towards these desired states
	err = common.WriteObjects(nodeStateChangeList, false)
	if err != nil {
		return common.ErrorEF("failed to update node status", err, fields)
	}

	task, objs, err := NewTask(mzinfo.Mzid, mzinfo.Instance,
		NewStage(actions...),
	)
	if err != nil {
		return common.ErrorE("failed to create task", err)
	}

	if setup {
		err = waitForIncoming(mzinfo, task)
		if err != nil {
			err2 := common.WriteObjects(failList, false)
			if err != nil {
				common.WarnF(
					"failed to undo nodestatus update- nodes in bad state",
					log.Fields{"launch err": err, "write err": err2},
				)
			}
			return common.ErrorEF("failed to wait for incoming task id", err, fields)
		}
	}

	err = LaunchTask(task, objs)
	if err != nil {
		// if we failed to launch task, we want to try and undo
		// everything we've already done related to state.
		err2 := common.WriteObjects(failList, false)
		if err2 != nil {
			common.WarnF(
				"failed to undo nodestatus update- nodes in bad state",
				log.Fields{"launch err": err, "write err": err2},
			)
		}
		return common.ErrorE("failed to launch node setup task", err)
	}

	return nil
}

// ListNodes lists the node state of all nodes in a testbed.
func ListNodes() ([]*NodeState, error) {

	var result []*NodeState
	objsize := 0
	err := common.WithEtcd(func(c *etcd.Client) error {

		ctx, cancel := context.WithTimeout(context.TODO(), 3*time.Second)
		resp, err := c.Get(ctx, "/nodestatus", etcd.WithPrefix())
		cancel()
		if err != nil {
			return err
		}

		for _, kv := range resp.Kvs {

			ns := &NodeState{}
			err := json.Unmarshal(kv.Value, ns)
			if err != nil {
				return fmt.Errorf("failed to unmarshal node status: %v", err)
			}
			result = append(result, ns)
			objsize += len([]byte(kv.Value))

		}

		return nil

	})
	if err != nil {
		return nil, err
	}

	log.Debugf("Nodestate prefix Size: %d", objsize)

	return result, nil
}

// GetNodeStatus returns the node state of node
func GetNodeStatus(nodeName string) (*NodeState, error) {
	obj := &NodeState{
		Name: nodeName,
	}
	// If node does not exist, we return an error, rather than ReadNew and return
	// the empty object
	err := common.Read(obj)
	if err != nil {
		return nil, err
	}
	return obj, nil
}

func nodeSpecConfigure(
	topo *xir.Net, name string, svc net.IP, xpName string,
	mzinfo *MzInfo, model *site.NodeModel, modelData map[string]interface{},
	withFoundry bool, setup bool,
) (*NodeSpec, *NodeInfo, error) {
	fields := log.Fields{"mzid": mzinfo.Mzid, "node": name}

	_, _, node := topo.GetNode(name)
	if node == nil {
		return nil, nil, common.ErrorF("node not found", fields)
	}

	ie := GetInfraEndpoint(node)
	if ie == nil {
		return nil, nil, common.ErrorF("missing infra network endpoint", fields)
	}

	// get the mac
	mac, ok := ie.Props["mac"].(string)
	if !ok {
		return nil, nil, common.ErrorF("missing mac", fields)
	}
	mac = strings.ToLower(mac)

	// get the interface name
	ifname, ok := ie.Props["name"].(string)
	if !ok {
		return nil, nil, common.ErrorF("missing interface name", fields)
	}

	// get the experiment specified node name otherwise use the node id
	xname, ok := modelData["name"].(string)
	if !ok {
		xname = xpName
	}

	var binding *ImageBinding
	var err error
	if setup {
		binding, err = userImageData(modelData, node)
	} else {
		// always recycle to default image
		binding, err = imageSpecFromName(DefaultImages.Default, node)
	}

	if err != nil {
		return nil, nil, common.ErrorF("bad image data", fields)
	}

	// get kernel append info for the selected node
	if binding.Image.Append == "" {

		sysinfo := sys.Sys(node)
		if sysinfo == nil {

			fields["node"] = node.Label()
			fields["stuff"] = fmt.Sprintf("%+v", node.Props)
			if !ok {
				return nil, nil, common.ErrorF("missing kernel sysconfig", fields)
			}

		}
		resourceInfo := tb.GetResourceSpec(node)

		binding.Image.Append = sysinfo.OS.Config.Append
		binding.Image.Blockdevice = sysinfo.OS.Config.Rootdev
		binding.Image.Disk = resourceInfo.DefaultImage
	}

	var mc *foundry.MachineConfig = nil
	if withFoundry {
		mc, err = machineConfig(mzinfo.Mzid, model, node)
		if mc != nil {
			mc.Id = mac
		}
		if err != nil {
			log.WithFields(fields).Warn("failed to create foundry config")
		}
	}

	ns := &NodeSpec{
		Resource:    name,
		Xname:       xname,
		Mac:         mac,
		Interface:   ifname,
		Binding:     binding,
		Config:      mc,
		SvcEndpoint: svc.String(),
		Infranet: &TargetNetwork{
			Vni: mzinfo.Vni,
			Vid: mzinfo.Vid,
		},
	}

	ni := &NodeInfo{
		Mzid:    mzinfo.Mzid,
		Machine: name,
		XpName:  xname,
		Image:   binding,
		Config:  mc,
		Nex: &nex.Member{
			Mac:  mac,
			Name: name,
		},
	}

	return ns, ni, nil
}

func nodeSpecSetup(
	topo *xir.Net, name string, svc net.IP, xpName string,
	mzinfo *MzInfo, model *site.NodeModel, modelData map[string]interface{},
	withFoundry, setup bool,
) (*NodeSpec, *NodeInfo, error) {
	return nodeSpecConfigure(
		topo, name, svc, xpName, mzinfo, model, modelData, withFoundry, setup,
	)
}

func nodeSpecDestroy(
	topo *xir.Net, name string, svc net.IP, xpName string,
	mzinfo *MzInfo, model *site.NodeModel, modelData map[string]interface{},
	withFoundry, setup bool,
) (*NodeSpec, error) {
	ns, _, err := nodeSpecConfigure(
		topo, name, svc, xpName, mzinfo, model, modelData, withFoundry, setup,
	)
	return ns, err
}
