package task

import (
	"fmt"
	"net"
	"time"

	"github.com/opencontainers/runtime-spec/specs-go"
	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/tech/cogs/pkg/common"
	"gitlab.com/mergetb/tech/nex/pkg"
)

// InitInfranet creates an enclave for an infranet and then deploys and
// configures the base containers within that enclave: foundry, etcd, nex
func InitInfranet(mzinfo *MzInfo, props map[string]interface{}) error {

	fields := log.Fields{
		"mzinfo": mzinfo.Mzid,
	}

	createEnclave := enclaveAction(mzinfo, CreateEnclave)

	nexCtr := nexCtrAction(mzinfo)

	foundryCtr := foundryCtrAction(mzinfo)

	etcdCtr := etcdCtrAction(mzinfo)

	moactldCtr := moactldCtrAction(mzinfo)

	belugactldCtr := belugactldCtrAction(mzinfo)

	svcaddr := SvcCidr(mzinfo.Vindex)
	addr, _, err := net.ParseCIDR(svcaddr)
	if err != nil {
		return common.ErrorEF("failed to parse svcaddr", err, log.Fields{
			"mzid": mzinfo.Mzid,
		})
	}

	nexInit := nexCreateNetworkActions(mzinfo, addr)

	nexStaticMembers := nexStaticMembersActions(mzinfo, addr)

	task, objs, err := NewTask(mzinfo.Mzid, mzinfo.Instance,
		NewStage(
			createEnclave,
		),
		NewStage(
			addVtep(mzinfo),
			nexCtr,
			foundryCtr,
			etcdCtr,
			moactldCtr,
			belugactldCtr,
		),
		NewStage(
			nexInit...,
		),
		NewStage(
			nexStaticMembers...,
		),
		NewStage(
			setStatusAction(mzinfo, Active),
		),
	)
	if err != nil {
		return common.ErrorE("failed to create new task", err)
	}

	err = LaunchTask(task, objs)
	if err != nil {
		return cleanAfterFailure("", mzinfo, err, fields)
	}

	// update the mzinfo object with TaskID
	mzinfo.IncomingTaskID = task.ID
	err = common.RUC(mzinfo, func(o common.Object) { o.(*MzInfo).IncomingTaskID = task.ID })
	if err != nil {
		return cleanAfterFailure(task.ID, mzinfo, err, fields)
	}

	return nil

}

// DeleteMzInfo deletes the mzinfo data
func DeleteMzInfo(mzinfo *MzInfo) error {

	fields := log.Fields{
		"mzid": mzinfo.Mzid,
	}

	// Undo all the counters
	vindex := common.CountSet{Name: VindexKey}
	vni := common.CountSet{Name: InfraVniKey}
	vid := common.CountSet{Name: InfraVidKey}
	vobj := []common.Object{&vindex, &vni, &vid}

	n, err := common.ReadObjects(vobj)
	if err != nil || n != 3 {
		return common.ErrorEF("failed to read virtual network counters", err, fields)
	}

	vindex = vindex.Remove(mzinfo.Vindex)
	vni = vni.Remove(mzinfo.Vni)
	vid = vid.Remove(mzinfo.Vid)

	vobj = []common.Object{&vindex, &vni, &vid}

	if mzinfo.Mzid == "" {
		return common.ErrorEF("mzid missing from mzinfo", err, fields)
	}
	mzinfoKey := fmt.Sprintf("/mzinfo/%s", mzinfo.Mzid)

	err = common.RunObjectTxPrefix(vobj, mzinfoKey)
	if err != nil {
		return common.ErrorEF("failed transaction deleting mzinfo", err, fields)
	}

	return nil
}

func cleanAfterFailure(taskID string, mzinfo *MzInfo, err error, fields log.Fields) error {
	log.Errorf("CleanAfterFailure is Executing for: %s", mzinfo.Mzid)
	if taskID != "" {
		// this is a bit of dangerous code
		// 1 we should make sure that task.ID is not empty, if it is we nuke everything
		if taskID != "" {
			return common.ErrorEF("task ID is not valid, cannot clean up", err, fields)
		}
		err2 := CancelTask(mzinfo.Mzid, taskID)
		if err2 != nil {
			return common.ErrorEF(fmt.Sprintf("Delete task failed: %v", err2), err, fields)
		}
	}
	// next we need to delete the mzinfo info state if it also exists
	err3 := DeleteMzInfo(mzinfo)
	if err3 != nil {
		return common.ErrorEF(fmt.Sprintf("Delete mzinfo failed: %v", err3), err, fields)
	}
	return err
}

// actions .......

func addVtep(mzinfo *MzInfo) *Action { return modVtep(mzinfo, false) }
func delVtep(mzinfo *MzInfo) *Action { return modVtep(mzinfo, true) }

func modVtep(mzinfo *MzInfo, remove bool) *Action {

	action := SetCanopyServiceVtep
	if remove {
		action = RemoveCanopyServiceVtep
	}

	return &Action{
		Kind:       CanopyKind,
		Mzid:       mzinfo.Mzid,
		MzInstance: mzinfo.Instance,
		Action:     action,
		Data: &VtepSpec{
			Vni: mzinfo.Vni,
			Vid: mzinfo.Vid,
			Vteps: []Vtep{{
				/// XXX Node: InfraCanopyHost,
				/// XXX Dev:      cfg.Net.VtepIfx,
				/// XXX TunnelIP: cfg.Net.ServiceTunnelIP,
				Bridge: mzbr(mzinfo.Vindex),
			}},
		},
	}
}

func nexCtrAction(mzinfo *MzInfo) *Action {
	return &Action{
		Kind:       ContainerKind,
		Mzid:       mzinfo.Mzid,
		MzInstance: mzinfo.Instance,
		Action:     LaunchCtr,
		Data: ContainerSpec{
			Container: Container{
				Name:      "nex",
				Namespace: mzinfo.Mzid,
				Vindex:    mzinfo.Vindex,
				Image:     infrapodImages.Nex,
			},
			Mounts: []*ContainerMount{{
				Name: NexMountName,
				Mount: specs.Mount{
					Source:      NexMountSource,
					Destination: NexMountDest,
					Options:     DefaultMountOptions,
					Type:        DefaultMountType,
				},
			}},
			Environment: map[string]string{
				NexDomainEnv: mzinfo.Mzid,
			},
		},
	}
}

func foundryCtrAction(mzinfo *MzInfo) *Action {
	return &Action{
		Kind:       ContainerKind,
		Mzid:       mzinfo.Mzid,
		MzInstance: mzinfo.Instance,
		Action:     LaunchCtr,
		Data: ContainerSpec{
			Container: Container{
				Name:      "foundry",
				Namespace: mzinfo.Mzid,
				Vindex:    mzinfo.Vindex,
				Image:     infrapodImages.Foundry,
			},
			Mounts: []*ContainerMount{
				{
					Name: FoundryCertMountName,
					Mount: specs.Mount{
						Source:      FoundryCertMountSource,
						Destination: FoundryCertMountDest,
						Options:     DefaultMountOptions,
						Type:        DefaultMountType,
					},
				},
				{
					Name: FoundryKeyMountName,
					Mount: specs.Mount{
						Source:      FoundryKeyMountSource,
						Destination: FoundryKeyMountDest,
						Options:     DefaultMountOptions,
						Type:        DefaultMountType,
					},
				},
			},
			Environment: map[string]string{
				FoundryEnv: FoundryArgs,
			},
		},
	}
}

func etcdCtrAction(mzinfo *MzInfo) *Action {
	return &Action{
		Kind:       ContainerKind,
		Mzid:       mzinfo.Mzid,
		MzInstance: mzinfo.Instance,
		Action:     LaunchCtr,
		Data: ContainerSpec{
			Container: Container{
				Name:      "etcd",
				Namespace: mzinfo.Mzid,
				Vindex:    mzinfo.Vindex,
				Image:     infrapodImages.Etcd,
			},
		},
	}

}

func moactldCtrAction(mzinfo *MzInfo) *Action {

	return &Action{
		Kind:       ContainerKind,
		Mzid:       mzinfo.Mzid,
		MzInstance: mzinfo.Instance,
		Action:     LaunchCtr,
		Data: ContainerSpec{
			Container: Container{
				Name:      "moactld",
				Namespace: mzinfo.Mzid,
				Vindex:    mzinfo.Vindex,
				Image:     infrapodImages.Moactl,
			},
			Mounts: []*ContainerMount{{
				Name: MoactldMountName,
				Mount: specs.Mount{
					Source:      MoactldMountSource,
					Destination: MoactldMountDest,
					Options:     DefaultMountOptions,
					Type:        DefaultMountType,
				},
			}},
		},
	}

}

func belugactldCtrAction(mzinfo *MzInfo) *Action {

	return &Action{
		Kind:       ContainerKind,
		Mzid:       mzinfo.Mzid,
		MzInstance: mzinfo.Instance,
		Action:     LaunchCtr,
		Data: ContainerSpec{
			Container: Container{
				Name:      belugactldCtrName,
				Namespace: mzinfo.Mzid,
				Image:     infrapodImages.Belugactl,
			},
			Mounts: []*ContainerMount{
				{
					Name: belugactldCertMountName,
					Mount: specs.Mount{
						Source:      belugactldCertMountSrc,
						Destination: belugactldCertMountDst,
						Options:     DefaultMountOptions,
						Type:        DefaultMountType,
					},
				},
				{
					Name: belugactldKeyMountName,
					Mount: specs.Mount{
						Source:      belugactldKeyMountSrc,
						Destination: belugactldKeyMountDst,
						Options:     DefaultMountOptions,
						Type:        DefaultMountType,
					},
				},
				{
					Name: belugactldBelugaCfgMountName,
					Mount: specs.Mount{
						Source:      belugactldBelugaCfgMountSrc,
						Destination: belugactldBelugaCfgMountDst,
						Options:     DefaultMountOptions,
						Type:        DefaultMountType,
					},
				},
			},
		},
	}
}

func nexCreateNetworkActions(mzinfo *MzInfo, addr net.IP) []*Action {
	createActions := []*Action{
		// node network
		{
			Kind:       NexKind,
			Mzid:       mzinfo.Mzid,
			MzInstance: mzinfo.Instance,
			Action:     CreateNexNetwork,
			Data: &NexNetworkSpec{
				Endpoint: addr.String(),
				Network: nex.Network{
					Name:        mzinfo.Mzid,
					Subnet4:     defaultInfraSubnet,
					Gateways:    []string{DefaultInternalGateway},
					Nameservers: []string{defaultInfraSubnetBegin},
					Dhcp4Server: defaultInfraSubnetBegin,
					Domain:      mzinfo.Mzid,
					Range4: &nex.AddressRange{
						Begin: defaultInfraDhcpBegin,
						End:   defaultInfraSubnetEnd,
					},
				},
			},
		},
		// infrastructure network
		{
			Kind:       NexKind,
			Mzid:       mzinfo.Mzid,
			MzInstance: mzinfo.Instance,
			Action:     CreateNexNetwork,
			Data: &NexNetworkSpec{
				Endpoint: addr.String(),
				Network: nex.Network{
					Name:        fmt.Sprintf("static.%s", mzinfo.Mzid),
					Subnet4:     defaultInfraSubnet,
					Gateways:    []string{defaultInfraGateway},
					Nameservers: []string{defaultInfraSubnetBegin},
					Dhcp4Server: defaultInfraSubnetBegin,
					Domain:      mzinfo.Mzid,
				},
			},
		},
	}
	return createActions
}

func nexStaticMembersActions(mzinfo *MzInfo, addr net.IP) []*Action {

	// split into two actions because they use the same ip address, and it causes problems with trying to update etcd in a single request
	actions := []*Action{
		{
			Kind:       NexKind,
			Mzid:       mzinfo.Mzid,
			MzInstance: mzinfo.Instance,
			Action:     AddNexMembers,
			Data: &NexMemberSpec{
				Endpoint: addr.String(),
				MemberList: nex.MemberList{
					Net: fmt.Sprintf("static.%s", mzinfo.Mzid),
					List: []*nex.Member{
						{
							Mac:  "00:00:00:00:00:01",
							Name: "foundry",
							Ip4:  &nex.Lease{Address: defaultInfraSubnetBegin},
						},
					},
				},
			},
		},
		{
			Kind:       NexKind,
			Mzid:       mzinfo.Mzid,
			MzInstance: mzinfo.Instance,
			Action:     AddNexMembers,
			Data: &NexMemberSpec{
				Endpoint: addr.String(),
				MemberList: nex.MemberList{
					Net: fmt.Sprintf("static.%s", mzinfo.Mzid),
					List: []*nex.Member{
						{
							Mac:  "00:00:00:00:00:02",
							Name: "moactl",
							Ip4:  &nex.Lease{Address: defaultInfraSubnetBegin},
						},
					},
				},
			},
		},
		{
			Kind:       NexKind,
			Mzid:       mzinfo.Mzid,
			MzInstance: mzinfo.Instance,
			Action:     AddNexMembers,
			Data: &NexMemberSpec{
				Endpoint: addr.String(),
				MemberList: nex.MemberList{
					Net: fmt.Sprintf("static.%s", mzinfo.Mzid),
					List: []*nex.Member{
						{
							Mac:  "00:00:00:00:00:03",
							Name: "belugactl",
							Ip4:  &nex.Lease{Address: defaultInfraSubnetBegin},
						},
					},
				},
			},
		},
	}
	return actions
}

func enclaveAction(mzinfo *MzInfo, action string) *Action {

	svcaddr := SvcCidr(mzinfo.Vindex)

	return &Action{
		Kind:       PlumbingKind,
		Mzid:       mzinfo.Mzid,
		MzInstance: mzinfo.Instance,
		Action:     action,
		Data: &EnclaveSpec{
			Mzid:       mzinfo.Mzid,
			Vindex:     mzinfo.Vindex,
			SvcAddress: SvcAddress(mzinfo.Vindex),
			Interfaces: []*EnclaveVeth{
				{
					Inner:         InfrapodMznIfx,
					Outer:         ifr(mzinfo.Vindex),
					Vni:           mzinfo.Vni,
					Vid:           mzinfo.Vid,
					EvpnAdvertise: true,
					Bridge:        mzbr(mzinfo.Vindex),
					Address:       defaultInfraGateway,
				},
				{
					Inner:   InfrapodSvcIfx,
					Outer:   svceth(mzinfo.Vindex),
					Bridge:  svcbr,
					Address: svcaddr,
				},
			},
		},
	}
}

func setStatusAction(mzinfo *MzInfo, status MzStatus) *Action {
	return &Action{
		Kind:       BookkeepingKind,
		Mzid:       mzinfo.Mzid,
		MzInstance: mzinfo.Instance,
		Action:     SetStatus,
		Data:       status,
	}
}

// teardown ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// TeardownInfranet tears down an infrapod. This deletes the network enclave and
// the containers within the enclave.
func TeardownInfranet(mzinfo *MzInfo, props map[string]interface{}) error {

	task, objs, err := NewTask(mzinfo.Mzid, mzinfo.Instance,
		// delete interfaces
		NewStage(
			enclaveAction(mzinfo, DestroyEnclave),
			delVtep(mzinfo),
		),
		NewStage(
			&Action{
				Kind:       ContainerKind,
				Mzid:       mzinfo.Mzid,
				MzInstance: mzinfo.Instance,
				Action:     DeleteCtrRecord,
				Data: &DeleteContainerData{
					Name:      "nex",
					Namespace: mzinfo.Mzid,
				},
			},
			&Action{
				Kind:       ContainerKind,
				Mzid:       mzinfo.Mzid,
				MzInstance: mzinfo.Instance,
				Action:     DeleteCtrRecord,
				Data: &DeleteContainerData{
					Name:      "foundry",
					Namespace: mzinfo.Mzid,
				},
			},
			&Action{
				Kind:       ContainerKind,
				Mzid:       mzinfo.Mzid,
				MzInstance: mzinfo.Instance,
				Action:     DeleteCtrRecord,
				Data: &DeleteContainerData{
					Name:      belugactldCtrName,
					Namespace: mzinfo.Mzid,
				},
			},
			&Action{
				Kind:       ContainerKind,
				Mzid:       mzinfo.Mzid,
				MzInstance: mzinfo.Instance,
				Action:     DeleteCtrRecord,
				Data: &DeleteContainerData{
					Name:      "etcd",
					Namespace: mzinfo.Mzid,
				},
			},
			&Action{
				Kind:       ContainerKind,
				Mzid:       mzinfo.Mzid,
				MzInstance: mzinfo.Instance,
				Action:     DeleteCtrRecord,
				Data: &DeleteContainerData{
					Name:      "moactld",
					Namespace: mzinfo.Mzid,
				},
			},
			&Action{
				Kind:       PlumbingKind,
				Mzid:       mzinfo.Mzid,
				MzInstance: mzinfo.Instance,
				Action:     DestroyInfranet,
			},
		),
		NewStage(
			&Action{
				Kind:       ContainerKind,
				Mzid:       mzinfo.Mzid,
				MzInstance: mzinfo.Instance,
				Action:     DeleteCtrNS,
				Data: &DeleteContainerNsData{
					Name: mzinfo.Mzid,
				},
			},
		),
		NewStage(
			&Action{
				Kind:       BookkeepingKind,
				Mzid:       mzinfo.Mzid,
				MzInstance: mzinfo.Instance,
				Action:     DeleteMzinfo,
			},
		),
	)
	if err != nil {
		return common.ErrorE("failed to create new task", err)
	}

	tasks, err := ListTasks(mzinfo.Mzid)
	if err != nil {
		return err
	}

	actions, err := GetMzidAS(task.Mzid, task.ID)
	if err != nil {
		return err
	}

	if len(actions) > 0 {
		for _, t := range tasks {
			complete, err := task.Complete(actions)
			if err != nil {
				return err
			}
			if !complete {
				task.AddDep(t.ID)
			}
		}
	}

	err = LaunchTask(task, objs)
	if err != nil {
		return common.ErrorE("failed to launch task", err)
	}
	mzinfo.TeardownTaskID = task.ID

	return nil

}

func waitForIncoming(mzi *MzInfo, tsk *Task) error {

	for i := 0; i < 10; i++ {
		if mzi.IncomingTaskID != "" {
			break
		}
		err := common.Read(mzi)
		if err != nil {
			return common.ErrorE("failed to read mzinfo", err)
		}
		time.Sleep(1 * time.Second)
	}

	if mzi.IncomingTaskID == "" {
		return common.Error("wait for incoming task id failed")
	}

	tsk.AddDep(mzi.IncomingTaskID)

	return nil

}
