package task

import (
	"fmt"

	"github.com/mitchellh/mapstructure"
	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/xir/lang/go/v0.2"
	"gitlab.com/mergetb/xir/lang/go/v0.2/tb"
)

// GetInfraEndpoint tries to find an endpoint from the specified node on an
// infrastructure network. If one can not be found nil is returned.
func GetInfraEndpoint(n *xir.Node) *xir.Endpoint {

	for _, e := range n.Endpoints {
		for _, nbr := range e.Neighbors {
			if tb.HasRole(nbr.Endpoint.Parent, tb.InfraSwitch) {

				return e

			}
		}
	}

	return nil

}

// Merge2Local transforms a Merge node ID into a node label
func Merge2Local(net *xir.Net, devID string) (string, error) {

	_, _, node := net.GetNode(devID)
	if node == nil {
		log.WithFields(log.Fields{
			"uuid": devID,
		}).Error("could not find node")
		return "", fmt.Errorf("node not found")
	}

	return node.Label(), nil

}

// GetNodeSwitch gets the switch attached to a node. If no leaf switch is
// attached to the node, nil is returned.
func GetNodeSwitch(nodeEndpoint *xir.Endpoint) *xir.Endpoint {

	for _, n := range nodeEndpoint.Neighbors {
		if tb.HasRole(n.Endpoint.Parent, tb.InfraSwitch, tb.XpSwitch) {
			return n.Endpoint
		}
	}

	return nil

}

// ExtractConstraint extracts the constraint at the specified key as an XIR
// constraint object.
func ExtractConstraint(props map[string]interface{}, key string) *xir.Constraint {

	data, ok := props[key]
	if !ok {
		return nil
	}

	constraint := &xir.Constraint{}
	err := mapstructure.Decode(data, constraint)
	if err != nil {
		return nil
	}

	return constraint

}

// AllResourceIDs returns all the resource IDs in a network.
func AllResourceIDs(net *xir.Net) []string {

	var ids []string
	for _, x := range net.Nodes {
		ids = append(ids, x.Id)
	}
	for _, x := range net.Links {
		ids = append(ids, x.Id)
	}

	for _, x := range net.Nets {
		ids = append(ids, AllResourceIDs(x)...)
	}

	return ids

}
