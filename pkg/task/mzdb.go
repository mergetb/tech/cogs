package task

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"gitlab.com/mergetb/tech/cogs/pkg/common"

	etcd "github.com/coreos/etcd/clientv3"
	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/site/api"
)

// low level network objects ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// PortObj tracks various switch port properties.
type PortObj struct {
	Mzid string
	Host string
	Port string
	Mode PortMode
	Vids []int
	Up   bool

	version int64
}

// PortMode defines the VLAN mode of a port.
type PortMode int

const (
	// Access indicates VLAN access mode for a port
	Access PortMode = iota
	// Trunk indicates VLAN trunk mode for a port
	Trunk
)

// Key returns the datastore key for this object.
func (x *PortObj) Key() string {
	return fmt.Sprintf("/port/%s/%s", x.Host, x.Port)
}

// GetVersion returns the current datastore version of the object
func (x *PortObj) GetVersion() int64 { return x.version }

// SetVersion sets the current datastore version of the object
func (x *PortObj) SetVersion(v int64) { x.version = v }

// Value returns this object as an interface{}
func (x *PortObj) Value() interface{} { return x }

// VtepObj tracks VTEP parameters in the datastore
type VtepObj struct {
	Mzid string
	Host string
	Name string
	Vni  int
	Vid  int

	version int64
}

// Key returns the datastore key for this object.
func (x *VtepObj) Key() string {
	return fmt.Sprintf("/vtep/%s/%s", x.Host, x.Name)
}

// GetVersion returns the current datastore version of the object
func (x *VtepObj) GetVersion() int64 { return x.version }

// SetVersion sets the current datastore version of the object
func (x *VtepObj) SetVersion(v int64) { x.version = v }

// Value returns this object as an interface{}
func (x *VtepObj) Value() interface{} { return x }

// container ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// EnclaveVeth tracks information about virtual ethernet interfaces for infrapod
// enclaves.
type EnclaveVeth struct {
	Inner         string
	Outer         string
	Vni           int  `yaml:",omitempty"`
	Vid           int  `yaml:",omitempty"`
	Vindex        int  `yaml:",omitempty"`
	EvpnAdvertise bool `yaml:",omitempty"`
	Bridge        string
	Address       string
	HostMac       string `yaml:",omitempty"`
	CtrMac        string `yaml:",omitempty"`

	version int64
}

// Key returns the datastore key for this object.
func (x *EnclaveVeth) Key() string {
	return fmt.Sprintf("/eve/%d/%s", x.Vindex, x.Outer)
}

// GetVersion returns the current datastore version of the object
func (x *EnclaveVeth) GetVersion() int64 { return x.version }

// SetVersion sets the current datastore version of the object
func (x *EnclaveVeth) SetVersion(v int64) { x.version = v }

// Value returns this object as an interface{}
func (x *EnclaveVeth) Value() interface{} { return x }

// NsnatSpec tracks specification information about network namespace double
// NATs.
type NsnatSpec struct {
	Src     string
	Dst     string
	Addr    string
	Gateway string
}

// network namespace ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// VxLan Record
type VxLan struct {
	Vni        int
	Interfaces []string

	version int64
}

// Key returns the datastore key for this object.
func (x *VxLan) Key() string { return fmt.Sprintf("/vxlan/%d", x.Vni) }

// GetVersion returns the current datastore version of the object
func (x *VxLan) GetVersion() int64 { return x.version }

// SetVersion sets the current datastore version of the object
func (x *VxLan) SetVersion(v int64) { x.version = v }

// Value returns this object as an interface{}
func (x *VxLan) Value() interface{} { return x }

// RemoveInterface removes an interface from a VxLan record
func (x *VxLan) RemoveInterface(mac string) {

	var ifxs []string

	for _, i := range x.Interfaces {
		if i != mac {
			ifxs = append(ifxs, i)
		}
	}

	x.Interfaces = ifxs

}

// mzinfo ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// Key returns the datastore key for this object.
func (m *MzInfo) Key() string {
	return fmt.Sprintf("/mzinfo/%s/mzi", m.Mzid)
}

// GetVersion returns the current datastore version of the object
func (m *MzInfo) GetVersion() int64 { return m.ver }

// SetVersion sets the current datastore version of the object
func (m *MzInfo) SetVersion(v int64) { m.ver = v }

// Value returns this object as an interface{}
func (m *MzInfo) Value() interface{} { return m }

// linkinfo ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// Key returns the datastore key for this object.
func (l *LinkInfo) Key() string {
	return fmt.Sprintf("/mzinfo/%s/links/%s", l.Mzid, l.XpID)
}

// GetVersion returns the current datastore version of the object
func (l *LinkInfo) GetVersion() int64 { return l.ver }

// SetVersion sets the current datastore version of the object
func (l *LinkInfo) SetVersion(v int64) { l.ver = v }

// Value returns this object as an interface{}
func (l *LinkInfo) Value() interface{} { return l }

// ListLinkInfo lists all linkinfos for a mzid
func ListLinkInfos(mzid string) ([]*LinkInfo, error) {

	var result []*LinkInfo

	objsize := 0
	err := common.WithEtcd(func(c *etcd.Client) error {
		key := fmt.Sprintf("/mzinfo/%s/links", mzid)

		ctx, cancel := context.WithTimeout(context.TODO(), 3*time.Second)
		resp, err := c.Get(ctx, key, etcd.WithPrefix())
		cancel()
		if err != nil {
			return err
		}

		for _, kv := range resp.Kvs {
			mzi := &LinkInfo{}
			err := json.Unmarshal(kv.Value, mzi)
			if err != nil {
				return err
			}
			result = append(result, mzi)
			objsize += len([]byte(kv.Value))

		}
		return nil
	})
	if err != nil {
		return nil, err
	}

	log.Debugf("LinkInfo prefix Size: %d", objsize)
	return result, nil
}

// mz fragment ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// MzfIndex tracks materialization fragments.
type MzfIndex struct {
	Resource string
	Fragment *site.MzFragment

	ver int64
}

// Key returns the datastore key for this object.
func (x *MzfIndex) Key() string { return "/mzf/" + x.Resource }

// GetVersion returns the current datastore version of the object
func (x *MzfIndex) GetVersion() int64 { return x.ver }

// SetVersion sets the current datastore version of the object
func (x *MzfIndex) SetVersion(v int64) { x.ver = v }

// Value returns this object as an interface{}
func (x *MzfIndex) Value() interface{} { return x }

// helper functions ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// ListMaterializations lists all materializations with the given prefix.
func ListMaterializations(prefix string) ([]*MzInfo, error) {

	var result []*MzInfo
	var mziSuffix = "/mzi"

	objsize := 0
	err := common.WithEtcd(func(c *etcd.Client) error {

		key := fmt.Sprintf("/mzinfo/%s", prefix)

		ctx, cancel := context.WithTimeout(context.TODO(), 3*time.Second)
		resp, err := c.Get(ctx, key, etcd.WithPrefix())
		cancel()
		if err != nil {
			return err
		}

		for _, kv := range resp.Kvs {
			word := kv.Key
			// word always greater than 4, looking for keys with ending /mzi
			// for the mzinfo object
			suffix := string(word[len(word)-4:])
			if suffix == mziSuffix {
				mzi := &MzInfo{}
				err := json.Unmarshal(kv.Value, mzi)
				if err != nil {
					return err
				}
				result = append(result, mzi)
				objsize += len([]byte(kv.Value))
			}
		}
		return nil
	})
	if err != nil {
		return nil, err
	}

	log.Debugf("Mzinfo prefix Size: %d", objsize)
	return result, nil
}

// enclave ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// Key returns the datastore key for this object.
func (e *EnclaveSpec) Key() string {
	return fmt.Sprintf("/enclave/%s", e.Mzid)
}

// GetVersion returns the current datastore version of the object
func (e *EnclaveSpec) GetVersion() int64 { return e.ver }

// SetVersion sets the current datastore version of the object
func (e *EnclaveSpec) SetVersion(v int64) { e.ver = v }

// Value returns this object as an interface{}
func (e *EnclaveSpec) Value() interface{} { return e }

// vlmap ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// Key returns the datastore key for this object.
func (v *VLMap) Key() string {
	return fmt.Sprintf("/vlmap/%s", v.Mzid)
}

// GetVersion returns the current datastore version of the object
func (v *VLMap) GetVersion() int64 { return v.ver }

// SetVersion sets the current datastore version of the object
func (v *VLMap) SetVersion(x int64) { v.ver = x }

// Value returns this object as an interface{}
func (v *VLMap) Value() interface{} { return v }

// vlmap ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// Key returns the datastore key for this object.
func (n *NodeState) Key() string {
	return fmt.Sprintf("/nodestatus/%s", n.Name)
}

// GetVersion returns the current datastore version of the object
func (n *NodeState) GetVersion() int64 { return n.ver }

// SetVersion sets the current datastore version of the object
func (n *NodeState) SetVersion(x int64) { n.ver = x }

// Value returns this object as an interface{}
func (n *NodeState) Value() interface{} { return n }

// MoaEmulationSpec ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// Mzinfo

// Key returns the datastore key for this object.
func (ni *NodeInfo) Key() string {
	return fmt.Sprintf("/mzinfo/%s/nodes/%s", ni.Mzid, ni.Machine)
}

// GetVersion returns the current datastore version of the object
func (ni *NodeInfo) GetVersion() int64 { return ni.ver }

// SetVersion sets the current datastore version of the object
func (ni *NodeInfo) SetVersion(x int64) { ni.ver = x }

// Value returns this object as an interface{}
func (ni *NodeInfo) Value() interface{} { return ni }

// ListNodeInfo lists all linkinfos for a mzid
func ListNodeInfo(mzid string) ([]*NodeInfo, error) {

	var result []*NodeInfo

	objsize := 0
	err := common.WithEtcd(func(c *etcd.Client) error {
		key := fmt.Sprintf("/mzinfo/%s/nodes", mzid)

		ctx, cancel := context.WithTimeout(context.TODO(), 3*time.Second)
		resp, err := c.Get(ctx, key, etcd.WithPrefix())
		cancel()
		if err != nil {
			return err
		}

		for _, kv := range resp.Kvs {
			mzi := &NodeInfo{}
			err := json.Unmarshal(kv.Value, mzi)
			if err != nil {
				return err
			}
			result = append(result, mzi)
			objsize += len([]byte(kv.Value))

		}
		return nil
	})
	if err != nil {
		return nil, err
	}

	log.Debugf("NodeInfo prefix Size: %d", objsize)
	return result, nil
}
