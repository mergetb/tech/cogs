package task

import (
	"fmt"

	"github.com/opencontainers/runtime-spec/specs-go"
)

const (
	// DefaultMountType specifies the default way in which files and directories
	// are mounted into containers.
	DefaultMountType = "bind"
)

var (
	// DefaultMountOptions specifies the default options for file and directory
	// mounts for infrapod containers.
	DefaultMountOptions = []string{"rbind", "ro"}
)

// Container tracks container parameters in the datastore
type Container struct {
	Name      string
	Namespace string
	Vindex    int
	Image     string

	version int64
}

// Key returns the datastore key for this object.
func (x *Container) Key() string {
	return fmt.Sprintf("/ctr/%s/%s", x.Namespace, x.Name)
}

// GetVersion returns the current datastore version of the object
func (x *Container) GetVersion() int64 { return x.version }

// SetVersion sets the current datastore version of the object
func (x *Container) SetVersion(v int64) { x.version = v }

// Value returns this object as an interface{}
func (x *Container) Value() interface{} { return x }

// CtrComponent tracks low level containerd properties
type CtrComponent struct {
	CtrName      string `yaml:",omitempty"`
	CtrNamespace string `yaml:",omitempty"`
}

// ContainerMount tracks information about container mounts.
type ContainerMount struct {
	Name        string
	specs.Mount `mapstructure:",squash" yaml:",inline"`

	CtrComponent `mapstructure:",squash" yaml:",inline"`

	version int64
}

// Key returns the datastore key for this object.
func (x *ContainerMount) Key() string {
	return fmt.Sprintf("/ctr-mnt/%s/%s/%s", x.CtrNamespace, x.CtrName, x.Name)
}

// GetVersion returns the current datastore version of the object
func (x *ContainerMount) GetVersion() int64 { return x.version }

// SetVersion sets the current datastore version of the object
func (x *ContainerMount) SetVersion(v int64) { x.version = v }

// Value returns this object as an interface{}
func (x *ContainerMount) Value() interface{} { return x }

// ContainerSpec tracks information about containers.
type ContainerSpec struct {
	Container   `mapstructure:",squash" yaml:",inline"`
	Mounts      []*ContainerMount
	Environment map[string]string
}

// Env returns the environment of a container spec as a list of strings.
func (ctr *ContainerSpec) Env() []string {

	var result []string
	for k, v := range ctr.Environment {
		result = append(result, fmt.Sprintf("%s=%s", k, v))
	}
	return result

}
