package task

import (
	"fmt"

	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/tech/cogs/pkg/common"
	"gitlab.com/mergetb/xir/lang/go/v0.2"
	"gitlab.com/mergetb/xir/lang/go/v0.2/tb"
)

// An EnclaveSpec describes a network enclave in which an infrapod lives.
type EnclaveSpec struct {
	Mzid       string
	Vindex     int
	Interfaces []*EnclaveVeth
	SvcAddress string

	ver int64
}

// A InterfaceMacAdvSpec describes an interface to derive a type-2 EVPN (MACADV)
// advertisement from..
type InterfaceMacAdvSpec struct {
	Netns     string
	Interface string
	Vni       int
	Vindex    int
}

// A MacAdvSpec describes a type-2 EVPN (MACADV) advertisement.
type MacAdvSpec struct {
	Mac    string
	Vni    int
	Vindex int
	Router string
	Addr   string
	Asn    int
}

func InfranetLinkInfos(
	nodes []string, vni, vid int, mzid string, tbmodel *xir.Net) ([]*LinkInfo, error) {

	var result []*LinkInfo

	for _, x := range nodes {

		fields := log.Fields{"node": x}

		_, _, n := tbmodel.GetNode(x)
		if n == nil {
			return nil, common.ErrorF("node not found", fields)
		}

		r := tb.GetResourceSpec(n)
		if r == nil {
			return nil, common.ErrorF("resource not found", fields)
		}

		links := r.GetLinksByRole(tb.InfraLink)
		if len(links) == 0 {
			return nil, common.ErrorF("resource has no infralinks", fields)
		}
		if len(links) > 1 {
			log.WithFields(fields).Warn(
				"resource has more than 1 infralink, using first infralink")
		}

		result = append(result,
			&LinkInfo{
				Mzid: mzid,
				Kind: InfraLink,
				XpID: fmt.Sprintf("infranet/%s", x),
				TBVLinks: []*TBVLink{{
					Vni: vni,
					Vid: vid,
					Edges: []*EdgeInfo{{
						NodeName:      x,
						NodeInterface: links[0].Name,
					}},
				}},
			},
		)

	}

	return result, nil

}

func CreateInfranetLinkplan(nodes []string, mzi *MzInfo) (*Action, error) {

	linkinfos, err := InfranetLinkInfos(
		nodes, mzi.Vni, mzi.Vid, mzi.Mzid, common.TbModel())

	if err != nil {
		return nil, err
	}

	return &Action{
		Kind:       PlumbingKind,
		Mzid:       mzi.Mzid,
		MzInstance: mzi.Instance,
		Action:     PlanInfranet,
		Data:       linkinfos,
	}, nil

}
