package task

import (
	"encoding/binary"
	"fmt"
	"net"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/tech/cogs/pkg/common"
	"gitlab.com/mergetb/tech/foundry/api"
	"gitlab.com/mergetb/tech/nex/pkg"
)

// MzStatus indicates the overal status of a materialization
type MzStatus int

type Images struct {
	Node     *NodeImages
	Infrapod *InfrapodImages
}

type InfrapodImages struct {
	Belugactl string
	Moactl    string
	Foundry   string
	Etcd      string
	Nex       string
}

type NodeImages struct {
	Default string
}

const (
	// MzNone indicates that there is no materialization status
	MzNone MzStatus = iota
	// Materializing indicates that a materialization is in progress
	Materializing
	// Active indicates that all materialization tasks have finished
	Active
	// Dematerializing indicates a dematerialization is in progress
	Dematerializing
)

var infrapodImages = InfrapodImages{
	Belugactl: "docker.io/mergetb/belugactld:latest",
	Moactl:    "docker.io/mergetb/moactld:latest",
	Foundry:   "docker.io/mergetb/foundry:latest",
	Etcd:      "docker.io/mergetb/etcd:latest",
	Nex:       "docker.io/mergetb/nex:latest",
}

// DefaultImages is an exported variabled used by rex/sled.go to determine
// if the image contained within the node spec is different from the default
// image.  This is necessary for sledc's daemon mode which downloads by
// default the default image on demat, and on materialization must verify
// if it can kexec or if it needs to copy a new image down to sledc.
var DefaultImages = NodeImages{
	Default: "debian:10",
}

func SetImageConfig(cfg *Images) {

	if cfg != nil {

		if cfg.Infrapod != nil {

			if cfg.Infrapod.Belugactl != "" {
				infrapodImages.Belugactl = cfg.Infrapod.Belugactl
			}

			if cfg.Infrapod.Moactl != "" {
				infrapodImages.Moactl = cfg.Infrapod.Moactl
			}

			if cfg.Infrapod.Foundry != "" {
				infrapodImages.Foundry = cfg.Infrapod.Foundry
			}

			if cfg.Infrapod.Etcd != "" {
				infrapodImages.Etcd = cfg.Infrapod.Etcd
			}

			if cfg.Infrapod.Nex != "" {
				infrapodImages.Nex = cfg.Infrapod.Nex
			}
		}

		if cfg.Node != nil {

			if cfg.Node.Default != "" {
				DefaultImages.Default = cfg.Node.Default
			}

		}

	}

	log.Debugf("belugactl: %s", infrapodImages.Belugactl)
	log.Debugf("moactl:    %s", infrapodImages.Moactl)
	log.Debugf("foundry:   %s", infrapodImages.Foundry)
	log.Debugf("etcd:      %s", infrapodImages.Etcd)
	log.Debugf("nex:       %s", infrapodImages.Nex)
}

// String returns the string form of an MzStatus
func (s MzStatus) String() string {

	switch s {
	case MzNone:
		return "none"
	case Materializing:
		return "materializing"
	case Active:
		return "active"
	case Dematerializing:
		return "dematerializing"
	default:
		return "none"
	}

}

// NodeStateStatus tracks the current and target state for a testbed node
type NodeStateStatus struct {
	Current NodeStatus
	Target  NodeStatus
}

// NodeState tracks the state of individual testbed nodes.
type NodeState struct {
	Name   string
	Mzid   string
	Status NodeStateStatus

	ver int64
}

// NodeStatus indicates the state of a node.
type NodeStatus int

const (
	// NodeNone indicates that there is no available state for a node
	NodeNone NodeStatus = iota
	// Setup indicates a node is being setup
	Setup
	// Recycling indicates a node is being recycled
	Recycling
	// Ready indicates a node has been setup and is ready for use
	Ready
	// Clean indicates a node has been recycled
	Clean
)

func (s NodeStatus) String() string {
	switch s {
	case NodeNone:
		return "None"
	case Setup:
		return "Setup"
	case Recycling:
		return "Recycling"
	case Ready:
		return "Ready"
	case Clean:
		return "Clean"
	default:
		return "Unknown"
	}
}

// NodeInfo contains various information about a testbed node
type NodeInfo struct {
	Mzid    string // key under mzinfo
	Machine string // key under nodes

	XpName string
	Image  *ImageBinding
	Config *foundry.MachineConfig
	Nex    *nex.Member

	ver int64
}

// MzInfo contains various information about a materialization
type MzInfo struct {
	Mzid     string
	Instance string
	Vindex   int
	Vni      int
	Vid      int

	Status MzStatus

	IncomingTaskID string
	TeardownTaskID string

	NodeSetupTaskID   string
	NodeRecycleTaskID string

	LinkCreateTaskID  string
	LinkDestroyTaskID string

	ver int64
}

const (
	// InfrapodMznIfx is the name of the materialization interface in an infrapod
	InfrapodMznIfx = "ceth0"
	// InfrapodSvcIfx is the name of the service interface in an infrapod
	InfrapodSvcIfx = "ceth1"
	// DefaultInternalGateway is the infrapod default internal gateway
	DefaultInternalGateway = "172.30.0.1"
	// DefaultInternalGatewayCIDR is the infrapod default internal gateway CIDR
	DefaultInternalGatewayCIDR = "172.30.0.1/16"
	// DefaultServiceGateway is the default service gateway for an infrapod
	DefaultServiceGateway = "172.31.0.1"

	// HarborVni is the VXLAN VNI used for the harbor
	HarborVni = 2
	// HarborVid is the VLAN VID used for the harbor
	HarborVid = 2
	// MaxSystemVindex is the maximum value that can be used for system-level
	// virtual network indices.
	MaxSystemVindex = 100

	// VindexKey is the datastore key used to access the vindex counter
	VindexKey = "vindex"
	// InfraVniKey is the datastore key used to access the infrapod VXLAN VNI
	// counter
	InfraVniKey = "ivni"
	// InfraVidKey is the datastore key used to access the infrapod VLAN VID
	// counter
	InfraVidKey = "ivid"
	// XpVniKey is the datastore key used to access the experiment VXLAN VNI
	// counter
	XpVniKey = "xvni"
	// XpVidKey is the datastore key used to access the experiment VLAN VID
	// counter
	XpVidKey = "xvid"

	// HarborControlAddr is the address used to access harbor infrapod containers
	// from the host
	HarborControlAddr = "172.31.0.2"

	// HarborMzid is the materialization name of the harbor
	HarborMzid = "harbor.mergetb.io" //TODO XXX change to harbor.mergetb.io

	// MoaInterfaceType defines the type of interface moa is to use
	MoaInterfaceType = "xdpinterface"
	// MoaXDPMode defines the express data path mode Moa should use
	MoaXDPMode = "skb"

	svcbr                   = "svcbr"
	defaultInfraSubnet      = "172.30.0.0/16"
	defaultInfraGateway     = "172.30.0.1/16"
	defaultInfraSubnetBegin = "172.30.0.1"
	defaultInfraDhcpBegin   = "172.30.0.10"
	defaultInfraSubnetEnd   = "172.30.255.255"
	defaultSvcStart         = "172.31.0.10"
	defaultSvcPrefix        = 16
	defaultNetCounterStart  = 100
	defaultVidCounterStart  = 10
	vindexPoolSize          = 1 << 24
	vniPoolSize             = 1 << 24
	vidPoolSize             = 3000 - 10
)

const (
	// RetryWait is how long to wait for various gRPC operations
	RetryWait = 1 * time.Second
)

//getNetworkCounters provide a function to get a single network
//counter at a time
func getNetworkCounter(counter string) (int, common.CountSet, error) {

	var vset common.CountSet
	switch counter {
	case VindexKey:
		vset = common.CountSet{Name: VindexKey, Size: vindexPoolSize,
			Offset: defaultNetCounterStart}
	case InfraVniKey:
		vset = common.CountSet{Name: InfraVniKey, Size: vniPoolSize,
			Offset: defaultNetCounterStart}
	case InfraVidKey:
		vset = common.CountSet{Name: InfraVidKey, Size: vidPoolSize,
			Offset: defaultVidCounterStart}
	case XpVniKey:
		vset = common.CountSet{Name: XpVniKey, Size: vniPoolSize,
			Offset: defaultNetCounterStart}
	case XpVidKey:
		vset = common.CountSet{Name: XpVidKey, Size: vidPoolSize,
			Offset: defaultVidCounterStart}

	default:
		return -1, vset, common.Error("unknown network counter type requested")
	}

	_, err := common.ReadObjects([]common.Object{&vset})
	if err != nil {
		return -1, vset, common.ErrorE("vset read failed", err)
	}

	viNext, vi, err := vset.Add()
	if err != nil {
		return -1, vset, common.ErrorE("vset next failed", err)
	}

	return viNext, vi, nil
}

//TODO: retry mechanism for getting network counter
func mzNetRetry(mzinfo *MzInfo) (*MzInfo, error) {

	vindexNext, vindex, err := getNetworkCounter(VindexKey)
	if err != nil {
		return nil, common.ErrorE("vindex read error", err)
	}
	mzinfo.Vindex = vindexNext

	vniNext, vni, err := getNetworkCounter(InfraVniKey)
	if err != nil {
		return nil, common.ErrorE("vni read error", err)
	}
	mzinfo.Vni = vniNext

	vidNext, vid, err := getNetworkCounter(InfraVidKey)
	if err != nil {
		return nil, common.ErrorE("vid read error", err)
	}
	mzinfo.Vid = vidNext

	err = common.WriteObjects([]common.Object{&vindex, &vni, &vid, mzinfo}, true)
	if err != nil {
		mzinfo.Vindex = 0
		mzinfo.Vni = 0
		mzinfo.Vid = 0
		return nil, common.ErrorE("vset writeback error", err)
	}
	return mzinfo, nil
}

func GetMzInfo(mzid, instance string) (*MzInfo, error) {

	mzinfo := &MzInfo{Mzid: mzid}
	err := common.ReadNew(mzinfo)
	if err != nil {
		return nil, common.ErrorE("mzinfo read failure", err)
	}
	mzinfo.Instance = instance
	// if the info already exists just return it
	if mzinfo.ver > 0 {
		return mzinfo, nil
	}

	var mzi *MzInfo
	for i := 0; i < 20; i++ {
		mzi, err = mzNetRetry(mzinfo)
		if err == nil {
			return mzi, nil
		}
	}

	return nil, common.Error("unable to write mzinfo/net counter back")

}

func MzInfoRead(mzid string) (*MzInfo, error) {
	mzinfo := &MzInfo{
		Mzid:   mzid,
		Status: MzNone,
	}
	err := common.ReadNew(mzinfo)
	if err != nil {
		return nil, common.ErrorE("mzinfo read failure", err)
	}
	return mzinfo, err
}

func MzInfoWait(mzid, instance string) (*MzInfo, error) {

	mzinfo := &MzInfo{
		Mzid: mzid,
	}
	err := common.Read(mzinfo)
	if err != nil || mzinfo.Status == MzNone || mzinfo.Instance != instance {
		for i := 0; i < 10; i++ {
			time.Sleep(RetryWait)
			err = common.Read(mzinfo)
			if err == nil && mzinfo.Status != MzNone && mzinfo.Instance == instance {
				break
			}
		}
		if err != nil {
			return nil, common.ErrorE("mzinfo read failure", err)
		}
	}
	return mzinfo, err

}

// Del TODO
func (li *LinkInfo) Del() error {

	fields := log.Fields{
		"mzid": li.Mzid,
		"xpid": li.XpID,
	}

	log.WithFields(fields).Debug("remove link info")
	err := common.ReadNew(li)
	if err != nil {
		return common.ErrorE("linkinfo read failure", err)
	}
	// if the linkinfo does not exist nothing to do
	if li.ver == 0 {
		common.WarnF("linkinfo not found", fields)
		return nil
	}

	vni := common.CountSet{Name: XpVniKey, Size: vniPoolSize, Offset: 100}
	vid := common.CountSet{Name: XpVidKey, Size: vidPoolSize, Offset: 10}

	_, err = common.ReadObjects([]common.Object{&vni, &vid})
	if err != nil {
		return common.ErrorE("vni/vid read failed", err)
	}

	for _, vl := range li.TBVLinks {
		vni = vni.Remove(vl.Vni)
		vid = vid.Remove(vl.Vid)
	}

	// update everything in one shot txn
	otx := common.ObjectTx{
		Put:    []common.Object{&vni, &vid},
		Delete: []common.Object{li},
	}
	err = common.RunObjectTx(otx)
	if err != nil {
		return common.ErrorE("vni/vid/linkinfo writeback failed", err)
	}

	return nil

}

// Init a link info object by allocating virtual link ids.
func (li *LinkInfo) Init() error {

	err := common.ReadNew(li)
	if err != nil {
		return common.ErrorE("linkinfo read failure", err)
	}
	// if the info already exists just return
	if li.ver > 0 {
		return nil
	}

	for i := 0; i < 20; i++ {
		err = li.AssignIDs()
		if err == nil {
			return nil
		}
	}

	return common.Error("linkinfo failed to writeback net counters")

}

// AssignIDs TODO
func (li *LinkInfo) AssignIDs() error {

	vni := common.CountSet{Name: XpVniKey, Size: vniPoolSize, Offset: 100}
	vid := common.CountSet{Name: XpVidKey, Size: vidPoolSize, Offset: 10}
	vniNext := 0
	vidNext := 0

	_, err := common.ReadObjects([]common.Object{&vni, &vid})
	if err != nil {
		return common.ErrorE("vni/vid read failed", err)
	}

	for _, vl := range li.TBVLinks {

		vniNext, vni, err = vni.Add()
		if err != nil {
			return common.ErrorE("vni next failed", err)
		}

		vidNext, vid, err = vid.Add()
		if err != nil {
			return common.ErrorE("vid next failed", err)
		}

		vl.Vni = vniNext
		vl.Vid = vidNext

	}

	err = common.WriteObjects([]common.Object{&vni, &vid, li}, true)
	if err != nil {
		return common.ErrorE("vni/vid writeback failed", err)
	}
	return nil
}

// SvcCidr returns the CIDR of a service given the associated virtual network
// index.
func SvcCidr(vindex int) string {

	return fmt.Sprintf("%s/%d", svcAddr(vindex).String(), defaultSvcPrefix)

}

// SvcAddress returns the IP address of a CIDR given the associated virtual
// network index.
func SvcAddress(vindex int) string {

	return svcAddr(vindex).String()

}

func svcAddr(vindex int) net.IP {

	begin := binary.BigEndian.Uint32([]byte(net.ParseIP(defaultSvcStart).To4()))
	next := begin + uint32(vindex)

	buf := make([]byte, 4)
	binary.BigEndian.PutUint32(buf, next)
	return net.IP(buf)

}

func mzbr(index int) string   { return fmt.Sprintf("mzbr%d", index) }
func ifr(index int) string    { return fmt.Sprintf("ifr%d", index) }
func svceth(index int) string { return fmt.Sprintf("svc%d", index) }
