package task

import (
	"encoding/json"
	"github.com/mitchellh/mapstructure"
	log "github.com/sirupsen/logrus"

	"gitlab.com/mergetb/site/api"
	"gitlab.com/mergetb/tech/cogs/pkg/common"
	"gitlab.com/mergetb/tech/foundry/api"
	"gitlab.com/mergetb/xir/lang/go/v0.2"
	"gitlab.com/mergetb/xir/lang/go/v0.2/tb"
)

const (
	// FoundryCertMountName is the name of the foundry TLS cert container mount
	// point
	FoundryCertMountName = "foundry-cert"
	// FoundryCertMountSource is the host location of the foundry TLS cert
	FoundryCertMountSource = "/etc/foundry/manage.pem"
	// FoundryCertMountDest is the container filesystem location of the foundry
	// TLS cert
	FoundryCertMountDest = "/etc/foundry/manage.pem"
	// FoundryKeyMountName is the name of the foundry TLS key container mount
	// point
	FoundryKeyMountName = "foundry-key"
	// FoundryKeyMountSource is the host location of the foundry TLS key
	FoundryKeyMountSource = "/etc/foundry/manage-key.pem"
	// FoundryKeyMountDest is the container filesystem location of the foundry TLS
	// key
	FoundryKeyMountDest = "/etc/foundry/manage-key.pem"
	// FoundryEnv is the environment variable used to pass args to foundry
	FoundryEnv = "FOUNDRYD_ARGS"
	// FoundryArgs are the args passed to the foundry infrapod container
	FoundryArgs = "-maxsize 512"
)

// extract machine configuration infromation from merge experiment specs
func machineConfig(
	mzid string,
	model *site.NodeModel,
	tbnode *xir.Node,
) (*foundry.MachineConfig, error) {

	data := props(model.Model)

	// extract the base machine config
	mc, err := foundryConfig(data)
	if err != nil {
		return nil, err
	}

	// configure the node's interfaces
	err = ifconfig(mzid, mc, model, tbnode)
	if err != nil {
		return nil, err
	}

	if mc == nil || mc.Config == nil {
		return nil, common.ErrorEF("failed to read linkinfo", err,
			log.Fields{"machineconfig": mc})
	}

	// expand rootfs by default
	mc.Config.ExpandRootfs = true

	return mc, nil

}

func ifconfig(mzid string, cfg *foundry.MachineConfig, model *site.NodeModel,
	tbnode *xir.Node) error {

	ifmap := make(map[string][]*site.InterfaceModel)
	for _, ifx := range model.Interfaces {
		ifmap[ifx.Phyid] = append(ifmap[ifx.Phyid], ifx)
	}

	for phy, ifxs := range ifmap {

		ep := tbnode.GetEndpointById(phy)
		if ep == nil {
			return common.ErrorF("endpoint not found", log.Fields{
				"id": phy,
			})
		}

		lis := make([]common.Object, 0)
		vMap := make(map[string]*foundry.Interface)

		for _, ifx := range ifxs {
			// if there is more than one xp-interface per physical interface, need to
			// break them out over vlans
			if len(ifxs) > 1 {
				lis = append(lis, &LinkInfo{Mzid: mzid, XpID: ifx.Vlid})
			}

			fi := &foundry.Interface{Name: ep.Label()}
			// pickup any base configuration from the interface model
			model := props(ifx.Model)
			bc := basicIfxConfig(model)
			if bc != nil {
				fi.Addrs = bc.Ip.Addrs
				fi.Mtu = uint32(bc.Ip.Mtu)
			}

			vMap[ifx.Vlid] = fi
		}

		// do our bulk read for all interfaces
		if len(lis) > 0 {
			// fetch the testbed link info for the vlid
			err := common.ReadWaitObjects(lis, nil)
			if err != nil {
				return common.ErrorEF("failed to read linkinfo", err, log.Fields{
					"linkObjs": lis,
				})
			}

			// now we can add the vlan to the link
			for _, link := range lis {
				li := link.(*LinkInfo)

				vl := li.EdgeLink(tbnode.Label())
				if vl == nil {
					return common.ErrorF("linkinfo has no record of this node", log.Fields{
						"node": tbnode.Label(),
					})
				}

				vMap[li.XpID].Vlan = &foundry.Vlan{Vid: int32(vl.Vid)}
			}
		}

		for _, fi := range vMap {
			cfg.Config.Interfaces = append(cfg.Config.Interfaces, fi)
		}
	}

	return nil
}

func foundryConfig(data map[string]interface{}) (*foundry.MachineConfig, error) {

	info, ok := data["foundry"]
	if !ok {
		return nil, common.Error("missing foundry data")
	}

	if info == nil {
		return nil, common.Error("foundry config is nil")
	}

	cfg := &foundry.MachineConfig{}
	err := mapstructure.Decode(info, cfg)
	if err != nil {
		return nil, common.ErrorE("failed to parse foundry config", err)
	}

	return cfg, nil

}

func basicIfxConfig(data map[string]interface{}) *tb.BasicInterfaceConfig {

	cfg := &tb.BasicInterfaceConfig{}
	err := mapstructure.Decode(data, cfg)
	if err != nil {
		common.WarnE("no basic interface config found: %v", err)
		return nil
	}

	if cfg == nil {
		common.Warn("interface config nil")
		return nil
	}

	return cfg

}

func props(data string) map[string]interface{} {

	m := make(map[string]interface{})
	err := json.Unmarshal([]byte(data), &m)
	if err != nil {
		return nil
	}

	return m

}
