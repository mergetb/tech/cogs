package task

import (
	"fmt"
	"gitlab.com/mergetb/xir/lang/go/v0.2"
)

// LinkInfo maps a virtual link id in the experiment space e.g., an identifier
// for a link in an experiment onto a set of virtual and physical testbed links.
// Core links are physical testbed links that interconnect edge links.
type LinkInfo struct {
	Mzid     string
	Vindex   int
	Kind     LinkKind
	XpID     string     // virtual link identifier from merge
	Props    xir.Props  // User defined link properties
	TBVLinks []*TBVLink // A list of testbed virtual links
	Core     []string   // Non-edge link resource ids

	ver int64
}

// TBVLink represents a virtual testbed link in terms of the underlying physical
// links it's constructed from. Edge links are physical testbed links that
// connect directly to end-hosts.
type TBVLink struct {
	Vni            int
	Vid            int
	Edges          []*EdgeInfo
	Advertisements []string
}

// EdgeInfo contains information about the node at the edge of a link
type EdgeInfo struct {
	Resource      string `json:",omitempty"`
	Mac           string `json:",omitempty"`
	NodeName      string `json:",omitempty"`
	NodeInterface string `json:",omitempty"`
	Multidegree   bool   `json:",omitempty"`
}

// LinkInfo Methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// EdgeLink gets the edge link for a specified node
func (li *LinkInfo) EdgeLink(name string) *TBVLink {

	for _, l := range li.TBVLinks {
		for _, e := range l.Edges {
			if e.NodeName == name {
				return l
			}
		}
	}

	return nil

}

func (li *LinkInfo) Edges() []*EdgeInfo {
	var es []*EdgeInfo
	for _, x := range li.TBVLinks {
		es = append(es, x.Edges...)
	}
	return es
}

func (e *EdgeInfo) String() string {
	return fmt.Sprintf("%s:%s", e.NodeName, e.NodeInterface)
}

type LinkKind string

const (
	// InfraLink is an infrastructure link
	InfraLink LinkKind = "infra"

	// XpLink is an experiment link
	XpLink LinkKind = "xp"

	// MoaLink is a Moa emulated link
	MoaLink LinkKind = "moa"
)

func ParseLinkKind(s string) (LinkKind, error) {

	switch LinkKind(s) {
	case InfraLink:
		return InfraLink, nil
	case XpLink:
		return XpLink, nil
	case MoaLink:
		return MoaLink, nil
	}

	return LinkKind(""), fmt.Errorf("unknown link kind '%s'", s)

}
