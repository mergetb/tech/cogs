package task

import (
	"fmt"
	"strings"

	log "github.com/sirupsen/logrus"

	"gitlab.com/mergetb/tech/cogs/pkg/common"
	"gitlab.com/mergetb/tech/sled/pkg"
	"gitlab.com/mergetb/xir/lang/go/v0.2"
	"gitlab.com/mergetb/xir/lang/go/v0.2/sys"
	"gitlab.com/mergetb/xir/lang/go/v0.2/tb"
)

func userImageData(model map[string]interface{}, node *xir.Node) (*ImageBinding, error) {

	imageConstraint := ExtractConstraint(model, "image")
	if imageConstraint == nil {
		return imageSpecFromName(DefaultImages.Default, node)
	}

	imageValue, ok := imageConstraint.Value.(string)
	if !ok {
		return imageSpecFromName(DefaultImages.Default, node)
	}

	return imageSpecFromName(imageValue, node)
}

func imageSpecFromName(name string, node *xir.Node) (*ImageBinding, error) {

	fields := log.Fields{
		"name": name,
		"node": node.Label(),
	}

	parts := strings.Split(name, ":")
	if len(parts) != 2 {
		return nil, common.ErrorF(
			"invalid image name, must be <name>:<version>", log.Fields{
				"name": name,
			})
	}

	variant := ""
	r := tb.GetResourceSpec(node)
	if r != nil && r.ImageVariant != "" {
		variant = fmt.Sprintf("%s-", r.ImageVariant)
	}

	sysinfo := sys.Sys(node)
	if sysinfo == nil {
		return nil, common.ErrorF("failed to get system information for node", fields)
	}

	if sysinfo.OS == nil {
		return nil, common.ErrorF("failed to get OS info for node", fields)
	}

	if sysinfo.OS.Config == nil {
		return nil, common.ErrorF("failed to get OS config for node", fields)
	}

	os, version := parts[0], parts[1]

	imgBind := &ImageBinding{
		Image: &sled.Image{
			Name:        name,
			Kernel:      fmt.Sprintf("%s-%s-%skernel", os, version, variant),
			Initramfs:   fmt.Sprintf("%s-%s-%sinitramfs", os, version, variant),
			Disk:        fmt.Sprintf("%s-%s-%sdisk", os, version, variant),
			Append:      sysinfo.OS.Config.Append,
			Blockdevice: sysinfo.OS.Config.Rootdev,
		},
	}

	log.Debugf("Image: %#v", imgBind.Image)
	return imgBind, nil

}

// ImageDefaults contains default image information for a node type.
type ImageDefaults struct {
	Image sled.Image
	ver   int64
}

// Key returns the datastore key for this object.
func (d *ImageDefaults) Key() string { return "/defaults/image" }

// GetVersion returns the current datastore version of the object
func (d *ImageDefaults) GetVersion() int64 { return d.ver }

// SetVersion sets the current datastore version of the object
func (d *ImageDefaults) SetVersion(v int64) { d.ver = v }

// Value returns this object as an interface{}
func (d *ImageDefaults) Value() interface{} { return d }
