package task

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"reflect"
	"runtime/debug"
	"sort"
	"time"

	etcd "github.com/coreos/etcd/clientv3"
	"github.com/teris-io/shortid"
	"gitlab.com/mergetb/tech/cogs/pkg/common"

	//https://github.com/go-yaml/yaml/issues/139
	"github.com/mergetb/yaml/v3"

	log "github.com/sirupsen/logrus"
)

const (
	// TimeFormat is the format used by the cogs in logs.
	TimeFormat = "2 Jan 06 15:04:05.00 MST"
	// IDAlphabet is the alphabet used for task UUIDs
	IDAlphabet = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_@"

	// TaskPrefix string constant
	taskPrefix = "task"
	// StagePrefix string constant
	stagePrefix = "stage"
	// ActionPrefix string constant
	actionPrefix = "action"
	// ActionStatusPrefix string constant
	actionStatusPrefix = "status/action"
	// ActionReadyPrefix string constant
	actionReadyPrefix = "status/ready"
	// ActionErrorPrefix string constant
	actionErrorPrefix = "status/error"
)

// A Task is a collecion of stages that are executed in sequence by a coglet.
type Task struct {
	Mzid      string   `yaml:",omitempty"`
	ID        string   `yaml:",omitempty"`
	Instance  string   `yaml:",omitempty"`
	Stages    []string `yaml:",omitempty"`
	Deps      []string `yaml:",omitempty"`
	Timestamp string   `yaml:",omitempty"`

	version int64
}

// Key returns the datastore key for this object.
func (t *Task) Key() string {
	return fmt.Sprintf("/%s/%s/%s", taskPrefix, t.Mzid, t.ID)
}

// GetVersion returns the current datastore version of the object
func (t *Task) GetVersion() int64 { return t.version }

// SetVersion sets the current datastore version of the object
func (t *Task) SetVersion(v int64) { t.version = v }

// Value returns this object as an interface{}
func (t *Task) Value() interface{} { return t }

// AddDep adds a dependency to a task. The id argument must be a UUID for an
// existing task. Once added, this task will be ignored by coglets until the
// specified UUID has completed successfully, or is masked.
func (t *Task) AddDep(id string) {
	for _, x := range t.Deps {
		if x == id {
			return
		}
	}
	t.Deps = append(t.Deps, id)
}

// Complete returns if the task is complete. A task is considered complete if
// all its actions are either complete or masked.
// this function assumes GetMzidAS(task.Mzid, task.ID) as input
func (t *Task) Complete(actionstatuses map[string]*ActionStatus) (bool, error) {

	// check if the map is nil or has elements
	if actionstatuses == nil {
		return false, ErrASMapNil
	}

	if len(actionstatuses) == 0 {
		return false, ErrASMapEmpty
	}

	for _, as := range actionstatuses {
		if !as.Complete && !as.Masked {
			log.Tracef("non-complete: %s: %v", as.ID, as)
			return false, nil
		}
	}

	return true, nil
}

// Incomplete returns all incomplete actionstatuses
// this function assumes GetMzidAS(task.Mzid, task.ID) as input
func (t *Task) Incomplete(actionstatuses map[string]*ActionStatus) ([]*ActionStatus, error) {

	// check if the map is nil or has elements
	if actionstatuses == nil {
		return nil, ErrASMapNil
	}

	if len(actionstatuses) == 0 {
		return nil, ErrASMapEmpty
	}

	var incomplete []*ActionStatus
	for _, as := range actionstatuses {
		if !as.Complete && !as.Masked {
			incomplete = append(incomplete, as)
		}
	}

	return incomplete, nil
}

// Failed returns if a task has failed.
// this function assumes GetMzidAS(task.Mzid, task.ID) as input
func (t *Task) Failed(actionstatuses map[string]*ActionStatus) ([]*ActionStatus, error) {

	var result []*ActionStatus

	// check if the map is nil or has elements
	if actionstatuses == nil {
		return result, ErrASMapNil
	}

	if len(actionstatuses) == 0 {
		return result, ErrASMapEmpty
	}

	for _, as := range actionstatuses {
		if as.Error != nil && !as.Masked {
			result = append(result, as)
		}
	}

	return result, nil
}

// ClearErrors clears all errors from the underlying actions in a task. This
// means that it will become elegible for execution by a coglet again.
// this function assumes GetMzidAS(task.Mzid, task.ID) as input
func (t *Task) ClearErrors(actionstatuses map[string]*ActionStatus) error {

	// check if the map is nil or has elements
	if actionstatuses == nil {
		return ErrASMapNil
	}

	if len(actionstatuses) == 0 {
		return ErrASMapEmpty
	}

	changeList := make([]common.Object, 0)
	delList := make([]common.Object, 0)
	for _, as := range actionstatuses {
		// only add an item to the change list if its error is non-nil
		if as.Error != nil {
			as.Error = nil

			// here we update the status of the aciton
			changeList = append(changeList, as)
			// but we need to put it into the ready queue
			changeList = append(changeList, as.ToReady())

			// and also remove the status from the error queue
			delList = append(delList, as.ToError())
		}
	}

	// only send the requests if there are actual items that need to be written
	if len(changeList) > 0 {
		otx := common.ObjectTx{
			Put:    changeList,
			Delete: delList,
		}
		err := common.RunObjectTx(otx)
		if err != nil {
			return common.ErrorEF("failed to clear errors", err,
				log.Fields{"actions": changeList},
			)
		}
	}

	return nil
}

// Reset marks all actions in a task as incomplete and clears any errors.
// this function assumes GetMzidAS(task.Mzid, task.ID) as input
func (t *Task) Reset(actionstatuses map[string]*ActionStatus) error {

	// check if the map is nil or has elements
	if actionstatuses == nil {
		return ErrASMapNil
	}

	if len(actionstatuses) == 0 {
		return ErrASMapEmpty
	}

	changeList := make([]common.Object, 0)
	delList := make([]common.Object, 0)
	for _, as := range actionstatuses {
		if as.Error != nil {
			// need to remove error ridden actions from error queue
			delList = append(delList, as.ToError())
		}
		as.Masked = false
		as.Complete = false
		as.Error = nil
		// update the action status
		changeList = append(changeList, as)
		// we also need to put the actions back in the ready queue
		changeList = append(changeList, as.ToReady())
	}

	// only send the requests if there are actual items that need to be written
	if len(changeList) > 0 {
		otx := common.ObjectTx{
			Put:    changeList,
			Delete: delList,
		}
		err := common.RunObjectTx(otx)
		if err != nil {
			return common.ErrorEF("failed to reset actions", err,
				log.Fields{"actions": changeList})
		}
	}

	return nil
}

// SetComplete sets all the action statuses for a task to complete
// this function assumes GetMzidAS(task.Mzid, task.ID) as input
func (t *Task) SetComplete() error {

	actions, err := GetMzidActions(t.Mzid, t.ID)
	if err != nil {
		return common.ErrorEF("Failed to get Tasks actions", err, log.Fields{"task": t.ID})
	}

	for _, a := range actions {
		err := a.SetActionComplete()
		if err != nil {
			return common.ErrorEF("Failed to set complete", err,
				log.Fields{"task": t.ID, "action": a},
			)
		}
	}

	return nil
}

// DepsSatisfied calculates if all dependencies of a task are satisfied
// it returns those statuses which are not, or empty if there are none
// this function assumes GetMzidAS(task.Mzid, task.ID) as input
func (t *Task) DepsSatisfied() ([]*ActionStatus, error) {

	var asList []*ActionStatus
	for _, id := range t.Deps {
		dep := &Task{Mzid: t.Mzid, ID: id}

		err := common.Read(dep)
		if err != nil {
			log.WithFields(log.Fields{"task": t.ID, "dep": id}).Warn(
				"failed to read task dependency")
			continue
		}

		actions, err := GetMzidAS(dep.Mzid, dep.ID)
		if err != nil {
			return nil, err
		}

		if len(actions) > 0 {
			// get a list of actions which require completion
			// before this task can begin
			notfinished, err := dep.Incomplete(actions)
			if err != nil {
				return nil, err
			}
			asList = append(asList, notfinished...)

		} else {
			fields := log.Fields{
				"dependency task": dep,
			}
			common.WarnF("task without actions", fields)
		}
	}
	return asList, nil
}

// Wait blocks until a task is either complete or failed.
// this function assumes GetMzidAS(task.Mzid, task.ID) as input
func (t *Task) Wait() error {
	complete := false
	var failed []*ActionStatus

	for !complete && failed == nil {
		actionMap, err := GetMzidAS(t.Mzid, t.ID)
		if err != nil {
			return err
		}

		failed, err = t.Failed(actionMap)
		if err != nil {
			return err
		}

		complete, err = t.Complete(actionMap)
		if err != nil {
			return err
		}

		time.Sleep(250 * time.Millisecond)
	}

	return nil
}

func addMzidDeps(task *Task) error {
	tasks, err := EtcdTaskPrefix(task.Mzid)
	if err != nil {
		return err
	}
	for _, t := range tasks {
		actionMap, err := GetMzidAS(t.Mzid, t.ID)
		if err != nil {
			return err
		}

		failed, err := t.Failed(actionMap)
		if err != nil && err != ErrASMapEmpty {
			return err
		}

		complete, err := t.Complete(actionMap)
		if err != nil && err != ErrASMapEmpty {
			return err
		}

		if !complete || failed != nil {
			task.AddDep(t.ID)
		}
	}
	return nil
}

// ListTasks all tasks for a materialization
func ListTasks(mzid string) ([]*Task, error) {
	tasks, err := EtcdTaskPrefix(mzid)
	if err != nil {
		return nil, err
	}
	return tasks, nil
}

// DeleteTasks removes everything related to the mzid (tasks, stages, actions, etc)
func DeleteTasks(mzid string) error {
	if mzid == "" {
		return common.Error("DeleteTasks disallows empty mzid")
	}

	// because this gets called in book keeping, we also need the status in
	// the ready queue to be removed.  And for whatever reason, the tasks
	// in the error queue.  In order to do that, we need to find the actions
	// for this mzid, and remove them manually

	// this should be a transaction, delete everything or keep everything
	delList := make([]common.Object, 0)

	tasks, err := EtcdTaskPrefix(mzid)
	if err != nil {
		return err
	}
	for _, task := range tasks {
		delList = append(delList, task)
		stages, err := GetTaskStages(task.Mzid, task.ID)
		if err != nil {
			return err
		}
		for _, stage := range stages {
			delList = append(delList, stage)
			for _, actionid := range stage.Actions {
				action := &Action{
					Mzid:   mzid,
					Taskid: task.ID,
					ID:     actionid,
				}
				delList = append(delList, action)
				as := action.ToStatus()
				delList = append(delList, as)
			}
		}
		// before we delete, we need to look through to see if we need
		// to delete anything from the active or error queues
		ases, err := GetMzidAS(task.Mzid, task.ID)
		if err != nil {
			return err
		}
		for _, as := range ases {
			if !as.Complete {
				delList = append(delList, as.ToReady())
			}
			if as.Error != nil {
				delList = append(delList, as.ToError())
			}
		}
	}

	return common.DeleteObjects(delList)
}

// NewTask takes an mzid, and stages and returns the new task
// It will also write out all the stages and actions to etcd
// and is responsible for initializing both stages and actions
func NewTask(mzid, instance string, stages ...*StageWithActions) (*Task, *[]common.Object, error) {

	task := &Task{
		Mzid:     mzid,
		Instance: instance,
	}

	// we are going to write a lot of objects back
	writes := make([]common.Object, 0)

	// create a new ID for the task
	id, err := GenerateID()
	if err != nil {
		return nil, nil, err
	}
	task.ID = id

	// create timestamp for when the new task was created
	task.Timestamp = time.Now().Format(TimeFormat)

	task.Stages = make([]string, 0)

	// order matters for each of these stages, so we
	// write them out now into etcd before memory changes
	for order, stage := range stages {
		stage.Stage.Order = order

		// create a new ID for the stage
		stage.Stage.ID, err = GenerateID()
		if err != nil {
			return nil, nil, err
		}

		stage.Stage.Taskid = task.ID
		stage.Stage.Mzid = task.Mzid
		task.Stages = append(task.Stages, stage.Stage.ID)

		// we'll need to create action ids and append
		// to this list for tracking
		stage.Stage.Actions = make([]string, 0)

		// for each action associated with a stage, we
		// need to create an ID and also write to etcd
		for _, a := range stage.Actions {

			// create a new ID for the action
			a.ID, err = GenerateID()
			if err != nil {
				return nil, nil, err
			}
			a.Stageid = stage.Stage.ID
			a.Taskid = task.ID

			// we need to create a status for our actions
			as := &ActionStatus{
				ID:     a.ID,
				Taskid: a.Taskid,
				Mzid:   a.Mzid,
			}

			// separately, we need to add this to the ready queue
			asr := as.ToReady()

			writes = append(writes, a)
			writes = append(writes, as)
			writes = append(writes, asr)

			// add action to stage list
			stage.Stage.Actions = append(stage.Stage.Actions, a.ID)
		}

		// we've update the stage, now we need to add to write
		writes = append(writes, stage.Stage)
	}

	return task, &writes, nil
}

// LaunchTask launches the specified task
func LaunchTask(task *Task, taskObjects *[]common.Object) error {
	// check for existing tasks with this mzid
	err := addMzidDeps(task)
	if err != nil {
		return err
	}

	*taskObjects = append(*taskObjects, task)

	// write out task now
	return common.WriteObjects(*taskObjects, false)
}

// CancelTask removes all data attached to a task
func CancelTask(mzid, taskid string) error {
	t := &Task{ID: taskid, Mzid: mzid}
	err := common.Read(t)
	if err != nil {
		return err
	}
	return t.Cancel()
}

// Cancel cancels the tasks with the specified prefix as well as the attached
// action statuses
func (t *Task) Cancel() error {

	var statusObjs []common.Object
	actionstatuses, err := GetMzidAS(t.Mzid, t.ID)
	if err != nil {
		return err
	}

	// get all actions and action statuses to clean out
	for _, as := range actionstatuses {
		// if the action status is not complete or has an error,
		// we need to also remove those from action ready/error queue
		if !as.Complete {
			statusObjs = append(statusObjs, as.ToReady())
		}
		if as.Error != nil {
			statusObjs = append(statusObjs, as.ToError())
		}
		action := &ActionStatus{
			Mzid:   as.Mzid,
			Taskid: as.Taskid,
			ID:     as.ID,
		}
		statusObjs = append(statusObjs, as)
		statusObjs = append(statusObjs, action)
	}

	stages, err := GetTaskStages(t.Mzid, t.ID)
	if err != nil {
		return err
	}

	// get all stages attached to this task
	for _, stage := range stages {
		statusObjs = append(statusObjs, stage)
	}

	err = common.DeleteObjects(statusObjs)
	if err != nil {
		return err
	}

	return common.Delete(t)

}

// ExtractTask takes a json string an unmarshals it into a Task object
func ExtractTask(taskJSON string) (*Task, error) {
	t := &Task{}
	err := json.Unmarshal([]byte(taskJSON), t)
	if err != nil {
		return nil, err
	}
	return t, nil
}

///////////////////////////////////////////////////////////////////////////////////
//////////////////////////////  STAGES  ///////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////

// GetStage retrieves stage from etcd based on ID
func GetStage(mzid, taskid, stageid string) (*Stage, error) {
	stage := &Stage{
		Mzid:   mzid,
		Taskid: taskid,
		ID:     stageid,
	}
	err := common.Read(stage)
	return stage, err
}

// StageWithActions holds the array of actions
type StageWithActions struct {
	Stage   *Stage
	Actions []*Action
}

// AddActions to a StageWithAction struct
func (s *StageWithActions) AddActions(actions ...*Action) {
	s.Actions = append(s.Actions, actions...)
}

// A Stage is a collection of actions. Actions within a stage are executed
// concurrently.
type Stage struct {
	Mzid    string   `yaml:",omitempty"`
	Taskid  string   `yaml:",omitempty"`
	ID      string   `yaml:",omitempty"`
	Order   int      `yaml:",omitempty"` // instead of linked list implementation, we just know where we should be
	Actions []string `yaml:",omitempty"`

	version int64
}

// Key returns the datastore key for this object.
func (s *Stage) Key() string {
	return fmt.Sprintf("/%s/%s/%s/%s", stagePrefix, s.Mzid, s.Taskid, s.ID)
}

// GetVersion returns the current datastore version of the object
func (s *Stage) GetVersion() int64 { return s.version }

// SetVersion sets the current datastore version of the object
func (s *Stage) SetVersion(v int64) { s.version = v }

// Value returns this object as an interface{}
func (s *Stage) Value() interface{} { return s }

// NewStage creates a new stage.
func NewStage(actions ...*Action) *StageWithActions {
	return &StageWithActions{
		Stage:   &Stage{},
		Actions: actions,
	}
}

// GetTaskStagesMap return all stages associated with a task
func GetTaskStagesMap(mzid, taskid string) (map[string]*Stage, error) {

	key := fmt.Sprintf("%s/%s", mzid, taskid)
	stages, err := EtcdStagePrefix(key)
	if err != nil {
		return nil, err
	}

	stageMap := make(map[string]*Stage)
	for _, x := range stages {
		stageMap[x.ID] = x
	}

	return stageMap, nil
}

// GetTaskStages return all stages associated with a task
func GetTaskStages(mzid, taskid string) ([]*Stage, error) {

	key := fmt.Sprintf("%s/%s", mzid, taskid)
	stages, err := EtcdStagePrefix(key)
	if err != nil {
		return nil, err
	}

	// return the sorted list of stages by their order
	sort.Slice(stages[:], func(i, j int) bool {
		return stages[i].Order < stages[j].Order
	})

	return stages, nil
}

// ExtractStage takes a json string an unmarshals it into a Stage object
func ExtractStage(stageJSON string) (*Stage, error) {
	t := &Stage{}
	err := json.Unmarshal([]byte(stageJSON), t)
	if err != nil {
		return nil, err
	}
	return t, nil
}

///////////////////////////////////////////////////////////////////////////////////
//////////////////////////////  ACTIONS  //////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////

// MzOperation indicates the type of a materialization operation an action is a
// part of.
type MzOperation string

const (
	// MzOutgoing indicates a dematerialization process.
	MzOutgoing MzOperation = "outgoing"
	// MzIncoming indicates a materialization process.
	MzIncoming MzOperation = "incoming"
	// MzMod indicates that a materialization is being modified.
	MzMod MzOperation = "mod"
)

// An Action is the basic unit of coglet execution. It defines a single task
// that a coglet thread executes until completion or failure.
// TODO: Remove from Etcd
type Action struct {
	Kind       string      `yaml:",omitempty"`
	Mzid       string      `yaml:",omitempty"`
	MzOp       MzOperation `yaml:",omitempty"`
	MzInstance string      `yaml:",omitempty"`
	Action     string      `yaml:",omitempty"`
	Data       interface{} `yaml:",omitempty"`
	ID         string      `yaml:",omitempty"`
	Taskid     string      `yaml:",omitempty"`
	Stageid    string      `yaml:",omitempty"`

	version int64
}

// Key returns the datastore key for this object.
func (a *Action) Key() string {
	return fmt.Sprintf("/%s/%s/%s/%s", actionPrefix, a.Mzid, a.Taskid, a.ID)
}

// GetVersion returns the current datastore version of the object
func (a *Action) GetVersion() int64 { return a.version }

// SetVersion sets the current datastore version of the object
func (a *Action) SetVersion(v int64) { a.version = v }

// Value returns this object as an interface{}
func (a *Action) Value() interface{} { return a }

// SetActionComplete sets the current action's status to complete
func (a *Action) SetActionComplete() error {
	log.Tracef("SetActionComplete: %s", a.ID)

	as := &ActionStatus{
		Mzid:   a.Mzid,
		Taskid: a.Taskid,
		ID:     a.ID,
	}

	// pull out object
	err := common.Read(as)
	if err != nil {
		return err
	}
	// set to complete
	as.Complete = true

	// delete from ready queue and write update back to action queue
	otx := common.ObjectTx{
		Put:    []common.Object{as},
		Delete: []common.Object{as.ToReady()},
	}
	err = common.RunObjectTx(otx)

	return err
}

// Task returns the parent task of an action or an error if unable to
// return the task
func (a *Action) Task() (*Task, error) {
	if a.Mzid == "" || a.Taskid == "" {
		fields := log.Fields{
			"action": a,
		}
		return nil, common.ErrorF("action missing id or task", fields)
	}
	task := &Task{Mzid: a.Mzid, ID: a.Taskid}
	err := common.Read(task)
	if err != nil {
		return nil, err
	}
	return task, nil
}

// Stage returns the parent stage of an action.
func (a *Action) Stage() (*Stage, error) {
	if a.Mzid == "" || a.Stageid == "" || a.Taskid == "" {
		fields := log.Fields{
			"action": a,
		}
		return nil, common.ErrorF("action missing id or stage", fields)
	}
	stage := &Stage{Mzid: a.Mzid, Taskid: a.Taskid, ID: a.Stageid}
	err := common.Read(stage)
	if err != nil {
		return nil, err
	}
	return stage, nil
}

// ToStatus creates the action status object which can then be used
// to read from database
func (a *Action) ToStatus() *ActionStatus {
	return &ActionStatus{
		Mzid:   a.Mzid,
		Taskid: a.Taskid,
		ID:     a.ID,
	}
}

// MaskAction masks any error condition on an action. This makes a failed task
// appear complete from the perspective of dependency resolution.
func MaskAction(mzid, taskid, actionid string, unmask bool) error {

	as, err := GetActionStatus(mzid, taskid, actionid)
	if err != nil {
		return common.ErrorE("failed to get action", err)
	}
	as.Masked = true && !unmask

	// based on what unmask is we either need to add or delete from ready queue
	addList := []common.Object{as}
	delList := make([]common.Object, 0)
	if as.Masked {
		delList = append(delList, as.ToReady())
	} else {
		addList = append(addList, as.ToReady())
	}

	// delete from ready queue and write update back to action queue
	otx := common.ObjectTx{
		Put:    addList,
		Delete: delList,
	}
	err = common.RunObjectTx(otx)

	return err
}

// GetAction returns the action from storage
func GetAction(mzid, taskid, actionid string) (*Action, error) {
	action := &Action{
		Mzid:   mzid,
		Taskid: taskid,
		ID:     actionid,
	}

	err := common.Read(action)
	return action, err
}

// GetMzidActions all actions associated to an mzid
func GetMzidActions(mzid, taskid string) (map[string]*Action, error) {
	fields := log.Fields{
		"mzid":   mzid,
		"taskid": taskid,
	}
	if mzid == "" || taskid == "" {
		return nil, common.ErrorF("invalid key", fields)
	}

	key := fmt.Sprintf("%s/%s", mzid, taskid)
	actions, err := EtcdActionPrefix(key)
	if err != nil {
		return nil, err
	}

	actionMap := make(map[string]*Action)
	for _, x := range actions {
		actionMap[x.ID] = x
	}

	return actionMap, nil
}

// GetAllActions all actions
func GetAllActions() (map[string]*Action, error) {

	// empty prefix gets us everything
	actions, err := EtcdActionPrefix("")
	if err != nil {
		return nil, err
	}

	actionMap := make(map[string]*Action)
	for _, x := range actions {
		actionMap[x.ID] = x
	}

	return actionMap, nil
}

// ExtractAction extracts actionstatus
func ExtractAction(actionJSON string) (*Action, error) {
	action := &Action{}
	err := json.Unmarshal([]byte(actionJSON), action)
	if err != nil {
		return nil, err
	}
	if action.Mzid == "" || action.ID == "" {
		debug.PrintStack()
		return nil, common.ErrorF("Invalid action status", log.Fields{"action": action})
	}
	return action, nil
}

///////////////////////////////////////////////////////////////////////////////////
//////////////////////////////  ACTION STATUSES  //////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////

var (
	// ErrASMapEmpty is the error returned by functions when given an empty map
	// of actions
	ErrASMapEmpty error = fmt.Errorf("empty action status map")
	// ErrASMapNil is the error returned by functions when given a nil map of
	// actions
	ErrASMapNil error = fmt.Errorf("nil action status map")
)

// ActionStatus is the status of an action. Removed from the task
// to reduce writes to the task object
type ActionStatus struct {
	Mzid     string  `yaml:",omitempty"`
	Taskid   string  `yaml:",omitempty"`
	ID       string  `yaml:",omitempty"`
	Complete bool    `yaml:",omitempty"`
	Error    *string `yaml:",omitempty"`
	Masked   bool    `yaml:",omitempty"`

	version int64
}

// Key returns the datastore key for this object.
func (as *ActionStatus) Key() string {
	return fmt.Sprintf("/%s/%s/%s/%s", actionStatusPrefix, as.Mzid, as.Taskid, as.ID)
}

// GetVersion returns the current datastore version of the object
func (as *ActionStatus) GetVersion() int64 { return as.version }

// SetVersion sets the current datastore version of the object
func (as *ActionStatus) SetVersion(v int64) { as.version = v }

// Value returns this object as an interface{}
func (as *ActionStatus) Value() interface{} { return as }

// ToReady returns the ASReady struct
func (as *ActionStatus) ToReady() *ASReady {
	return &ASReady{
		Mzid:     as.Mzid,
		Taskid:   as.Taskid,
		Actionid: as.ID,
	}
}

// ToError returns the ASError struct
func (as *ActionStatus) ToError() *ASError {
	return &ASError{
		Mzid:     as.Mzid,
		Taskid:   as.Taskid,
		Actionid: as.ID,
	}
}

// GetActionStatus returns the actionstatus associated to an action's actionstatus uuid
func GetActionStatus(mzid, taskid, asID string) (*ActionStatus, error) {
	fields := log.Fields{"actionStatus": asID}
	if asID == "" {
		return nil, common.ErrorF("invalid ID", fields)
	}

	as := &ActionStatus{
		Mzid:   mzid,
		Taskid: taskid,
		ID:     asID,
	}

	err := common.Read(as)
	if err != nil {
		return nil, common.ErrorEF("unable to retrieve status", err, fields)
	}
	return as, err
}

// ExtractAS extracts actionstatus
func ExtractAS(asJSON string) (*ActionStatus, error) {
	as := &ActionStatus{}
	err := json.Unmarshal([]byte(asJSON), as)
	if err != nil {
		return nil, err
	}
	if as.Mzid == "" || as.ID == "" {
		debug.PrintStack()
		return nil, common.ErrorF("Invalid action status", log.Fields{"action": as})
	}
	return as, nil
}

// ExtractBadAS extracts actionstatus
func ExtractBadAS(asJSON string) (*ActionStatus, error) {
	as := &ActionStatus{}
	err := json.Unmarshal([]byte(asJSON), as)
	if err != nil {
		return nil, err
	}
	return as, nil
}

// GetAllAS all action statuses
func GetAllAS() (map[string]*ActionStatus, error) {
	actionstatuses, err := EtcdActionStatusPrefix("")
	if err != nil {
		return nil, err
	}
	asMap := make(map[string]*ActionStatus)
	for _, x := range actionstatuses {
		asMap[x.ID] = x
	}
	return asMap, nil
}

// GetMzidAS all actions associated to an mzid
func GetMzidAS(mzid, taskid string) (map[string]*ActionStatus, error) {
	if mzid == "" || taskid == "" {
		return nil, fmt.Errorf("sent empty to mzidas: mzid:%s task:%s", mzid, taskid)
	}

	key := fmt.Sprintf("%s/%s", mzid, taskid)
	actionstatuses, err := EtcdActionStatusPrefix(key)
	if err != nil {
		return nil, err
	}
	asMap := make(map[string]*ActionStatus)
	for _, x := range actionstatuses {
		asMap[x.ID] = x
	}
	return asMap, nil
}

///////////////////////////////////////////////////////////////////////////////////
//////////////////////////////  AS OTHER  /////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////

// ASError is a struct that fits within our storage achitecture for managing
// actions in the error queue
type ASError struct {
	Mzid     string `yaml:",omitempty"`
	Taskid   string `yaml:",omitempty"`
	Actionid string `yaml:",omitempty"`

	version int64
}

// Key returns the datastore key for this object.
func (as *ASError) Key() string {
	return fmt.Sprintf("/%s/%s", actionErrorPrefix, as.Actionid)
}

// GetVersion returns the current datastore version of the object
func (as *ASError) GetVersion() int64 { return as.version }

// SetVersion sets the current datastore version of the object
func (as *ASError) SetVersion(v int64) { as.version = v }

// Value returns this object as an interface{}
func (as *ASError) Value() interface{} { return as }

// ASReady is a struct that fits within our storage achitecture for managing
// actions in the incomplete queue
type ASReady struct {
	Mzid     string `yaml:",omitempty"`
	Taskid   string `yaml:",omitempty"`
	Actionid string `yaml:",omitempty"`

	version int64
}

// Key returns the datastore key for this object.
func (as *ASReady) Key() string {
	return fmt.Sprintf("/%s/%s", actionReadyPrefix, as.Actionid)
}

// GetVersion returns the current datastore version of the object
func (as *ASReady) GetVersion() int64 { return as.version }

// SetVersion sets the current datastore version of the object
func (as *ASReady) SetVersion(v int64) { as.version = v }

// Value returns this object as an interface{}
func (as *ASReady) Value() interface{} { return as }

// GetAllReadyAS returns the list of all action statuses that are ready
func GetAllReadyAS() ([]*ActionStatus, error) {

	readies, err := EtcdActionReadyPrefix()
	if err != nil {
		return nil, err
	}

	var actionstatuses []*ActionStatus
	for _, ready := range readies {
		as := &ActionStatus{
			Mzid:   ready.Mzid,
			Taskid: ready.Taskid,
			ID:     ready.Actionid,
		}
		err := common.Read(as)
		if err != nil {
			fields := log.Fields{
				"action status": as,
			}
			common.WarnEF("unable to find action status for readyeady", err, fields)
			continue
		}
		actionstatuses = append(actionstatuses, as)
	}

	return actionstatuses, nil
}

// ExtractASReady takes a json string an unmarshals it into a ASReady object
func ExtractASReady(asReadyJSON string) (*ASReady, error) {
	asr := &ASReady{}
	err := json.Unmarshal([]byte(asReadyJSON), asr)
	if err != nil {
		return nil, err
	}
	return asr, nil
}

// ExtractASError takes a json string an unmarshals it into a ASError object
func ExtractASError(asErrorJSON string) (*ASError, error) {
	ase := &ASError{}
	err := json.Unmarshal([]byte(asErrorJSON), ase)
	if err != nil {
		return nil, err
	}
	return ase, nil
}

///////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////  MISC  //////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////

// YamlTask are backwards compatable tasks
type YamlTask struct {
	Mzid      string              `yaml:",omitempty"`
	ID        string              `yaml:",omitempty"`
	Instance  string              `yaml:",omitempty"`
	Stages    []*StageWithActions `yaml:",omitempty"`
	Timestamp string              `yaml:",omitempty"`
}

// GenerateID generates an ID or Key for Stage/Action
func GenerateID() (string, error) {
	sid, err := shortid.New(1, IDAlphabet, uint64(time.Now().UnixNano()))
	if err != nil {
		return "", err
	}
	return sid.MustGenerate(), nil
}

// ReadTask reads a task from a YAML file.
func ReadTask(file string) (*Task, *[]common.Object, error) {
	buf, err := ioutil.ReadFile(file)
	if err != nil {
		return nil, nil, err
	}

	*yaml.DefaultMapType = reflect.TypeOf(map[string]interface{}{})

	t := &YamlTask{}
	err = yaml.Unmarshal(buf, t)
	if err != nil {
		return nil, nil, err
	}

	// previously mzids were stored at the action level
	// if they exist, pull them out so the task and stage
	// share the same mzid as the actions
	fileMzid := ""
	if len(t.Stages) > 0 && len(t.Stages[0].Actions) > 0 {
		fileMzid = t.Stages[0].Actions[0].Mzid
	}
	if fileMzid == "" {
		fileMzid, err = GenerateID()
		if err != nil {
			return nil, nil, err
		}
	}

	// do a quick check to make sure all the actions share the same mzid
	// this was a previous assumption that we will now enforce as we put
	// the mzid into the task
	tmpMzid := ""
	for _, x := range t.Stages {
		for _, a := range x.Actions {
			if tmpMzid == "" {
				tmpMzid = a.Mzid
			}
			if tmpMzid != a.Mzid {
				fields := log.Fields{
					"mzid a": tmpMzid,
					"mzid b": a.Mzid,
				}
				return nil, nil, common.ErrorF("inconsistent mzids", fields)
			}
		}
	}

	for i := range t.Stages {
		t.Stages[i] = NewStage(t.Stages[i].Actions...)
		t.Stages[i].Stage.Mzid = fileMzid
	}

	instanceID, err := GenerateID()
	if err != nil {
		return nil, nil, err
	}

	t.Instance = instanceID
	t.Mzid = fileMzid

	// convert old task to new tasks
	task, objs, err := NewTask(t.Mzid, t.Instance,
		t.Stages...,
	)

	return task, objs, err
}

// EtcdPrefix is helper function for all the code calling etcd with prefix
// make code more DRY
func EtcdPrefix(objStr, optkey string) ([]interface{}, error) {

	key := ""
	switch objStr {
	case taskPrefix:
		key = fmt.Sprintf("/%s", taskPrefix)
	case stagePrefix:
		key = fmt.Sprintf("/%s", stagePrefix)
	case actionPrefix:
		key = fmt.Sprintf("/%s", actionPrefix)
	case actionStatusPrefix:
		key = fmt.Sprintf("/%s", actionStatusPrefix)
	case actionReadyPrefix:
		key = fmt.Sprintf("/%s", actionReadyPrefix)
	case actionErrorPrefix:
		key = fmt.Sprintf("/%s", actionErrorPrefix)
	default:
		return nil, common.ErrorF("unknown prefix str", log.Fields{"str": objStr})
	}

	// append the optional key (taskid, mzid, etc)
	key = key + "/" + optkey

	// minimize the work we do while we hold etcd
	data, err := common.WithMinEtcd(func(c *etcd.Client) (interface{}, error) {
		ctx, cancel := context.WithTimeout(context.TODO(), 3*time.Second)
		resp, err := c.Get(ctx, key, etcd.WithPrefix())
		cancel()
		if err != nil {
			return nil, err
		}
		return resp, nil
	})
	if err != nil {
		return nil, err
	}

	var objList []interface{}
	resp := data.(*etcd.GetResponse)
	for _, kv := range resp.Kvs {
		switch objStr {
		case taskPrefix:
			t, err := ExtractTask(string(kv.Value))
			if err != nil {
				return nil, err
			}
			objList = append(objList, t)
		case stagePrefix:
			s, err := ExtractStage(string(kv.Value))
			if err != nil {
				return nil, err
			}
			objList = append(objList, s)
		case actionPrefix:
			a, err := ExtractAction(string(kv.Value))
			if err != nil {
				return nil, err
			}
			objList = append(objList, a)
		case actionStatusPrefix:
			as, err := ExtractAS(string(kv.Value))
			if err != nil {
				return nil, err
			}
			objList = append(objList, as)
		case actionReadyPrefix:
			asr, err := ExtractASReady(string(kv.Value))
			if err != nil {
				return nil, err
			}
			objList = append(objList, asr)
		case actionErrorPrefix:
			ase, err := ExtractASError(string(kv.Value))
			if err != nil {
				return nil, err
			}
			objList = append(objList, ase)
		}
	}

	return objList, err
}

// EtcdTaskPrefix uses EtcdPrefix helper function and type casts back to Task
func EtcdTaskPrefix(prefix string) ([]*Task, error) {
	objs, err := EtcdPrefix(taskPrefix, prefix)
	tasks := make([]*Task, len(objs))
	for i, x := range objs {
		tasks[i] = x.(*Task)
	}
	return tasks, err
}

// EtcdStagePrefix uses EtcdPrefix helper function and type casts back to Stage
func EtcdStagePrefix(prefix string) ([]*Stage, error) {
	objs, err := EtcdPrefix(stagePrefix, prefix)
	stages := make([]*Stage, len(objs))
	for i, x := range objs {
		stages[i] = x.(*Stage)
	}
	return stages, err
}

// EtcdActionPrefix uses EtcdPrefix helper function and type casts back to Action
func EtcdActionPrefix(prefix string) ([]*Action, error) {
	objs, err := EtcdPrefix(actionPrefix, prefix)
	actions := make([]*Action, len(objs))
	for i, x := range objs {
		actions[i] = x.(*Action)
	}
	return actions, err
}

// EtcdActionStatusPrefix uses EtcdPrefix helper function and type casts back to ActionStatus
func EtcdActionStatusPrefix(prefix string) ([]*ActionStatus, error) {
	objs, err := EtcdPrefix(actionStatusPrefix, prefix)
	as := make([]*ActionStatus, len(objs))
	for i, x := range objs {
		as[i] = x.(*ActionStatus)
	}
	return as, err
}

// EtcdActionReadyPrefix uses EtcdPrefix helper function and type casts back to ActionReady
func EtcdActionReadyPrefix() ([]*ASReady, error) {
	objs, err := EtcdPrefix(actionReadyPrefix, "")
	asrs := make([]*ASReady, len(objs))
	for i, x := range objs {
		asrs[i] = x.(*ASReady)
	}
	return asrs, err
}

// EtcdActionErrorPrefix uses EtcdPrefix helper function and type casts back to ActionError
func EtcdActionErrorPrefix() ([]*ASError, error) {
	objs, err := EtcdPrefix(actionErrorPrefix, "")
	ases := make([]*ASError, len(objs))
	for i, x := range objs {
		ases[i] = x.(*ASError)
	}
	return ases, err
}

// ParseAction when action is found, execute corresponding task
func ParseAction(as *ActionStatus) (*Task, error) {
	fields := log.Fields{
		"mzid":     as.Mzid,
		"id":       as.ID,
		"complete": as.Complete,
		"error":    as.Error,
		"masked":   as.Masked,
	}
	if as.Taskid == "" {
		return nil, common.ErrorF("action status without task id", fields)
	}
	t := &Task{
		Mzid: as.Mzid,
		ID:   as.Taskid,
	}
	err := common.Read(t)
	if err != nil {
		return nil, common.ErrorEF("failed to read task", err, fields)
	}

	return t, nil
}
