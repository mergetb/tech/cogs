package task

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"

	log "github.com/sirupsen/logrus"

	"gitlab.com/mergetb/site/api"
	"gitlab.com/mergetb/tech/cogs/pkg/common"
	"gitlab.com/mergetb/xir/lang/go/v0.2"
	"gitlab.com/mergetb/xir/lang/go/v0.2/tb"
)

// A LinkAction is a Create or Destroy Link
type LinkAction int

const (
	// CreateLink creates links
	CreateLink LinkAction = iota
	// DestroyLink creates links
	DestroyLink
)

// Canopy takes the link action and applies it to canopy links
func (a LinkAction) Canopy() string {
	switch a {
	case CreateLink:
		return SetCanopyLinks
	case DestroyLink:
		return RemoveCanopyLinks
	}
	return "?"
}

// Moa takes the link action and applies it to moa links
func (a LinkAction) Moa() string {
	switch a {
	case CreateLink:
		return CreateMoaLink
	case DestroyLink:
		return RemoveMoaLink
	}
	return "?"
}

// CreateLinks creates a set of virtual links for the specified materialization
// ID given a set of materialization fragments that are link specs.
func CreateLinks(
	topo *xir.Net,
	mzInfo *MzInfo,
	fragments []*site.MzFragment,
) error {

	for _, fragment := range fragments {
		createLinkFile(mzInfo.Mzid, fragment)
	}

	return modLinks(topo, mzInfo, fragments, CreateLink)
}

// DestroyLinks destroys a set of virtual links for the specofied
// materialization ID given a set of materialization fragments that are links
// specs.
func DestroyLinks(
	topo *xir.Net,
	mzInfo *MzInfo,
	fragments []*site.MzFragment,
) error {

	err := modLinks(topo, mzInfo, fragments, DestroyLink)
	if err != nil {
		for _, fragment := range fragments {
			deleteLinkFile(mzInfo.Mzid, fragment)
		}
	}
	return err

}

func getMac(e *xir.Endpoint) (string, error) {

	mac, ok := e.Props.GetString("mac")
	if !ok {
		return "", fmt.Errorf("%s.%s has no mac", e.Parent.Label(), e.Label())
	}

	return mac, nil

}

func modMoaLinks(mzInfo *MzInfo, data []*LinkInfo, action string) *Action {

	return &Action{
		Kind:       MoaKind,
		Mzid:       mzInfo.Mzid,
		MzInstance: mzInfo.Instance,
		Action:     action,
		Data:       data,
	}

}

func BasicLinkConstraints(model xir.Props) (latency int32, capacity int64, loss float64) {

	c, _ := model.GetConstraint("capacity")
	v, _ := c.Int()
	capacity = int64(v) // bits/sec

	c, _ = model.GetConstraint("latency")
	v, _ = c.Int()
	latency = int32(v) // nanoseconds

	c, _ = model.GetConstraint("loss")
	f, ok := c.Value.(float64)
	log.WithFields(log.Fields{"f": f, "ok": ok}).Debug("get loss constraint")
	if ok {
		loss = f
	}

	return
}

// VLMentry captures information about a virtual link materialization
type VLMentry struct {
	Vni int
	Vid int
}

// VLMap maps virtual link identifers to VLMentries
type VLMap struct {
	Mzid    string
	Entries map[int]*VLMentry

	ver int64
}

func isMoaLink(linkModel *site.LinkModel) bool {

	p := props(linkModel.Model)

	_, ok := p["emulator"].(string)
	if ok {
		return true
	}

	_, ok = p["capacity"]
	if ok {
		p["emulator"] = "emu1"
		return true
	}

	_, ok = p["latency"]
	if ok {
		p["emulator"] = "emu1"
		return true
	}

	_, ok = p["loss"]
	if ok {
		p["emulator"] = "emu1"
		return true
	}

	return false
}

func fragmentComposition(
	topo *xir.Net, fragment *site.MzFragment, linkModel *site.LinkModel,
) ([]*EdgeInfo, []string, error) {

	fields := log.Fields{}

	var edges []*EdgeInfo
	var core []string

	for _, resource := range fragment.Resources {

		fields["resource"] = resource
		_, _, tblink := topo.GetLink(resource)
		if tblink == nil {
			fields["link"] = resource
			return nil, nil, common.ErrorF("link does not exist", fields)
		}

		hasNodes := false
		for _, x := range tblink.Endpoints {
			if hasEndpoint(linkModel, x.Id()) && tb.HasRole(
				x.Endpoint.Parent, tb.Node) {

				hasNodes = true
				mac, err := getMac(x.Endpoint)
				fields["endpoint"] = fmt.Sprintf("%+v", *x.Endpoint)
				if err != nil {
					return nil, nil, common.ErrorEF(
						"error getting mac", err, fields)
				}
				lanes, ok := linkModel.Lanes[resource]
				if !ok {
					common.WarnF("link model has no lanes", fields)
				}
				edges = append(edges, &EdgeInfo{
					Resource:      resource,
					Mac:           mac,
					NodeName:      x.Endpoint.Parent.Label(),
					NodeInterface: x.Endpoint.Label(),
					Multidegree:   lanes > 1,
				})

			}
		}

		if !hasNodes {
			core = append(core, resource)
		}

	}

	return edges, core, nil

}

func hasEndpoint(linkModel *site.LinkModel, id string) bool {
	for _, x := range linkModel.Endpoints {
		if x == id {
			return true
		}
	}
	return false
}

func modLinks(
	topo *xir.Net,
	mzInfo *MzInfo,
	fragments []*site.MzFragment,
	action LinkAction,
) error {

	fields := log.Fields{
		"op":        "ModifyLinks",
		"mzid":      mzInfo.Mzid,
		"fragments": len(fragments),
	}
	log.WithFields(fields).Info("modifying links")

	var links, moaLinks []*LinkInfo
	var createLinks, deleteLinks []*LinkInfo

	for _, fragment := range fragments {

		// extract the link model from the fragment
		linkModel := fragment.GetLinkModel()
		if linkModel == nil {
			return common.ErrorF("could not read fragment link model", fields)
		}

		// create the linkinfo object

		linkInfo := &LinkInfo{
			Kind: XpLink,
			// moa needs some minimal info
			Mzid:   mzInfo.Mzid,
			Vindex: mzInfo.Vindex,
			XpID:   linkModel.Vlid,
		}

		if action == CreateLink {

			var err error
			linkInfo, err = GenLinkInfo(mzInfo, fragment, linkModel, topo, true)
			if err != nil {
				return common.ErrorEF(
					"generating link info failed", err, fields,
				)
			}

			createLinks = append(createLinks, linkInfo)
		}

		if action == DestroyLink {
			deleteLinks = append(deleteLinks, linkInfo)
		}

		if isMoaLink(linkModel) {
			linkInfo.Kind = MoaLink
			moaLinks = append(moaLinks, linkInfo)
		}
		links = append(links, linkInfo)

	}

	if action == DestroyLink {
		var objs []common.Object
		for _, x := range links {
			objs = append(objs, x)
		}
		_, err := common.ReadObjects(objs)
		if err != nil {
			return common.ErrorEF("failed to read links", err, fields)
		}
	}

	if len(createLinks) > 0 {
		// go a single go of assign vid, vlans to all the links
		err := bulkInitLinks(links)
		if err != nil {
			return common.ErrorEF("failed to initial bulk links", err, fields)
		}
	}

	if len(deleteLinks) > 0 {
		err := bulkDelLinks(links)
		if err != nil {
			return common.ErrorEF("failed to initial bulk links", err, fields)
		}
	}

	// create stage
	stage := NewStage()
	moastage := NewStage()

	// normal links
	stage.AddActions(modCanopyLinks(mzInfo, links, action.Canopy()))
	// moa links
	if len(moaLinks) > 0 {
		moastage.AddActions(modMoaLinks(mzInfo, moaLinks, action.Moa()))
	}

	// create and launch task
	task, objs, err := NewTask(mzInfo.Mzid, mzInfo.Instance, stage, moastage)
	if err != nil {
		return err
	}

	if action != DestroyLink {
		err = waitForIncoming(mzInfo, task)
		if err != nil {
			return fmt.Errorf("failed to wait for incoming task id: %v", err)
		}
	}

	err = LaunchTask(task, objs)
	if err != nil {
		return fmt.Errorf("failed to launch mod links stage: %v", err)
	}

	return nil

}

func GenLinkInfo(
	mzInfo *MzInfo,
	fragment *site.MzFragment,
	linkModel *site.LinkModel,
	topo *xir.Net,
	init bool,
) (*LinkInfo, error) {

	edgeInfo, core, err := fragmentComposition(topo, fragment, linkModel)
	if err != nil {
		return nil, err
	}

	linkInfo := &LinkInfo{
		Mzid:   mzInfo.Mzid,
		Vindex: mzInfo.Vindex,
		XpID:   linkModel.Vlid,
		Core:   core,
		Kind:   XpLink,
		Props:  props(linkModel.Model),
	}

	if isMoaLink(linkModel) {

		linkInfo.Kind = MoaLink

		// if this is a moa link, all edges get their own tesetbed virtual links

		for _, ei := range edgeInfo {

			tbvl := &TBVLink{Edges: []*EdgeInfo{ei}}

			for _, ai := range edgeInfo {
				if ei.Mac == ai.Mac {
					continue
				}
				tbvl.Advertisements = append(tbvl.Advertisements, ai.Mac)
			}

			linkInfo.TBVLinks = append(linkInfo.TBVLinks, tbvl)

		}

	} else {
		// if this is a regular link, all edges are in the same testbed virtual
		// link
		linkInfo.TBVLinks = []*TBVLink{{Edges: edgeInfo}}
	}

	return linkInfo, nil
}

func createLinkFile(mzid string, fragment *site.MzFragment) {

	fields := log.Fields{
		"mzid":     mzid,
		"fragment": fragment.Id,
	}

	linkModel := fragment.GetLinkModel()
	if linkModel == nil {
		common.WarnF("could not read fragment link model", fields)
		return
	}

	dir := fmt.Sprintf("/var/log/mz/%s", mzid)
	file := fmt.Sprintf("%s/link-%s.json", dir, fragment.Id)
	err := os.MkdirAll(dir, 0755)
	if err != nil {
		log.WithError(err).Warnf("failed to create mzdir")
	} else {
		out, err := json.MarshalIndent(linkModel, "", "  ")
		if err == nil {
			err := ioutil.WriteFile(file, out, 0644)
			if err != nil {
				log.WithError(err).Warnf("failed to save link data")
			}
		} else {
			log.WithError(err).Warnf("failed to save link data")
		}
	}

}

func deleteLinkFile(mzid string, fragment *site.MzFragment) {

	dir := fmt.Sprintf("/var/log/mz/%s", mzid)
	file := fmt.Sprintf("%s/link-%s.json", dir, fragment.Id)
	os.RemoveAll(file)

}

func modCanopyLinks(mzi *MzInfo, data []*LinkInfo, action string) *Action {

	return &Action{
		Kind:       CanopyKind,
		Mzid:       mzi.Mzid,
		MzInstance: mzi.Instance,
		Action:     action,
		Data:       data,
	}

}

// bulkInitLinks iterates through all the materialization links
// and for each one assigns a vid, vni based on what is available
// it does a single writeback allocating the vid/vni, which is
// a shared resource, so this could be the source of some contention
func bulkInitLinks(links []*LinkInfo) error {
	for i := 0; i < 20; i++ {
		err := bulkAssign(links, true)
		if err == nil {
			return nil
		}
	}

	return common.Error("linkinfo failed to writeback net counters")
}

// bulkDelLinks does the inverse of bulkInitLinks, mainly it loops
// through each link and removes/frees the vid/vni thats been allocated
func bulkDelLinks(links []*LinkInfo) error {
	for i := 0; i < 20; i++ {
		err := bulkAssign(links, false)
		if err == nil {
			return nil
		}
	}

	return common.Error("linkinfo failed to writeback net counters")
}

// bulkAssign does the heavy lifting for bulkInitLinks, attempting to find
// available vni/vids to select for all the links
func bulkAssign(links []*LinkInfo, add bool) error {
	fields := log.Fields{
		"add": add,
	}

	vni := common.CountSet{Name: XpVniKey, Size: vniPoolSize, Offset: 100}
	vid := common.CountSet{Name: XpVidKey, Size: vidPoolSize, Offset: 10}
	vniNext := 0
	vidNext := 0

	// do a single read to get the latest vni/vid objects
	_, err := common.ReadObjects([]common.Object{&vni, &vid})
	if err != nil {
		return common.ErrorE("vni/vid read failed", err)
	}

	storObjs := make([]common.Object, 0)

	// heavy lifting to iterate all links and assign vni/vids
	for _, link := range links {
		for _, vl := range link.TBVLinks {
			if add {
				vniNext, vni, err = vni.Add()
				if err != nil {
					return common.ErrorEF("vni next failed", err, fields)
				}

				vidNext, vid, err = vid.Add()
				if err != nil {
					return common.ErrorEF("vid next failed", err, fields)
				}

				vl.Vni = vniNext
				vl.Vid = vidNext

			} else {
				log.Debugf("removing counters %d/%d", vl.Vni, vl.Vid)
				vni = vni.Remove(vl.Vni)
				vid = vid.Remove(vl.Vid)
			}

		}
		storObjs = append(storObjs, link)
	}

	if add {
		// add the vni, vid to what we need to write back
		storObjs = append(storObjs, &vni, &vid)

		// single writeback, pray it works
		err = common.WriteObjects(storObjs, true)
		if err != nil {
			return err
		}
	} else {
		// we are deleteing the link so we need to add the counters back
		// and delete the links
		otx := common.ObjectTx{
			Put:    []common.Object{&vni, &vid},
			Delete: storObjs,
		}

		err = common.RunObjectTx(otx)
		if err != nil {
			return err
		}
	}

	return nil
}
