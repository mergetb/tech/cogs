package task

import (
	"gitlab.com/mergetb/tech/nex/pkg"
)

const (
	// NexMountName is the name of the mount used for mounting nex configs.
	NexMountName = "nex-config"
	// NexMountSource is the host location for the nex container config mount.
	NexMountSource = "/etc/cogs/nex.yml"
	// NexMountDest is the container location for the nex container config mount.
	NexMountDest = "/etc/nex/nex.yml"
	// NexDomainEnv is the nex domain name environment variable key.
	NexDomainEnv = "DOMAIN"
)

// NexNetworkSpec is a wrapper around a nex network specification that adds an
// endpoint at which the nex service may be reached.
type NexNetworkSpec struct {
	// Core Nex data
	Network nex.Network

	// Nex endpoint address
	Endpoint string
}

// NexMemberSpec is a wrapper around a nex member list that adds an endpoint at
// which the nex service may be reached.
type NexMemberSpec struct {
	nex.MemberList `mapstructure:",squash" yaml:",inline"`
	Endpoint       string
}
