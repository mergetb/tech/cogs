package task

const (
	belugactldCtrName = "belugactld"

	belugactldBelugaCfgMountName = "beluga-cfg"
	belugactldBelugaCfgMountSrc  = "/etc/cogs/runtime.yml"
	belugactldBelugaCfgMountDst  = "/etc/cogs/runtime.yml"

	belugactldCertMountName = "belugactl-cert"
	belugactldCertMountSrc  = "/etc/beluga/manage.pem"
	belugactldCertMountDst  = "/etc/beluga/manage.pem"

	belugactldKeyMountName = "belugactl-key"
	belugactldKeyMountSrc  = "/etc/beluga/manage-key.pem"
	belugactldKeyMountDst  = "/etc/beluga/manage-key.pem"
)
