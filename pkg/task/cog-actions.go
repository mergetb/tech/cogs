package task

import (
	"gitlab.com/mergetb/tech/foundry/api"
	"gitlab.com/mergetb/tech/nex/pkg"
	"gitlab.com/mergetb/tech/sled/pkg"
)

// Constants used by the harbor materialization
const (
	HarborNet      = "harbor.net"
	HarborSvcAddr  = "172.31.0.2" //XXX hardcode
	HarborVindex   = 2
	HarborInfraVNI = 2
	HarborInfraVID = 2
	HarborXpVNI    = 2
)

// Task family and action names shared between Driver and Rex
const (

	// Container tasks
	ContainerKind   = "container"
	LaunchCtr       = "launch"
	PullCtrImage    = "pull-image"
	CreateCtr       = "create"
	CreateCtrTask   = "create-task"
	CreateCtrNs     = "create-ns"
	ConfigCtrLo     = "config-lo"
	CreateCtrRecord = "create-record"
	DeleteCtr       = "delete"
	DeleteCtrTask   = "delete-task"
	DeleteCtrImg    = "delete-img"
	DeleteCtrRecord = "delete-record"
	DeleteCtrNS     = "delete-ctr-ns"

	// Node tasks
	NodeSetupKind   = "NodeSetup"
	NodeRecycleKind = "NodeRecycle"

	// Plumbing tasks
	PlumbingKind          = "plumbing"
	CreateEnclave         = "create-enclave"
	DestroyEnclave        = "destroy-enclave"
	AdvertiseInterfaceMac = "advertise-interface-mac"
	AdvertiseMac          = "advertise-mac"
	WithdrawMac           = "withdraw-mac"
	DeleteNS              = "delete-ns"
	PlanInfranet          = "plan-infranet"
	DestroyInfranet       = "destroy-infranet"

	// Nex tasks
	NexKind          = "Nex"
	CreateNexNetwork = "CreateNetwork"
	DeleteNexNetwork = "DeleteNetwork"
	DeleteNexMembers = "DeleteMembers"
	AddNexMembers    = "AddMembers"

	// Canopy tasks
	CanopyKind              = "Canopy"
	SetCanopyLinks          = "SetLinks"
	SetCanopyServiceVtep    = "SetServiceVtep"
	RemoveCanopyServiceVtep = "RemoveServiceVtep"
	RemoveCanopyLinks       = "RemoveLinks"

	// Moa tasks
	MoaKind           = "Moa"
	InitEmulation     = "InitEmulation"
	CreateMoaLink     = "CreateMoaLink"
	RemoveMoaLink     = "RemoveMoaLink"
	FinalizeEmulation = "FinalizeEmulation"
	StartEmulation    = "StartEmulation"
	TeardownEmulation = "TeardownEmulation"
	MoaControlKind    = "MoaControl"

	// Bookkeeping tasks
	BookkeepingKind = "bookkeeping"
	DeleteMzinfo    = "DeleteMzinfo"
	SetStatus       = "UpdateStatus"
)

const (
	// InfraCanopyHost is the name of the host that infrapods enter the network
	// on. We currently assume this is the same host rex is running on.
	InfraCanopyHost = "localhost"

	// Svcbr is the name of the service bridge that connects an infrastructure
	// host to infrapod containers
	Svcbr = "svcbr"
)

//// Nex //////////////////////////////////////////////////////////////////////

// NexDeleteNetwork deletes the specified nex DHCP/DNS network by name.
func NexDeleteNetwork(name string) *Action {

	return &Action{
		Kind:   NexKind,
		Mzid:   name,
		Action: DeleteNexNetwork,
		Data: nex.Network{
			Name: name,
		},
	}
}

// NexDeleteMembers deletes the specified endpoints from the nex DHCP/DNS
// network identified by the provided mzid.
func NexDeleteMembers(mzid string, endpoints []Endpoint) *Action {

	return &Action{
		Kind:   NexKind,
		Mzid:   mzid,
		Action: DeleteNexMembers,
		Data:   endpoints,
	}
}

// NexCreateMembers adds the specified endpoints as nex DHCP/DNS members on the
// network identified by the provided mzid.
func NexCreateMembers(mzid string, endpoints []Endpoint) *Action {

	return &Action{
		Kind:   NexKind,
		Mzid:   mzid,
		Action: AddNexMembers,
		Data:   endpoints,
	}
}

//// Container ////////////////////////////////////////////////////////////////

// ContainerImageData encapsulates data needed to pull or remove a container
// image.
type ContainerImageData struct {
	Image     string
	Namespace string
}

// DeleteContainerData encapsulates information needed to delete a container.
type DeleteContainerData struct {
	Name      string
	Namespace string
}

// DeleteContainerIfxData encapsulates data needed to delete a container
// interface.
type DeleteContainerIfxData struct {
	Name         string
	EvpnWithdraw bool
}

// DeleteContainerNsData encapsulates data needed to delete a container
// namespace.
type DeleteContainerNsData struct {
	Name string
}

//// Canopy ///////////////////////////////////////////////////////////////////

// LinkKind represents a canopy link kind

// VLinkEdge defines how a virtual link terminates at its edge.
type VLinkEdge int

const (
	// VlanAccess indicates a virtual link terminates as a VLAN access port
	VlanAccess VLinkEdge = iota
	// VlanTrunk indicates a virtual link terminates as a VLAN trunk port
	VlanTrunk
	// Vxlan indicates a virtual link terminates as a VXLAN VTEP
	Vxlan
)

// An Endpoint describes how a testbed node connects to a virtual network.
type Endpoint struct {
	// The node being connected
	Node string
	// The interface being connected
	Interface string
	// Whether or not the interface multiplexes multiple virtual links
	Multidegree bool
}

// BelugaSpec encapsulates required data for beluga power control commands.
type BelugaSpec struct {
	Soft  bool
	Nodes []string
}

// Vtep encapsulates required data for canopy VTEP commands.
type Vtep struct {
	//Node     string XXX should alwyas be the host rex is running on
	//Dev      string XXX rex-local configuration
	Bridge string
	//TunnelIP string XXX rex local information
}

// VtepSpec encapsulates a canopy VTEP specification.
type VtepSpec struct {
	Vni   int
	Vid   int
	Vteps []Vtep
}

//// node spec ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// A NodeSpec encapsulates the data needed to provision or recycle a testbed
// node.
type NodeSpec struct {
	Resource    string
	Xname       string
	Mac         string
	Interface   string
	Multidegree bool
	SvcEndpoint string
	Infranet    *TargetNetwork
	Binding     *ImageBinding
	Config      *foundry.MachineConfig
}

// TargetNetwork describes the infrastructure network a testbed node will attach
// to.
type TargetNetwork struct {
	Vni int
	Vid int
}

//// sled /////////////////////////////////////////////////////////////////////

// An ImageBinding contains the required information to bind an image to a node
// using sled.
type ImageBinding struct {
	Image *sled.Image // image storage information
	Kexec *sled.Kexec // local kexec after initial sled write
}

// An ImageSpec contains all the information needed to create a set of image
// bindings through sled.
type ImageSpec struct {
	Endpoint string // location of sledb
	List     []ImageBinding
	NoVerify bool
}

//// foundry //////////////////////////////////////////////////////////////////

// A FoundrySetSpec encapsulates the information needed to configure a set of
// machines using foundry.
type FoundrySetSpec struct {
	Endpoint string
	Machines []*foundry.MachineConfig
}
