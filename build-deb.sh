#!/bin/bash

TARGET=${TARGET:-amd64}
DEBUILD_ARGS=${DEBUILD_ARGS:-""}

rm -f build/*.build*
rm -f build/*.change
rm -f build/*.deb

debuild -e V=1 -e prefix=/usr $DEBUILD_ARGS -i -us -uc -b

mv ../*.build* build/
mv ../*.changes build/
mv ../*.deb build/
