#!/bin/bash

#TODO replace me with real code


if [[ $# -ne 2 ]]; then
  echo "usage: enclave <netns> <index> <srcnet> <srcip> <igw> <egw>"
  echo "example: enclage ry.bakery.muffins 47 172.22.3.0/24 172.22.3.185 172.30.0.1 172.22.3.1"
  exit 1
fi

ns=$1
index=$2
srcnet=$3
srcip=$4
iw=$5

ns="ip netns exec $ns"

if [[ $DOWN -eq 1 ]]; then
  ip netns del $ns
  ip link del mzbr$index
  ip link del vrf$index

  ip rule del pref 200 iif mzbr${index} table $index
  ip rule del pref 200 oif mzbr${index} table $index
  ip rule del pref 200 iif vrf${index} table $index
  ip rule del pref 200 oif vrf${index} table $index
  ip rule del pref 200 oif ifr${index} table $index
  ip rule del pref 200 iif gw${index} table $index
  ip rule del pref 200 oif gw${index} table $index
  ip rule del pref 200 oif ifr${index} table $index
  ip rule del fwmark $index priority 10 table $index
  ip route del table $index default
  ip route del table $index $srcnet dev eth0
  rm /etc/iproute2/rt_tables.d/$ns.conf

  exit 0
fi

# bridge
## TODO in code
# -------> ip link add link mzbr${index} gw${index} type macvlan mode passthru
# -------> ip addr add 172.30.0.1/16 dev gw${index}
# -------> ip link set up dev gw${index}
$ns ip route add default via $igw dev eth2 ###

#vrf
ip link add vrf${index} type vrf table $index ###
echo "$index vrf${index}" > /etc/iproute2/rt_tables.d/$ns.conf ###
ip rule add pref 200 iif mzbr${index} table $index ###
ip rule add pref 200 oif mzbr${index} table $index ###
ip rule add pref 200 iif vrf${index} table $index ###
ip rule add pref 200 oif vrf${index} table $index ###
ip rule add pref 200 iif gw${index} table $index ###
ip rule add pref 200 oif gw${index} table $index ###
ip rule add pref 200 oif ifr${index} table $index ###
ip route add table $index ${srcnet} dev eth0 ###
ip route add table $index default via $egw ###
ip link set master vrf${index} dev gw${index}
ip link set dev vrf${index} up
ip rule add fwmark $index priority 10 table $index ###

