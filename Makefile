svcs = build/rex build/driver
utils = build/cog build/merge-confgen build/cnc build/mario build/rscheck

prefix ?= /usr/local

VERSION = $(shell git describe --always --long --dirty --tags)
LDFLAGS = "-X gitlab.com/mergetb/tech/cogs/pkg/common.Version=$(VERSION)"

.PHONY: all
all: $(svcs) $(utils)

.PHONY: qc
qc: all | .tools/golangci-lint
	$(QUIET) .tools/golangci-lint run

.PHONY: test
test: fabric-test

.PHONY: fabric-test
fabric-test: pkg/fabric/linkplan_test.go pkg/fabric/*.go
	cd pkg/fabric; go test

build/rex: rex/main.go \
		rex/*.go \
		pkg/fabric/*.go \
		pkg/task/*.go \
		pkg/runtime/*.go \
		pkg/common/*.go | build
	$(go-build)

build/driver: driver/main.go \
		driver/*.go \
		pkg/task/*.go \
		pkg/common/*.go | build
	$(go-build)

build/cog: util/cogctl/main.go \
		util/cogctl/*.go \
		pkg/fabric/*.go \
		pkg/task/*.go \
		pkg/runtime/*.go \
		pkg/common/*.go | build
	$(go-build)


build/cnc: util/cnc/main.go util/cnc/*.go
	$(go-build)

build/mario: util/mario/main.go util/mario/*.go pkg/fabric/*.go
	$(go-build)

build/rscheck: util/rscheck/main.go util/rscheck/*.go pkg/task/*.go
	$(go-build)

build/merge-confgen: util/merge-confgen/main.go \
		util/merge-confgen/*.go \
		pkg/runtime/*.go | build
	$(go-build)

build:
	$(QUIET) mkdir -p build

.PHONY: clean
clean:
	$(QUIET) rm -rf build

.PHONY: install
install: build/rex build/driver build/cog.bash_autocomplete
	install -D build/rex $(DESTDIR)$(prefix)/bin/rex
	install -D build/driver $(DESTDIR)$(prefix)/bin/driver
	install -D build/cog $(DESTDIR)$(prefix)/bin/cog
	install -D build/cog.bash_autocomplete $(DESTDIR)/usr/share/bash-completion/completions/cog

build/cog.bash_autocomplete: build/cog
	build/cog -c debian/config/runtime.yml autocomplete > $@

include merge.mk
