module gitlab.com/mergetb/tech/cogs

go 1.12

require (
	github.com/Microsoft/hcsshim v0.8.10 // indirect
	github.com/containerd/containerd v1.3.2
	github.com/containerd/continuity v0.0.0-20200928162600-f2cc35102c2a // indirect
	github.com/containerd/cri v1.19.0 // indirect
	github.com/containerd/fifo v0.0.0-20201026212402-0724c46b320c // indirect
	github.com/containerd/typeurl v1.0.1 // indirect
	github.com/coreos/etcd v3.3.18+incompatible
	github.com/davecgh/go-spew v1.1.1
	github.com/docker/distribution v2.7.1+incompatible // indirect
	github.com/docker/go-events v0.0.0-20190806004212-e31b211e4f1c // indirect
	github.com/fatih/color v1.9.0
	github.com/gin-gonic/gin v1.5.0 // indirect
	github.com/gofrs/uuid v3.2.0+incompatible
	github.com/golang/protobuf v1.3.5
	github.com/mergetb/yaml/v3 v3.0.1
	github.com/mitchellh/mapstructure v1.2.2
	github.com/opencontainers/image-spec v1.0.1 // indirect
	github.com/opencontainers/runc v0.1.1 // indirect
	github.com/opencontainers/runtime-spec v1.0.2
	github.com/osrg/gobgp v2.0.0+incompatible
	github.com/sirupsen/logrus v1.6.0
	github.com/spf13/cobra v1.0.0
	github.com/syndtr/gocapability v0.0.0-20200815063812-42c35b437635 // indirect
	github.com/teris-io/shortid v0.0.0-20171029131806-771a37caa5cf
	gitlab.com/mergetb/facilities/dcomp/model v0.1.3
	gitlab.com/mergetb/mcc v0.1.12
	gitlab.com/mergetb/site v0.1.7-0.20200227020539-b858619a6bf6
	gitlab.com/mergetb/tech/beluga v0.1.15-1
	gitlab.com/mergetb/tech/canopy v0.2.9-0.20200128043525-51ab1bfb3ab2
	gitlab.com/mergetb/tech/foundry v0.3.6
	gitlab.com/mergetb/tech/gobble v0.1.4-0.20200418202003-193e4b3f83c7
	gitlab.com/mergetb/tech/network-emulation/moa v0.2.0
	gitlab.com/mergetb/tech/nex v0.5.4
	gitlab.com/mergetb/tech/rally v0.0.2 // indirect
	gitlab.com/mergetb/tech/rtnl v0.1.10-0.20191007010639-4a31a3b4d513
	gitlab.com/mergetb/tech/sled v0.8.0
	gitlab.com/mergetb/xir v0.2.15
	golang.org/x/sys v0.0.0-20200331124033-c3d80250170d
	google.golang.org/grpc v1.27.0
	gotest.tools v2.2.0+incompatible // indirect
)

replace github.com/coreos/etcd => github.com/coreos/etcd v3.3.11+incompatible

replace github.com/docker/distribution v2.7.1+incompatible => github.com/docker/distribution v2.7.1-0.20190205005809-0d3efadf0154+incompatible
