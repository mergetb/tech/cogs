package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/tech/cogs/pkg/common"
	"gitlab.com/mergetb/tech/cogs/pkg/task"
)

// MattermostAlertMessage is a messaging struct for Mattermost.
// There may be some overlap with Slack, this has not been tested.
// https://docs.mattermost.com/developer/webhooks-incoming.html
type MattermostAlertMessage struct {
	Text        string                  `json:",omitempty"`
	Channel     string                  `json:",omitempty"`
	Attachments []*MattermostAlertField `json:",omitempty"`
}

// MattermostAlertField is a map for each type of attachment supported
// https://docs.mattermost.com/developer/message-attachments.html
type MattermostAlertField struct {
	Short   bool                     `json:",omitempty"`
	Title   string                   `json:",omitempty"`
	Value   string                   `json:",omitempty"`
	Text    string                   `json:",omitempty"`
	Pretext string                   `json:",omitempty"`
	Color   string                   `json:",omitempty"`
	Actions []*MattermostAlertAction `json:",omitempty"`
}

// MattermostAlertAction is an actionable message, these include
// creating buttons which local integrations can pick up on and use.
// the goal here would be to then have those integration connect
// back to the site/facility to remove the task error without needed
// to ssh directly onto the site/facility.
// https://docs.mattermost.com/developer/interactive-messages.html
type MattermostAlertAction struct {
	Id          string                 `json:",omitempty"`
	Name        string                 `json:",omitempty"`
	Integration *MattermostAlertButton `json:",omitempty"`
}

// typeMattermostAlertButton specifically handles the button
// creation/rendering/actionability for mattermost
type MattermostAlertButton struct {
	Url     string            `json:",omitempty"`
	Context map[string]string `json:",omitempty"`
}

// for each alert specified in the configuration file,
// send an alert to that source.
func sendAlerts(a *task.Action, actionErr error) error {
	// verify that are config file is not nil and that there are
	// alerts to take action on
	if cfg == nil || cfg.Alerts == nil {
		return nil
	}

	// go through the alerts and start sending messages
	for _, alert := range cfg.Alerts {
		if alert.Address == "" {
			return common.Error("Alert missing address field")
		}

		switch alert.Type {
		case "slack":
			fallthrough
		case "mattermost":
			err := sendMatterMost(a, actionErr, alert)
			if err != nil {
				return common.ErrorE("unable to send alert", err)
			}
		case "email":
			log.Error("Email not implemented")
		default:
			log.Errorf("Unknown alert method: %s", alert.Type)
		}
	}

	return nil
}

func PrintAlerts() {
	log.Info("Runtime Alerts:")
	if cfg == nil || cfg.Alerts == nil {
		log.Info("None")
		return
	}

	for _, alert := range cfg.Alerts {
		log.Infof("%+v", alert)
	}
}

func sendMatterMost(
	a *task.Action, errMsg error, alert *common.AlertConfig,
) error {

	// Create a single message with title of the site reporting the error
	// below that will be 2 attachments.
	// attach 1: Action details
	// attach 2: Error message with actionable button
	am := &MattermostAlertMessage{
		Text: fmt.Sprintf("[%s] site %s:",
			time.Now().Format(task.TimeFormat), cfg.Site,
		),
		Attachments: []*MattermostAlertField{
			{
				Pretext: "Action",
				Text:    fmt.Sprintf("Kind: %s, Mzid: %s", a.Kind, a.Mzid),
			},
		},
	}

	am.Attachments = append(am.Attachments, &MattermostAlertField{
		Pretext: "Status",
		Color:   "#FF0000", // red color
		Text:    fmt.Sprintf("Error: %v", errMsg),
		/*
			// TODO: have a button to phone back to dcomptb and retry task
			Actions: []*MattermostAlertAction{
				{
					Id:   "retry",
					Name: "Retry",
					Integration: &MattermostAlertButton{
						Context: map[string]string{
							"action": "send_admin_task_retry",
						},
					},
				},
			},
		*/
	})

	// build our messages into json for mattermost to parse
	msg, err := json.Marshal(am)
	if err != nil {
		return err
	}

	// send webhook post
	resp, err := http.Post(
		alert.Address, "application/json", bytes.NewBuffer(msg),
	)

	// if we got an error or non-200 code we should log that
	if err != nil {
		return err
	}
	if resp.StatusCode != 200 {
		log.Warningf("alert recieved non-200 code: %s", resp.Status)
	}

	return nil
}
