package main

import (
	"context"
	"fmt"
	"time"

	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"

	"gitlab.com/mergetb/tech/beluga/pkg"
	"gitlab.com/mergetb/tech/cogs/pkg/common"
	"gitlab.com/mergetb/tech/cogs/pkg/task"

	ctlapi "gitlab.com/mergetb/tech/beluga/pkg/belugactld"
)

const (
	belugactlCert     = "/etc/beluga/manage.pem"
	belugactlMgmtPort = 27001
)

func withBeluga(f func(beluga.BelugaClient) error) error {

	conn, err := connection(
		fmt.Sprintf("%s:%d", cfg.Beluga.Address, cfg.Beluga.Port),
		grpc.WithInsecure(),
	)
	if err != nil {
		log.WithError(err).Error("failed to dial beluga")
		return err
	}

	return f(beluga.NewBelugaClient(conn))

}

func belugaOn(device string, soft bool) error {

	return belugaFunc(beluga.DoRequest_On, device, soft)

}

func belugaCycle(device string, soft bool) error {

	return belugaFunc(beluga.DoRequest_Cycle, device, soft)

}

func belugaFunc(op beluga.DoRequest_Action, device string, soft bool) error {

	return withBeluga(func(c beluga.BelugaClient) error {

		_, err := c.Do(context.TODO(), &beluga.DoRequest{
			Action: op,
			Device: device,
			Soft:   soft,
		})

		return err

	})

}

//
// belugactl functions. (belugactl is an infrapod service for calling
// beluga functions on behalf of end users)
//
func addBelugactlConfig(spec *task.NodeSpec) error {

	if spec == nil {
		return fmt.Errorf("empty node spec")
	}

	return withBelugactl(
		spec.SvcEndpoint,
		func(ctx context.Context, cli ctlapi.BelugactldManageClient) error {
			_, err := cli.SetNodeInfo(
				ctx,
				&ctlapi.SetNodeInfoRequest{
					Resource: spec.Resource,
					Xpname:   spec.Xname,
				},
			)

			return err
		},
	)
}

func withBelugactl(endpoint string, f func(context.Context, ctlapi.BelugactldManageClient) error) error {

	conn, cli, err := belugaCtlConnect(endpoint)
	if err != nil {
		return err
	}
	defer conn.Close()

	ctx, cancel := context.WithTimeout(context.TODO(), 3*time.Second)
	defer cancel()

	return f(ctx, cli)
}

func belugaCtlConnect(endpoint string) (*grpc.ClientConn, ctlapi.BelugactldManageClient, error) {

	endpoint = fmt.Sprintf("%s:%d", endpoint, belugactlMgmtPort)

	//------
	creds, err := tlsConfig(endpoint, belugactlCert)
	if err != nil {
		return nil, nil, common.ErrorE("belugactl tls config failed", err)
	}
	//------

	conn, err := grpc.Dial(endpoint,
		grpc.WithTransportCredentials(creds),
		grpc.WithDefaultCallOptions(
			grpc.MaxCallRecvMsgSize(common.MaxMessageSize),
			grpc.MaxCallSendMsgSize(common.MaxMessageSize),
		),
	)
	if err != nil {
		return nil, nil, common.ErrorE("belugactl connection failed", err)
	}

	return conn, ctlapi.NewBelugactldManageClient(conn), nil

}
