// +build mock

package main

import (
	"math/rand"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/tech/cogs/pkg/common"
	"gitlab.com/mergetb/tech/cogs/pkg/task"
	sled "gitlab.com/mergetb/tech/sled/pkg"
)

func areWeMocked() bool {
	return true
}

// mockSledImage manages the imaging process of sled.
// The logic here is to do the same writes that would be caused
// by sledc -> sledd -> sledapi -> etcd communications.
// Then after some amount of time we return without any issues.
func mockSledImage(spec *task.NodeSpec) error {
	fields := log.Fields{
		"node":    spec.Resource,
		"binding": spec.Binding,
	}

	if spec.Binding == nil {
		return common.ErrorF("spec binding is nil", fields)
	}
	if spec.Binding.Image == nil {
		return common.ErrorF("spec image is nil", fields)
	}

	cs := &sled.CommandSet{}
	wr := &sled.WriteResponse{
		Device:    spec.Binding.Image.Blockdevice,
		Image:     spec.Binding.Image.Disk,
		Kernel:    spec.Binding.Image.Kernel,
		Initramfs: spec.Binding.Image.Initramfs,
	}
	kr := &sled.KexecResponse{
		Append:    spec.Binding.Image.Append,
		Kernel:    "/tmp/kernel",
		Initramfs: "/tmp/initrd",
	}

	cs.Write = wr
	cs.Kexec = kr

	// this will cause a write to etcd through a PutCommandRequest
	err := addCommandSet(task.HarborSvcAddr, spec.Mac, cs)
	if err != nil {
		return common.ErrorEF("add command set failed", err, fields)
	}

	// here we need to sleep a random amount of time to simulate
	// waiting for the sledc to image
	time.Sleep(time.Duration(rand.Intn(10)) * time.Second)

	return nil
}

// mockSledDemat is responsible for mocking the demat process in rex.
func mockSledDemat(spec *task.NodeSpec) error {
	fields := log.Fields{
		"node":    spec.Resource,
		"binding": spec.Binding,
	}

	if spec.Binding == nil {
		return common.ErrorF("spec binding is nil", fields)
	}
	if spec.Binding.Image == nil {
		return common.ErrorF("spec image is nil", fields)
	}

	cs := &sled.CommandSet{}
	wr := &sled.WriteResponse{
		Device:    spec.Binding.Image.Blockdevice,
		Image:     spec.Binding.Image.Disk,
		Kernel:    spec.Binding.Image.Kernel,
		Initramfs: spec.Binding.Image.Initramfs,
	}

	cs.Write = wr
	cs.Daemon = sled.EnumStatus_CONFIGURED

	// this will cause a write to etcd through a PutCommandRequest
	err := addCommandSet(task.HarborSvcAddr, spec.Mac, cs)
	if err != nil {
		return common.ErrorEF("add command set failed", err, fields)
	}

	// here we need to sleep a random amount of time to simulate
	// waiting for the sledc to image
	time.Sleep(time.Duration(rand.Intn(20)) * time.Second)

	return nil
}

func nodeSetupImage(spec *task.NodeSpec, fields log.Fields) error {
	err := mockSledImage(spec)
	if err != nil {
		return common.ErrorEF("mock sledding failed", err, fields)
	}

	return err
}

func nodeRecycleImage(spec *task.NodeSpec, fields log.Fields) error {
	err := mockSledDemat(spec)
	if err != nil {
		return common.ErrorEF("mock demat failed", err, fields)
	}

	return err
}
