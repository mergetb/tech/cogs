package main

import (
	"fmt"
	"testing"

	"gitlab.com/mergetb/tech/cogs/pkg/common"
	"gitlab.com/mergetb/tech/cogs/pkg/runtime"
	"gitlab.com/mergetb/tech/cogs/pkg/task"
)

func TestAlert(t *testing.T) {

	alert := &task.Action{
		Mzid:   "drugs.are.bad.mkay",
		Taskid: "mrmackey",
		ID:     "southpark",
	}

	cfg = &runtime.Config{
		Site: "Testing",
		Alerts: []*common.AlertConfig{
			{
				Type: "mattermost",
			},
		},
		Etcd: &common.ServiceConfig{
			Address: "localhost",
			Port:    2379,
			TLS:     nil,
		},
	}

	common.SetEtcdConfig(cfg.Etcd)

	err := sendAlerts(alert, fmt.Errorf("oops."))
	if err != nil {
		t.Fatal(err)
	}
}
