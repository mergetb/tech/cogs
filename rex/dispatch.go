package main

import (
	"fmt"

	"gitlab.com/mergetb/tech/cogs/pkg/common"
	"gitlab.com/mergetb/tech/cogs/pkg/task"
)

func doAction(action *task.Action) error {

	switch action.Kind {

	case task.ContainerKind:
		return doContainerAction(action)

	case task.NexKind:
		return doNexAction(action)

	case task.CanopyKind:
		return doCanopyAction(common.TbModel(), action)

	case task.PlumbingKind:
		return doPlumbingAction(action)

	case task.BookkeepingKind:
		return doBookkeepingAction(action)

	case task.NodeSetupKind:
		return nodeSetup(action)

	case task.NodeRecycleKind:
		return nodeRecycle(action)

	case task.MoaKind:
		return doMoaAction(action)

	default:
		return fmt.Errorf("unknown action kind %s", action.Kind)
	}

}
