package main

import (
	"context"
	"fmt"
	"time"

	"github.com/mitchellh/mapstructure"
	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/tech/nex/pkg"
	"google.golang.org/grpc"

	"gitlab.com/mergetb/tech/cogs/pkg/common"
	"gitlab.com/mergetb/tech/cogs/pkg/task"
)

func doNexAction(action *task.Action) error {

	switch action.Action {

	case task.CreateNexNetwork:
		return doCreateNexNetwork(action)

	case task.AddNexMembers:
		return doAddNexMembers(action)

	default:
		return fmt.Errorf("unknown nex action '%s'", action.Action)

	}

}

func doCreateNexNetwork(action *task.Action) error {

	spec := &task.NexNetworkSpec{}
	err := mapstructure.Decode(action.Data, spec)
	if err != nil {
		log.
			WithError(err).
			WithFields(log.Fields{"kind": "nex"}).
			Error("failed to parse nex network")
		return err
	}

	err = validateSpec(spec)
	if err != nil {
		log.
			WithError(err).
			WithFields(log.Fields{"kind": "nex"}).
			Error("bad spec")
		return err
	}

	return withNexClient(spec.Endpoint, func(client nex.NexClient) error {

		// clear out any existing network
		_, err = client.DeleteNetwork(context.TODO(),
			&nex.DeleteNetworkRequest{Name: spec.Network.Name})
		if err != nil {
			log.Warn(err)
		}

		_, err = client.AddNetwork(context.TODO(),
			&nex.AddNetworkRequest{Network: &spec.Network})
		if err != nil {
			return common.ErrorE("failed to create nex network", err)
		}

		return action.SetActionComplete()

	})

}

func doAddNexMembers(action *task.Action) error {

	spec := &task.NexMemberSpec{}
	err := mapstructure.Decode(action.Data, &spec)
	if err != nil {
		return common.ErrorE("failed to decode nex task", err)
	}

	return withNexClient(spec.Endpoint, func(client nex.NexClient) error {

		spec.MemberList.Force = true
		_, err := client.AddMembers(context.TODO(), &spec.MemberList)
		if err != nil {
			return common.ErrorE("failed to add nex members", err)
		}

		return action.SetActionComplete()

	})

}

func addNexMember(net string, spec *task.NodeSpec) error {

	return withNexClient(spec.SvcEndpoint, func(client nex.NexClient) error {

		_, err := client.AddMembers(context.TODO(), &nex.MemberList{
			Net:   net,
			Force: true,
			List: []*nex.Member{{
				Mac:  spec.Mac,
				Name: spec.Xname,
			}},
		})
		return err

	})

}

func dial(addr string) (*grpc.ClientConn, nex.NexClient) {

	endpoint := fmt.Sprintf("%s:6000", addr)
	conn, err := grpc.Dial(endpoint,
		grpc.WithInsecure(),
		grpc.WithDefaultCallOptions(
			grpc.MaxCallRecvMsgSize(common.MaxMessageSize),
			grpc.MaxCallSendMsgSize(common.MaxMessageSize),
		),
	)
	if err != nil {
		log.Fatal(err)
	}

	return conn, nex.NewNexClient(conn)
}

func withNexClient(addr string, f func(nex.NexClient) error) error {

	conn, cli := dial(addr)
	defer conn.Close()

	var err error
	for i := 0; i < 10; i++ {
		_, err = cli.GetNetworks(context.TODO(), &nex.GetNetworksRequest{})
		if err == nil {
			return f(cli)
		}
		time.Sleep(500 * time.Millisecond)
	}

	return common.ErrorE("failed to connect to nex", err)

}

func validateSpec(spec *task.NexNetworkSpec) error {

	if spec.Network.Name == "" {
		return fmt.Errorf("nex network name missing")
	}

	return nil

}
