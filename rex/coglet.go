package main

import (
	"context"
	"os"
	"time"

	"github.com/coreos/etcd/clientv3"
	"github.com/gofrs/uuid"
	"gitlab.com/mergetb/tech/cogs/pkg/common"
	"gitlab.com/mergetb/tech/cogs/pkg/runtime"
)

const (
	timeout = 5 * time.Second
)

var (
	clid = uuid.Must(uuid.NewV4()).String()
)

func registerCoglet(c *clientv3.Client, cog string) (clientv3.LeaseID, error) {

	host, err := os.Hostname()
	if err != nil {
		return -1, err
	}

	ci := &runtime.CogletInfo{
		ID:   clid,
		Cog:  cog,
		Host: host,
		Pid:  os.Getpid(),
	}

	ctx, cancel := context.WithTimeout(context.TODO(), timeout)
	lease, err := c.Grant(ctx, 5)
	defer cancel()
	if err != nil {
		return -1, err
	}

	return lease.ID, common.Write(ci, clientv3.WithLease(lease.ID))

}
