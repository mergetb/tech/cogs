package main

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/tech/cogs/pkg/common"
	"gitlab.com/mergetb/tech/cogs/pkg/task"
)

func doBookkeepingAction(action *task.Action) error {

	fields := log.Fields{
		"action": action.Action,
		"mzid":   action.Mzid,
	}

	log.WithFields(fields).Info("begin bookkeeping action")

	switch action.Action {

	case task.DeleteMzinfo:

		mzinfo := &task.MzInfo{Mzid: action.Mzid}
		err := common.Read(mzinfo)
		if err != nil {
			common.WarnEF("failed to read mzinfo", err, fields)
		}

		err = task.DeleteMzInfo(mzinfo)
		if err != nil {
			return common.ErrorEF("failed to delete mzinfo", err, fields)
		}

		err = task.DeleteTasks(action.Mzid)
		if err != nil {
			return common.ErrorEF("failed to delete tasks", err, fields)
		}

	case task.SetStatus:

		status, ok := action.Data.(float64)
		if !ok {
			fields["status"] = action.Data
			return common.ErrorF("set status data invalid", fields)
		}

		mzinfo := &task.MzInfo{Mzid: action.Mzid}
		err := common.Read(mzinfo)
		if err != nil {
			return common.ErrorEF("failed to read mzinfo", err, fields)
		}

		err = common.RUC(mzinfo, func(o common.Object) {
			o.(*task.MzInfo).Status = task.MzStatus(status)
		})
		if err != nil {
			log.Warn(err)
		}

		return action.SetActionComplete()

	}

	return nil
}
