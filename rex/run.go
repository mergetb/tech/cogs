package main

import (
	"context"
	"encoding/json"
	"fmt"
	"sync"
	"sync/atomic"
	"time"

	"github.com/coreos/etcd/clientv3"
	"github.com/coreos/etcd/mvcc/mvccpb"
	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/tech/cogs/pkg/common"
	"gitlab.com/mergetb/tech/cogs/pkg/task"
)

func exec(etcdp **clientv3.Client, cog string, doTask func(*task.Task) error) error {

	// make sure our etcd connection is a thing
	err := common.EnsureEtcd(etcdp)
	if err != nil {
		return err
	}

	// use etcd watch to see if there are any actions (ready) to be executed
	// we will only break out of this if we fail to etcd connect
	rch := (*etcdp).Watch(context.Background(), "/status/ready/", clientv3.WithPrefix())
	log.Debugf("begin watch: %v", rch)

	// on first time run, we want to see everything in the action ready list
	err = touchAllSR()
	if err != nil {
		return err
	}

	for wresp := range rch {

		// make sure our etcd connection is a thing, because we dont leave the watch
		err := common.EnsureEtcd(etcdp)
		if err != nil {
			return err
		}

		// for the responses in the channel, only call Triage on a task once
		taskList := make(map[string]bool)

		for _, x := range wresp.Events {
			if x.Type == mvccpb.DELETE {
				continue
			}

			asr := &task.ASReady{}
			err := json.Unmarshal(x.Kv.Value, asr)
			if err != nil {
				common.WarnE("failed to unmarshal", err)
				continue
			}

			as := &task.ActionStatus{
				Mzid:   asr.Mzid,
				Taskid: asr.Taskid,
				ID:     asr.Actionid,
			}

			// only pick up tasks we havent seen to triage
			_, ok := taskList[as.Taskid]
			if !ok {
				taskList[as.Taskid] = true
				log.Tracef("parsing task: %#v", as.Taskid)

				t, err := task.ParseAction(as)
				if err != nil {
					log.Errorf("failed to parse action %v", err)
					// may have been some other issue, try to retouch for
					// watch to pick back up
					err := common.Touch(asr)
					if err != nil {
						log.Warn(err)
					}
					continue
				}

				// Triage has a barrier to prevent running dependencies
				go func() {
					err := triageTask(*etcdp, t, doTask)
					if err != nil {
						log.Warn(err)
					}
				}()
			}
		}
	}

	return nil
}

func triageTask(
	etcd *clientv3.Client,
	t *task.Task,
	doTask func(*task.Task) error,
) error {

	fields := log.Fields{
		"mzid": t.Mzid,
		"id":   t.ID,
	}
	log.WithFields(fields).Info("triaging task")

	// get the action statuses (which could be updated based on mzid)
	actions, err := task.GetMzidAS(t.Mzid, t.ID)
	if err != nil {
		return common.ErrorEF("get mzid as", err, fields)
	}

	log.WithFields(fields).Tracef("task has %d actions", len(actions))

	// If the action map is not empty, then we can check on the
	// tasks to see if they are complete or failed.  If the action
	// map is empty, then we are likely looking at a task just spawned
	if len(actions) > 0 {

		failed, err := t.Failed(actions)
		if err != nil {
			return common.ErrorEF("failed tasks", err, fields)
		}

		complete, err := t.Complete(actions)
		if err != nil {
			return common.ErrorEF("not complete", err, fields)
		}

		if complete || failed != nil {
			return nil
		}

		// get all the actions which are incomplete
		incomplete, err := t.DepsSatisfied()
		if err != nil {
			return common.ErrorEF("error in dependency check", err, fields)
		}

		// if there are actions to be had, we must wait until they finish
		for _, as := range incomplete {
			// this is going to wait for infinity or completed
			err := waitForStatus(etcd, t, as)
			if err != nil {
				log.Warn(err)
			}
		}
	}

	// lease the task
	lease, err := leaseTask(etcd, t)
	if err != nil {
		return common.ErrorEF("failed to lease task", err, fields)
	}

	if lease == 0 { // lease taken
		return nil
	}

	// the task is not complete and we have the lease, carry out the task
	handleTask(etcd, t, lease, doTask)

	return nil

}

func waitForStatus(etcdp *clientv3.Client, t *task.Task, as *task.ActionStatus) error {

	// check before we go into watch if the action is complete
	// get the key revision from last time we looked it up so
	// we can have watch replay any that we've since missed
	rev, err := common.ReadRevision(as)
	if err != nil && err != common.ErrNotFound {
		return err
	}
	// if the key is not found the action status was deleted, which can
	// happen if the action status was a demat and a subsequent mat had
	// it for a dependency - in which case we are done waiting
	if err == common.ErrNotFound {
		return nil
	}
	if as.Complete {
		return nil
	}

	log.Tracef("%s waiting for: %s", t.ID, as.ID)

	// watch the key now, it will watch until end of time
	rch := (*etcdp).Watch(context.TODO(), as.Key(), clientv3.WithRev(rev))
	for wresp := range rch {
		for _, x := range wresp.Events {
			if x.Type == mvccpb.DELETE {
				continue
			}

			askv := &task.ActionStatus{}
			err := json.Unmarshal(x.Kv.Value, askv)
			if err != nil {
				return err
			}

			log.Tracef("watch awake for %s on: %s", t.ID, as.ID)

			// if the action has been set to complete, done watching
			// otherwise we watch until the update comes through
			if askv.Complete {
				return nil
			}
		}
	}

	// will never execute this
	return nil
}

func revokeLease(etcd *clientv3.Client, lease clientv3.LeaseID) {
	ctx, cancel := context.WithTimeout(context.TODO(), 200*time.Millisecond)
	defer cancel()
	_, err := etcd.Revoke(ctx, lease)
	if err != nil {
		log.WithError(err).Warn("error revoking task lease")
	}
}

func keepLeaseAlive(etcd *clientv3.Client, lease clientv3.LeaseID) (
	chan bool, error) {

	ka, err := etcd.KeepAlive(context.TODO(), lease)
	if err != nil {
		return nil, err
	}

	done := make(chan bool)
	go func(ka <-chan *clientv3.LeaseKeepAliveResponse, done chan bool) {
		for {
			select {
			case <-done:
				return
			case <-ka:
				log.WithFields(log.Fields{"lease": lease}).Trace("lease keepalive")
			}
		}
	}(ka, done)

	return done, nil

}

func touchAllSR() error {
	allASR, err := task.EtcdActionReadyPrefix()
	if err != nil {
		return err
	}

	// only touch if something is in the ready queue
	if len(allASR) > 0 {
		touches := []common.Object{}
		for _, asr := range allASR {
			touches = append(touches, asr)
		}
		err = common.TouchObjects(touches)
		if err != nil {
			return err
		}
	}
	return nil
}

func doTask(t *task.Task) error {

	fields := log.Fields{
		"mzid": t.Mzid,
		"id":   t.ID,
	}
	log.WithFields(fields).Trace("doing task")

	stages, err := task.GetTaskStages(t.Mzid, t.ID)
	if err != nil {
		return err
	}

	log.WithFields(fields).Tracef("found %d stages", len(stages))

	var as *task.ActionStatus

	for _, stage := range stages {

		log.WithFields(fields).Tracef("executing stage %s", stage.ID)

		// actions within a stage can be done in parallel, but stages themselves
		// must be serialized
		var wg sync.WaitGroup

		// atomic counter used by go routines to count errors
		var errorCount uint32

		for _, actionID := range stage.Actions {

			action, err := task.GetAction(t.Mzid, t.ID, actionID)
			if err != nil {
				return err
			}

			if action == nil {
				continue
			}

			as, err = task.GetActionStatus(action.Mzid, t.ID, action.ID)
			if err != nil {
				log.Error(err)
				continue
			}

			if as.Masked {
				continue
			}

			if !as.Complete {
				wg.Add(1)
				go func(a *task.Action) {

					defer wg.Done()

					errVal := new(string)
					err := doAction(a)
					if err != nil {
						*errVal = err.Error()
						log.Error(err)
						atomic.AddUint32(&errorCount, 1)
						go func() {
							errA := sendAlerts(a, err)
							if errA != nil {
								log.Warn(errA)
							}
						}()
					} else {
						errVal = nil
					}

					// only update action status if err is not nil
					if errVal != nil {
						as := &task.ActionStatus{
							Mzid:   a.Mzid,
							Taskid: t.ID,
							ID:     a.ID,
						}
						err = common.RUC(as, func(o common.Object) {
							o.(*task.ActionStatus).Error = errVal
						})
					}

					if err != nil {
						common.WarnE("failed to update status", err)
						atomic.AddUint32(&errorCount, 1)
					}

				}(action)
			}

		}

		wg.Wait()

		fields["error-count"] = errorCount
		log.WithFields(fields).Tracef("stage %s finished", stage.ID)

		// bail if anything in the stage failed
		if errorCount > 0 {
			return fmt.Errorf("stage failed")
		}

	}
	return nil
}

// leaseTask attempts to take a lease out on a task. A coglet may not execute a
// task until it has a lease on the task.
func leaseTask(c *clientv3.Client, t *task.Task) (clientv3.LeaseID, error) {

	fields := log.Fields{
		"mzid": t.Mzid,
		"id":   t.ID,
	}
	log.WithFields(fields).Trace("leasing task")

	//get lease grant
	lease, err := c.Grant(context.TODO(), int64(cfg.Tuning.TaskLease))
	if err != nil {
		log.WithError(err).Error("failed to get lease grant")
		return 0, fmt.Errorf("lease failed")
	}

	// try to lease the task
	kvc := clientv3.NewKV(c)
	leasekey := fmt.Sprintf("/lease/%s", t.ID)
	txr, err := kvc.Txn(context.TODO()).
		If(
			clientv3.Compare(clientv3.Version(leasekey), "=", 0),
		).
		Then(
			clientv3.OpPut(leasekey, clid, clientv3.WithLease(lease.ID)),
		).
		Commit()
	if err != nil {
		log.WithError(err).Error("transaction error")
		return 0, fmt.Errorf("transaction error")
	}
	if !txr.Succeeded {
		//This means that the criteriea was not met, which likely just means that
		//another coglet has task or all state is gtg (target == current)
		log.WithFields(log.Fields{"lease": leasekey}).Trace("taken")
		return 0, nil
	}

	return lease.ID, nil
}

// handleTask attempts to execute the provided task. The datastore is updated
// with task state evolution as the stages and actions are executed.
func handleTask(
	etcd *clientv3.Client,
	task *task.Task,
	lease clientv3.LeaseID,
	doTask func(*task.Task) error,
) {

	fields := log.Fields{
		"id": task.ID,
	}

	log.WithFields(fields).Info("Handling Task")

	// keep the lease alive while the task is executing
	done, err := keepLeaseAlive(etcd, lease)
	if err != nil {
		log.WithError(err).WithFields(fields).Error("keepalive failed")
		return
	}

	// actually execute the task
	err = doTask(task)
	if err != nil {
		log.WithError(err).WithFields(fields).Error("task failed")
	}

	// kill the keepalive thread
	done <- true

	// either way we revoke the lease, if we failed, another lower half will
	// come along and try to drive this to completion
	revokeLease(etcd, lease)
	log.WithFields(fields).Info("task complete")
}
