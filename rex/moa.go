package main

import (
	"context"
	"fmt"
	"net"

	"github.com/mitchellh/mapstructure"
	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/tech/cogs/pkg/common"
	"gitlab.com/mergetb/tech/cogs/pkg/runtime"
	"gitlab.com/mergetb/tech/cogs/pkg/task"
	"gitlab.com/mergetb/tech/network-emulation/moa/pkg/manager"
	"gitlab.com/mergetb/tech/network-emulation/moa/pkg/moactld"
	"gitlab.com/mergetb/xir/lang/go/v0.2"
)

func doMoaAction(action *task.Action) error {
	fields := log.Fields{"action": action.Action}
	log.WithFields(fields).Info("begin moa action")

	data := []*task.LinkInfo{}
	err := mapstructure.Decode(action.Data, &data)
	if err != nil {
		return common.ErrorEF("failed to decode linkinfo", err, fields)
	}

	switch action.Action {

	case task.CreateMoaLink:
		err = createMoaLinks(data)
		if err != nil {
			return err
		}

	case task.RemoveMoaLink:
		err = removeMoaLinks(data)
		if err != nil {
			return err
		}

	default:
		return common.ErrorF("unknown moa action", fields)

	}

	return action.SetActionComplete()

}

//TODO all that's really being done here is stopping and discarding the moa
//emulation, then going on to delete the moa status data structure. This
//simplification is possible because of agregated
func removeMoaLinks(data []*task.LinkInfo) error {
	emuid := int32(data[0].Vindex)

	addr := fmt.Sprintf("%s:%d", runtime.DefaultEmulator, 50052)
	r, _, _, err := runtime.EmuInfo()
	if err != nil {
		return err
	}

	for _, linkInfo := range data {

		err := runtime.WithdrawMoaLink(
			linkInfo,
			r.System.OS.Config.Evpn.TunnelIP+"/32",
			r.System.OS.Config.Evpn.ASN,
		)
		if err != nil {
			return err
		}

	}

	err = manager.WithMoa(addr, func(c manager.MoaClient) error {

		log.Debugf("destroying moa emulation %d", emuid)
		_, err := c.Destroy(context.TODO(), &manager.EmulationRequest{EmulationId: emuid})
		if err != nil {
			return common.ErrorE("destroy emulation failed", err)
		}

		log.Debug("moa emulation destroyed")

		return nil
	})
	if err != nil {
		return common.ErrorE("destroy emulation failed", err)
	}

	return nil

}

func getUserTags(model xir.Props) []string {
	var tags []string
	modelTags, _ := model["tags"].([]interface{})
	for _, x := range modelTags {
		tags = append(tags, x.(string))
	}
	return tags
}

func createMoaLinks(data []*task.LinkInfo) error {

	emuId := int32(data[0].Vindex) // emulation ID
	mzid := data[0].Mzid           // materialization ID

	r, _, _, err := runtime.EmuInfo()
	if err != nil {
		return err
	}

	// fetch the node info for this mzid
	// this is used solely for information purposes to provide the experiment node
	// name to the user in moacmd
	nodeInfo, err := task.ListNodeInfo(mzid)
	if err != nil {
		return common.ErrorE("failed to get list of nodes for mz", err)
	}
	log.WithFields(log.Fields{"nodeinfo": nodeInfo}).Debug("node info list")

	var objs []*manager.LinkSpec

	for _, linkInfo := range data {

		obj := createMoaLink(linkInfo, nodeInfo)
		objs = append(objs, obj)

		err = runtime.AdvertiseMoaLink(
			linkInfo,
			r.System.OS.Config.Evpn.TunnelIP+"/32",
			r.System.OS.Config.Evpn.ASN,
		)
		if err != nil {
			return err
		}

	}

	addr := fmt.Sprintf("%s:%d", runtime.DefaultEmulator, 50052)
	err = manager.WithMoa(addr, func(c manager.MoaClient) error {

		log.Debug("creating emulation")
		_, err = c.Create(context.TODO(), &manager.CreateRequest{
			EmulationId: emuId,
			Mzid:        mzid,
			Links:       objs,
		})
		if err != nil {
			return common.ErrorE("creating emulation failed", err)
		}

		return nil

	})
	if err != nil {
		return err
	}

	// perform dns lookup here to avoid needing dns in the moactld container
	addrs, err := net.LookupHost(runtime.DefaultEmulator)
	if err != nil {
		log.WithFields(log.Fields{
			"err": err,
			"emu": runtime.DefaultEmulator,
		}).Error("unable to get IP addr for emulator")
		addr = runtime.DefaultEmulator //fallback
	} else {
		addr = addrs[0]
	}

	// get the address for the moactld infrapod
	ctlHost := task.SvcAddress(data[0].Vindex)

	// configure moactld
	fields := log.Fields{
		"control":               ctlHost,
		runtime.DefaultEmulator: addr,
		"emuid":                 emuId,
	}
	log.WithFields(fields).Debug("initializing moactld")

	err = moactld.WithMoaControl(
		ctlHost, func(c moactld.MoaControlClient) error {
			_, err := c.Init(context.TODO(), &moactld.InitRequest{
				EmulationHost: addr,
				EmulationPort: manager.ControlPort,
				EmulationId:   emuId,
			})
			if err != nil {
				return common.ErrorE("initializing moactld failed", err)
			}
			return nil
		})
	if err != nil {
		return err
	}

	return nil

}

// return the node node in the experiment given the testbed node name
func getExpNodeName(nodeInfo []*task.NodeInfo, s string) string {
	for _, n := range nodeInfo {
		if n.Machine == s {
			return n.XpName
		}
	}
	return ""
}

func createMoaLink(linkInfo *task.LinkInfo, nodeInfo []*task.NodeInfo) *manager.LinkSpec {

	latency, capacity, loss := task.BasicLinkConstraints(linkInfo.Props)

	var vteps []string
	var nodenames []string
	for _, spec := range linkInfo.TBVLinks {
		vtep := fmt.Sprintf("vtep%d", spec.Vni)
		vteps = append(vteps, vtep)
		log.WithFields(log.Fields{"edges": spec.Edges}).Debug("edge info")
		nodenames = append(nodenames, getExpNodeName(nodeInfo, spec.Edges[0].NodeName))
	}

	return &manager.LinkSpec{
		Interfaces: vteps,
		Params: &manager.LinkParams{
			Capacity:    capacity,
			Latency:     latency,
			Loss:        float32(loss),
			EnforceLoss: true,
		},
		Tags:     getUserTags(linkInfo.Props),
		ExpNodes: nodenames,
	}
}
