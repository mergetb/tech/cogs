package main

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"io/ioutil"
	"time"

	api "gitlab.com/mergetb/tech/foundry/api"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"

	"gitlab.com/mergetb/tech/cogs/pkg/common"
	"gitlab.com/mergetb/tech/cogs/pkg/task"
)

const (
	foundryCert     = "/etc/foundry/manage.pem"
	foundryMgmtPort = 27000
)

func addFoundryConfig(spec *task.NodeSpec) error {

	if spec == nil {
		common.Warn("addFoundry: spec is nil")
		return nil
	}

	return withFoundry(spec.SvcEndpoint,
		func(ctx context.Context, cli api.ManageClient) error {
			if spec.Config == nil {
				common.Warn("addFoundry: spec config is nil")
				return nil
			}

			_, err := cli.Set(ctx, &api.SetRequest{
				Configs: []*api.MachineConfig{
					spec.Config,
				},
			})
			return err

		},
	)

}

func foundryConnect(endpoint string) (*grpc.ClientConn, api.ManageClient, error) {

	endpoint = fmt.Sprintf("%s:%d", endpoint, foundryMgmtPort)

	//------
	creds, err := tlsConfig(endpoint, foundryCert)
	if err != nil {
		return nil, nil, common.ErrorE("foundry tls config failed", err)
	}
	//------

	conn, err := grpc.Dial(endpoint,
		grpc.WithTransportCredentials(creds),
		grpc.WithDefaultCallOptions(
			grpc.MaxCallRecvMsgSize(common.MaxMessageSize),
			grpc.MaxCallSendMsgSize(common.MaxMessageSize),
		),
	)
	if err != nil {
		return nil, nil, common.ErrorE("foundry connection failed", err)
	}

	return conn, api.NewManageClient(conn), nil

}

func withFoundry(
	endpoint string, f func(context.Context, api.ManageClient) error) error {

	conn, cli, err := foundryConnect(endpoint)
	if err != nil {
		return err
	}
	defer conn.Close()

	ctx, cancel := context.WithTimeout(context.TODO(), 3*time.Second)
	defer cancel()

	return f(ctx, cli)

}

func tlsConfig(endpoint, cert string) (credentials.TransportCredentials, error) {

	b, err := ioutil.ReadFile(cert)
	if err != nil {
		return nil, err
	}
	cp := x509.NewCertPool()
	if !cp.AppendCertsFromPEM(b) {
		return nil, fmt.Errorf("credentials: failed to append certificates")
	}
	return credentials.NewTLS(&tls.Config{
		ServerName:         endpoint,
		RootCAs:            cp,
		InsecureSkipVerify: true,
	}), nil

}
