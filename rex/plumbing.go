package main

import (
	"fmt"

	"github.com/mitchellh/mapstructure"
	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/tech/rtnl"

	"gitlab.com/mergetb/tech/cogs/pkg/common"
	"gitlab.com/mergetb/tech/cogs/pkg/fabric"
	"gitlab.com/mergetb/tech/cogs/pkg/runtime"
	"gitlab.com/mergetb/tech/cogs/pkg/task"
)

func doPlumbingAction(action *task.Action) error {

	switch action.Action {

	case task.CreateEnclave:
		return doCreateEnclave(action)

	case task.DestroyEnclave:
		return doDestroyEnclave(action)

	case task.AdvertiseInterfaceMac:
		return doAdvertiseInterfaceMac(action)

	case task.AdvertiseMac:
		return doAdvertiseMac(action)

	case task.WithdrawMac:
		return doWithdrawMac(action)

	case task.PlanInfranet:
		return doPlanInfranet(action)

	case task.DestroyInfranet:
		return doDestroyInfranet(action)

	default:
		return fmt.Errorf("unknown plumbing action '%s'", action.Action)
	}

}

// Enclave creation ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

func doCreateEnclave(action *task.Action) error {

	spec := &task.EnclaveSpec{}
	err := mapstructure.Decode(action.Data, spec)
	if err != nil {
		return fmt.Errorf("invalid enclave spec")
	}

	err = runtime.CreateInfrapodNS(action.Mzid)
	if err != nil {
		return err
	}

	ctx, err := rtnl.OpenDefaultContext()
	if err != nil {
		return err
	}
	defer ctx.Close()

	err = runtime.EnclaveBridge(spec, ctx)
	if err != nil {
		return err
	}

	err = runtime.EnclaveIptables(action.Mzid, spec)
	if err != nil {
		return err
	}

	for _, ifx := range spec.Interfaces {

		ifx.Vindex = spec.Vindex

		err = runtime.CreateIfx(action.Mzid, ifx, true)
		if err != nil {
			return err
		}

	}

	err = runtime.NsDefaultRoute(action.Mzid, spec)
	if err != nil {
		return err
	}

	err = common.Write(spec)
	if err != nil {
		return common.ErrorEF("failed to save enclave spec", err, log.Fields{
			"mzid": action.Mzid,
		})
	}

	return action.SetActionComplete()

}

// Enclave destruction ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

func doDestroyEnclave(action *task.Action) error {

	fields := log.Fields{"mzid": action.Mzid}
	log.WithFields(fields).Info("destroy enclave")

	spec := &task.EnclaveSpec{Mzid: action.Mzid}
	err := common.Read(spec)
	if err != nil {
		return common.ErrorEF("failed to read enclave spec", err, fields)
	}

	err = runtime.DelEnclaveIptables(action.Mzid, spec)
	if err != nil {
		log.Warn(err)
	}

	ctx, err := rtnl.OpenDefaultContext()
	if err != nil {
		return common.ErrorEF("failed to open netlink context", err, fields)
	}
	defer ctx.Close()

	for _, ifx := range spec.Interfaces {

		ifx.Vindex = spec.Vindex

		err = runtime.DeleteIfx(action.Mzid, ifx)
		if err != nil {
			return err
		}

	}

	err = runtime.DelEnclaveBridge(spec, ctx)
	if err != nil {
		return common.ErrorEF("failed to delete enclave rbidge", err, fields)
	}

	err = runtime.DeleteInfrapodNS(action.Mzid)
	if err != nil {
		log.WithError(err).Warn("failed to delete infrapod netns")
	}

	err = delEnclaveVtep(spec, ctx)
	if err != nil {
		log.Warn(err)
	}

	err = common.Delete(spec)
	if err != nil {
		common.WarnF("failed to delete enclave spec", fields)
	}

	return action.SetActionComplete()

}

func doAdvertiseInterfaceMac(action *task.Action) error {

	fields := log.Fields{
		"mzid":     action.Mzid,
		"instance": action.MzInstance,
		"action":   action.Action,
		"op":       action.MzOp,
	}

	spec := &task.InterfaceMacAdvSpec{}
	err := mapstructure.Decode(action.Data, spec)
	if err != nil {
		return common.ErrorEF("invalid InterfaceMacAdvSpec", err, fields)
	}

	var ctx *rtnl.Context
	if spec.Netns == "" {

		ctx, err = rtnl.OpenDefaultContext()
		if err != nil {
			return common.ErrorEF("rtnl default context", err, fields)
		}

	} else {

		ctx, err = rtnl.OpenContext(spec.Netns)
		if err != nil {
			return common.ErrorEF("rtnl context", err, fields)
		}

	}
	defer ctx.Close()

	lnk, err := rtnl.GetLink(ctx, spec.Interface)
	if err != nil {
		return common.ErrorEF("failed to get link", err, fields)
	}

	err = evpnAdvMac(task.MacAdvSpec{
		Vni:    spec.Vni,
		Vindex: spec.Vindex,
		Mac:    lnk.Info.Address.String(),
	})
	if err != nil {
		return common.ErrorEF("macadv failed", err, fields)
	}

	return action.SetActionComplete()

}

func doAdvertiseMac(action *task.Action) error {

	fields := log.Fields{
		"mzid":     action.Mzid,
		"instance": action.MzInstance,
		"action":   action.Action,
		"op":       action.MzOp,
	}

	spec := &task.MacAdvSpec{}
	err := mapstructure.Decode(action.Data, spec)
	if err != nil {
		return common.ErrorEF("invalid MacAdvSpec object", err, fields)
	}

	err = evpnAdvMac(*spec)
	if err != nil {
		return common.ErrorEF("macadv failed", err, fields)
	}

	return action.SetActionComplete()

}

func doWithdrawMac(action *task.Action) error {

	fields := log.Fields{
		"mzid":     action.Mzid,
		"instance": action.MzInstance,
		"action":   action.Action,
		"op":       action.MzOp,
	}

	spec := &task.MacAdvSpec{}
	err := mapstructure.Decode(action.Data, spec)
	if err != nil {
		return common.ErrorEF("invalid MacAdvSpec object", err, fields)
	}

	err = evpnWithdrawMac(*spec)
	if err != nil {
		return common.ErrorEF("macwithdraw failed", err, fields)
	}

	return action.SetActionComplete()

}

func evpnAdvMac(spec task.MacAdvSpec) error {

	err := runtime.EvpnAdvertiseMac(
		spec.Addr, spec.Router, spec.Mac, spec.Asn, spec.Vni, spec.Vindex)
	if err != nil {
		return err
	}

	err = runtime.EvpnAdvertiseMulticast(
		spec.Addr, spec.Router, spec.Asn, spec.Vni, spec.Vindex)
	if err != nil {
		return err
	}

	return nil
}

func evpnWithdrawMac(spec task.MacAdvSpec) error {

	err := runtime.EvpnWithdrawMac(
		spec.Addr, spec.Router, spec.Mac, spec.Asn, spec.Vni, spec.Vindex)
	if err != nil {
		return err
	}

	/*
		Unclear if this needs doing
		err = task.EvpnAdvertiseMulticast(spec.Router, spec.Vni, spec.Vindex)
		if err != nil {
			return err
		}
	*/

	return nil
}

func delEnclaveVtep(spec *task.EnclaveSpec, ctx *rtnl.Context) error {

	vt := rtnl.NewLink()
	vt.Info.Name = fmt.Sprintf("vtep%d", spec.Vindex)

	err := vt.Absent(ctx)
	if err != nil {
		return common.ErrorEF("failed to remove enclave vtep", err, log.Fields{
			"name": vt.Info.Name,
		})
	}

	return nil

}

// Infranet planning ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

func doPlanInfranet(action *task.Action) error {

	var linkinfos []*task.LinkInfo
	err := mapstructure.Decode(action.Data, &linkinfos)
	if err != nil {
		return fmt.Errorf("invalid infranet: %v", err)
	}

	lp, err := newLinkPlan(
		common.TbModel(), action.Mzid, fabric.InfraLP, linkinfos...)
	if err != nil {
		return fmt.Errorf("failed to create linkplan: %v", err)
	}

	err = lp.Save()
	if err != nil {
		return fmt.Errorf("failed to save linkplan: %v", err)
	}

	return action.SetActionComplete()

}

func doDestroyInfranet(action *task.Action) error {

	err := delInfralink(common.TbModel(), action.Mzid)
	if err != nil {
		return err
	}

	return action.SetActionComplete()

}
