package main

import (
	"fmt"

	"github.com/mitchellh/mapstructure"
	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/tech/canopy/lib"
	"google.golang.org/grpc/status"

	"gitlab.com/mergetb/tech/cogs/pkg/common"
	"gitlab.com/mergetb/tech/cogs/pkg/fabric"
	"gitlab.com/mergetb/tech/cogs/pkg/runtime"
	"gitlab.com/mergetb/tech/cogs/pkg/task"
	"gitlab.com/mergetb/xir/lang/go/v0.2"
)

func doCanopyAction(topo *xir.Net, action *task.Action) error {

	fields := log.Fields{"action": action.Action}

	log.WithFields(fields).Info("begin canopy action")

	var err error

	switch action.Action {

	case task.SetCanopyLinks:
		var info []*task.LinkInfo
		info, err = decodeLinkInfo(action)
		if err == nil {
			err = doSetLinks(topo, action.Mzid, fabric.ExperimentLP, info, true)
		}

	case task.RemoveCanopyLinks:
		err = doRemoveLinks(topo, action.Mzid, fabric.ExperimentLP, true)

	case task.SetCanopyServiceVtep:
		var spec *task.VtepSpec
		spec, err = decodeVtepSpec(action)
		if err == nil {
			err = doSetServiceVtep(action.Mzid, *spec)
		}

	case task.RemoveCanopyServiceVtep:
		var spec *task.VtepSpec
		spec, err = decodeVtepSpec(action)
		if err == nil {
			err = doRemoveServiceVtep(action.Mzid, *spec)
		}

	default:
		err = fmt.Errorf("unknown canopy task '%s'", action.Action)
	}

	if err == nil {
		return action.SetActionComplete()
	}

	s, ok := status.FromError(err)
	if ok {
		err = fmt.Errorf(s.Message())
	}
	log.WithFields(fields).WithError(err).Error("canopy task failed")
	return err

}

func doSetLinks(
	topo *xir.Net,
	mzid string,
	role fabric.LinkPlanRole,
	links []*task.LinkInfo,
	save bool) error {

	//TODO this clobbers the existing infranet linkplan ....
	lp, err := newLinkPlan(topo, mzid, role)
	if err != nil {
		return common.ErrorE("new linkplan failed", err)
	}

	for _, link := range links {
		err := lp.AddLink(link)
		if err != nil {
			log.Warn(err)
		}
	}

	err = lp.Create().Exec()
	if err != nil {
		return common.ErrorE("linkplan exec failed", err)
	}

	err = lp.Advertise()
	if err != nil {
		return err
	}

	if save {
		return lp.Save()
	}

	return nil
}

func doRemoveLinks(
	topo *xir.Net,
	mzid string,
	role fabric.LinkPlanRole,
	save bool) error {

	lp, err := newLinkPlan(topo, mzid, role)
	if err != nil {
		return err
	}

	err = common.Read(lp)
	if err != nil {
		return common.ErrorE("failed to read linkplan", err)
	}

	err = lp.Destroy().Exec()
	if err != nil {
		return err
	}

	err = lp.Withdraw()
	if err != nil {
		return err
	}

	if save {
		return lp.Delete()
	}
	return nil

}

func newLinkPlan(
	topo *xir.Net, mzid string, role fabric.LinkPlanRole, info ...*task.LinkInfo,
) (*fabric.LinkPlan, error) {

	mtu := runtime.GetConfig(false).Net.VtepMtu
	return fabric.NewLinkPlan(mzid, topo, role, mtu, info...)

}

func doSetServiceVtep(mzid string, spec task.VtepSpec) error {

	fields := log.Fields{
		"mzid": mzid,
		"vni":  spec.Vni,
	}

	log.WithFields(fields).Info("setting up vteps")

	cc := canopy.NewClient()
	cfg := runtime.GetConfig(false)

	for _, v := range spec.Vteps {

		//always go through the local host to create the node as we are running
		//(rex) on the node we want to create the vtep on
		vNode := "localhost"
		fields["node"] = vNode
		fields["dev"] = cfg.Net.VtepIfx //v.Dev
		fields["bridge"] = v.Bridge
		fields["tunnelip"] = cfg.Net.ServiceTunnelIP //v.TunnelIP

		log.WithFields(fields).Info("vtep")

		vtepName := fmt.Sprintf("vtep%d", spec.Vni)

		cc.SetVtep(
			&canopy.VtepPresence{
				Presence: canopy.Presence_Present,
				Vni:      int32(spec.Vni),
				Parent:   cfg.Net.VtepIfx,         //v.Dev,
				TunnelIp: cfg.Net.ServiceTunnelIP, //v.TunnelIP,
			},
			vtepName, vNode,
		)
		cc.SetMaster(
			&canopy.Master{
				Presence: canopy.Presence_Present,
				Value:    v.Bridge,
			},
			vtepName, vNode,
		)
		cc.SetMtu(int32(cfg.Net.VtepMtu), vtepName, vNode)
		cc.SetLink(
			canopy.UpDown_Up,
			vtepName, vNode,
		)

	}

	err := cc.Commit()
	if err != nil {
		log.WithError(err).Error("service vtep commit failed")
		return err
	}

	return nil

}

func doRemoveServiceVtep(mzid string, spec task.VtepSpec) error {

	fields := log.Fields{
		"mzid":  mzid,
		"vni":   spec.Vni,
		"count": len(spec.Vteps),
	}

	log.WithFields(fields).Info("removing up vteps")

	cc := canopy.NewClient()
	cfg := runtime.GetConfig(false)

	for _, v := range spec.Vteps {

		//always go through the local host to create the node as we are running
		//(rex) on the node we want to create the vtep on
		vNode := "localhost"

		fields["node"] = vNode
		fields["dev"] = cfg.Net.VtepIfx //v.Dev
		fields["bridge"] = v.Bridge

		log.WithFields(fields).Info("removing vtep")

		vtepName := fmt.Sprintf("vtep%d", spec.Vni)

		cc.SetVtep(
			&canopy.VtepPresence{
				Presence: canopy.Presence_Absent,
				Vni:      int32(spec.Vni),
				Parent:   cfg.Net.VtepIfx, //v.Dev,
			},
			vtepName, vNode,
		)

	}

	err := cc.Commit()
	if err != nil {
		log.WithError(err).Error("service vtep commit failed")
		return err
	}

	return nil

}

func decodeLinkInfo(action *task.Action) ([]*task.LinkInfo, error) {

	var info []*task.LinkInfo
	err := mapstructure.Decode(action.Data, &info)
	if err != nil {
		log.
			WithError(err).
			WithFields(log.Fields{"kind": "canopy"}).
			Error("failed to parse LinkInfo")
		return nil, err
	}
	return info, nil

}

func decodeVtepSpec(action *task.Action) (*task.VtepSpec, error) {

	spec := &task.VtepSpec{}
	err := mapstructure.Decode(action.Data, &spec)
	if err != nil {
		log.
			WithError(err).
			WithFields(log.Fields{"kind": "canopy"}).
			Error("failed to parse VtepSpec")
		return nil, err
	}
	return spec, nil

}

func setHarborInfralink(topo *xir.Net, spec *task.NodeSpec) error {

	return doSetLinks(topo, task.HarborMzid, fabric.InfraLP, []*task.LinkInfo{{
		Mzid: task.HarborMzid,
		Kind: task.InfraLink,
		XpID: fmt.Sprintf("infranet/%s", spec.Xname),
		TBVLinks: []*task.TBVLink{{
			Vni: task.HarborVni,
			Vid: task.HarborVid,
			Edges: []*task.EdgeInfo{{
				NodeName:      spec.Resource,
				NodeInterface: spec.Interface,
			}},
		}},
	}}, false)

}

func setInfralink(topo *xir.Net, mzid string, spec *task.NodeSpec) error {

	return doSetLinks(topo, mzid, fabric.InfraLP, []*task.LinkInfo{{
		Mzid: mzid,
		Kind: task.InfraLink,
		XpID: fmt.Sprintf("infranet/%s", spec.Xname),
		TBVLinks: []*task.TBVLink{{
			Vni: spec.Infranet.Vni,
			Vid: spec.Infranet.Vid,
			Edges: []*task.EdgeInfo{{
				NodeName:      spec.Resource,
				NodeInterface: spec.Interface,
			}},
		}},
	}}, false)

}

func delInfralink(topo *xir.Net, mzid string) error {

	return doRemoveLinks(topo, mzid, fabric.InfraLP, true)

}
