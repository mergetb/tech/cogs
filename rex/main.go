package main

/* Rex
 * ---
 *
 *    This is the entry point for the Coggosaurus Rex. Rex is responsible for
 *    observing the Cogs etcd database, formulating task dependency graphs for
 *    pending tasks and optimally executing tasks across concurrent threads
 *    according to dependency chains.
 *
 *    This code is structured as follows
 *
 *    - run.go:	Contains the watch loop, dependency graph and execution entry
 *              code.
 *
 *	  - dispatch.go: Contains the central dispatch point where the task kind is
 *	                 read and we decide on the subsystem-specific handler
 *	                 function that will carry out the task.
 *
 *    - everything else: implementation details for specific tasks such as
 *                       beluga, canopy nex ... are grouped into source files
 *                       according to the subsystems they are interacting with.
 *
 */

import (
	"context"
	"flag"

	_ "net/http/pprof"

	"github.com/coreos/etcd/clientv3"
	log "github.com/sirupsen/logrus"

	"gitlab.com/mergetb/tech/cogs/pkg/common"
	"gitlab.com/mergetb/tech/cogs/pkg/runtime"
)

var (
	// Persistent connection to the Cogs etcd data store
	etcd        *clientv3.Client
	cfg         *runtime.Config
	debug       = flag.Bool("debug", false, "enable debugging")
	trace       = flag.Bool("trace", false, "enable tracing")
	runtimePath = flag.String("runtime", "/etc/cogs/runtime.yml", "runtime configuration file")
)

func main() {
	flag.Parse()

	log.WithFields(log.Fields{
		"version": common.Version,
		"id":      clid[:8],
		"mocked":  areWeMocked(),
	}).Info("rex starting")

	if *debug {
		log.SetLevel(log.DebugLevel)
	}

	if *trace {
		log.SetLevel(log.TraceLevel)
	}

	runtime.ConfigPath = *runtimePath

	cfg = runtime.GetConfig(true)

	SetSledConfig(cfg.Sled)
	common.SetEtcdConfig(cfg.Etcd)

	err := common.EnsureEtcd(&etcd)
	if err != nil {
		log.Fatal(err)
	}
	log.Trace("connected to etcd")

	PrintAlerts()

	runtime.Init()

	lease, err := registerCoglet(etcd, "rex")
	if err != nil {
		log.Fatal(err)
	}
	log.Trace("registered tasklet")

	for {
		_, err := etcd.KeepAliveOnce(context.TODO(), lease)
		if err != nil {
			log.Warn(err)
		}
		err = exec(&etcd, "node", doTask)
		if err != nil {
			log.Warn(err)
		}
	}
}
