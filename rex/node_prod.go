// +build !mock

package main

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/tech/cogs/pkg/common"
	"gitlab.com/mergetb/tech/cogs/pkg/task"
)

func areWeMocked() bool {
	return false
}

func nodeSetupImage(spec *task.NodeSpec, fields log.Fields) error {
	var err error

	/*
		sled daemon log:
		* There are three states the node could be in at this point:
			1. Node is off
			2. Node is on, but not in daemon mode
			3. Node is on, and is in daemon mode
		(3) First we will check by sending "pings" (healthchecks) to the daemon.
		If we get a response, we know the node is in daemon mode, and already
		has the default image on it.  We then need to check if the default image
		is correct, and if not, we need to send a write update along with the
		kexec update.
		(1,2) If the node does not respond with a health check, we need to treat
		the node as being off, and start by making sure the node has power, update
		sledapi with the full commandset, and then cycle the node for sledc to
		pick up the new command set.
	*/

	// 1. check if the node is healthy, if not, we need to restart the node
	healthy := checkNodeHealth(spec.Mac, true)
	if healthy {
		log.Infof("%s is in daemon mode", spec.Mac)

		// 2a. if it is, then we just need to send a sled daemon update telling
		// sled to do a kexec (function will check and add write if needed)
		err = sledUpdateDB(spec, kexecReady)
		if err != nil {
			return common.ErrorEF("failed to update sled db", err, fields)
		}
	} else {
		log.Infof("%s is not in daemon mode, restarting", spec.Mac)

		// 2b. if it is not, we need to power it on, and send normal sled cs
		// ensure node has juice
		err = belugaOn(spec.Resource, false)
		if err != nil {
			return common.ErrorEF("failed to ensure node power", err, fields)
		}

		// update sled with the full command set (wipe, write, kexec)
		err = sledUpdateDB(spec, fullCommand)
		if err != nil {
			return common.ErrorEF("failed to update sled db", err, fields)
		}

		// soft cycle the node
		err = belugaCycle(spec.Resource, true)
		if err != nil {
			return common.ErrorEF("failed to soft cycle node", err, fields)
		}
	}

	// image the node
	err = sledImage(spec, false)
	if err != nil {
		return common.ErrorEF("failed to image node", err, fields)
	}

	return nil
}

func nodeRecycleImage(spec *task.NodeSpec, fields log.Fields) error {
	// on dematerialization, now we tell sled that we should put sledc in daemon
	// mode, this enables very quick sledding on materialization as the node,
	// even when not using the correct image, is already pxe'd and in daemon mode.
	err := sledUpdateDB(spec, daemonConfigure)
	if err != nil {
		return common.ErrorEF("failed to update sled db", err, fields)
	}

	// soft cycle the node
	err = belugaCycle(spec.Resource, true)
	if err != nil {
		return common.ErrorEF("failed to recycle node power", err, fields)
	}

	// now we wait for the sledc daemon to come back online from the power
	// cycle and confirm it is waiting for commands.
	err = sledDaemon(spec)
	if err != nil {
		return common.ErrorEF("failed to daemon node", err, fields)
	}

	return err
}
