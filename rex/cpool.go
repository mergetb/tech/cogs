package main

/* rex connection pool
 * -------------------
 *
 * This code implements the rex connection pool.
 *
 */

import (
	"sync"

	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
)

var _cmapMtx sync.Mutex
var _cmap = make(map[string]*grpc.ClientConn)

// connection makes a connection to the specified gRPC endpoint. If the
// connection already exists, a cached connection is uesd. Otherwise a new
// connection is created.
func connection(endpoint string, opts ...grpc.DialOption) (*grpc.ClientConn, error) {

	_cmapMtx.Lock()
	defer _cmapMtx.Unlock()

	var err error
	cnx, ok := _cmap[endpoint]
	if !ok {
		log.Warnf("creating new grpc connection %s", endpoint)
		cnx, err = grpc.Dial(endpoint, opts...)
		if err != nil {
			return nil, err
		}
		_cmap[endpoint] = cnx
	}

	return cnx, nil

}
