package main

import (
	"context"
	"fmt"
	"google.golang.org/grpc"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/tech/cogs/pkg/common"
	"gitlab.com/mergetb/tech/cogs/pkg/runtime"
	"gitlab.com/mergetb/tech/cogs/pkg/task"
	sled "gitlab.com/mergetb/tech/sled/pkg"
)

var (
	// ConnTimeout is the ammount of time sled will wait on sledapi
	ConnTimeout = 10 * time.Second

	// DematTimeout is the amount of time we wait until the daemon is up
	// (dematerialization)
	DematTimeout = 100

	// MatTimeout is the amount of time we wait for the commandset to work
	// (materialization)
	MatTimeout = 100
)

// checkNodeHealth is the health check by rex/node.go. We return true
// if the node is in daemon mode, and false otherwise. The healthcheck
// has a timeout set to ConnTimeout (10 seconds).
func checkNodeHealth(mac string, ignoreError bool) bool {
	resp, err := healthcheck(task.HarborSvcAddr, mac, ignoreError)
	if err != nil {
		return false
	}
	if resp != nil {
		return true
	}
	return false
}

// if the node is in daemon mode with the default image already written
// check first to see if the spec indicates a non-default image, if so
// set that to the write command.
func checkDefaultImage(spec *task.NodeSpec, cs *sled.CommandSet, wr *sled.WriteResponse) {
	selectedImage := spec.Binding.Image.Name
	defaultImage := task.DefaultImages.Default
	fields := log.Fields{
		"spec":    selectedImage,
		"default": defaultImage,
	}

	if selectedImage != defaultImage {
		log.WithFields(fields).Debugf("non-default image")
		cs.Write = wr
	}
}

// sledUpdateDB is responsible for putting in the correct commandset
// to sledapi (database) based on what rex wants the node state to be
// either daemon (demat), kexec (mat when in daemon), or full
// commandset (mat when not in daemon).
func sledUpdateDB(spec *task.NodeSpec, updateType string) error {
	fields := log.Fields{
		"node":    spec.Resource,
		"binding": spec.Binding,
	}

	if spec.Binding == nil {
		return common.ErrorF("spec binding is nil", fields)
	}
	if spec.Binding.Image == nil {
		return common.ErrorF("spec image is nil", fields)
	}

	cs := &sled.CommandSet{}
	wr := &sled.WriteResponse{
		Device:    spec.Binding.Image.Blockdevice,
		Image:     spec.Binding.Image.Disk,
		Kernel:    spec.Binding.Image.Kernel,
		Initramfs: spec.Binding.Image.Initramfs,
	}
	kr := &sled.KexecResponse{
		Append:    spec.Binding.Image.Append,
		Kernel:    "/tmp/kernel",
		Initramfs: "/tmp/initrd",
	}
	switch updateType {
	case daemonConfigure:
		cs.Write = wr
		cs.Daemon = sled.EnumStatus_CONFIGURED
	case kexecReady:
		// checkDefaultImage will modify the commandset with the
		// write response above if the default image is different.
		// from sledc order of operations, write will take precendence
		// followed by kexec
		cs.Write = nil
		checkDefaultImage(spec, cs, wr)
		cs.Kexec = kr
		cs.Daemon = sled.EnumStatus_READY
	case fullCommand:
		cs.Write = wr
		cs.Kexec = kr
	}
	log.Tracef("update: %s %v", spec.Mac, cs)

	err := addCommandSet(task.HarborSvcAddr, spec.Mac, cs)
	if err != nil {
		return common.ErrorEF("add command set failed", err, fields)
	}
	return nil
}

// sledImage is response for waiting for sled to confirm that the node
// has kexec'd into the correct image.
func sledImage(spec *task.NodeSpec, noverify bool) error {
	fields := log.Fields{"node": spec.Resource}
	if !noverify {
		err := waitForMac(task.HarborSvcAddr, spec.Mac)
		if err != nil {
			return common.ErrorEF("wait for completion failed", err, fields)
		}
	}

	return nil
}

// sledDamon is the wait function responsible for verifying the daemon is up
// and running after a dematerialization
func sledDaemon(spec *task.NodeSpec) error {
	fields := log.Fields{"node": spec.Resource, "mac": spec.Mac}

	success := false
	for i := 0; i < DematTimeout; i++ {
		healthy := checkNodeHealth(spec.Mac, true)
		if healthy {
			success = true
			break
		}
		time.Sleep(1 * time.Second)
	}
	if !success {
		return common.ErrorF("timed out waiting for daemon", fields)
	}

	return nil
}

// addCommandSet sends the command set to sledapi.  The commandset must be
// the absolute commandset, sled does not support additive operations.
func addCommandSet(endpoint, mac string, cmd *sled.CommandSet) error {
	err := WithSled(endpoint, func(c sled.SledapiClient) error {
		ctx, cancel := context.WithTimeout(context.TODO(), ConnTimeout)
		defer cancel()
		_, err := c.PutCommand(ctx, &sled.PutCommandRequest{Mac: mac, Command: cmd})
		return err
	})
	if err != nil {
		return common.ErrorE("failed to add command set", err)
	}
	return nil
}

// healthcheck is responsible for asking sledapi through sledd about the sled
// daemon client.
func healthcheck(endpoint, mac string, ignoreError bool) (*sled.Empty, error) {
	var err error
	err = WithSled(endpoint, func(c sled.SledapiClient) error {
		ctx, cancel := context.WithTimeout(context.TODO(), ConnTimeout)
		defer cancel()
		_, err = c.HealthCheck(ctx,
			&sled.GetDaemonRequest{Mac: mac})
		return err
	})
	if err != nil {
		return nil, err
	}
	return &sled.Empty{}, nil
}

// waitForMac is responsible for on materializations, waiting for the sledapi
// showing that the related mac address's commandset for all the commands
// have finished.
func waitForMac(endpoint, mac string) error {

	fields := log.Fields{"mac": mac}
	log.WithFields(fields).Infof("sled: waiting on mac")

	cs, err := getMacStatus(endpoint, mac)
	if err != nil {
		return err
	}
	if cs.Command == nil {
		return common.ErrorEF("bad command", err, fields)
	}
	if cs.Command.Statusid == "" {
		return common.ErrorEF("bad status id", err, fields)
	}
	if err != nil {
		return err
	}

	done, err := csComplete(endpoint, cs.Command)
	if err != nil {
		return common.ErrorEF("sled failed", err, fields)
	}
	if done {
		log.WithFields(fields).Infof("sled: image run success")
		return nil
	}

	return common.ErrorF("sled timeout", fields)
}

// getMacStatus is responsible for getting the commandset for the mac
func getMacStatus(endpoint, mac string) (*sled.GetCommandResponse, error) {
	fields := log.Fields{"mac": mac, "endpoint": endpoint}

	var cs *sled.GetCommandResponse
	var err error
	err = WithSled(endpoint, func(c sled.SledapiClient) error {
		ctx, cancel := context.WithTimeout(context.TODO(), ConnTimeout)
		defer cancel()
		cs, err = c.GetCommand(ctx, &sled.GetCommandRequest{Mac: mac})
		if err != nil {
			return common.ErrorEF("sled status check failed", err, fields)
		}
		return nil
	})
	if err != nil {
		return nil, err
	}
	return cs, nil
}

// csComplete checks if all the commands are complete -> true, else false.
func csComplete(endpoint string, cs *sled.CommandSet) (bool, error) {
	fields := log.Fields{
		"statusid": cs.Statusid,
	}
	statusid := cs.Statusid

	// timeout timer if we fail to wipe, write, and kexec.
	// note if wipes are used, this approach should be re-written
	// such that wipes are sent on daemon mode requests, and the timeout
	// there extended, rather than using the materialization's timeout.
	timeout := MatTimeout

	// all the commands are set default to true.  commands are set to false
	// if the command is not nil (meaning there is something to do)
	wipe := true
	write := true
	kexec := true
	// Device, Device, and Kernel and basically required fields
	if cs.Wipe != nil && cs.Wipe.Devices != nil {
		wipe = false
	}
	if cs.Write != nil && cs.Write.Device != "" {
		write = false
	}
	if cs.Kexec != nil && cs.Kexec.Kernel != "" {
		kexec = false
	}

	// now we wait for all the commands not finished
	for (!wipe || !write || !kexec) && timeout > 0 {
		// this will ensure order of operations, wipe -> write -> kexec
		// we currently implement this approach rather than querying the
		// status of each task all at once because we know that there is
		// significant delay between each task.  A wipe will be minutes
		// to hours.  A write will be 3-30 seconds. A kexec is around 1
		// second.  So iteratively querying in order of wipe, write, kexec
		// we send less messages to sledapi while not losing anything.
		taskCmd := ""
		if !kexec {
			taskCmd = sled.CmdKexec
		}
		if !write {
			taskCmd = sled.CmdWrite
		}
		if !wipe {
			taskCmd = sled.CmdWipe
		}
		log.Tracef("Checking %s at t-%d", taskCmd, timeout)

		// get the status of the current command we want to know about
		taskStatus, err := getTaskStatus(endpoint, taskCmd, statusid)
		if err != nil {
			fields["task"] = taskCmd
			return false, common.ErrorEF("failed to get task status", err, fields)
		}
		log.Tracef("status: %#v", taskStatus)

		// ask wether that command is complete according to that command
		done, err := taskComplete(taskCmd, taskStatus.Status)
		if err != nil {
			fields["task"] = taskCmd
			return false, common.ErrorEF("task failed", err, fields)
		}
		log.Tracef("id: %s, cmd: %s, status: %#v", statusid, taskCmd, taskStatus)
		if done {
			switch taskCmd {
			case sled.CmdKexec:
				kexec = true
			case sled.CmdWrite:
				write = true
			case sled.CmdWipe:
				wipe = true
			}
		}
		time.Sleep(1 * time.Second)
		timeout--
	}
	return (wipe && write && kexec), nil
}

// taskComplete parses sled specific responses with rex goal oriented boolean
func taskComplete(name string, rq sled.Response_Status) (bool, error) {
	if rq == sled.Response_error {
		return false, fmt.Errorf(name)
	}
	if rq == sled.Response_ok {
		return true, nil
	}
	// for kexec, we will never get an okay, as okay would happen
	// on confirmation of success which is impossible for kexec.
	if rq == sled.Response_ack && name == "kexec" {
		return true, nil
	}
	return false, nil
}

// getTaskStatus asks the sledapi about the status for a specific command
func getTaskStatus(endpoint, task, id string) (*sled.Response, error) {
	fields := log.Fields{
		"task": task,
		"id":   id,
	}

	var resp *sled.Response
	var err error
	err = WithSled(endpoint, func(c sled.SledapiClient) error {
		ctx, cancel := context.WithTimeout(context.TODO(), ConnTimeout)
		defer cancel()
		switch task {
		case sled.CmdWipe:
			resp, err = c.GetWipeStatus(ctx, &sled.GetStatusRequest{Id: id})
		case sled.CmdWrite:
			resp, err = c.GetWriteStatus(ctx, &sled.GetStatusRequest{Id: id})
		case sled.CmdKexec:
			resp, err = c.GetKexecStatus(ctx, &sled.GetStatusRequest{Id: id})
		}
		return err
	})
	if err != nil {
		return nil, sled.ErrorEF("failed to get sled status", err, fields)
	}
	return resp, nil
}

// WithSled handles sled connections
func WithSled(endpoint string, f func(c sled.SledapiClient) error) error {
	cli, err := sledClient(endpoint)
	if err != nil {
		return err
	}
	return f(cli)
}

// FIXME: Add certificates for authenticated communitcation client <---> server
func sledClient(addr string) (sled.SledapiClient, error) {
	conn, err := connection(fmt.Sprintf("%s:9913", addr), grpc.WithInsecure())
	if err != nil {
		log.Errorf("could not connect to sled server - %v", err)
		return nil, err
	}

	return sled.NewSledapiClient(conn), nil
}

// SetSledConfig configures the sled timeout values
func SetSledConfig(cfg *runtime.SledConfig) {
	if cfg != nil {
		if cfg.MatTimeout != 0 {
			MatTimeout = cfg.MatTimeout
			log.Infof("Sled Materialization timeout set to: %d", MatTimeout)
		}
		if cfg.DematTimeout != 0 {
			DematTimeout = cfg.DematTimeout
			log.Infof("Sled Dematerialization timeout set to: %d", DematTimeout)
		}
		if cfg.ConnTimeout != 0 {
			ConnTimeout = time.Duration(cfg.ConnTimeout) * time.Second
			log.Infof("Sled Connection timeout set to: %fs", ConnTimeout.Seconds())
		}
	}
}
