package main

import (
	"github.com/mitchellh/mapstructure"
	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/tech/cogs/pkg/common"
	"gitlab.com/mergetb/tech/cogs/pkg/task"
)

const (
	daemonConfigure = "configure"
	kexecReady      = "ready"
	fullCommand     = "commandset"
)

func nodeSetup(action *task.Action) error {

	fields := log.Fields{
		"mzid":   action.Mzid,
		"action": action.Action,
	}

	log.WithFields(fields).Info("begin node setup")

	spec := &task.NodeSpec{}
	err := mapstructure.Decode(action.Data, &spec)
	if err != nil {
		return common.ErrorEF("failed to decode node setup spec", err, fields)
	}

	s := &task.NodeState{
		Name: spec.Resource,
	}
	// Read before we update node status
	err = common.Read(s)
	if err != nil {
		return common.ErrorEF("failed to read node state", err, fields)
	}

	// set the status to setup
	err = common.RUC(s, func(o common.Object) {
		o.(*task.NodeState).Mzid = action.Mzid
		o.(*task.NodeState).Status.Current = task.Setup
	})
	if err != nil {
		return common.ErrorEF("failed to set the node state to mat", err, fields)
	}

	// ensure all infralinks on harbor
	err = setHarborInfralink(common.TbModel(), spec)
	if err != nil {
		return common.ErrorEF("failed to set infralink", err, fields)
	}

	// set nex member name
	err = addNexMember(action.Mzid, spec)
	if err != nil {
		return common.ErrorEF("failed to add nex member", err, fields)
	}

	// setup foundry
	err = addFoundryConfig(spec)
	if err != nil {
		return common.ErrorEF("failed to add foundry config", err, fields)
	}

	// setup belugactld info
	err = addBelugactlConfig(spec)
	if err != nil {
		return common.ErrorEF("failed to add belugactl config", err, fields)
	}

	err = nodeSetupImage(spec, fields)
	if err != nil {
		return err
	}

	// put the node on it's materialization network
	err = setInfralink(common.TbModel(), action.Mzid, spec)
	if err != nil {
		return common.ErrorEF("failed to place node in mzn network", err, fields)
	}

	// set the status to ready/done
	err = common.RUC(s, func(o common.Object) {
		o.(*task.NodeState).Status.Current = task.Ready
	})
	if err != nil {
		return common.ErrorEF("failed to update node state", err, fields)
	}

	log.WithFields(fields).Info("node setup complete")

	return action.SetActionComplete()

}

func nodeRecycle(action *task.Action) error {
	fields := log.Fields{
		"mzid":   action.Mzid,
		"action": action.Action,
	}

	log.WithFields(fields).Info("begin node recycle")

	spec := &task.NodeSpec{}
	err := mapstructure.Decode(action.Data, &spec)
	if err != nil {
		return common.ErrorEF("failed to decode node recycle spec", err, fields)
	}

	s := &task.NodeState{Name: spec.Resource}
	// Read before we update node status
	err = common.Read(s)
	if err != nil {
		return common.ErrorEF("failed to read node state", err, fields)
	}

	// set node state to recycle
	err = common.RUC(s, func(o common.Object) {
		o.(*task.NodeState).Mzid = action.Mzid
		o.(*task.NodeState).Status.Current = task.Recycling
	})
	if err != nil {
		return common.ErrorEF("failed to update node state to recycle", err, fields)
	}

	// put the node on the harbor network
	err = setHarborInfralink(common.TbModel(), spec)
	if err != nil {
		return common.ErrorEF("failed to set infralink", err, fields)
	}

	err = nodeRecycleImage(spec, fields)
	if err != nil {
		return err
	}

	// set node state to recycle
	err = common.RUC(s, func(o common.Object) {
		o.(*task.NodeState).Mzid = "harbor.dcomptb.io"
		o.(*task.NodeState).Status.Current = task.Clean
	})
	if err != nil {
		return common.ErrorEF("failed to update node state", err, fields)
	}

	log.WithFields(fields).Info("node recycle complete")

	return action.SetActionComplete()
}
