package main

import (
	"fmt"

	"github.com/mitchellh/mapstructure"
	"gitlab.com/mergetb/tech/cogs/pkg/runtime"
	"gitlab.com/mergetb/tech/cogs/pkg/task"
)

func doContainerAction(action *task.Action) error {

	switch action.Action {

	case task.LaunchCtr:
		return doLaunchContainer(action)

	case task.CreateCtr:
		return doCreateContainer(action)

	case task.CreateCtrRecord:
		return doCreateContainerRecord(action)

	case task.PullCtrImage:
		return doPullContainerImage(action)

	case task.CreateCtrTask:
		return doCreateContainerTask(action)

	case task.ConfigCtrLo:
		return doConfigLo(action)

	case task.DeleteCtr:
		return doDeleteContainer(action)

	case task.DeleteCtrImg:
		return doDeleteContainerImg(action)

	case task.DeleteCtrTask:
		return doDeleteContainerTask(action)

	case task.DeleteCtrRecord:
		return doDeleteContainerRecord(action)

	case task.DeleteCtrNS:
		return doDeleteCtrNS(action)

	default:
		return fmt.Errorf("unknown container action '%s'", action.Action)

	}

}

func doLaunchContainer(a *task.Action) error {

	spec := &task.ContainerSpec{}
	err := mapstructure.Decode(a.Data, spec)
	if err != nil {
		return fmt.Errorf("invalid launch container data")
	}

	err = runtime.CreateContainerRecord(spec)
	if err != nil {
		return err
	}

	err = runtime.PullContainerImage(spec.Namespace, spec.Image)
	if err != nil {
		return err
	}

	err = runtime.SetupContainerLo(a.Mzid)
	if err != nil {
		return err
	}

	err = runtime.CreateContainer(spec)
	if err != nil {
		return err
	}

	err = runtime.CreateContainerTask(spec)
	if err != nil {
		return err
	}

	return a.SetActionComplete()

}

func doCreateContainer(action *task.Action) error {

	spec := &task.ContainerSpec{}
	err := mapstructure.Decode(action.Data, spec)
	if err != nil {
		return fmt.Errorf("invalid create container data")
	}

	err = runtime.CreateContainer(spec)
	if err != nil {
		return err
	}

	return action.SetActionComplete()

}

func doCreateContainerRecord(action *task.Action) error {

	spec := &task.ContainerSpec{}
	err := mapstructure.Decode(action.Data, spec)
	if err != nil {
		return fmt.Errorf("invalid create container data")
	}

	err = runtime.CreateContainerRecord(spec)
	if err != nil {
		return err
	}

	return action.SetActionComplete()
}

func doCreateContainerTask(action *task.Action) error {

	spec := &task.ContainerSpec{}
	err := mapstructure.Decode(action.Data, spec)
	if err != nil {
		return fmt.Errorf("invalid create container data")
	}

	err = runtime.CreateContainerTask(spec)
	if err != nil {
		return err
	}

	return action.SetActionComplete()
}

func doPullContainerImage(action *task.Action) error {

	spec := &task.ContainerImageData{}
	err := mapstructure.Decode(action.Data, spec)
	if err != nil {
		return fmt.Errorf("invalid image name")
	}

	err = runtime.PullContainerImage(spec.Namespace, spec.Image)
	if err != nil {
		return err
	}

	return action.SetActionComplete()
}

func doConfigLo(action *task.Action) error {

	err := runtime.SetupContainerLo(action.Mzid)
	if err != nil {
		return err
	}

	return action.SetActionComplete()
}

func doDeleteContainer(action *task.Action) error {

	spec := &task.DeleteContainerData{}
	err := mapstructure.Decode(action.Data, spec)
	if err != nil {
		return fmt.Errorf("invalid contianer data")
	}

	err = runtime.DeleteContainer(spec.Name, spec.Namespace)
	if err != nil {
		return err
	}

	return action.SetActionComplete()
}

func doDeleteContainerImg(action *task.Action) error {

	spec := &task.DeleteContainerData{}
	err := mapstructure.Decode(action.Data, spec)
	if err != nil {
		return fmt.Errorf("invalid contianer data")
	}

	err = runtime.DeleteContainerImg(spec.Name, spec.Namespace)
	if err != nil {
		return err
	}

	return action.SetActionComplete()
}

func doDeleteContainerTask(action *task.Action) error {

	spec := &task.DeleteContainerData{}
	err := mapstructure.Decode(action.Data, spec)
	if err != nil {
		return fmt.Errorf("invalid container data")
	}

	err = runtime.DeleteContainerTask(spec.Name, spec.Namespace)
	if err != nil {
		return err
	}

	return action.SetActionComplete()
}

func doDeleteContainerRecord(action *task.Action) error {

	spec := &task.DeleteContainerData{}
	err := mapstructure.Decode(action.Data, spec)
	if err != nil {
		return fmt.Errorf("invalid container data")
	}

	err = runtime.DeleteContainerRecord(spec.Name, action.Mzid)
	if err != nil {
		return err
	}

	return action.SetActionComplete()
}

func doDeleteCtrNS(action *task.Action) error {

	spec := &task.DeleteContainerNsData{}
	err := mapstructure.Decode(action.Data, spec)
	if err != nil {
		return fmt.Errorf(
			"invalid DeleteContainerNSData data '%#v %T'", action.Data, action.Data)
	}

	err = runtime.DeleteContainerNs(spec.Name)
	if err != nil {
		return fmt.Errorf("failed to delete container namespace")
	}

	return action.SetActionComplete()
}
