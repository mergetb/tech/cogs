package main

/* The Cogs Merge Driver
 * ---------------------
 *
 *  This is the entry point for the Cogs Merge Driver implementation. This code
 *  is structured in two main parts
 *
 *    - driver.go: Contains scaffolding for handlers that implement the Merge
 *                 Driver gRPC interface.
 *
 *    - merge.go:  Contains the code that translates Merge driver requests into
 *                 Cog tasks.
 */

import (
	"flag"
	"net"
	"time"

	"github.com/coreos/etcd/clientv3"
	"gitlab.com/mergetb/site/api"
	"gitlab.com/mergetb/tech/cogs/pkg/common"
	"gitlab.com/mergetb/tech/cogs/pkg/task"

	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
)

var (
	// Persistent connection to the Cogs etcd data store
	etcd *clientv3.Client
	cfg  *Config

	// flags
	configFile = flag.String("cfg", "/etc/cogs/driver.yml", "config file to use")
	listen     = flag.String("listen", "0.0.0.0:10000", "listen address")
	debug      = flag.Bool("debug", false, "enable debugging output")
	cmdrCert   = flag.String(
		"cmdr-cert", "/etc/merge/cmdr.pem", "commander TLS cert")

	GRPCServerOptions = []grpc.ServerOption{
		grpc.MaxRecvMsgSize(common.MaxMessageSize),
		grpc.MaxSendMsgSize(common.MaxMessageSize),
	}
)

func main() {

	flag.Parse()

	log.WithFields(log.Fields{
		"version": common.Version,
	}).Infof("merge driver starting")

	if *debug {
		log.SetLevel(log.DebugLevel)
	}

	cfg = GetConfig()
	common.SetEtcdConfig(cfg.Etcd)
	task.SetImageConfig(cfg.Images)

	err := common.EnsureEtcd(&etcd)
	if err != nil {
		log.Fatal(err)
	}

	gsrv := grpc.NewServer(GRPCServerOptions...)
	site.RegisterDriverServer(gsrv, &drv{})
	l, err := net.Listen("tcp", *listen)
	if err != nil {
		log.WithError(err).Fatal("failed to listen")
	}

	log.Infof("listening on %s", *listen)

	go func() {
		time.Sleep(3 * time.Second)
		registerWithCommander()
	}()

	log.Fatal(gsrv.Serve(l))

}
