package main

/* driver.go
 * ---------
 *
 *   This file implements the Merge driver gRPC interface. Most functionality
 *   here is basic gRPC handler scaffolding with a bit of logging and locking
 *   where necessary. The actual implementation of commands is in merge.go
 *
 */

import (
	"context"
	"fmt"

	"github.com/coreos/etcd/clientv3/concurrency"
	"gitlab.com/mergetb/site/api"
	"gitlab.com/mergetb/tech/cogs/pkg/common"

	log "github.com/sirupsen/logrus"
)

type drv struct{}

// General Methods ============================================================

// Health provides a endpoint for checking the health of the driver
func (d *drv) Health(
	ctx context.Context, rq *site.HealthRequest) (*site.HealthResponse, error) {

	log.Info("health check")

	return &site.HealthResponse{}, nil

}

// NotifyIncomming performs the initial setup or final teardown of a
// materialization.
func (d *drv) NotifyIncoming(
	ctx context.Context, rq *site.IncomingRequest) (*site.IncomingResponse, error) {

	fields := log.Fields{
		"request":  rq.Mzid,
		"instance": rq.Instance,
	}

	s, err := concurrency.NewSession(etcd)
	if err != nil {
		return nil, common.ErrorEF("failed to create incoming session", err, fields)
	}
	defer s.Close()

	mtx := concurrency.NewMutex(s, fmt.Sprintf("/incoming/%s", rq.Mzid))
	err = mtx.Lock(context.TODO())
	if err != nil {
		return nil, common.ErrorEF("failed to lock incoming mutex", err, fields)
	}

	defer func() {
		err = mtx.Unlock(context.TODO())
		if err != nil {
			common.WarnEF("failed to unlock incoming mutex", err, fields)
		}
	}()

	return &site.IncomingResponse{}, handleIncoming(rq)

}

// Status returns the status of a particular materialization.
func (d *drv) Status(
	ctx context.Context, rq *site.StatusRequest) (*site.StatusResponse, error) {

	return handleStatus(rq)

}

// Node Methods ===============================================================

// Setup creates node setup tasks for a set of nodes.
func (d *drv) Setup(
	ctx context.Context, rq *site.MzRequest) (*site.MzResponse, error) {

	fields := log.Fields{
		"request":  rq.Mzid,
		"instance": rq.Instance,
	}

	err := handleNodeSetup(rq)
	if err != nil {
		return nil, common.ErrorEF("Setup failed", err, fields)
	}
	return &site.MzResponse{}, nil
}

// Setup creates node recycle tasks for a set of nodes.
func (d *drv) Recycle(
	ctx context.Context, rq *site.MzRequest) (*site.MzResponse, error) {

	fields := log.Fields{
		"request":  rq.Mzid,
		"instance": rq.Instance,
	}

	err := handleNodeRecycle(rq)
	if err != nil {
		return nil, common.ErrorEF("Recycle failed", err, fields)
	}
	return &site.MzResponse{}, nil
}

// Configure provisions a set of nodes according to a specified configuration.
// TODO not implemented
func (d *drv) Configure(
	ctx context.Context, rq *site.MzRequest) (*site.MzResponse, error) {

	return &site.MzResponse{}, nil

}

// Restart sends a reset signal to a set of specified nodes.
// TODO not implemented
func (d *drv) Restart(
	ctx context.Context, rq *site.MzRequest) (*site.MzResponse, error) {

	return &site.MzResponse{}, nil

}

// Reset wipes a resource to it's initial state.
// TODO not implemented
func (d *drv) Reset(
	ctx context.Context, rq *site.MzRequest) (*site.MzResponse, error) {

	return &site.MzResponse{}, nil

}

// TurnOn sends an on signal to the set of specified nodes.
// TODO not implemented
func (d *drv) TurnOn(
	ctx context.Context, rq *site.MzRequest) (*site.MzResponse, error) {

	return &site.MzResponse{}, nil

}

// TurnOn sends an off signal to the set of specified nodes.
// TODO not implemented
func (d *drv) TurnOff(
	ctx context.Context, rq *site.MzRequest) (*site.MzResponse, error) {

	return &site.MzResponse{}, nil

}

// Link Methods ===============================================================

// CreateLink generates link creation tasks for the set of specified links.
func (d *drv) CreateLink(
	ctx context.Context, rq *site.MzRequest) (*site.MzResponse, error) {

	fields := log.Fields{
		"request":  rq.Mzid,
		"instance": rq.Instance,
	}

	err := handleLinkCreate(rq)
	if err != nil {
		return nil, common.ErrorEF("CreateLink failed", err, fields)
	}

	return &site.MzResponse{}, nil
}

// DestroyLink generates link creation tasks for the set of specified links.
func (d *drv) DestroyLink(
	ctx context.Context, rq *site.MzRequest) (*site.MzResponse, error) {

	fields := log.Fields{
		"request":  rq.Mzid,
		"instance": rq.Instance,
	}

	err := handleLinkDestroy(rq)
	if err != nil {
		return nil, common.ErrorEF("DestroyLink failed", err, fields)
	}

	return &site.MzResponse{}, nil
}

// ModifyLink generates link modification tasks for the specified set of links
// TODO not implemented
func (d *drv) ModifyLink(
	ctx context.Context, rq *site.MzRequest) (*site.MzResponse, error) {

	return &site.MzResponse{}, nil

}

// ConnectLink generates link connection tasks for the specified set of links
// TODO not implemented
func (d *drv) ConnectLink(
	ctx context.Context, rq *site.MzRequest) (*site.MzResponse, error) {

	return &site.MzResponse{}, nil

}

// DisconnectLink generates link disconnection tasks for the specified set of
// links
// TODO not implemented
func (d *drv) DisconnectLink(
	ctx context.Context, rq *site.MzRequest) (*site.MzResponse, error) {

	return &site.MzResponse{}, nil

}
