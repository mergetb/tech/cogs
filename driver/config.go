package main

import (
	"io/ioutil"

	"github.com/mergetb/yaml/v3"
	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/tech/cogs/pkg/common"
	"gitlab.com/mergetb/tech/cogs/pkg/task"
)

type Config struct {
	Commander *common.ServiceConfig
	Driver    *common.ServiceConfig //my own service config to give to commander
	Etcd      *common.ServiceConfig
	Images    *task.Images
}

// LoadConfig loads the cogs config from a well known file location.
func GetConfig() *Config {

	data, err := ioutil.ReadFile(*configFile)
	if err != nil {
		log.Fatalf("could not read configuration file /etc/cogs/driver.yml %v", err)
	}

	cfg := new(Config)
	err = yaml.Unmarshal(data, cfg)
	if err != nil {
		log.Fatalf("could not parse configuration file %v", err)
	}

	return cfg

}
