package main

/* merge.go
 * --------
 *
 *    This file implements the Merge driver commands. The primary goal of the
 *    Cogs Driver is to create tasks and actions from Merge requests and not to
 *    actually carry out requests. When tasks and actions are created, the task
 *    metadata is pushed to the Cogs etcd data store. There is a separate Cogs
 *    component called Rex that is responsible for monitoring the tasks and
 *    carrying them out as they come in. Thus a producer consumer model exists
 *    between Driver instances and Rex instances. Both Driver and Rex are
 *    designed to be replicated and scaled horizontally.
 *
 */

import (
	"context"
	"encoding/json"
	"sort"
	"time"

	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"

	"gitlab.com/mergetb/site/api"
	cm "gitlab.com/mergetb/site/api"
	"gitlab.com/mergetb/tech/cogs/pkg/common"
	"gitlab.com/mergetb/tech/cogs/pkg/task"
)

// Handle an incoming request from Merge. In the case of an incoming setup
// request a materialization infrapod is setup. In the case of an incoming
// teardown a materialization infrapod is torn down.
func handleIncoming(rq *cm.IncomingRequest) error {

	fields := log.Fields{
		"request":  rq.Mzid,
		"instance": rq.Instance,
	}

	log.WithFields(fields).Info("incoming")

	// first read the mzinfo based on mzid, if the mzinfo does not exist
	// a minimal object is instantiated, but not written to db
	// in the switch on the code below is where the object will be written.
	mzi, err := task.MzInfoRead(rq.Mzid)
	if err != nil {
		log.
			WithError(err).
			WithFields(fields).
			Error("failed to read mzinfo")
		return nil
	}

	// extract properties payload
	props := make(map[string]interface{})
	err = json.Unmarshal([]byte(rq.Data), &props)
	if err != nil {
		log.WithFields(fields).WithError(err).Warn(
			"bad or incomplete incoming data")
	}

	switch rq.Code {

	// setup infranet
	case cm.IncomingRequest_Setup:
		log.WithFields(fields).Info("incoming setup")

		switch mzi.Status {
		case task.MzNone:
			// create counters, and write out mzinfo to db
			mzi, err := task.GetMzInfo(rq.Mzid, rq.Instance)
			if err != nil {
				return common.ErrorEF("failed to get mzinfo", err, fields)
			}
			mzi.Status = task.Materializing
			err = common.Write(mzi)
			if err != nil {
				return common.ErrorE("failed to update mzinfo", err)
			}
			err = task.InitInfranet(mzi, props)
			if err != nil {
				return common.ErrorEF("failed to init infranet", err, fields)
			}
			return nil
		case task.Materializing, task.Active:
			return nil
		case task.Dematerializing:
			return common.ErrorF("dematerialization in progress", fields)
		default:
			return common.ErrorF("unknown materialization state", fields)
		}

	// tear down infranet
	case cm.IncomingRequest_Teardown:
		log.WithFields(fields).Info("incoming teardown")

		switch mzi.Status {
		case task.MzNone:
			return common.ErrorF("cannot demat unmaterialized", fields)
		case task.Materializing:
			return common.ErrorF("cannot demat materializing", fields)
		case task.Active:
			mzi.Status = task.Dematerializing
			err := common.Write(mzi)
			if err != nil {
				return common.ErrorE("failed to update mzinfo", err)
			}
			err = task.TeardownInfranet(mzi, props)
			if err != nil {
				common.WarnEF("failed to tear down infranet", err, fields)
			}
			return err
		case task.Dematerializing:
			return nil
		}

	}

	return common.ErrorF("unknown incoming code", fields)
}

func handleStatus(rq *cm.StatusRequest) (*cm.StatusResponse, error) {

	log.WithFields(log.Fields{
		"mzid":     rq.Mzid,
		"instance": rq.Instance,
	}).Trace("status request")

	return status(rq.Mzid, rq.Instance, rq)

}

func handleNodeSetup(rq *cm.MzRequest) error {

	fields := log.Fields{
		"mzid":     rq.Mzid,
		"instance": rq.Instance,
	}

	log.WithFields(fields).Info("node setup")

	mzi, err := task.MzInfoWait(rq.Mzid, rq.Instance)
	if err != nil {
		log.
			WithError(err).
			WithFields(fields).
			Error("failed to read mzinfo")
		return nil
	}

	switch mzi.Status {
	case task.MzNone:
		return common.ErrorF("materialization does not exist", fields)
	case task.Materializing, task.Active:
		return task.NodeSetup(common.TbModel(), rq.Mzid, rq.Instance, rq.Fragments)
	case task.Dematerializing:
		return common.ErrorF("dematerialization in progress", fields)
	default:
		return common.ErrorF("unknown materialization state", fields)
	}

}

func handleLinkCreate(rq *cm.MzRequest) error {

	fields := log.Fields{
		"mzid":     rq.Mzid,
		"instance": rq.Instance,
	}

	log.WithFields(fields).Info("link create")

	mzi, err := task.MzInfoWait(rq.Mzid, rq.Instance)
	if err != nil {
		log.
			WithError(err).
			WithFields(fields).
			Error("failed to read mzinfo")
		return nil
	}

	switch mzi.Status {
	case task.MzNone:
		return common.ErrorF("materialization does not exist", fields)
	case task.Materializing, task.Active:
		return task.CreateLinks(common.TbModel(), mzi, rq.Fragments)
	case task.Dematerializing:
		return common.ErrorF("dematerialization in progress", fields)
	default:
		return common.ErrorF("unknown materialization state", fields)
	}

}

func handleNodeRecycle(rq *cm.MzRequest) error {

	fields := log.Fields{
		"mzid":     rq.Mzid,
		"instance": rq.Instance,
	}

	log.WithFields(fields).Info("node recycle")

	mzi, err := task.MzInfoWait(rq.Mzid, rq.Instance)
	if err != nil {
		log.
			WithError(err).
			WithFields(fields).
			Error("failed to read mzinfo")
		return nil
	}

	switch mzi.Status {
	case task.MzNone:
		return common.ErrorF("materialization does not exist", fields)
	case task.Materializing, task.Active, task.Dematerializing:
		return task.NodeRecycle(common.TbModel(), rq.Mzid, rq.Instance, rq.Fragments)
	default:
		return common.ErrorF("unknown materialization state", fields)
	}

}

func handleLinkDestroy(rq *cm.MzRequest) error {

	fields := log.Fields{
		"mzid":     rq.Mzid,
		"instance": rq.Instance,
	}

	log.WithFields(fields).Info("link destroy")

	mzi, err := task.MzInfoWait(rq.Mzid, rq.Instance)
	if err != nil {
		log.
			WithError(err).
			WithFields(fields).
			Error("failed to read mzinfo")
		return nil
	}

	switch mzi.Status {
	case task.MzNone:
		return common.ErrorF("materialization does not exist", fields)
	case task.Materializing, task.Active, task.Dematerializing:
		return task.DestroyLinks(common.TbModel(), mzi, rq.Fragments)
	default:
		return common.ErrorF("unknown materialization state", fields)
	}

}

// RegisterWithCommander registers the cogs Merge driver implementation with the
// local commander. This function registers all nodes and links in the testbed
// to the cogs driver.
func registerWithCommander() {

	devices := task.AllResourceIDs(common.TbModel())

	cfg := GetConfig()

	err := cmdr(cfg, func(cc cm.CommanderClient) error {

		register := &cm.RegisterRequest{
			Host:   cfg.Driver.Address,
			Port:   int32(cfg.Driver.Port),
			Driver: "dcomp-driver",
			Ids:    devices,
		}

		ctx, cancel := context.WithTimeout(context.TODO(), 60*time.Second)
		defer cancel()
		_, err := cc.Register(ctx, register)
		if err != nil {
			log.WithError(err).Fatal("failed to connect to commander")
		}

		return nil

	})

	if err != nil {
		log.Warn(err)
	}
}

// CommanderClient creates a Merge Commander client based on the config spec of
// the cogs environment.
func commanderClient(cfg *Config) (
	*grpc.ClientConn, cm.CommanderClient, error) {

	creds, err := credentials.NewClientTLSFromFile(*cmdrCert, "")
	if err != nil {
		return nil, nil, err
	}

	conn, err := grpc.Dial(
		cfg.Commander.Endpoint(),
		grpc.WithTransportCredentials(creds),
		grpc.WithDefaultCallOptions(
			grpc.MaxCallRecvMsgSize(common.MaxMessageSize),
			grpc.MaxCallSendMsgSize(common.MaxMessageSize),
		),
	)
	if err != nil {
		return nil, nil, common.ErrorEF("could not connect to commander service",
			err,
			log.Fields{"addr": cfg.Commander.Endpoint()},
		)
	}
	return conn, cm.NewCommanderClient(conn), nil
}

// Cmdr executes a function against the local commander with automatic
// connection lifetime management.
func cmdr(cfg *Config, f func(cm.CommanderClient) error) error {

	conn, ccc, err := commanderClient(cfg)
	if err != nil {
		return err
	}
	defer conn.Close()

	return f(ccc)

}

func sortForPortal(output []*site.Task) {
	sort.Slice(output, func(i, j int) bool {

		if !output[i].Complete && output[j].Complete {
			return true
		}
		if !output[j].Complete && output[i].Complete {
			return false
		}

		if output[i].Kind < output[j].Kind {
			return true
		}
		if output[i].Kind > output[j].Kind {
			return false
		}

		return output[i].Name > output[j].Name
	})
}

func status(mzid, instance string, rq *site.StatusRequest) (*site.StatusResponse, error) {

	result := &site.StatusResponse{}

	tasks, err := task.ListTasks(mzid)
	if err != nil {
		return nil, common.ErrorE("failed to list tasks", err)
	}

	// sort tasks based on timestamps
	sort.Slice(tasks, func(i, j int) bool {
		t0, _ := time.Parse(task.TimeFormat, tasks[i].Timestamp)
		t1, _ := time.Parse(task.TimeFormat, tasks[j].Timestamp)
		if t0.Before(t1) && !t0.Equal(t1) {
			return true
		}
		return false
	})

	for _, t := range tasks {

		// if the task does not have the correct mzid or instance, skip it
		if t.Instance != instance || t.Mzid != mzid {
			continue
		}

		// otherwise get all the associated actions/statuses
		actionstatus, err := task.GetMzidAS(t.Mzid, t.ID)
		if err != nil {
			return nil, err
		}
		actions, err := task.GetMzidActions(t.Mzid, t.ID)
		if err != nil {
			return nil, err
		}

		for _, as := range actionstatus {
			var errmsg string
			if as.Error != nil {
				errmsg = *as.Error
			}

			action, ok := actions[as.ID]
			if !ok {
				fields := log.Fields{
					"action status": as,
					"actions":       actions,
				}
				return nil, common.ErrorF("action status missing action", fields)
			}

			result.Tasks = append(result.Tasks, &site.Task{
				Kind:     action.Kind,
				Name:     action.Action,
				Error:    errmsg,
				Complete: as.Complete,
			})
		}
	}

	sortForPortal(result.Tasks)

	return result, nil
}
