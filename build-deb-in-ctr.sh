#!/bin/bash

set -e

docker build -f debian/builder.dock -t cogs-builder .
docker run -v `pwd`:/cogs cogs-builder /cogs/build-deb.sh

