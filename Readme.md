# Cogs

The Merge testbed orchestration system.

## Building

This code requires Go 1.12+. We use the Go module system for dependency
management. You will need to set up your Go environment as described
[here](https://blog.golang.org/using-go-modules)

Code can be built as follows

```shell
make
```
